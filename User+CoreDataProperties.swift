//
//  User+CoreDataProperties.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/8/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//
//

import Foundation
import CoreData


extension User {

    @nonobjc public class func createFetchRequest() -> NSFetchRequest<User> {
        return NSFetchRequest<User>(entityName: "User")
    }

    @NSManaged public var combosCreated: Int
    @NSManaged public var movesLearned: Int
    @NSManaged public var name: String
    @NSManaged public var uid: String
    @NSManaged public var primaryDevice: String
    @NSManaged public var otherDevices: [String]?

}

