//
//  Video+CoreDataProperties.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/22/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//
//

import Foundation
import CoreData


extension Video {

    @nonobjc public class func createFetchRequest() -> NSFetchRequest<Video> {
        return NSFetchRequest<Video>(entityName: "Video")
    }

    @NSManaged public var moveName: String
    @NSManaged public var url: String
    @NSManaged public var hoopCount: Int

}
