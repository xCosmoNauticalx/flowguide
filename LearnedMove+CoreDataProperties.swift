//
//  LearnedMove+CoreDataProperties.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/7/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//
//

import Foundation
import CoreData


extension LearnedMove {

    @nonobjc public class func createFetchRequest() -> NSFetchRequest<LearnedMove> {
        return NSFetchRequest<LearnedMove>(entityName: "LearnedMove")
    }

    @NSManaged public var docID: String
    @NSManaged public var hoopCount: Int
    @NSManaged public var name: String
    @NSManaged public var parentNode: [String]
    @NSManaged public var combo: NSSet

}

// MARK: Generated accessors for combo
extension LearnedMove {

    @objc(addComboObject:)
    @NSManaged public func addToCombo(_ value: Combo)

    @objc(removeComboObject:)
    @NSManaged public func removeFromCombo(_ value: Combo)

    @objc(addCombo:)
    @NSManaged public func addToCombo(_ values: NSSet)

    @objc(removeCombo:)
    @NSManaged public func removeFromCombo(_ values: NSSet)

}
