//
//  Combo+CoreDataProperties.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 6/8/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//
//

import Foundation
import CoreData


extension Combo {

    @nonobjc public class func createFetchRequest() -> NSFetchRequest<Combo> {
        return NSFetchRequest<Combo>(entityName: "Combo")
    }

    @NSManaged public var hoopCount: Int
    @NSManaged public var docID: String
    @NSManaged public var name: String?
    @NSManaged public var favorite: Bool
    @NSManaged public var moves: NSOrderedSet
    @NSManaged public var created: Int64

}

// MARK: Generated accessors for moves
extension Combo {

    @objc(insertObject:inMovesAtIndex:)
    @NSManaged public func insertIntoMoves(_ value: LearnedMove, at idx: Int)

    @objc(removeObjectFromMovesAtIndex:)
    @NSManaged public func removeFromMoves(at idx: Int)

    @objc(insertMoves:atIndexes:)
    @NSManaged public func insertIntoMoves(_ values: [LearnedMove], at indexes: NSIndexSet)

    @objc(removeMovesAtIndexes:)
    @NSManaged public func removeFromMoves(at indexes: NSIndexSet)

    @objc(replaceObjectInMovesAtIndex:withObject:)
    @NSManaged public func replaceMoves(at idx: Int, with value: LearnedMove)

    @objc(replaceMovesAtIndexes:withMoves:)
    @NSManaged public func replaceMoves(at indexes: NSIndexSet, with values: [LearnedMove])

    @objc(addMovesObject:)
    @NSManaged public func addToMoves(_ value: LearnedMove)

    @objc(removeMovesObject:)
    @NSManaged public func removeFromMoves(_ value: LearnedMove)

    @objc(addMoves:)
    @NSManaged public func addToMoves(_ values: NSOrderedSet)

    @objc(removeMoves:)
    @NSManaged public func removeFromMoves(_ values: NSOrderedSet)

}
