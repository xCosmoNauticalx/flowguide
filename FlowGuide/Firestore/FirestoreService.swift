//
//  FirestoreService.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/4/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import Firebase
import FirebaseAuth
import CoreData


enum UpdateType: String {
    case install = "needsInstall"
    case delete = "needsDelete"
}


class FirestoreService {
    
    
// MARK: - Read only
    
    
    
    // MARK: User Info
    
    /// Get Messaging Token
    static func getToken(dispatch: DispatchGroup, completed: @escaping (String) -> Void) {
        // Get Firestore Messaging Token
        var token = ""
        dispatch.enter()
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instance ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                token = result.token
            }
            dispatch.leave()
        }
        dispatch.notify(queue: .main, execute: {
            completed(token)
            print("Got Token")
        })
        
    }
    
    
    /// Get current user
    static func getCurrentUser(dispatch: DispatchGroup, completed: @escaping (TempUser) -> Void) {
        let userRef = FirestoreReferenceManager.users
        let tempUser = TempUser()
        
        dispatch.enter()
        
        guard let uid = Auth.auth().currentUser?.uid else { return }
        
        userRef.document(uid).getDocument { (document, err) in
            if let err = err {
                print("Error getting User's document: \(err)")
            } else {
                let data = document!.data()
                tempUser.primaryDevice = data?["primaryDevice"] as! String
                tempUser.name = data?["name"] as! String
                tempUser.uid = uid
                tempUser.movesLearned = data?["movesLearned"] as! Int
                tempUser.combosCreated = data?["combosCreated"] as! Int
                if let otherDevices = data?["otherDevices"] as! [String]? {
                    tempUser.otherDevices = otherDevices
                }
            }
            dispatch.leave()
        }
        dispatch.notify(queue: .main, execute: {
            completed(tempUser)
            print("Got User")
        })
    }
    
    
    /// Get current user's device ID
    static func getDeviceID(dispatch: DispatchGroup, completed: @escaping (String) -> Void) {
        let userRef = FirestoreReferenceManager.users
        var deviceID = "1234"
        
        // Enter dispatch
        dispatch.enter()
        
        // Get user's uid
        guard let uid = Auth.auth().currentUser?.uid else {
            return
        }
        
        // Find their document and get the deviceID
        userRef.document(uid).getDocument { (document, err) in
            if let err = err {
                print("Error getting User's document: \(err)")
            } else {
                let data = document!.data()
                deviceID = data?["deviceID"] as! String
                print("User's enabled device: \(deviceID)")
            }
            // Exit dispatch
            dispatch.leave()
        }
        dispatch.notify(queue: .main, execute: {
            completed(deviceID)
        })
    }
    
    
    
    // MARK: Archive Moves
    
    /// Gets Moves from Firestore and stores it's values locally as a Move instance (Used in Archive)
    static func getMoves(belongingTo parentNode: String, fromCache: Bool, dispatch: DispatchGroup, completed: @escaping ([TempMove]) -> Void) {
        let moveRef = FirestoreReferenceManager.moves
        var tabMoves = [TempMove]()
        var source: FirestoreSource!
        
        switch fromCache {
        case true:
            source = .cache
        case false:
            source = .default
        }
        
        dispatch.enter()
        
        moveRef.whereField("parentNode", arrayContains: parentNode).getDocuments(source: source) { (QuerySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                if source == .cache {
                    print("Getting moves from cache")
                } else {
                    print("Getting moves from server")
                }
                for document in QuerySnapshot!.documents {
                    tabMoves.append(TempMove(name: document.data()["name"] as! String,
                                         urls: document.data()["urls"] as! [String],
                                         docID: document.documentID as String,
                                         hoopCount: document.data()["hoopCount"] as! Int,
                                         parentNode: document.data()["parentNode"] as! [String]))
                    print("Number of moves running: \(tabMoves.count)")
                }
            }
            dispatch.leave()
        }
        dispatch.notify(queue: .main, execute: {
            print("Number of moves: \(tabMoves.count)")
            completed(tabMoves)
        })
    }
    
    
    /// Get single move to display in MoveDetailVC
    static func getMove(withID docID: String, dispatch: DispatchGroup, completed: @escaping (TempMove) -> Void) {
        let moveRef = FirestoreReferenceManager.moves
        let tempMove = TempMove()
        // Enter dispatch
        dispatch.enter()
        
        // Find Move document and get urls
        moveRef.document(docID).getDocument { (document, err) in
            if let err = err {
                print("Error getting document for \(docID): \(err)")
            } else {
                let data = document!.data()
                tempMove.name = data?["name"] as! String
                tempMove.urls = data?["urls"] as! [String]
                tempMove.docID = docID
                tempMove.hoopCount = data?["hoopCount"] as! Int
                tempMove.parentNode = data?["parentNode"] as! [String]
                
                print("Successfully created TempMove \(tempMove.name)")
            }
            // Exit dispatch
            dispatch.leave()
        }
        dispatch.notify(queue: .main, execute: {
            completed(tempMove)
        })
    }
    
    
    
    // MARK: Library Moves
    
    /// Get moves user has already learned (Used in Library & Generator)
    static func getLearnedMoves(hoopCount: Int, dispatch: DispatchGroup, completed: @escaping ([TempMove]) -> Void) {
        let userRef = FirestoreReferenceManager.users
        var tabMoves = [TempMove]()
        
        dispatch.enter()
        
        guard let currentUser = Auth.auth().currentUser?.uid else {
            return
        }

        userRef.document(currentUser).collection("Learned").whereField("hoopCount", isEqualTo: hoopCount).getDocuments() { (QuerySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                for document in QuerySnapshot!.documents {
                    tabMoves.append(TempMove(name: document.data()["name"] as! String,
                                         urls: document.data()["urls"] as! [String],
                                         docID: document.documentID as String,
                                         hoopCount: document.data()["hoopCount"] as! Int,
                                         parentNode: document.data()["parentNode"] as! [String]))
                    print("Number of moves running: \(tabMoves.count)")
                }
            }
            dispatch.leave()
        }
        dispatch.notify(queue: .main, execute: {
            print("Number of moves: \(tabMoves.count)")
            completed(tabMoves)
        })
    }
    
    
    static func getLearnedMoves(forUser uid: String, dispatch: DispatchGroup, completed: @escaping ([TempMove]) -> Void) {
        let userRef = FirestoreReferenceManager.users
        var tabMoves = [TempMove]()
        
        dispatch.enter()
        userRef.document(uid).collection("Learned").getDocuments() { (QuerySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
               for document in QuerySnapshot!.documents {
                tabMoves.append(TempMove(name: document.data()["name"] as! String,
                                         urls: document.data()["urls"] as! [String],
                                         docID: document.data()["docID"] as! String,
                                         hoopCount: document.data()["hoopCount"] as! Int,
                                         parentNode: document.data()["parentNode"] as! [String]))
                }
            }
            dispatch.leave()
        }
        dispatch.notify(queue: .main, execute: {
            print("Number of moves: \(tabMoves.count)")
            completed(tabMoves)
        })
    }
    
    
    /// Check if move was ever learned and return dictionary with bool & devices that are waiting to delete if applicable
    static func checkIfLearned(_ move: TempMove, forUser currentUser: String, dispatch: DispatchGroup, completed: @escaping ([ Bool : [String]? ]) -> Void) {
        let userRef = FirestoreReferenceManager.users
        var result: [ Bool : [String]? ]!
        dispatch.enter()
        // Check if the document exists
        let docRef = userRef.document(currentUser).collection("Learned").document(move.docID)
        docRef.getDocument() { (document, err) in
            if let err = err {
                print("Error getting document: \(err)")
            } else {
                if let document = document {
                    if document.exists {
                        // It does, check if any devices are waiting to delete
                        if let needsInstall = document.data()?["needsDelete"] as! [String]? {
                            // Some devices are waiting to delete. Return them so they can be moved to toInstall
                            result = [ true : needsInstall ]
                        } else {
                            result = [ false : nil ]
                        }
                    } else {
                        result = [ false : nil ]
                    }
                }
            }
            dispatch.leave()
        }
        dispatch.notify(queue: .main, execute: {
            completed(result)
        })
    }
    
    
    

// MARK: - Read / Write
    
    
    
    // MARK: Library Moves
    
    /// Add move to user's Library
    static func addLearnedMove(_ move: TempMove, devices: [String]? = nil) {
        
        let userRef = FirestoreReferenceManager.users
        guard let currentUser = Auth.auth().currentUser?.uid else { return }
        let docRef = userRef.document(currentUser).collection("Learned").document(move.docID)
        let dispatch = DispatchGroup()
        
        checkIfLearned(move, forUser: currentUser, dispatch: dispatch) { (result) in
            if result.keys.contains(true) == true {
                // Move was learned, move devices to toInstall and delete needsDelete
                let needsInstall = result[true]
                print(needsInstall)
                docRef.updateData(["needsInstall": needsInstall, "needsDelete": FieldValue.delete()])
            } else {
                // Move isn't in Firestore, create it
                if devices != nil {
                    createMove(move: move, currentUser: currentUser, needsInstall: devices)
                } else {
                   createMove(move: move, currentUser: currentUser)
                }
            }
        }
        /// Increment MovesLearned counter
        userRef.document(currentUser).updateData(["movesLearned": FieldValue.increment(Int64(1))])
    }
    
    
    /// Create Learned Move document, used in updating
    static func createMove(move: TempMove, currentUser: String, needsInstall: [String]? = nil) {
        let userRef = FirestoreReferenceManager.users
        let docRef = userRef.document(currentUser).collection("Learned").document(move.docID)
        var data: [ String : Any ]!
        data = ["name": move.name, "hoopCount": move.hoopCount, "parentNode": move.parentNode]
        // If needsInstall isn't empty, devices should be added to firestore
        if needsInstall != nil {
            data["needsInstall"] = needsInstall
        }
        // Add document to firestore
        docRef.setData(data) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added successfully")
            }
        }

    }
    
    
    /// Batch write when user subscribes
    static func batchWriteMoves(_ moves: [LearnedMove]) {
        let userRef = FirestoreReferenceManager.users
        var batchRef = FirestoreReferenceManager.batch
        guard let currentUser = Auth.auth().currentUser?.uid else { return }
        var mutableMoves = moves
        var counter = 0
        
        func addMoves() {
            for move in mutableMoves {
                let docRef = userRef.document(currentUser).collection("Learned").document(move.docID)
                let data = ["name": move.name, "hoopCount": move.hoopCount, "parentNode": move.parentNode] as [String : Any]
                batchRef.setData(data, forDocument: docRef)
                // Remove from array since we already got it
                if let index = mutableMoves.firstIndex(of: move) {
                    mutableMoves.remove(at: index)
                }
                counter += 1
                if counter == 500 {
                    batchRef.commit() { err in
                        if let err = err {
                            print("Error writing batch \(err)")
                        } else {
                            print("Batch write succeeded")
                        }
                    }
                    // Clear batch
                    batchRef = FirestoreReferenceManager.batch
                    break
                }
            }
        }
        
        // While there are still moves in the array:
        while !mutableMoves.isEmpty {
            // Add the moves
            addMoves()
            // When we hit 500,
            if counter == 500 {
                // Reset counter & loop through remaining moves
                counter = 0
                addMoves()
            }
        }
    }
    
    
    /// Remove move from user's library
    static func removeLearnedMove(_ move: TempMove, devices: [String]? = nil, currentUser: String? = nil) {
        
        let userRef = FirestoreReferenceManager.users
        guard let currentUser = Auth.auth().currentUser?.uid else { return }
        let docRef = userRef.document(currentUser).collection("Learned").document(move.docID)
        
        if devices != nil {
            // Other devices need to delete, update document with their IDs
            docRef.updateData(["needsDelete": devices, "needsInstall": FieldValue.delete()]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    print("Successfully updated document. \(devices?.count) more devices need to delete")
                }
            }
        } else {
            // No other devices exist, delete from Firestore
            docRef.delete() { err in
                if let err = err {
                    print("Error deleting document: \(err)")
                } else {
                    print("Document deleted successfully")
                }
            }
        }
        /// Decrement MovesLearned counter
        userRef.document(currentUser).updateData(["movesLearned": FieldValue.increment(Int64(-1))])
    }
    
    
    /// Increment or Decrement User's Moves Learned Count
    static func updateMoveCount(by num: Int) {
        let userRef = FirestoreReferenceManager.users
        guard let currentUser = Auth.auth().currentUser?.uid else { return }
        
        userRef.document(currentUser).updateData(["movesLearned": FieldValue.increment(Int64(num))])
    }

    
    
    // MARK: Combos
    
    /// Add combo to user's library
    static func addCombo(_ combo: TempCombo, devices: [String]? = nil) {
        // Get docIDs for the moves in the combo
        var moveNames = [String]()
        for move in combo.moves {
            moveNames.append(move.docID)
        }
        
        let userRef = FirestoreReferenceManager.users
        guard let currentUser = Auth.auth().currentUser?.uid else { return }
        var data: [ String : Any ]!
        data = ["hoopCount": combo.hoopCount, "moves": moveNames, "favorite": false]
        let docRef = userRef.document(currentUser).collection("Combos").document(combo.docID)
        
        if devices != nil {
            // Other devices need to install
            data["needsInstall"] = devices
        }
        
        docRef.setData(data) { err in
            if let err = err {
                print("Error adding combo: \(err)")
            } else {
                print("Combo added successfully")
            }
        }
        /// Increment CombosCreated counter
        userRef.document(currentUser).updateData(["combosCreated": FieldValue.increment(Int64(1))])
    }
    
    
    /// Remove combo from user's library
    static func removeCombo(_ combo: TempCombo, devices: [String]? = nil) {
               
        let userRef = FirestoreReferenceManager.users
        guard let currentUser = Auth.auth().currentUser?.uid else { return }
        let docRef = userRef.document(currentUser).collection("Combos").document(combo.docID)
        
        if devices != nil {
            // Other devices need to delete, update document with their IDs
            docRef.updateData(["needsDelete": devices, "needsInstall": FieldValue.delete()]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    print("Successfully updated document. \(devices?.count) more devices need to delete")
                }
            }
        } else {
            // No other devices exist, delete from Firestore
            docRef.delete() { err in
                if let err = err {
                    print("Error deleting document: \(err)")
                } else {
                    print("Document deleted successfully")
                }
            }
        }
        /// Decrement CombosCreated counter
        userRef.document(currentUser).updateData(["combosCreated": FieldValue.increment(Int64(-1))])
    }
    
    
    
    // MARK: Videos
    
    /// Add saved Video
    static func addVideo(forMoveNamed name: String, withURL url: String, hoopCount: Int) {
        
        let userRef = FirestoreReferenceManager.users
        guard let currentUser = Auth.auth().currentUser?.uid else {
            return
        }
        print(currentUser)
        
        let now = Date().currentTimeMillis()
        
        userRef.document(currentUser).collection("Videos").document(url).setData(["name": name, "url": url, "hoopCount": hoopCount, "created": now]) { err in
            if let err = err {
                print("Error adding document: \(err)")
            } else {
                print("Document added successfully")
            }
        }
    }
    
    
    /// Remove saved Video
    static func removeVideo(_ video: Video) {
        
        let userRef = FirestoreReferenceManager.users
        guard let currentUser = Auth.auth().currentUser?.uid else {
            return
        }
        
        let docID = video.url
        
        userRef.document(currentUser).collection("Videos").document(docID).delete() { err in
            if let err = err {
                print("Error deleting document: \(err)")
            } else {
                print("Firestore: Document deleted successfully")
            }
        }
            
    }
    
    
    // MARK: User Data
    
    /// Add new device to list
    static func updateDevices(forUser uid: String, withDevice deviceID: String) {
        let userRef = FirestoreReferenceManager.users
        
        userRef.document(uid).updateData(["otherDevices": FieldValue.arrayUnion([deviceID])]) { err in
            if let err = err {
                print("Error updating document: \(err)")
            } else {
                print("Document updated successfully")
            }
        }
        
    }
    
    
    /// Replace Primary Device
    static func replacePrimaryDevice(forUser uid: String, with deviceID: String) {
        let userRef = FirestoreReferenceManager.users
        userRef.document(uid).updateData(["primaryDevice": deviceID]) { (err) in
            if let err = err {
                print("Error updating document: \(err)")
            } else {
                print("Successfully updated document with deviceID: \(deviceID)")
            }
        }
        
    }
        
    
// MARK: - Updates
    
    /// Save or Delete moves pending in Firestore
    static func syncMoveUpdates(forType updateType: UpdateType, dispatch: DispatchGroup, completed: @escaping ([TempMove]) -> Void) {
        let userRef = FirestoreReferenceManager.users
        guard let currentUser = Auth.auth().currentUser?.uid else { return }
        guard let thisDevice = UIDevice.current.identifierForVendor?.uuidString else { return }
        var tabMoves = [TempMove]()
        
        dispatch.enter()
        userRef.document(currentUser).collection("Learned").whereField(updateType.rawValue, arrayContains: thisDevice).getDocuments() { (QuerySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                print("Getting documents that \(updateType.rawValue)")
                for document in QuerySnapshot!.documents {
                    let data = document.data()
                    let move = TempMove(name: data["name"] as! String,
                                        urls: data["urls"] as! [String],
                                        docID: document.documentID as String,
                                        hoopCount: data["hoopCount"] as! Int,
                                        parentNode: data["parentNode"] as! [String])
                    tabMoves.append(move)
                    // Get list of devices
                    let devices = data[updateType.rawValue] as! [String]
                    var newDevices = [String]()
                    // Remove this device from list of devices
                    for device in devices {
                        if device != thisDevice {
                            newDevices.append(device)
                        }
                    }
                    if !newDevices.isEmpty {
                        // Other devices still need to update
                        switch updateType {
                        case .install:
                            installMoveUpdate(move, currentUser: currentUser, devices: newDevices)
                        case .delete:
                            deleteMoveUpdate(move, currentUser: currentUser, devices: newDevices)
                        }
                    } else {
                        // No other devices need to update
                        switch updateType {
                        case .install:
                            installMoveUpdate(move, currentUser: currentUser)
                        case .delete:
                            deleteMoveUpdate(move, currentUser: currentUser)
                        }
                    }
                    print("Number of moves running: \(tabMoves.count)")
                }
            }
            dispatch.leave()
        }
        dispatch.notify(queue: .main, execute: {
            print("Number of moves: \(tabMoves.count)")
            completed(tabMoves)
        })
    }
    
    
    /// Remove this device from needsInstall since we've just installed it
    static func installMoveUpdate(_ move: TempMove, currentUser: String, devices: [String]? = nil) {
        let userRef = FirestoreReferenceManager.users
        let docRef = userRef.document(currentUser).collection("Learned").document(move.docID)
        if devices == nil {
            // We're the last device that needs to install, remove "needsInstall" field
            docRef.updateData(["needsInstall": FieldValue.delete()])
        } else {
            // Other devices still need to install, update "needsInstall"
            docRef.updateData(["needsInstall": devices]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    print("Successfully updated document: \(move.name)")
                }
            }
        }
    }
    
    
    /// Remove this device from needsDelete or delete it completely since we've just deleted it
    static func deleteMoveUpdate(_ move: TempMove, currentUser: String, devices: [String]? = nil) {
        let userRef = FirestoreReferenceManager.users
        let docRef = userRef.document(currentUser).collection("Learned").document(move.docID)
        if devices == nil {
            // We're the last device that needs to delete, delete document from Firestore
            docRef.delete() { err in
                if let err = err {
                    print("Error deleting document: \(err)")
                } else {
                    print("Successfully deleted document")
                }
            }
        } else {
            docRef.updateData(["needsDelete": devices]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    print("Successfully updated document. \(devices?.count) more devices need to delete")
                }
            }
        }
        
    }
    
    
    /// Sync Combo Updates
    static func syncComboUpdates(forType updateType: UpdateType, dispatch: DispatchGroup, completed: @escaping ([TempCombo]) -> Void) {
        let userRef = FirestoreReferenceManager.users
        guard let currentUser = Auth.auth().currentUser?.uid else { return }
        guard let thisDevice = UIDevice.current.identifierForVendor?.uuidString else { return }
        var tabCombos = [TempCombo]()
        
        dispatch.enter()
        userRef.document(currentUser).collection("Combos").whereField(updateType.rawValue, arrayContains: thisDevice).getDocuments() { (QuerySnapshot, err) in
            if let err = err {
                print("Error getting documents: \(err)")
            } else {
                print("Getting documents that \(updateType.rawValue)")
                for document in QuerySnapshot!.documents {
                    let data = document.data()
                    let combo = TempCombo(hoopCount: data["hoopCount"] as! Int,
                                          docID: document.documentID as String,
                                          favorite: data["favorite"] as! Bool,
                                          moveDocIDs: data["moves"] as? [String])
                    if let name = data["name"] as! String? {
                        combo.name = name
                    }
                    tabCombos.append(combo)
                    // Get list of devices
                    let devices = data[updateType.rawValue] as! [String]
                    var newDevices = [String]()
                    // Remove this device from list of devices
                    for device in devices {
                        if device != thisDevice {
                            newDevices.append(device)
                        }
                    }
                    if !newDevices.isEmpty {
                        // Other devices still need to update
                        switch updateType {
                        case .install:
                            installComboUpdate(combo, currentUser: currentUser, devices: newDevices)
                        case .delete:
                            deleteComboUpdate(combo, currentUser: currentUser, devices: newDevices)
                        }
                    } else {
                        // No other devices need to update
                        switch updateType {
                        case .install:
                            installComboUpdate(combo, currentUser: currentUser)
                        case .delete:
                            deleteComboUpdate(combo, currentUser: currentUser)
                        }
                    }
                    print("Number of moves running: \(tabCombos.count)")
                }
            }
            dispatch.leave()
        }
        dispatch.notify(queue: .main, execute: {
            print("Number of moves: \(tabCombos.count)")
            completed(tabCombos)
        })
    }
    
    
    /// Remove this device from needsInstall since we've just installed it
    static func installComboUpdate(_ combo: TempCombo, currentUser: String, devices: [String]? = nil) {
        let userRef = FirestoreReferenceManager.users
        let docRef = userRef.document(currentUser).collection("Combos").document(combo.docID)
        if devices == nil {
            // We're the last device that needs to install, remove "needsInstall" field
            docRef.updateData(["needsInstall": FieldValue.delete()])
        } else {
            // Other devices still need to install, update "needsInstall"
            docRef.updateData(["needsInstall": devices]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    print("Successfully updated document: \(combo.docID)")
                }
            }
        }
    }
    
    
    /// Remove this device from needsDelete or delete it completely since we've just deleted it
    static func deleteComboUpdate(_ combo: TempCombo, currentUser: String, devices: [String]? = nil) {
        let userRef = FirestoreReferenceManager.users
        let docRef = userRef.document(currentUser).collection("Combos").document(combo.docID)
        if devices == nil {
            // We're the last device that needs to delete, delete document from Firestore
            docRef.delete() { err in
                if let err = err {
                    print("Error deleting document: \(err)")
                } else {
                    print("Successfully deleted document")
                }
            }
        } else {
            docRef.updateData(["needsDelete": devices]) { err in
                if let err = err {
                    print("Error updating document: \(err)")
                } else {
                    print("Successfully updated document. \(devices?.count) more devices need to delete")
                }
            }
        }
        
    }
    
    
    
}
