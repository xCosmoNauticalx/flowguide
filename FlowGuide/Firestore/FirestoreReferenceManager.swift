//
//  FirestoreReferenceManager.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/4/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import Firebase

struct FirestoreReferenceManager {
    
    static let db = Firestore.firestore()
    static let moves = db.collection("Moves")
    static let users = db.collection("Users")
    static let batch = db.batch()
    
}
