//
//  AuthErrorCode.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/7/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import Firebase

extension AuthErrorCode {
    
    var errorMessage: String {
        switch self {
        case .emailAlreadyInUse:
            return "This email is already in use."
        case .userNotFound:
            return "No account found."
        case .userDisabled:
            return "Your account has been disabled. Please contact support."
        case .invalidEmail, .invalidSender, .invalidRecipientEmail:
            return "Please enter a valid email."
        case .networkError:
            return "Network error. Please try again."
        case .weakPassword:
            return "Password must contain at least 8 charachters, one number and one special charachter."
        case .wrongPassword:
            return "Incorrect password."
        default:
            return "Unknown error occurred"
        }
    }
    
}
