//
//  Animator.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/6/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class FadePushAnimation: NSObject, UIViewControllerAnimatedTransitioning {

    let animationDuration = 0.5
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return animationDuration
    }
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        // Get VC to be pushed
        guard let toVC = transitionContext.viewController(forKey: .to) else { return }
        
        // Add the toVC to the container of Views in the transition context
        transitionContext.containerView.addSubview(toVC.view)
        toVC.view.alpha = 0.0
        
        UIView.animate(withDuration: animationDuration, animations: {
            toVC.view.alpha = 1
        }, completion: { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }
}
