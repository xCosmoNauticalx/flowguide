//
//  FadePopAnimation.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/28/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class FadePopAnimation: NSObject, UIViewControllerAnimatedTransitioning {
    
    let animationDuration = 0.5
    
    func transitionDuration(using transitionContext: UIViewControllerContextTransitioning?) -> TimeInterval {
        return animationDuration
    }
    
    
    func animateTransition(using transitionContext: UIViewControllerContextTransitioning) {
        guard
            let fromVC = transitionContext.viewController(forKey: .from),
            let toVC = transitionContext.viewController(forKey: .to)
            else { return }
        
        transitionContext.containerView.insertSubview(toVC.view, belowSubview: fromVC.view)
        
        UIView.animate(withDuration: animationDuration, animations: {
            fromVC.view.alpha = 0
        }, completion: { _ in
            transitionContext.completeTransition(!transitionContext.transitionWasCancelled)
        })
    }

}
