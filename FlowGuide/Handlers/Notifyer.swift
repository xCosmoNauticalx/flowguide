//
//  Notifyer.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/7/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import UIKit
import UserNotifications

class Notifyer {
    
    /// Show preliminary alert telling user why they should enable notifications
    static func askToRegisterForNotifications(_ vc: UIViewController) {
        
        if #available(iOS 10.0, *) {
            // Check if user is registered
            UNUserNotificationCenter.current().getNotificationSettings { (settings) in
                if settings.authorizationStatus == .authorized {
                    return
                } else {
                    // Tell user why they need notifications
                    let alert = UIAlertController(title: "Allow Notifications?", message: "Flow Guide uses notifications to alert you when new tutorials and features have been added. We know how annoying notifications can be and we promise not to spam you.", preferredStyle: .alert)
                    let yes = UIAlertAction(title: "Allow", style: .default) { (action: UIAlertAction) in
                        registerForNotifications()
                    }
                    let no = UIAlertAction(title: "Not Now", style: .cancel, handler: nil)
                    
                    alert.addAction(yes)
                    alert.addAction(no)
                    DispatchQueue.main.async {
                        vc.present(alert, animated: true)
                    }
                }
            }
        }
        
    }
    
    /// Register app to APNs
    static func registerForNotifications() {
        // Shows a permission dialogue on first run
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
          // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = UIApplication.shared.delegate as! AppDelegate

          let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
          UNUserNotificationCenter.current().requestAuthorization(
            options: authOptions,
            completionHandler: {_, _ in })
        } else {
          let settings: UIUserNotificationSettings =
          UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(settings)
        }

        UIApplication.shared.registerForRemoteNotifications()

        // [END register_for_notifications]
    }
    
    
    /// If user denied notifications, tell them to go to settings and enable them
    static func checkIfNotificationsAreEnabled(_ vc: UIViewController) {
        
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            if settings.authorizationStatus == .authorized {
                // Already authorized
            }
            else {
                // Either denied or notDetermined
                UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
                    (granted, error) in
                      // add your own
                    let alertController = UIAlertController(title: "Notification Alert", message: "Please enable notifications", preferredStyle: .alert)
                    let settingsAction = UIAlertAction(title: "Settings", style: .default) { (_) -> Void in
                        guard let settingsUrl = URL(string: UIApplication.openSettingsURLString) else {
                            return
                        }
                        if UIApplication.shared.canOpenURL(settingsUrl) {
                            UIApplication.shared.open(settingsUrl, completionHandler: { (success) in
                            })
                        }
                    }
                    let cancelAction = UIAlertAction(title: "Cancel", style: .default, handler: nil)
                    alertController.addAction(cancelAction)
                    alertController.addAction(settingsAction)
                    DispatchQueue.main.async {
                        vc.present(alertController, animated: true, completion: nil)

                    }
                }
            }
        }
    }
    
    
    
    
}
