//
//  WhiteBar.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 3/31/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class WhiteBar: UIView {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .white
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
}
