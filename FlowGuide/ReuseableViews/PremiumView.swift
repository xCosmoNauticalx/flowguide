//
//  PremiumView.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/8/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class PremiumView: UIView {

// MARK: - Properties
    
    /// Coordinators
    weak var premium: Premium?
    
    /// Data
    var color: UIColor = .purple
    
    /// Views
    let label: UILabel = {
        let l = UILabel()
        l.text = "Premium"
        l.font = UIFont(name: "HelveticaNeue-Bold", size: 30)
        l.textColor = .white
        l.textAlignment = .center
        return l
    }()
    
    let subLabel: UILabel = {
        let l = UILabel()
        l.text = "Unlock this feature and more with \nFlow Guide Premium"
        l.font = UIFont(name: "HelveticaNeue-Thin", size: 20)
        l.textColor = .white
        l.textAlignment = .center
        l.numberOfLines = 0
        l.lineBreakMode = .byWordWrapping
        return l
    }()
    
    let button: HighlightedButton = {
        let b = HighlightedButton()
        b.setTitle("Learn More", for: .normal)
        b.setTitleColor(.white, for: .normal)
        b.backgroundColor = .clear
        b.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        return b
    }()
    
    
    
// MARK: - Init
    
    init(color: UIColor, premium: Premium) {
        super.init(frame: CGRect())
        self.color = color
        self.premium = premium
        setUp()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    
// MARK: - Set Up
    
    func setUp() {
        
        /// Style
        button.styleHollowButton(color: color.cgColor)
        
        var size: CGFloat = 17
        
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            size = 22
        }
        
        let title = "Learn More"
        let attributes: [NSAttributedString.Key: Any] = [
            .font: UIFont(name: "HelveticaNeue", size: size)!,
            .foregroundColor: color]
        let attributedTitle = NSAttributedString(string: title, attributes: attributes)
        button.setAttributedTitle(attributedTitle, for: .normal)
        
        /// Add Views
        self.addSubview(label)
        self.addSubview(subLabel)
        self.addSubview(button)
        
        /// Constraints
        label.anchor(top: nil, leading: self.leadingAnchor, bottom: nil, trailing: self.trailingAnchor, padding: .init(top: 0, left: 60, bottom: 0, right: -60))
        label.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        subLabel.anchor(top: label.bottomAnchor, leading: label.leadingAnchor, bottom: nil, trailing: label.trailingAnchor, padding: .init(top: 10, left: 0, bottom: 0, right: 0))
        
        button.anchor(top: subLabel.bottomAnchor, leading: label.leadingAnchor, bottom: nil, trailing: label.trailingAnchor, padding: .init(top: 20, left: 0, bottom: 0, right: 0))
        
        /// Vary for iPad
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            button.heightAnchor.constraint(equalToConstant: 45).isActive = true
            label.font = UIFont(name: "HelveticaNeue-Bold", size: 40)
            subLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 25)
        }

    }
    
    
// MARK: - Methods
    
    @objc func buttonTapped(_ sender: UIButton!) {
        print("Tapped")
        premium?.presentPremiumPage()
    }
    
}


