//
//  TitleLabel.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/19/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class TitleLabel: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)
        createLabel()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        createLabel()
    }
    
    func createLabel() {
        
        self.font = UIFont(name: "HelveticaNeue", size: 39)
        self.backgroundColor = UIColor.clear
        self.adjustsFontSizeToFitWidth = true
        self.minimumScaleFactor = 0.7
        self.textAlignment = .center
        
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowRadius = 3.0
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 4, height: 4)
        self.layer.masksToBounds = false
    }

}
