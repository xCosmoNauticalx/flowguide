//
//  HollowFilledButton.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/1/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class HollowFilledButton: UIButton {

    override var isSelected: Bool {
        didSet {
            self.backgroundColor = isSelected ? UIColor.white : UIColor.clear
            self.clipsToBounds = isSelected ? false : true
        }
    }
    

}
