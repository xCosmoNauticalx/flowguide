//
//  NoMovesView.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/19/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class NothingHereView: UIView {

// MARK: - Properties
    
    /// Data
    var text: String
    var colors: [CGColor]
    
    /// Views
    let label: GradientLabel = {
        let l = GradientLabel()
        l.text = "There's nothing here."
        l.font = UIFont(name: "HelveticaNeue-Bold", size: 30)
        l.textAlignment = .center
        l.adjustsFontSizeToFitWidth = true
        l.minimumScaleFactor = 0.5
        l.layer.shadowColor = UIColor.black.cgColor
        l.layer.shadowRadius = 3.0
        l.layer.shadowOpacity = 0.5
        l.layer.shadowOffset = CGSize(width: 4, height: 4)
        l.layer.masksToBounds = false
        l.shouldBeHorizontal = false
        return l
    }()
    
    let subLabel: UILabel = {
        let l = UILabel()
        l.text = "Hop over to the Archive, learn a new move or two, and you'll be able to find it here after."
        l.font = UIFont(name: "HelveticaNeue-Thin", size: 19)
        l.textAlignment = .center
        l.textColor = .white
        l.numberOfLines = 0
        l.lineBreakMode = .byWordWrapping
        return l
    }()
    
    let container = UIView()
    
    
// MARK: - Init
    
    init(text: String, colors: [CGColor]) {
        self.text = text
        self.colors = colors
        super.init(frame: CGRect())
        setUp()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    

// MARK: - Set Up
    
    private func setUp() {
        
        /// Init
        label.gradientColors = colors
        subLabel.text = text
        
        /// Add Subviews
        //addSubview(container)
        self.addSubview(label)
        self.addSubview(subLabel)
        
        /// Constraints
        // Label
        label.anchor(top: nil, leading: self.leadingAnchor, bottom: nil, trailing: self.trailingAnchor, padding: .init(top: 0, left: 60, bottom: 0, right: -60))
        label.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        
        // Sub Label
        subLabel.anchor(top: label.bottomAnchor, leading: label.leadingAnchor, bottom: nil, trailing: label.trailingAnchor, padding: .init(top: 5, left: -25, bottom: 0, right: 25))
        
        /// Vary for iPad
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            label.font = UIFont(name: "HelveticaNeue-Bold", size: 38)
            subLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 29)
        }
        
    }

}
