//
//  CircleView.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/9/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class CircleView: UIView {
    
    var color: UIColor = .white

    init(color: UIColor) {
        super.init(frame: CGRect())
        self.color = color
        backgroundColor = .clear
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func draw(_ rect: CGRect) {
        
        // Get the Graphics Context
        if let context = UIGraphicsGetCurrentContext() {
            
            // Set the circle outerline-width
            context.setLineWidth(2.0);
            
            // Set the circle outerline-colour
            color.set()
            
            // Create Circle
            let center = CGPoint(x: frame.size.width/2, y: frame.size.height/2)
            let radius = (frame.size.width - 10)/2
            context.addArc(center: center, radius: radius, startAngle: 0.0, endAngle: .pi * 2.0, clockwise: true)
                
            // Draw
            context.strokePath()
        }
    }
    

}
