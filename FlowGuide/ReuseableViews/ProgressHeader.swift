//
//  ProgressHeader.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/22/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class ProgressHeader: UITableViewHeaderFooterView {

    let label: UILabel = {
        let l = UILabel()
        l.font = UIFont(name: "HelveticaNeue-Bold", size: 30)
        l.textAlignment = .left
        l.textColor = .background
        //l.layer.shadowColor = UIColor.black.cgColor
        //l.layer.shadowRadius = 3.0
        //l.layer.shadowOpacity = 0.5
        //l.layer.shadowOffset = CGSize(width: 4, height: 4)
        //l.layer.masksToBounds = false
        return l
    }()
    
    let background: UIView = {
        let v = UIView()
        v.backgroundColor = .clear
        return v
    }()
    
    let gradientView: UIView = {
        let v = UIView()
        v.layer.cornerRadius = 10
        v.layer.masksToBounds = false
        return v
    }()
    
    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        setUp()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        gradientView.frame = bounds
        background.frame = bounds
        
        func gradient(frame: CGRect) -> CAGradientLayer {
            let layer = CAGradientLayer()
            layer.frame = frame
            layer.cornerRadius = 10
            layer.colors = [UIColor.Progress.dark.cgColor, UIColor.Progress.mid.cgColor]
            layer.startPoint = CGPoint(x: 0.0, y: 0.0)
            layer.endPoint = CGPoint(x: 0.0, y: 1.0)

            return layer
        }
              
        gradientView.layer.insertSublayer(gradient(frame: gradientView.bounds), at: 0)
    }
    
    private func setUp() {
        
        self.backgroundView = background
        
        addSubview(gradientView)
        gradientView.fillSuperView()
        addSubview(label)
        label.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor, padding: .init(top: 0, left: 10, bottom: 0, right: 0))
        
    }
    
}
