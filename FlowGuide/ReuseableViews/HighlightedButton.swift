//
//  HighlightedButton.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/1/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class HighlightedButton: UIButton {

    override var isHighlighted: Bool {
        didSet {
            alpha = isHighlighted ? 0.5 : 1.0
        }
    }

}
