//
//  BubbleButton.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/21/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class BubbleButton: UIButton {
    
    let shapeLayer = CAShapeLayer()
    
    override func draw(_ rect: CGRect) {
        
        if let gradientColor = drawGradientColor(in: rect, colors: [UIColor.white.cgColor, UIColor.gray.cgColor]) {
            self.backgroundColor = gradientColor
        }

        
        let path = createBubble()
        let scale = CGAffineTransform(scaleX: 0.5, y: 0.5)
        path?.apply(scale)
        shapeLayer.path = path?.cgPath
        
        self.layer.addSublayer(shapeLayer)
        
        super.draw(rect)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
        addTarget(self, action: #selector(touchDown), for: .touchDown)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    private func setUp() {
        
        layoutIfNeeded()
        
        
        
    }
    
    
    func createBubble() -> UIBezierPath? {
        
        //// General Declarations
        let context = UIGraphicsGetCurrentContext()!


        //// Gradient Declarations
        let colors = [UIColor.white.cgColor, UIColor.white.cgColor, UIColor.lightGray.cgColor]
        guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(),
        colors: colors as CFArray,
        locations: [0.46, 1, 1]) else { return nil }

        //// Bezier 2 Drawing
        context.saveGState()

        let path = UIBezierPath()
        path.move(to: CGPoint(x: 169, y: 3))
        path.addCurve(to: CGPoint(x: 169, y: 84), controlPoint1: CGPoint(x: 169, y: 3), controlPoint2: CGPoint(x: 169, y: 84))
        path.addLine(to: CGPoint(x: 116.54, y: 84))
        path.addCurve(to: CGPoint(x: 86, y: 111), controlPoint1: CGPoint(x: 100.44, y: 96.17), controlPoint2: CGPoint(x: 86, y: 111))
        path.addCurve(to: CGPoint(x: 56.02, y: 84), controlPoint1: CGPoint(x: 86, y: 111), controlPoint2: CGPoint(x: 72.11, y: 96.32))
        path.addLine(to: CGPoint(x: 3, y: 84))
        path.addLine(to: CGPoint(x: 3, y: 3))
        path.addLine(to: CGPoint(x: 169, y: 3))
        path.addLine(to: CGPoint(x: 169, y: 3))
        path.close()
        context.saveGState()
        path.addClip()
        context.drawLinearGradient(gradient, start: CGPoint(x: 86, y: 3), end: CGPoint(x: 86, y: 111), options: [])
        context.restoreGState()

        context.restoreGState()

        
        return path
        
    }
    
    private func drawGradientColor(in rect: CGRect, colors: [CGColor]) -> UIColor? {
        let currentContext = UIGraphicsGetCurrentContext()
        currentContext?.saveGState()
        defer { currentContext?.restoreGState() }

        let size = rect.size
        UIGraphicsBeginImageContextWithOptions(size, false, 0)
        guard let gradient = CGGradient(colorsSpace: CGColorSpaceCreateDeviceRGB(),
                                        colors: colors as CFArray,
                                        locations: nil) else { return nil }

        let context = UIGraphicsGetCurrentContext()
        context?.drawLinearGradient(gradient,
                                    start: CGPoint.zero,
                                    end: CGPoint(x: size.width, y: 0),
                                    options: [])
        let gradientImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        guard let image = gradientImage else { return nil }
        return UIColor(patternImage: image)
    }
    
    /// Only registers touch if inside bezier path
    @objc func touchDown(button: BubbleButton, event: UIEvent) {
        
        if let touch = event.touches(for: button)?.first {
            let location = touch.location(in: button)
            
            if shapeLayer.path?.contains(location) == false {
                button.cancelTracking(with: nil)
            }
        }
        
    }
    
    
    
}
