//
//  MoveModel.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/4/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import UIKit

class MoveModel {
    
    var name: String
    var urls: [String]
    
    
    init(name: String, urls: [String]){
        self.name = name
        self.urls = urls
    }
    
}
