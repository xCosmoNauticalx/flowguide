//
//  FirebaseReferenceManager.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/4/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import Firebase

struct FirebaseReferenceManager {
    
    static let db = Firestore.firestore()
    static let rootMoves = db.collection("Moves")
    
}
