//
//  ShopData.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/26/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class ShopData: NSObject, UICollectionViewDataSource, UICollectionViewDelegate {

    weak var searchCoordinator: SearchCoordinator?

    func numberOfSections(in collectionView: UICollectionView) -> Int {

        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return 2
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        switch indexPath.item {
        case 0:
            guard let cell: FeaturedCell = collectionView.dequeueReusableCell(withReuseIdentifier: "FeaturedCell", for: indexPath) as? FeaturedCell else {
                fatalError("Unable to dequeue FeaturedCell.")
            }
            cell.setUp()
            return cell
        case 1:
            guard let cell: SearchCell = collectionView.dequeueReusableCell(withReuseIdentifier: "SearchCell", for: indexPath) as? SearchCell else {
                fatalError("Unable to dequeue SearchCell.")
            }
            cell.coordinator = searchCoordinator
            if cell.coordinator == nil {
                print("SearchCell doesn't have coordinator")
            }
            cell.setUp()
            return cell
        default:
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath)
            return cell
        }

    }
    
}
