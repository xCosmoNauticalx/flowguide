//
//  MenuController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/26/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

private let cellID = "Cell"

class MenuController: UICollectionViewController {

// MARK: - Properties
    
    private let data = MenuData()
    weak var delegate: MenuDelegate?
    let menuBar = WhiteBar()
    

// MARK: - Init
    override func loadView() {
        super.loadView()
        setUp()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    

// MARK: - Set Up
    private func setUp() {
        
        /// Collection View
        collectionView.backgroundColor = .clear
        collectionView.register(MenuCell.self, forCellWithReuseIdentifier: cellID)
        collectionView.collectionViewLayout = createLayout()
        collectionView.dataSource = data
        self.clearsSelectionOnViewWillAppear = false

        /// Menu Bar
        view.addSubview(menuBar)
        menuBar.layer.cornerRadius = 10
        
        menuBar.anchor(top: nil, leading: nil, bottom: view.bottomAnchor, trailing: nil, size: .init(width: 75, height: 1))
        menuBar.centerXAnchor.constraint(equalTo: view.centerXAnchor, constant: -(view.frame.width / 4)).isActive = true
        
    }

    private func createLayout() -> UICollectionViewLayout{
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1/2), heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 2)
        
        let section = NSCollectionLayoutSection(group: group)

        let layout = UICollectionViewCompositionalLayout(section: section)
         
        return layout
    }

    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let x = collectionView.frame.width / 4
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.menuBar.center.x = x
        })
        delegate?.didTapMenuItem(indexPath: indexPath)
    }
    

}
