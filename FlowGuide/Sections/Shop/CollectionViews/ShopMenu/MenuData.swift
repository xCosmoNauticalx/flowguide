//
//  FeaturedDataSource.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/16/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class MenuData: NSObject, UICollectionViewDataSource {
    
    let options = ["Featured", "Explore"]

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return options.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: MenuCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? MenuCell else {
            fatalError("Unable to dequeue ShopMenuCell.")
        }
        
        let option = options[indexPath.item]
        cell.label.text = option
        
        return cell
    }
    
}

