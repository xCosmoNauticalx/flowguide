//
//  MenuCell.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/26/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class MenuCell: UICollectionViewCell {
    
    let label: UILabel = {
        let l = UILabel()
        l.text = "Menu Item"
        l.textAlignment = .center
        l.textColor = .white
        l.alpha = 0.5
        l.font = UIFont(name: "Helvetica", size: 26)
        return l
    }()
    
    override var isSelected: Bool {
        didSet {
            label.alpha = isSelected ? 1.0 : 0.5
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(label)
        label.fillSuperView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
}
