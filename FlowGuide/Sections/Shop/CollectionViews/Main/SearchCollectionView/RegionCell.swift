//
//  SearchCell.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/16/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class RegionCell: UICollectionViewCell {
    
    let label: UILabel = {
        let l = UILabel()
        l.text = "Menu Item"
        l.textAlignment = .left
        l.textColor = .white
        l.font = UIFont(name: "Helvetica", size: 22)
        return l
    }()
    
    override var isSelected: Bool {
        didSet {
            toggleIsSelected()
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(label)
        label.fillSuperView()

    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func toggleIsSelected() {
        UIView.animate(withDuration: 0.01, delay: 0, options: [.curveEaseOut], animations: {
            self.alpha = self.isSelected ? 0.5 : 1.0
        })
    }
    
}
