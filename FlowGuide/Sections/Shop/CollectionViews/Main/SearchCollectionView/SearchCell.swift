//
//  SearchCell.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/24/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class SearchCell: UICollectionViewCell,  UICollectionViewDelegate {
    
// MARK: - Properties
    
    weak var coordinator: SearchCoordinator?
    let container = UIView()
    
    var country: HoopSmith.Country!
    
// MARK: - Set Up
    
    func setUp() {
        
        /// Get the VC into the Navigation Controller
        coordinator?.start(slideDelegate: nil)
        
        /// Add Views
        let navView = coordinator?.navigationController.view!
        addSubview(container)
        container.addSubview(navView!)
        
        /// Constraints
        navView!.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor, padding: .init(top: 15, left: 0, bottom: 0, right: 0))
        
        switch UIScreen.main.traitCollection.userInterfaceIdiom {
        case .pad:
            container.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor, padding: .init(top: 0, left: 100, bottom: 0, right: -100))
        default:
            container.fillSuperView()
        }
        
    }
    
}



