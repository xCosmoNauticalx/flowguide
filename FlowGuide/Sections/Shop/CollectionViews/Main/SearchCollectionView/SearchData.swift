//
//  SearchData.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/24/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class SearchData: NSObject, UICollectionViewDataSource, UICollectionViewDelegate {
    
    weak var coordinator: ShopCoordinator?
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of items
        return allRegions.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: RegionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? RegionCell else {
        fatalError("Unable to dequeue RegionCell.")
        }
        
        let region = allRegions[indexPath.item]
        cell.label.text = region.rawValue
    
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell: RegionCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? RegionCell else {
            fatalError("Unable to dequeue TutorialCell.")
        }
        
        cell.label.text = nil
    }


}
