//
//  SearchViewController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/27/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class SearchViewController: UIViewController, Titleable {

// MARK: - Properties
    
    /// Coordinator
    weak var coordinator: SearchCoordinator?
    
    /// Data
    private let data = SearchData()
    var smiths = [HoopSmith]()
    
    /// Views
    var titleLabel: TitleLabel = {
        let l = TitleLabel()
        l.font = UIFont(name: "HelveticaNeue", size: 26)
        l.text = "Select A Country"
        l.textColor = .white
        l.layer.shadowColor = UIColor.black.cgColor
        l.layer.shadowRadius = 1.5
        l.layer.shadowOpacity = 0.1
        l.layer.shadowOffset = CGSize(width: 2, height: 2)
        l.layer.masksToBounds = false
        return l
    }()
    
    let collectionView = SearchController(collectionViewLayout: UICollectionViewFlowLayout())
    let container = UIView()
    let gradientView = CAGradientLayer()
    
    
// MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        setUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        changeTitleText(to: "Select A Country")
        
        /// Clear selected cell since it won't do it itself for some reason
        collectionView.collectionView.indexPathsForSelectedItems?
            .forEach { collectionView.collectionView.deselectItem(at: $0, animated: false) }
        
        /// Show CollectionView again
        UIView.animate(withDuration: 0.3, animations: {
            self.collectionView.view.alpha = 1
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIView.animate(withDuration: 0.3, animations: {
            self.collectionView.view.alpha = 0
        })
    }
    
    override func viewDidLayoutSubviews() {
        gradientView.frame = view.bounds
    }
    

// MARK: - Set Up
    
    func setUp() {
        
        /// Format Title
        self.navigationController?.navigationBar.addSubview(titleLabel)
        titleLabel.fillSuperView()
        
        /// CollectionView
        collectionView.collectionView.dataSource = data
        collectionView.collectionView.delegate = self
        
        /// Add SubViews
        view.addSubview(container)
        container.addSubview(collectionView.view)
        
        /// Constraints
        // CollectionView
        collectionView.view.fillSuperView()
        
        // Container
        switch UIScreen.main.traitCollection.userInterfaceIdiom {
        case .pad:
            container.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 0, left: 100, bottom: 0, right: -100))
        default:
            container.fillSuperView()
        }
        
        /// CollectionView Gradient
        gradientView.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
        gradientView.locations = [0, 0.07, 0.9, 1]
        container.layer.mask = gradientView
        
        
        
    }

}

extension SearchViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let country = allRegions[indexPath.item]
        coordinator?.next(country, titleDelegate: self)
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? RegionCell else { return }
        
        cell.label.text = nil
    }
    
}
