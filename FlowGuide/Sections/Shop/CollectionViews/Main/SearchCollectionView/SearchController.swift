//
//  SearchController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/26/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class SearchController: UICollectionViewController {
    
// MARK: - Life Cycle

    override func loadView() {
        super.loadView()
        setUp()
    }
    
    
    
// MARK: - Set Up

    private func setUp() {
        
        self.collectionView.register(RegionCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        self.collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "Header")
        self.collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "Footer")
        self.collectionView.backgroundColor = .clear
        self.collectionView.collectionViewLayout = createLayout()
        self.clearsSelectionOnViewWillAppear = true
    }

    private func createLayout() -> UICollectionViewLayout {
        
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        item.contentInsets = NSDirectionalEdgeInsets(top: 0, leading: 25, bottom: 0, trailing: 0)
         
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(50))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        group.interItemSpacing = .fixed(10)
        
        let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(6.0))
        let footerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(25.0))
        let header = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)
        let footer = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: footerSize, elementKind: UICollectionView.elementKindSectionFooter, alignment: .bottom)

        let section = NSCollectionLayoutSection(group: group)
        section.boundarySupplementaryItems = [header, footer]

        let layout = UICollectionViewCompositionalLayout(section: section)
         
        return layout
    }
}


