//
//  FeaturedCell.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/24/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class FeaturedCell: UICollectionViewCell {
    
// MARK: - Properties
    
    /// Views
    let container = UIView()
    let viewController = FeaturedViewController()

    
// MARK: - Set Up
    
    func setUp() {
        
        /// Add Views
        addSubview(container)
        container.addSubview(viewController.view)
        
        /// Constraints
        viewController.view.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor, padding: .init(top: 15, left: 0, bottom: 0, right: 0))
        
        switch UIScreen.main.traitCollection.userInterfaceIdiom {
        case .pad:
            container.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor, padding: .init(top: 0, left: 100, bottom: 0, right: -100))
        default:
            container.fillSuperView()
        }
    }

    
}


