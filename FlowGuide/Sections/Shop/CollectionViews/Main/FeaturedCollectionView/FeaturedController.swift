//
//  FeaturedController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/26/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit


class FeaturedController: UICollectionViewController {
    
// MARK: - Properties
    
    /// Data
    let cellID = "Cell"
    
    /// Views
    let gradient = CAGradientLayer()
    
    
// MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        setUp()
    }

    
// MARK: - Set Up
    
    private func setUp() {
        self.collectionView.register(ShopCell.self, forCellWithReuseIdentifier: cellID)
        self.collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "Header")
        self.collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "Footer")
        self.collectionView.backgroundColor = .clear
        self.collectionView.collectionViewLayout = createLayout()
        
        /// Gradient Setup
//        gradient.frame = collectionView.bounds
//        gradient.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor]
//        //collectionGradient.startPoint = CGPoint(x: collectionView.frame.width / 2, y: -0.01)
//        gradient.locations = [0.01, 0.03, 0.1]
//        self.collectionView.superview?.layer.mask = gradient
    }

    private func createLayout() -> UICollectionViewLayout {
        
        // Large item with 2 smaller on right
        let mainItemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.66), heightDimension: .fractionalHeight(1.0))
        let mainItem = NSCollectionLayoutItem(layoutSize: mainItemSize)
        mainItem.contentInsets = NSDirectionalEdgeInsets(top: 4, leading: 4, bottom: 4, trailing: 4)
        
        let pairItemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(0.5))
        let pairItem = NSCollectionLayoutItem(layoutSize: pairItemSize)
        pairItem.contentInsets = NSDirectionalEdgeInsets(top: 4, leading: 4, bottom: 4, trailing: 4)
        
        let trailingGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.33), heightDimension: .fractionalHeight(1.0))
        let trailingGroup = NSCollectionLayoutGroup.vertical(layoutSize: trailingGroupSize, subitem: pairItem, count: 2)
        
        let mainWithPairGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalWidth(0.66))
        let mainWithPairGroup = NSCollectionLayoutGroup.horizontal(layoutSize: mainWithPairGroupSize, subitems: [mainItem, trailingGroup])
        
        // Triplet items
        let tripletItemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.33), heightDimension: .fractionalHeight(1.0))
        let tripletItem = NSCollectionLayoutItem(layoutSize: tripletItemSize)
        tripletItem.contentInsets = NSDirectionalEdgeInsets(top: 4, leading: 4, bottom: 4, trailing: 4)
        
        let tripletGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalWidth(0.33))
        let tripletGroup = NSCollectionLayoutGroup.horizontal(layoutSize: tripletGroupSize, subitems: [tripletItem, tripletItem, tripletItem])
        
        // Large item with 2 smaller on left
        let reverseMainWithPairGroup = NSCollectionLayoutGroup.horizontal(layoutSize: mainWithPairGroupSize, subitems: [trailingGroup, mainItem])
        
        // Put all the groups together
        let nestedGroupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalWidth(1.98))
        let nestedGroup = NSCollectionLayoutGroup.vertical(layoutSize: nestedGroupSize, subitems: [mainWithPairGroup, tripletGroup, reverseMainWithPairGroup, tripletGroup])

        
        let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(25.0))
        let footerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(25.0))
        let header = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)
        let footer = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: footerSize, elementKind: UICollectionView.elementKindSectionFooter, alignment: .bottom)
        
        let section = NSCollectionLayoutSection(group: nestedGroup)
        section.boundarySupplementaryItems = [header, footer]
        
        let layout = UICollectionViewCompositionalLayout(section: section)
        
        return layout
        
    }

}
