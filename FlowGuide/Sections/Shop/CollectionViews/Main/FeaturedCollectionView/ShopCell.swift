//
//  FeaturedCell.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/17/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class ShopCell: UICollectionViewCell {
    
    var shadowLayer = CAShapeLayer()
    let image: UIImageView = {
        let img = UIImageView()
        img.image = UIImage(named: "shopImage")
        return img
    }()
    
    override var isHighlighted: Bool {
        didSet {
            toggleIsSelected()
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(image)
        image.fillSuperView()
    }
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    
    // I didn't write this, and I don't fully understand it. But it works?
    override func draw(_ rect: CGRect) {
        let path = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: [.topLeft, .topRight, .bottomRight, .bottomLeft], cornerRadii: CGSize(width: 10, height: 10))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        self.layer.mask = mask
        // Handle Cell reuse case
        shadowLayer.removeFromSuperlayer()
            
        // Idk why these are here
        //shadowLayer.shadowPath = path.cgPath
        //shadowLayer.frame = self.layer.frame
        self.contentView.layer.insertSublayer(shadowLayer, below: self.layer)
        super.draw(rect)
    }
    
    
    func toggleIsSelected() {
        UIView.animate(withDuration: 0.1, delay: 0, options: [.curveEaseOut], animations: {
            self.alpha = self.isHighlighted ? 0.5 : 1.0
        })
    }
    
}
