//
//  FeaturedData.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/24/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class FeaturedData: NSObject, UICollectionViewDataSource, UICollectionViewDelegate {

    func numberOfSections(in collectionView: UICollectionView) -> Int {

        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {

        return 15
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell: ShopCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? ShopCell else {
        fatalError("Unable to dequeue ShopCell.")
        }
        cell.image.image = UIImage(named: "shopImage")
        cell.layer.cornerRadius = 8
        
        return cell
    }
    

    
}


