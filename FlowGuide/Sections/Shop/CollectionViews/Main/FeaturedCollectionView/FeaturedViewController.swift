//
//  FeaturedViewController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/27/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class FeaturedViewController: UIViewController {

// MARK: - Properties
    
    /// Data
    private let data = FeaturedData()
    
    /// Views
    let collectionView = FeaturedController(collectionViewLayout: UICollectionViewFlowLayout())
    let container = UIView()
    var gradientView = CAGradientLayer()
    
    
    
// MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        setUp()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    
    override func viewDidLayoutSubviews() {
        gradientView.frame = view.bounds
    }
    
    
// MARK: - Set Up

    func setUp() {
        
        /// CollectionView
        collectionView.collectionView.dataSource = data
        
        
        /// Add SubViews
        view.addSubview(container)
        container.addSubview(collectionView.view)
        
        /// Constraints
        // CollectionView
        collectionView.view.fillSuperView()
        
        // Container
        switch UIScreen.main.traitCollection.userInterfaceIdiom {
        case .pad:
            container.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 0, left: 100, bottom: 0, right: -100))
        default:
            container.fillSuperView()
        }
    
        /// CollectionView Gradient
        gradientView.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
        gradientView.locations = [0, 0.07, 0.93, 1]
        container.layer.mask = gradientView
        
    }
    
}
