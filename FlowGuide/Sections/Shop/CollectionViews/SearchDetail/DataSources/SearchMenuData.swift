//
//  SearchMenuData.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/2/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class SearchMenuData: NSObject, UICollectionViewDataSource {
    
// MARK: - Properties

    let options = ["All", "Day", "LED", "Fire"]

    
// MARK: - Collection View Data Source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return options.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: SearchMenuCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? SearchMenuCell else {
            fatalError("Unable to dequeue SearchMenuCell.")
        }
        
        let option = options[indexPath.item]
        cell.label.text = option
        
        if indexPath.item == 0 {
            cell.layoutSubviews()
            let path = UIBezierPath(roundedRect: cell.bounds,
                                    byRoundingCorners: [.topLeft, .bottomLeft],
                                    cornerRadii: CGSize(width: 5, height:  20))

            let maskLayer = CAShapeLayer()

            maskLayer.path = path.cgPath
            cell.layer.mask = maskLayer
        }
        
        if indexPath.item == 3 {
            cell.layoutSubviews()
            let path = UIBezierPath(roundedRect: cell.bounds,
                                    byRoundingCorners: [.topRight, .bottomRight],
                                    cornerRadii: CGSize(width: 5, height:  20))

            let maskLayer = CAShapeLayer()

            maskLayer.path = path.cgPath
            cell.layer.mask = maskLayer
        }
        
        return cell
    }
    
}
