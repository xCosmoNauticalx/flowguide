//
//  SearchDetailData.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/2/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class SearchDetailData: NSObject, UICollectionViewDataSource {

// MARK: - Properties
    
    var localSmiths = [HoopSmith]()
    var internationalSmiths = [HoopSmith]()
    var filteredLocalSmiths = [HoopSmith]()
    var filteredInternationalSmiths = [HoopSmith]()
    
    var isFiltering: Bool = false

    
// MARK: - Collection View Data Source
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 2
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        switch section {
        case 0:
            if isFiltering {
                return filteredLocalSmiths.count
            } else {
                return localSmiths.count
            }
        case 1:
            if isFiltering {
                return filteredInternationalSmiths.count
            } else {
                return internationalSmiths.count
            }
        default:
            return 0
        }
        
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: SearchDetailCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? SearchDetailCell else {
            fatalError("Unable to dequeue SearchDetailCell.")
        }
        
        let smith: HoopSmith!
        
        switch indexPath.section {
        case 0:
            if isFiltering {
                smith = filteredLocalSmiths[indexPath.item]
            } else {
                smith = localSmiths[indexPath.item]
            }
            cell.label.text = smith.name
            cell.url = smith.url
        case 1:
            if isFiltering {
                smith = filteredInternationalSmiths[indexPath.item]
            } else {
                smith = internationalSmiths[indexPath.item]
            }
            switch smith.location {
            case .us:
                cell.label.text = "\(smith.name) (US)"
                cell.url = smith.url
            case .uk:
                cell.label.text = "\(smith.name) (UK)"
                cell.url = smith.url
            default:
                cell.label.text = "\(smith.name) (\(smith.location.rawValue))"
                cell.url = smith.url
            }
        default:
            return cell
        }
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard let header: SearchHeaderView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "Header", for: indexPath) as? SearchHeaderView else {
            fatalError("Unable to dequeue SearchHeaderView.")
        }
        
        switch indexPath.section {
        case 0:
            header.label.text = "Local Shops"
        case 1:
            header.label.text = "International Shops"
        default:
            return header
        }
        
        return header
    }

}
