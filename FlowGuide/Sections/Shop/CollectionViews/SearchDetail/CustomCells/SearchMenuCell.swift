//
//  SearchMenuCell.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/2/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class SearchMenuCell: UICollectionViewCell {

// MARK: - Properties
    
    let label: UILabel = {
        let l = UILabel()
        l.text = "Menu Item"
        l.textAlignment = .center
        l.textColor = .white
        l.font = UIFont(name: "Helvetica", size: 20)
        return l
    }()
    
    override var isSelected: Bool {
        didSet {
            toggleIsSelected()
        }
    }
    
    
// MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        layoutIfNeeded()
        //self.layer.borderWidth = 1
        //self.layer.borderColor = UIColor.white.cgColor
        self.layer.masksToBounds = true
        
        addSubview(label)
        label.fillSuperView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    
// MARK: - Methods
    
    func toggleIsSelected() {
        UIView.animate(withDuration: 0.35, animations: {
            self.label.textColor = self.isSelected ? .background : .white
            self.backgroundColor = self.isSelected ? .white : .clear
        })
    }
    
}
