//
//  SearchDetailCell.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/2/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class SearchDetailCell: UICollectionViewCell {
    
// MARK: - Properties
    
    let label: UILabel = {
        let l = UILabel()
        l.text = "Shop name"
        l.textAlignment = .left
        l.textColor = .white
        l.font = UIFont(name: "Helvetica", size: 22)
        return l
    }()
    
    var url: String = ""
    
    
    
// MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(label)
        label.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor, padding: .init(top: 0, left: 8, bottom: 0, right: 0))
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
}
