//
//  SearchHeaderView.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/2/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class SearchHeaderView: UICollectionReusableView {
        
// MARK: - Properties
    
    let label: UILabel = {
        let l = UILabel()
        l.text = "Local or International?"
        l.textAlignment = .left
        l.textColor = .white
        l.font = UIFont(name: "Helvetica", size: 23)
        return l
    }()
    
    let background: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "headerBackground")
        i.contentMode = .scaleAspectFill
        return i
    }()
    
    
    
// MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(background)
        addSubview(label)
        background.fillSuperView()
        label.fillSuperView()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
}
