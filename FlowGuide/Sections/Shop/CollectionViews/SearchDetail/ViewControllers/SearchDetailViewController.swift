//
//  SearchDetailViewController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/2/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class SearchDetailViewController: UIViewController {

// MARK: - Properties
    
    /// Coordinator
    weak var coordinator: SearchCoordinator?
    weak var titleDelegate: Titleable?
    
    /// Data
    let data = SearchDetailData()
    var country: HoopSmith.Country = .us
    
    /// Views
    let menu = SearchMenuController(collectionViewLayout: UICollectionViewFlowLayout())
    let collectionView = SearchDetailCollectionController(collectionViewLayout: UICollectionViewFlowLayout())
    let container = UIView()
    let gradientView = CAGradientLayer()
    
    
    
// MARK: - Life Cycle
    override func loadView() {
        super.loadView()
        setUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        titleDelegate?.changeTitleText(to: country.rawValue)
        
        UIView.animate(withDuration: 0.3, animations: {
            self.menu.view.alpha = 1
            self.collectionView.view.alpha = 1
        })
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIView.animate(withDuration: 0.3, animations: {
            self.menu.view.alpha = 0
            self.collectionView.view.alpha = 0
        })
    }
    
    
    override func viewDidLayoutSubviews() {
        gradientView.frame = view.bounds
    }
    
    
    
// MARK: - Set Up
    
    private func setUp() {
        
        /// Back Button
        Utilities.styleNavBar(self, color: .white)
        
        /// Background Color
        self.view.backgroundColor = .clear
        
        /// CollectionView Set Up
        collectionView.collectionView.dataSource = data
        collectionView.collectionView.delegate = self
        menu.delegate = self
        menu.collectionView.selectItem(at: [0,0], animated: true, scrollPosition: .centeredHorizontally)
        data.isFiltering = false
        
        /// Add Views
        view.addSubview(container)
        container.addSubview(menu.view)
        container.addSubview(collectionView.view)
        
        /// Constraints
        // Container
        container.fillSuperView()
        
        // Menu
        menu.view.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: nil, trailing: container.trailingAnchor, padding: .init(top: 10, left: 20, bottom: 0, right: -20), size: .init(width: 0, height: 30))
        
        // CollectionView
        collectionView.view.anchor(top: menu.view.bottomAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor, padding: .init(top: 0, left: 20, bottom: 0, right: -20))
        
        /// CollectionView Gradient
        gradientView.colors = [UIColor.black.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
        gradientView.locations = [0, 0.07, 0.9, 1]
        container.layer.mask = gradientView
        
    }

}


extension SearchDetailViewController: MenuDelegate {
    
    func didTapMenuItem(indexPath: IndexPath) {
        
        // Clear any previously filtered smiths
        data.filteredLocalSmiths = []
        data.filteredInternationalSmiths = []
        
        var hoopType = HoopSmith.HoopType.day
        
        switch indexPath.item {
        case 0:
            data.isFiltering = false
            collectionView.collectionView.reloadData()
            return
        case 1:
            hoopType = .day
            data.isFiltering = true
        case 2:
            hoopType = .led
            data.isFiltering = true
        default:
            hoopType = .fire
            data.isFiltering = true

        }
        // Find the smiths that sell that hoop type and add them to the filtered lists
        for smith in data.localSmiths {
            if smith.hoopType.contains(hoopType) {
                data.filteredLocalSmiths.append(smith)
            }
        }
        
        for smith in data.internationalSmiths {
            if smith.hoopType.contains(hoopType) {
                data.filteredInternationalSmiths.append(smith)
            }
        }
        // Reload the collectionView
        collectionView.collectionView.reloadData()
        
    }
    
}


extension SearchDetailViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? SearchDetailCell else { return }
        let url = cell.url
        
        NotificationCenter.default.post(name: .shouldShowAd, object: nil)
        
        if url.contains("etsy.com") {
            print("Etsy Link")
            if let etsyURL = URL(string: url), UIApplication.shared.canOpenURL(etsyURL) {
                UIApplication.shared.open(etsyURL, options: [:], completionHandler: nil)
            }
        } else {
            print("Not Etsy Link")
            coordinator?.showSafari(url)
        }
        
        
    }
    
}
