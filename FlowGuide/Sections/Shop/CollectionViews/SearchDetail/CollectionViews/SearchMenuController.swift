//
//  SearchMenuController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/2/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class SearchMenuController: UICollectionViewController {
    
// MARK: - Properties
    
    /// Coordinators
    weak var delegate: MenuDelegate?
    
    /// Data
    let data = SearchMenuData()
    
    
// MARK: - Life Cycle

    override func loadView() {
        super.loadView()
        setUp()
    }
    
    
    
// MARK: - Set Up
    
    private func setUp() {
        
        /// Collection View
        collectionView.backgroundColor = .clear
        collectionView.register(SearchMenuCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        collectionView.collectionViewLayout = createLayout()
        collectionView.dataSource = data
        self.clearsSelectionOnViewWillAppear = false
        
        
        self.view.layer.borderWidth = 1
        self.view.layer.cornerRadius = 5
        self.view.layer.borderColor = UIColor.white.cgColor
        
        
    }
    

    private func createLayout() -> UICollectionViewLayout {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1/4), heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 4)
        
        let section = NSCollectionLayoutSection(group: group)

        let layout = UICollectionViewCompositionalLayout(section: section)
         
        return layout
    }
    
    
// MARK: - Collection View Delegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didTapMenuItem(indexPath: indexPath)
    }



}
