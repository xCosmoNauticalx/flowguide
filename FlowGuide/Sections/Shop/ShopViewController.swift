//
//  ShopViewController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/16/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit
import GoogleMobileAds

class ShopViewController: UICollectionViewController {

// MARK: - Properties
    
    /// Coordinator
    weak var coordinator: ShopCoordinator?
    
    /// Data
    private let data = ShopData()
    let userIsEntitled = PurchasesHelper.isUserEntitled()
    var shouldShowAd = false
    
    /// Views
    private let menuController = MenuController(collectionViewLayout: UICollectionViewFlowLayout())
    let titleLabel = TitleLabel()
    let bottomView = UIView()
    
    let background: UIImageView = {
        let b = UIImageView()
        b.image = UIImage(named: "darkBackground")
        return b
    }()
    
    /// Ads
    var bannerView: GADBannerView!
    var interstitial: GADInterstitial!

    
// MARK: - Life Cycle
    override func loadView() {
        super.loadView()
        setUp()

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        /// Tab Bar
        Utilities.styleTabBar(self, color: UIColor.white)
    }
    
    
// MARK: - Methods
    func setUp(){
        
        /// Notification Center
        if !userIsEntitled {
            NotificationCenter.default.addObserver(self, selector: #selector(showInterstitial), name: UIApplication.didBecomeActiveNotification, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(showInterstitial), name: .showInterstitial, object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(setBool), name: .shouldShowAd, object: nil)
        }
        
        /// Format Title
        let frame = CGRect(x: (self.navigationController?.navigationBar.frame.width)!/4, y: -25, width: (self.navigationController?.navigationBar.frame.width)!/2, height: 100)
        titleLabel.frame = frame
        titleLabel.text = "Find a Shop"
        titleLabel.textColor = UIColor.white
        self.navigationController?.navigationBar.addSubview(titleLabel)
        
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            titleLabel.font = UIFont(name: "HelveticaNeue", size: 49)
        }
        
        /// Set Background
        view.insertSubview(background, at: 0)
        background.fillSuperView()
        
        /// Menu Set Up
        let menuView = menuController.view!
        view.addSubview(menuView)
        menuView.anchor(top: view.safeAreaLayoutGuide.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 30, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 60))
        menuController.collectionView.selectItem(at: [0,0], animated: true, scrollPosition: .centeredHorizontally)
        menuController.delegate = self
        
        /// Create Ads
        if !userIsEntitled {
            interstitial = createAndLoadInterstitial()
            createAndLoadBanner()
        }
        
        ///Main Collection Set Up
        self.collectionView.dataSource = data
        self.collectionView.delegate = self
        self.collectionView.backgroundColor = .clear
        self.collectionView.register(FeaturedCell.self, forCellWithReuseIdentifier: "FeaturedCell")
        self.collectionView.register(SearchCell.self, forCellWithReuseIdentifier: "SearchCell")
        self.collectionView.isPagingEnabled = true
        self.collectionView.backgroundColor = .clear
        self.collectionView.showsHorizontalScrollIndicator = false
        if !userIsEntitled {
            self.collectionView.anchor(top: menuView.bottomAnchor, leading: view.leadingAnchor, bottom: bannerView.topAnchor, trailing: view.trailingAnchor)
        } else {
            self.collectionView.anchor(top: menuView.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        }
        
        /// Give Cells their own coordinators
        data.searchCoordinator = coordinator?.createSearchCoordinator()
        
    }



//MARK: - CollectionView Delegate
    
    /// Collection View Delegate
    override func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let x = scrollView.contentOffset.x
        let offset = x / 2
        menuController.menuBar.transform = CGAffineTransform(translationX: offset, y: 0)
    }
    
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let x = targetContentOffset.pointee.x
        let item = x / view.frame.width
        let indexPath = IndexPath(item: Int(item), section: 0)
        menuController.collectionView.selectItem(at: indexPath, animated: true, scrollPosition: .centeredHorizontally)
    }
    
    

}


// MARK: - Protocol Conformance

extension ShopViewController: MenuDelegate {
    
    /// Shop Delegate Conformance
    func didTapMenuItem(indexPath: IndexPath) {
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
}


// MARK: - Banner View

extension ShopViewController: GADBannerViewDelegate {
    
    func createAndLoadBanner() {
        bannerView = GADBannerView()
        bannerView.adUnitID = AdIdentifier.banner
        bannerView.rootViewController = self
        view.addSubview(bannerView)
        bannerView.anchor(top: collectionView.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, size: .init(width: 0, height: 60))
        bannerView.load(GADRequest())
    }
    
}


// MARK: - Interstitial Delegate

extension ShopViewController: GADInterstitialDelegate {
    
    func createAndLoadInterstitial() -> GADInterstitial {
        print("Creating new Interstitial")
        let interstitial = GADInterstitial(adUnitID: AdIdentifier.interstitial)
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    @objc func showInterstitial() {
        /// Show Ad
        if shouldShowAd {
            if interstitial.isReady {
                interstitial.present(fromRootViewController: self)
                shouldShowAd = false
            } else {
                print("Ad wasn't ready")
            }
        }
        
    }
    
    @objc func setBool() {
        shouldShowAd = true
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
        shouldShowAd = false
    }
    
    
}

