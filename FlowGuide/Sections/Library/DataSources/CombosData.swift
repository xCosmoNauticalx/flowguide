//
//  CombosData.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/28/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class CombosData: NSObject, UICollectionViewDataSource {
    
// MARK: - Properties
    
    weak var coordinator: LibraryCoordinator?
    
    var combos = [Combo]()
    var filteredCombos = [Combo]()
    var isShowingFavorites: Bool = false
    var oneHoop: [Combo]!
    var twoHoop: [Combo]!
    var threeHoop: [Combo]!
    var fourHoop: [Combo]!
    var fiveHoop: [Combo]!
    var sectionNames: [String]!
    
    
// MARK: - Data Source Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // If showing favorites, get number of sections and appropriate section titles
        if isShowingFavorites {
            var counter = 0
            sectionNames = [String]()
            if let _ = oneHoop {
                counter += 1
                sectionNames.append("Single Hoop")
            }
            if let _ = twoHoop {
                counter += 1
                sectionNames.append("Doubles")
            }
            if let _ = threeHoop {
                counter += 1
                sectionNames.append("Triples")
            }
            if let _ = fourHoop {
                counter += 1
                sectionNames.append("Quads")
            }
            if let _ = fiveHoop {
                counter += 1
                sectionNames.append("Quints")
            }
            return counter
        } else {
            return 1
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var count = 0
        if isShowingFavorites {
            switch section {
            case 0:
                if let _ = oneHoop {
                    return oneHoop.count
                } else if let _ = twoHoop {
                    return twoHoop.count
                } else if let _ = threeHoop {
                    return threeHoop.count
                } else if let _ = fourHoop {
                    return fourHoop.count
                } else if let _ = fiveHoop {
                    return fiveHoop.count
                }
            case 1:
                if let _ = twoHoop {
                    return twoHoop.count
                } else if let _ = threeHoop {
                    return threeHoop.count
                } else if let _ = fourHoop {
                    return fourHoop.count
                } else if let _ = fiveHoop {
                    return fiveHoop.count
                }
            case 2:
                if let _ = threeHoop {
                    return threeHoop.count
                } else if let _ = fourHoop {
                    return fourHoop.count
                } else if let _ = fiveHoop {
                    return fiveHoop.count
                }
            case 3:
                if let _ = fourHoop {
                    return fourHoop.count
                } else if let _ = fiveHoop {
                    return fiveHoop.count
                }
            case 4:
                if let _ = fiveHoop {
                    return fiveHoop.count
                }
            default:
                return 0
            }
        } else {
            count = filteredCombos.count
        }
        return count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        // Find the right array to use
        let data = findCorrectArray(indexPath)
        
        let combo = data[indexPath.item]
        var moves = [LearnedMove]()
        for m in combo.moves {
            moves.append(m as! LearnedMove)
        }
        let tempCombo = TempCombo(hoopCount: combo.hoopCount,
                                  docID: combo.docID,
                                  moves: moves,
                                  favorite: combo.favorite)
        
        guard let cell: ComboCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? ComboCell else {
            fatalError("Unable to dequeue ComboCell.")
        }
        
        cell.coordinator = coordinator
        if cell.coordinator == nil {
            print("SearchCell doesn't have coordinator")
        }
        
        //cell.setCombo(tempCombo)
        

        return cell
    }
    
    
//    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
//        guard let header: ComboHeaderView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "Header", for: indexPath) as? ComboHeaderView else {
//            fatalError("Unable to dequeue ComboHeaderView.")
//        }
//
//        return header
//    }
    
    
    
    private func findCorrectArray(_ indexPath: IndexPath) -> [Combo] {
        var data = [Combo]()
        if isShowingFavorites {
            switch indexPath.section {
            case 0:
                if let _ = oneHoop {
                    data = oneHoop
                } else if let _ = twoHoop {
                    data = oneHoop
                } else if let _ = threeHoop {
                    data = threeHoop
                } else if let _ = fourHoop {
                    data = fourHoop
                } else if let _ = fiveHoop {
                    data = fiveHoop
                }
            case 1:
                if let _ = twoHoop {
                    data = twoHoop
                } else if let _ = threeHoop {
                    data = threeHoop
                } else if let _ = fourHoop {
                    data = fourHoop
                } else if let _ = fiveHoop {
                    data = fiveHoop
                }
            case 2:
                if let _ = threeHoop {
                    data = threeHoop
                } else if let _ = fourHoop {
                    data = fourHoop
                } else if let _ = fiveHoop {
                    data = fiveHoop
                }
            case 3:
                if let _ = fourHoop {
                    data = fourHoop
                } else if let _ = fiveHoop {
                    data = fiveHoop
                }
            case 4:
                if let _ = fiveHoop {
                    data = fiveHoop
                }
            default:
                data = filteredCombos
            }
        } else {
            data = filteredCombos
        }
        return data
    }
    

}
