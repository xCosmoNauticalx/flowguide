//
//  LibraryFinalData.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/19/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class LibraryFinalData: NSObject, UICollectionViewDataSource {

    var page: Node = tutorials
    var moves = [LearnedMove]()
    var filteredNodes = [Node]()
    var nodesAndMoves = [Node: [LearnedMove]]()
    var count = 0
    
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {

        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        

        return moves.count
        
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    guard let cell: MoveCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? MoveCell else {
         fatalError("Unable to dequeue LibraryFinalCell.")
     }
        let name = moves[indexPath.item].name
        cell.label.text = name
        
        // Have cell layout views so frame can be known
        cell.layoutSubviews()
        
        func gradient(frame: CGRect) -> CAGradientLayer {
            let layer = CAGradientLayer()
            layer.frame = frame
            layer.colors = [UIColor.Library.dark.cgColor, UIColor.Library.mid.cgColor, UIColor.Library.light.cgColor]
            layer.startPoint = CGPoint(x: 0.0, y: 0.5)
            layer.endPoint = CGPoint(x: 1.0, y: 0.5)

            return layer
        }
              
        cell.gradientView.layer.insertSublayer(gradient(frame: cell.gradientView.bounds), at: 0)
        cell.gradientView.mask = cell.label
    
        return cell
    }
    

    
}
