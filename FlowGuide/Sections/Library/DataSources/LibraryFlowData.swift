//
//  LibraryFlowData.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/19/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class LibraryFlowData: NSObject, UICollectionViewDataSource {

    var page: Node = tutorials
    var moves = [LearnedMove]()
    var filteredNodes = [Node]()
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
       
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        let children = page.children
        
        for child in children {
            for move in moves {
                // if the move belongs to the child:
                if move.parentNode.contains(child.instanceName) {
                    // Check if child is already in filteredNodes
                    let name = child.instanceName
                    if filteredNodes.contains(where: { $0.instanceName == name }) {
                        // If it is, do nothing and continue the loop
                        continue
                    } else {
                        // If it is not, add it to the array
                        filteredNodes.append(child)
                    }
                }
            }
        }
        print("Moves: \(moves.count)")
        print("Filtered Nodes: \(filteredNodes.count)")
        // If none exist, show decoration view stating no moves in this category yet
        return filteredNodes.count
    }
    

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: GroupCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? GroupCell else {
             fatalError("Unable to dequeue GroupCell.")
         }
    
        let buttonName = filteredNodes[indexPath.item]
        cell.label.text = buttonName.name
        
        // Have cell layout views so frame can be known
        cell.layoutSubviews()
        
        print("GradientView Frame: \(cell.gradientView.frame)")
        print("GradientView Bounds: \(cell.gradientView.bounds)")
        func gradient(frame: CGRect) -> CAGradientLayer {
            let layer = CAGradientLayer()
            layer.frame = frame
            layer.colors = [UIColor.Library.dark.cgColor, UIColor.Library.mid.cgColor, UIColor.Library.light.cgColor]
            print("Gradient Frame: \(layer.frame)")
            print("Gradient Bounds: \(layer.bounds)")

            return layer
        }
              
        cell.gradientView.layer.insertSublayer(gradient(frame: cell.gradientView.bounds), at: 0)
        cell.gradientView.mask = cell.border
        
        cell.textGradient.layer.insertSublayer(gradient(frame: cell.textGradient.bounds), at: 0)
        cell.textGradient.mask = cell.label
    
        return cell
    }
}
