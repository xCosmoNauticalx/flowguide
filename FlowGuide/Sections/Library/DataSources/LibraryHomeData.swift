//
//  LibraryHomeData.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/19/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class LibraryHomeData: NSObject, UICollectionViewDataSource {

    var page: Node = library
    let categories = ["single_hoop_blue", "doubles", "multi_hoops", "saved_combos", "misc"]
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return page.children.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as! LibraryHomeCell
        
        let image = categories[indexPath.item]
        cell.imageView.image = UIImage(named: image)
              
        func gradient(frame: CGRect) -> CAGradientLayer {
            let layer = CAGradientLayer()
            layer.frame = frame
            layer.colors = [UIColor.Library.dark.cgColor, UIColor.Library.mid.cgColor]
                  
        
            return layer
        }
              
        cell.gradientView.layer.insertSublayer(gradient(frame: cell.bounds), at: 0)
        cell.gradientView.mask = cell.imageView
    
        return cell
    }
    
}
