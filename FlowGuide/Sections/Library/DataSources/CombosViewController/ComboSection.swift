//
//  ComboSections.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 6/3/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import UIKit

class ComboSection: Hashable {
    var id = UUID()
    // Properties used to categorize the combos: title and combos
    var title: String
    var combos: [Combo]
    
    init(title: String, combos: [Combo]) {
        self.title = title
        self.combos = combos
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
    
    static func == (lhs: ComboSection, rhs: ComboSection) -> Bool {
        lhs.id == rhs.id
    }
}


extension ComboSection {
    
    static var singleSection = ComboSection(title: "", combos: [Combo]())
    static var oneHoop = ComboSection(title: "Single Hoop", combos: [Combo]())
    static var twoHoop = ComboSection(title: "Doubles", combos: [Combo]())
    static var threeHoop = ComboSection(title: "Triples", combos: [Combo]())
    static var fourHoop = ComboSection(title: "Quads", combos: [Combo]())
    static var fiveHoop = ComboSection(title: "Quints", combos: [Combo]())
    static var allSections: [ComboSection] = [oneHoop, twoHoop, threeHoop, fourHoop, fiveHoop]
}
