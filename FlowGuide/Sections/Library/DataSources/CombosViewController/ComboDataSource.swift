//
//  ComboDataSource.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 6/3/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import UIKit

class ComboData {
    
    typealias DataSource = UICollectionViewDiffableDataSource<ComboSection, Combo>
    typealias Snapshot = NSDiffableDataSourceSnapshot<ComboSection, Combo>
    
    var combos = [Combo]()
    var filteredCombos = [Combo]()
    
    /// Create UICollectionViewDiffableDataSource
    static func makeDataSource(for collectionView: UICollectionView) -> DataSource {
        /// Cell Provider
        let dataSource = DataSource(collectionView: collectionView, cellProvider: { (collectionView, indexPath, combo) -> UICollectionViewCell? in
            guard let cell: ComboCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? ComboCell else {
                fatalError("Unable to dequeue ComboCell.")
            }
            cell.setCombo(combo)
            return cell
        })
        
        return dataSource
    }
    
    
    /// Applies snapshot to data source & takes bool that determines if changes should animate
    func applySnapshot(hoopCount: Int, animatingDifferences: Bool = true) {
        // Get Combos
        guard let combos = findCombos(hoopCount) else { return }
        // Create Snapshot
        var snapshot = Snapshot()
        // Add section to snapshot
        snapshot.appendSections([ComboSection.oneHoop])
        // Add items to section
        snapshot.appendItems(combos)
        // Apply Snapshot
        
        
    }
    
    
    /// Filter & add to sections
    
    
    /// Get all combos that have the hoopCount
    func findCombos(_ hoopCount: Int) -> [Combo]? {
        if combos.contains(where: {$0.hoopCount == hoopCount}) {
            var newCombos = [Combo]()
            for combo in combos {
                if combo.hoopCount == 1 {
                    newCombos.append(combo)
                }
            }
            return newCombos
        } else {
            return nil
        }
    }
    
    
    
}
