//
//  ComboMenuData.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/29/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class ComboMenuData: NSObject, UICollectionViewDataSource {
    
// MARK: - Properties
    
    let options: [IconType] = [.single, .doubles, .triples, .quads, .quints, .star]
    
// MARK: - Methods
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return options.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: ComboMenuCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? ComboMenuCell else {
            fatalError("Unable to dequeue ComboMenuCell.")
        }
        
        let icon = options[indexPath.item]
        cell.setUp(icon)
        if indexPath.item == 0 {
            cell.imageView.image = UIImage(named: "\(icon.rawValue)Dark")
        } else {
            cell.imageView.image = UIImage(named: icon.rawValue)
        }

        return cell
    }
    

}
