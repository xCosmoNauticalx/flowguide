//
//  ComboCellData.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 6/1/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class ComboCellData: NSObject, UICollectionViewDataSource {
    
    var combo: TempCombo!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
//        print("Number of Cells: \(combo.moves.count)")
//        print("For Combo: \(combo.docID)")
        return combo.moves.count
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: MoveCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? MoveCell else {
            fatalError("Unable to dequeue MoveCell.")
        }
        
        cell.label.font = UIFont(name: "HelveticaNeue", size: 20)
//        print("Cell index: \(indexPath.item)")
//        print("Combo: \(combo.docID)")
        let move = combo.moves[indexPath.item]
        cell.label.text = move.name
        
        return cell
    }
    

}
