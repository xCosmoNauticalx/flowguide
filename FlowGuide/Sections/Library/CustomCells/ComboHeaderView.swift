//
//  ComboHeaderView.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 6/1/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class ComboHeaderView: UICollectionReusableView {
    
// MARK: - Properties
    
    let label: UILabel = {
        let l = UILabel()
        l.text = "Hoop Count?"
        l.textAlignment = .left
        l.textColor = UIColor.Library.dark
        l.font = UIFont(name: "Helvetica", size: 23)
        return l
    }()
    
    
// MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
// MARK: - Set Up
    
    private func setUp() {
        addSubview(label)
        label.fillSuperView()
    }
        
}
