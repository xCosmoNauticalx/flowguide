//
//  LibraryHomeCell.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 1/29/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class LibraryHomeCell: UICollectionViewCell {
    
    weak var coordinator: LibraryCoordinator?
    
    @IBOutlet var gradientView: UIView!
    @IBOutlet var imageView: UIImageView!
    
    override var isHighlighted: Bool {
        didSet {
            toggleIsHighlighted()
        }
    }
 
    
    func toggleIsHighlighted() {
        UIView.animate(withDuration: 0.01, delay: 0, options: [.curveEaseOut], animations: {
            self.alpha = self.isHighlighted ? 0.5 : 1.0
        })
    }
}
