//
//  ComboMenuCell.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/29/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class ComboMenuCell: UICollectionViewCell {
    
// MARK: - Properties
    
    /// Data
    var lightImage: String = "singleIcon"
    var darkImage: String = "singleIconDark"
    
    let imageView: UIImageView = {
        let i = UIImageView()
        i.contentMode = .scaleAspectFit
        i.clipsToBounds = true
        return i
    }()
    
    
// MARK: - Overrides
    
    override var isSelected: Bool {
        didSet {
            toggleIsSelected()
        }
    }
    
    func toggleIsSelected() {
        UIView.animate(withDuration: 0.45, animations: {
            self.imageView.image = self.isSelected ? UIImage(named: self.darkImage) : UIImage(named: self.lightImage)
            self.backgroundColor = self.isSelected ? UIColor.Library.dark : .clear
        })
    }

    
    
// MARK: - Set Up
    
    func setUp(_ icon: IconType) {
        
        /// Add Subviews
        addSubview(imageView)
        
        /// Image Names
        lightImage = icon.rawValue
        darkImage = "\(icon.rawValue)Dark"
        
        /// Constraints & Corner Radius
        switch icon {
        case .single:
            imageView.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor, padding: .init(top: 8, left: 8, bottom: -8, right: -8))
            
            /// Corner Radius
            layoutSubviews()
            let path = UIBezierPath(roundedRect: self.bounds,
                                    byRoundingCorners: [.topLeft, .bottomLeft],
                                    cornerRadii: CGSize(width: 5, height:  20))

            let maskLayer = CAShapeLayer()

            maskLayer.path = path.cgPath
            self.layer.mask = maskLayer
            
        case .doubles:
            imageView.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor, padding: .init(top: 0, left: 6, bottom: 0, right: -6))
            
        case .star:
            imageView.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor, padding: .init(top: 6, left: 6, bottom: -6, right: -6))
            
            /// Corner Radius
            layoutSubviews()
            let path = UIBezierPath(roundedRect: self.bounds,
                                    byRoundingCorners: [.topRight, .bottomRight],
                                    cornerRadii: CGSize(width: 5, height:  20))

            let maskLayer = CAShapeLayer()

            maskLayer.path = path.cgPath
            self.layer.mask = maskLayer
            
        default:
            imageView.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor, padding: .init(top: 4, left: 4, bottom: -4, right: -4))
        }

    }

}


// MARK: - Enums

enum IconType: String {
    case single = "singleIcon"
    case doubles = "doublesIcon"
    case triples = "triplesIcon"
    case quads = "quadsIcon"
    case quints = "quintsIcon"
    case star = "hollowStar"
}
