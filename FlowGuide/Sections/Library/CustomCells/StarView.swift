//
//  StarView.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 6/2/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class StarView: UICollectionReusableView {
    
// MARK: - Properties
    
    let star: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "filledStar")
        return i
    }()
    
    
// MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
// MARK: - Set Up
    
    private func setUp() {
        addSubview(star)
        star.fillSuperView()
    }
    
}
