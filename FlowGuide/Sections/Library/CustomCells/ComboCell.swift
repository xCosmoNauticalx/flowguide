//
//  ComboCell.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/28/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class ComboCell: UICollectionViewCell {
    
// MARK: - Properties
    
    /// Coordinator
    weak var coordinator: LibraryCoordinator?
    
    /// Data
    var combo: Combo!
    var newCombo: Combo!
    var moves = [LearnedMove]()
    var shouldCreateStack: Bool = true
    
    var stackView: UIStackView!
    
    let borderView = UIView()
    let stackGradient = UIView()
    let borderGradient: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "comboGradient")
        i.contentMode = .scaleToFill
        return i
    }()
    
    
    override func prepareForReuse() {
        stackView.removeFromSuperview()
    }

    
// MARK: - Set Up
    
    func setCombo(_ combo: Combo) {
        self.combo = combo
        setUp()
    }
    
    func setUp() {

        moves = [LearnedMove]()

        print("Combos has \(combo.moves.count) moves")
        
        /// Add moves to new array since combo.moves is an NSOrderedSet
        for move in combo.moves {
            moves.append(move as! LearnedMove)
        }
        
        /// StackView
        stackView = createStack()
        
        print("First stack call: \(stackView.arrangedSubviews)")
        
        /// Add Subviews
        addSubview(borderView)
        addSubview(stackView)
        
        /// Constraints
        borderView.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor, padding: .init(top: 0, left: 40, bottom: 0, right: -40))
        stackView.anchor(top: borderView.topAnchor, leading: borderView.leadingAnchor, bottom: borderView.bottomAnchor, trailing: borderView.trailingAnchor)
        
        /// Draw Border
        borderView.layer.borderWidth = 1
        borderView.layer.cornerRadius = 20
        borderView.layer.borderColor = UIColor.Library.mid.cgColor

    }
    
    
// MARK: - Methods
    
    func createStack() -> UIStackView {
        /// Create new stackView
        let s = UIStackView()
        s.axis = .vertical
        s.distribution = .equalSpacing
        s.alignment = .center
        s.spacing = 5
        
            /// Add buttons
            for move in moves {
                let button = UIButton()
                let title = move.name
                let attributes: [NSAttributedString.Key: Any] = [
                    .foregroundColor: UIColor.Library.mid]
                let attributedTitle = NSAttributedString(string: title, attributes: attributes)
                button.setAttributedTitle(attributedTitle, for: .normal)
                button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
                s.addArrangedSubview(button)
                print("Added button: \(move.name)")
            }
        return s
    }
    
    
    @objc func buttonTapped(_ sender: UIButton!) {
        print("Tapped")
        print(stackView.arrangedSubviews)
        if let index = stackView.arrangedSubviews.firstIndex(of: sender) {
            let move = moves[index]
            coordinator?.moveDetail(docID: move.docID)
        } else {
            print("Couldn't get index")
        }
    }
    
    deinit {
        print("Deallocating ComboCell")
    }
    
}
