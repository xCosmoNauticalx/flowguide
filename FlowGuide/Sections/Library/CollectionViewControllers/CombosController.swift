//
//  CombosController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/28/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class CombosController: UICollectionViewController {
    
    let badgeElementKind = "badge-element-kind"

// MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        setUp()
    }
    
    
// MARK: - Set Up
    
    private func setUp() {
        
        /// Collection View
        self.collectionView.backgroundColor = .clear
        self.collectionView.register(ComboCell.self, forCellWithReuseIdentifier: "Cell")
        self.collectionView.register(ComboHeaderView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "Header")
        //self.collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "Footer")
        self.collectionView.register(StarView.self, forSupplementaryViewOfKind: badgeElementKind, withReuseIdentifier: "Badge")
        
        switch UIScreen.main.traitCollection.userInterfaceIdiom {
        case .pad:
            self.collectionView.collectionViewLayout = createPadLayout()
        default:
            self.collectionView.collectionViewLayout = createPhoneLayout()
        }
        
        
    }
    
    private func createPhoneLayout() -> UICollectionViewLayout {
        
        let badgeAnchor = NSCollectionLayoutAnchor(edges: [.top, .trailing], fractionalOffset: CGPoint(x: -1.3, y: -0.3))
        //let badgeAnchor = NSCollectionLayoutAnchor(edges: [.top, .trailing], absoluteOffset: CGPoint(x: -30, y: -30))
        let badgeSize = NSCollectionLayoutSize(widthDimension: .absolute(25),
                                              heightDimension: .absolute(25))
        let badge = NSCollectionLayoutSupplementaryItem(
            layoutSize: badgeSize,
            elementKind: badgeElementKind,
            containerAnchor: badgeAnchor)

        let size = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(50))
        //let item = NSCollectionLayoutItem(layoutSize: size)
        let item = NSCollectionLayoutItem(layoutSize: size, supplementaryItems: [badge])
        // item.contentInsets = NSDirectionalEdgeInsets(top: 5, leading: 5, bottom: 5, trailing: 5)
        item.edgeSpacing = NSCollectionLayoutEdgeSpacing(leading: nil, top: NSCollectionLayoutSpacing.fixed(20), trailing: nil, bottom: NSCollectionLayoutSpacing.fixed(0))
       
        let group = NSCollectionLayoutGroup.vertical(layoutSize: size, subitems: [item])
       
        let headerFooterSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(15.0))
        let header = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerFooterSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)
        let footer = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerFooterSize, elementKind: UICollectionView.elementKindSectionFooter, alignment: .bottom)
       
        let section = NSCollectionLayoutSection(group: group)
        section.interGroupSpacing = 25
        section.boundarySupplementaryItems = [header, /*footer*/]
       
        let layout = UICollectionViewCompositionalLayout(section: section)
       
        return layout
    }
    
    
    private func createPadLayout() -> UICollectionViewLayout {
        
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(0.85))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalWidth(0.14))
        let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitems: [item])
        
        let headerFooterSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(50.0))
        let header = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerFooterSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)
        
        let section = NSCollectionLayoutSection(group: group)
        section.interGroupSpacing = 30
        section.boundarySupplementaryItems = [header]
        
        let layout = UICollectionViewCompositionalLayout(section: section)
        
        return layout
        
    }
    
    private func createLayout() -> UICollectionViewLayout {
        
        let size = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(10))
        let item = NSCollectionLayoutItem(layoutSize: size)
        item.edgeSpacing = NSCollectionLayoutEdgeSpacing(leading: nil, top: NSCollectionLayoutSpacing.fixed(20), trailing: nil, bottom: NSCollectionLayoutSpacing.fixed(0))
        
        let group = NSCollectionLayoutGroup.vertical(layoutSize: size, subitems: [item])
        
        let section = NSCollectionLayoutSection(group: group)
        
        let layout = UICollectionViewCompositionalLayout(section: section)
        
        return layout
    }

}
