//
//  LibraryHomeCollectionController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/19/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit
import Firebase


class LibraryHomeController: UICollectionViewController, Storyboarded {

    
    // MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        setUp()
    }
    
    
// MARK: - Set Up

    
    private func setUp() {
           
           /// Collection View
           
           switch UIScreen.main.traitCollection.userInterfaceIdiom {
           case .pad:
               self.collectionView.collectionViewLayout = createPadLayout()
           default:
               self.collectionView.collectionViewLayout = createPhoneLayout()
           }
           

       }
       
       private func createPhoneLayout() -> UICollectionViewLayout {
           
           let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(0.9))
           let item = NSCollectionLayoutItem(layoutSize: itemSize)
           
           let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalWidth(0.38))
           let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitems: [item])
           //group.interItemSpacing = .fixed(15)
           
           let headerFooterSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(50.0))
           let header = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerFooterSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)
           let footer = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerFooterSize, elementKind: UICollectionView.elementKindSectionFooter, alignment: .bottom)
           
           let section = NSCollectionLayoutSection(group: group)
           section.boundarySupplementaryItems = [header, footer]
           section.interGroupSpacing = 15
           
           let layout = UICollectionViewCompositionalLayout(section: section)
           
           return layout
       }
       
       private func createPadLayout() -> UICollectionViewLayout {
           
           let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(0.9))
           let item = NSCollectionLayoutItem(layoutSize: itemSize)
           
           let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalWidth(0.26))
           let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitems: [item])
           
           let headerFooterSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(55.0))
           let header = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerFooterSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)
           let footer = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerFooterSize, elementKind: UICollectionView.elementKindSectionFooter, alignment: .bottom)
           
           let section = NSCollectionLayoutSection(group: group)
           section.boundarySupplementaryItems = [header, footer]
           section.interGroupSpacing = 30
           
           let layout = UICollectionViewCompositionalLayout(section: section)
           
           return layout
           
       }
        
    

}

