//
//  ComboMenuController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/29/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class ComboMenuController: UICollectionViewController {

// MARK: - Properties
    
    /// Coordinators
    weak var delegate: MenuDelegate?
    
    /// Data
    let data = ComboMenuData()
    
    
// MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        setUp()
    }
    
    
// MARK: - Set Up
    
    private func setUp() {
        
        /// Collection View
        self.collectionView.backgroundColor = .clear
        self.collectionView.dataSource = data
        self.collectionView.collectionViewLayout = createLayout()
        self.collectionView.register(ComboMenuCell.self, forCellWithReuseIdentifier: "Cell")
        self.clearsSelectionOnViewWillAppear = false
        
        self.view.layer.borderWidth = 1
        self.view.layer.cornerRadius = 5
        self.view.layer.borderColor = UIColor.Library.dark.cgColor
        
    }
    
    
    private func createLayout() -> UICollectionViewLayout {
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1/6), heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 6)
        
        let section = NSCollectionLayoutSection(group: group)

        let layout = UICollectionViewCompositionalLayout(section: section)
         
        return layout
    }
    
    
// MARK: - UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        delegate?.didTapMenuItem(indexPath: indexPath)
    }

}
