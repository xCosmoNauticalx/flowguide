//
//  LibraryFinalViewController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/9/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class LibraryMovesController: UICollectionViewController {
    
// MARK: - Properties
    
    /// Views
    let background: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "darkBackground")
        i.contentMode = .scaleAspectFill
        return i
    }()
    
// MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        setUp()
    }
    

// MARK: - Set Up
    
    private func setUp() {
        
        /// Collection View
        self.collectionView.backgroundView = background
        self.collectionView.register(MoveCell.self, forCellWithReuseIdentifier: "Cell")
        self.collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "Header")
        self.collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "Footer")
        
        switch UIScreen.main.traitCollection.userInterfaceIdiom {
        case .pad:
            self.collectionView.collectionViewLayout = createPadLayout()
        default:
            self.collectionView.collectionViewLayout = createPhoneLayout()
        }
    }
    
    
    private func createPhoneLayout() -> UICollectionViewLayout {
        
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(60))
        let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitems: [item])
        
        let headerFooterSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(30.0))
        let header = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerFooterSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)
        let footer = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerFooterSize, elementKind: UICollectionView.elementKindSectionFooter, alignment: .bottom)
        
        let section = NSCollectionLayoutSection(group: group)
        section.boundarySupplementaryItems = [header, footer]
        section.interGroupSpacing = 25
        
        let layout = UICollectionViewCompositionalLayout(section: section)
        
        return layout
    }
    
    
    private func createPadLayout() -> UICollectionViewLayout {
        
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalWidth(0.1))
        let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitems: [item])
        
        let headerFooterSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(50.0))
        let header = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerFooterSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)
        let footer = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerFooterSize, elementKind: UICollectionView.elementKindSectionFooter, alignment: .bottom)
        
        let section = NSCollectionLayoutSection(group: group)
        section.boundarySupplementaryItems = [header, footer]
        section.interGroupSpacing = 40
        
        let layout = UICollectionViewCompositionalLayout(section: section)
        
        return layout
    }
    


}


