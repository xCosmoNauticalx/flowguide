//
//  ContainerController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 3/12/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class LibraryContainerController: UIViewController {

    // MARK: - Properties
    
    weak var coordinator: LibraryCoordinator?
    var slideDelegate: Slideable?
    
    var menuViewController: MenuViewController!
    var centerController: LibraryHomeViewController!
    
    var isExpanded = false
    var titleLabel = TitleLabel()
    
    
    // MARK: - Init
    
    override func loadView() {
        super.loadView()
        setUp()
        configureLibraryHomeViewController()
    }
    
    
    

    
    
    // MARK: - Handlers
    
    private func setUp() {
        
        /// Format Title
        let frame = CGRect(x: (self.navigationController?.navigationBar.frame.width)!/6, y: -30, width: (self.navigationController?.navigationBar.frame.width)!/6 * 4, height: 100)
        titleLabel.frame = frame
        titleLabel.text = "My Library"
        titleLabel.textColor = UIColor.Library.dark
        
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            titleLabel.font = UIFont(name: "HelveticaNeue", size: 49)
        }
        
        self.navigationController?.navigationBar.addSubview(titleLabel)

        /// Add menu button
        let menuImage = UIBarButtonItem(image: UIImage(named: "menu"), style: .plain, target: self, action: #selector(menuTapped(_:)))
        navigationItem.leftBarButtonItem = menuImage
        
        /// Add radial
        let radialFrame = CGRect(x: -30, y: -45, width: (self.view.frame.width) + 70, height: 140)
        let radial = UIImageView(frame: radialFrame)
        radial.image = UIImage(named: "pinkRadial")
        radial.alpha = 0.33
        radial.tag = 2
        self.navigationController?.navigationBar.addSubview(radial)
        
        /// Hand titleDelegate to Coordinator
        coordinator?.titleDelegate = self
        
    }
    
    
    func configureLibraryHomeViewController() {
        
        centerController = LibraryHomeViewController()
        centerController.titleDelegate = self
        centerController.coordinator = coordinator

        view.addSubview(centerController.view)
        // Add the navigationController containing the HomeController as a child to the ContainerController
        addChild(centerController)
        // Home controller didMove to the parent container of self, which is the ContainerController
        centerController.didMove(toParent: self)
    }
    
    
    func animateStatusBar() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.setNeedsStatusBarAppearanceUpdate()
        }, completion: nil)
        
    }
    
    
    @objc func menuTapped(_ sender: UIBarButtonItem!) {
        slideDelegate?.handleMenuToggle()
    }

}

extension LibraryContainerController: Titleable {
    
    func changeTitleText(forTitle title: String) {
        titleLabel.text = title
    }
    
    
}



