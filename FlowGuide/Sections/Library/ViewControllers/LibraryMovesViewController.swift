//
//  LibraryFinalViewController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/4/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit
import GoogleMobileAds

class LibraryMovesViewController: UIViewController {

// MARK: - Properties
    
    /// Coordinators
    weak var coordinator: LibraryCoordinator?
    weak var titleDelegate: Titleable?
    
    /// Data
    let data = LibraryFinalData()
    let userIsEntitled = PurchasesHelper.isUserEntitled()
    var parents: [Node] = []
    var shouldRemoveRadial = false
    
    /// Views
    let background: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "darkBackground")
        i.contentMode = .scaleAspectFill
        return i
    }()
    
    let container = UIView()
    var gradientView = CAGradientLayer()
    
    let collectionView = LibraryMovesController(collectionViewLayout: UICollectionViewFlowLayout())
    let directory = DirectoryController(collectionViewLayout: UICollectionViewFlowLayout())
    
    /// Ads
    var bannerView: GADBannerView!
    
    
// MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        setUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        /// Title
        let text = data.page.name
        titleDelegate?.changeTitleText(to: text)
        titleDelegate?.changeTitleColor(to: UIColor.Library.dark)
        
        /// Show Radial View
        for view in self.navigationController?.navigationBar.subviews ?? [UIView()] {
            if view.tag == 2 {
                if view.alpha == 0 {
                    view.alpha = 0.33
                    shouldRemoveRadial = false
                }
            }
         }
        
        /// Tab Bar
        Utilities.styleTabBar(self, color: UIColor.Library.light)
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        /// Hide Radial View
        if shouldRemoveRadial {
            for view in self.navigationController?.navigationBar.subviews ?? [UIView()] {
                if view.tag == 2 {
                    view.alpha = 0
                }
            }
        }
    }
    
    
    override func viewDidLayoutSubviews() {
        if !userIsEntitled {
            gradientView.frame = container.bounds
        } else {
            gradientView.frame = view.bounds
        }
    }
    
    
// MARK: - Set Up
    
    private func setUp() {
        
        /// Back Button
        Utilities.styleNavBar(self, color: UIColor.Library.dark)
        
        /// CollectionView
        collectionView.collectionView.delegate = self
        collectionView.collectionView.dataSource = data
        
        /// Directory
        directory.data.parents = parents
        guard let coordinatorr = coordinator else { return }
        directory.setDelegate(with: coordinatorr)
        
        /// Add Views
        view.addSubview(background)
        background.fillSuperView()
        view.addSubview(container)
        container.addSubview(collectionView.view)
        view.addSubview(directory.view)

        collectionView.view.fillSuperView()
        
        /// Constraints
        background.fillSuperView()
        collectionView.view.fillSuperView()
        
        if !userIsEntitled {
            createAndLoadBanner()
            container.anchor(top: directory.view.bottomAnchor, leading: view.leadingAnchor, bottom: bannerView.topAnchor, trailing: view.trailingAnchor)
        } else {
            container.anchor(top: directory.view.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        }
        
        directory.view.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: container.topAnchor, trailing: view.trailingAnchor, padding: .init(top: 8, left: 15, bottom: 0, right: -15), size: .init(width: 0, height: 20))
        
        /// CollectionView Gradient
        gradientView.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
        gradientView.locations = [0, 0.06, 0.9, 1]
        container.layer.mask = gradientView
        
    }

}


// MARK: - UICollectionViewDelegate

extension LibraryMovesViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let docID = data.moves[indexPath.item].docID
        coordinator?.moveDetail(docID: docID)
        shouldRemoveRadial = true
    }
    
    
    /// Clear labels when no longer needed
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? MoveCell else { return }
        cell.label.text = nil
    }
    
}


// MARK: - Banner View

extension LibraryMovesViewController: GADBannerViewDelegate {
    
    func createAndLoadBanner() {
        bannerView = GADBannerView()
        bannerView.adUnitID = AdIdentifier.banner
        bannerView.rootViewController = self
        view.addSubview(bannerView)
        bannerView.anchor(top: container.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, size: .init(width: 0, height: 60))
        bannerView.load(GADRequest())
    }
    
}
