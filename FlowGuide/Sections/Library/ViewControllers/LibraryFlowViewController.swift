//
//  LibraryFlowViewController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/19/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit
import GoogleMobileAds

class LibraryFlowViewController: UIViewController {

// MARK: - Properties
    
    /// Coordinators
    weak var coordinator: LibraryCoordinator?
    var titleDelegate: Titleable?
    
    /// Data
    let data = LibraryFlowData()
    var shouldShowCollectionView: Bool = true
    let userIsEntitled = PurchasesHelper.isUserEntitled()
    var parents = [Node]()
    
    /// Views
    let background: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "darkBackground")
        i.contentMode = .scaleAspectFill
        return i
    }()
    
    let container = UIView()
    var gradientView = CAGradientLayer()
    let nothingView = NothingHereView(text: "Hop over to the Archive, learn a new move or two, and you'll be able to find it here after.", colors: [UIColor.Library.mid.cgColor, UIColor.Library.dark.cgColor])
    
    let collectionView = LibraryFlowController(collectionViewLayout: UICollectionViewFlowLayout())
    let directory = DirectoryController(collectionViewLayout: UICollectionViewFlowLayout())
    
    /// Ads
    var bannerView: GADBannerView!
    
    
// MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        setUp()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        /// Change Title
        let text = data.page.name
        titleDelegate?.changeTitleText(to: text)
        
        /// Tab Bar
        Utilities.styleTabBar(self, color: UIColor.Library.light)
        
    }
    
    
    override func viewDidLayoutSubviews() {
        if !userIsEntitled {
            gradientView.frame = container.bounds
        } else {
            gradientView.frame = view.bounds
        }
    }
    
    
// MARK: - Set Up
    
    private func setUp() {
        
        /// Back Button
        Utilities.styleNavBar(self, color: UIColor.Library.dark)
        
        /// CollectionView
        collectionView.collectionView.delegate = self
        collectionView.collectionView.dataSource = data
        
        /// Directory
        directory.data.parents = parents
        guard let coordinatorr = coordinator else { return }
        directory.setDelegate(with: coordinatorr)
        
        /// Add Views
        view.addSubview(background)
        background.fillSuperView()
        
        
        if shouldShowCollectionView == false {
            view.addSubview(nothingView)
            nothingView.anchor(top: nil, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor)
            nothingView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -50).isActive = true
        } else {
            view.addSubview(container)
            container.addSubview(collectionView.view)
            view.addSubview(directory.view)
            collectionView.view.fillSuperView()
            
            if !userIsEntitled {
                createAndLoadBanner()
                container.anchor(top: directory.view.bottomAnchor, leading: view.leadingAnchor, bottom: bannerView.topAnchor, trailing: view.trailingAnchor)
            } else {
                container.anchor(top: directory.view.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
            }
            
            directory.view.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: container.topAnchor, trailing: view.trailingAnchor, padding: .init(top: 8, left: 15, bottom: 0, right: -15), size: .init(width: 0, height: 20))
            
            /// CollectionView Gradient
            gradientView.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
            gradientView.locations = [0, 0.06, 0.9, 1]
            container.layer.mask = gradientView
        }
    }

}

// MARK: - UICollectionViewDelegate

extension LibraryFlowViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let nextPage = data.filteredNodes[indexPath.item]
        var filteredMoves = [LearnedMove]()
        // Filter moves to only include those that belong on the next page
        for move in data.moves {
            if move.parentNode.contains(nextPage.instanceName) {
                filteredMoves.append(move)
            }
        }
        // Add current Node to parents for next page
        var newParents = parents
        newParents.append(data.page)
        
        if nextPage.finalGroup == .none {
            coordinator?.next(withMoves: filteredMoves, page: nextPage, parents: newParents)
        } else {
            coordinator?.movesPage(page: nextPage, moves: filteredMoves, parents: newParents)
        }
    }
    
    
    /// Clear labels when no longer needed
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? GroupCell else { return }
        cell.label.text = nil
    }
    
}


// MARK: - Banner View 

extension LibraryFlowViewController: GADBannerViewDelegate {
    
    func createAndLoadBanner() {
        bannerView = GADBannerView()
        bannerView.adUnitID = AdIdentifier.banner
        bannerView.rootViewController = self
        view.addSubview(bannerView)
        bannerView.anchor(top: container.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, size: .init(width: 0, height: 60))
        bannerView.load(GADRequest())
    }
    
}
