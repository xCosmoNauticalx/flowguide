//
//  LibraryHomeViewController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/19/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class LibraryHomeViewController: UIViewController {

// MARK: - Properties
        
    /// Coordinators
    weak var coordinator: LibraryCoordinator?
    var titleDelegate: Titleable?

    /// Data
    let data = LibraryHomeData()
    
    let container = UIView()
    var gradientView = CAGradientLayer()
    
    let collectionView = LibraryHomeController.instantiate()
    
        
// MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        setUp()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        /// Replace Title
        let text = "My Library"
        titleDelegate?.changeTitleText(to: text)
        titleDelegate?.changeTitleColor(to: UIColor.Library.dark)
        
        /// Tab Bar
        Utilities.styleTabBar(self, color: UIColor.Library.light)
        
        /// Navigation Bar
        navigationController?.navigationBar.tintColor = UIColor.Library.dark
    }
    
    override func viewDidLayoutSubviews() {
        gradientView.frame = view.bounds
    }

    
// MARK: - Set Up
    private func setUp() {
        
        collectionView.collectionView.dataSource = data
        collectionView.collectionView.delegate = self
        
        view.addSubview(container)
        container.addSubview(collectionView.view)
        
        container.fillSuperView()
        collectionView.view.fillSuperView()
        
        /// CollectionView Gradient
        gradientView.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
        gradientView.locations = [0, 0.07, 0.9, 1]
        container.layer.mask = gradientView
        
    }
    

}


// MARK: - UICollectionViewDelegate

extension LibraryHomeViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let nextPage = data.page.children[indexPath.item]
        
        switch indexPath.item {
        case 0:
            coordinator?.next(withHoopCount: 1, page: nextPage)
        case 1:
            coordinator?.next(withHoopCount: 2, page: nextPage)
        case 2:
            // TODO: Get all multi moves, not just those with hoopCount of 3
            coordinator?.next(withHoopCount: 3, page: nextPage)
        case 3:
            coordinator?.combos()
        default:
            // TODO: Change to go to misc page. Don't think there's a hoopCount
            coordinator?.next(withHoopCount: 3, page: nextPage)
            
        }
    }

   
    // Uncomment this method to specify if the specified item should be selected
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        return true
    }
 

}

