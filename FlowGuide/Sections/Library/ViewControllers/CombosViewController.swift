//
//  CombosViewController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/28/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class CombosViewController: UIViewController {
    
// MARK: - Typealias
    
    typealias DataSource = UICollectionViewDiffableDataSource<ComboSection, Combo>
    typealias Snapshot = NSDiffableDataSourceSnapshot<ComboSection, Combo>

// MARK: - Properties
    
    /// Coordinator
    weak var coordinator: LibraryCoordinator?
    weak var titleDelegate: Titleable?
    
    /// Data
    let data = ComboData()
    private lazy var dataSource = makeDataSource()  // Must be lazy since the VC needs to finish initalizing first
    
    /// Views
    let background: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "darkBackground")
        i.contentMode = .scaleAspectFill
        return i
    }()
    
    let collectionView = CombosController(collectionViewLayout: UICollectionViewFlowLayout())
    let menu = ComboMenuController(collectionViewLayout: UICollectionViewFlowLayout())
    let container = UIView()
    let gradientView = CAGradientLayer()
    
    
// MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        setUp()
    }
    
    override func viewDidLoad() {
        /// Gesture Recognizer
        let longPressGR = UILongPressGestureRecognizer(target: self, action: #selector(longPress))
        longPressGR.minimumPressDuration = 0.5
        longPressGR.delaysTouchesBegan = true
        collectionView.collectionView.addGestureRecognizer(longPressGR)
    }

    
    override func viewWillAppear(_ animated: Bool) {
        
        /// Title
        let text = "Saved Combos"
        titleDelegate?.changeTitleText(to: text)
        titleDelegate?.changeTitleColor(to: UIColor.Library.dark)
        
        /// Show Radial View
        for view in self.navigationController?.navigationBar.subviews ?? [UIView()] {
            if view.tag == 2 {
                if view.alpha == 0 {
                    view.alpha = 0.33
                }
            }
         }
        
        /// Tab Bar
        Utilities.styleTabBar(self, color: UIColor.Library.light)
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        /// Hide Radial View
            for view in self.navigationController?.navigationBar.subviews ?? [UIView()] {
                if view.tag == 2 {
                    view.alpha = 0
                }
            }
    }
    
    override func viewDidLayoutSubviews() {
        gradientView.frame = container.bounds
    }

    
// MARK: - Set Up
    
    private func setUp() {
        
        /// Back Button
        Utilities.styleNavBar(self, color: UIColor.Library.dark)
        
        /// CollectionView
        collectionView.collectionView.delegate = self
        // Get initial combos
        applySnapshot(hoopCount: 1, animatingDifferences: false)
        
        /// Menu View
        menu.collectionView.selectItem(at: [0,0], animated: true, scrollPosition: .centeredHorizontally)
        menu.delegate = self
        
        menu.view.layer.shadowColor = UIColor.black.cgColor
        menu.view.layer.shadowRadius = 1.5
        menu.view.layer.shadowOpacity = 0.2
        menu.view.layer.shadowOffset = CGSize(width: 2, height: 2)
        menu.view.layer.masksToBounds = false
        
        /// Add Subviews
        view.addSubview(background)
        view.addSubview(container)
        container.addSubview(collectionView.view)
        view.addSubview(menu.view)
        
        /// Constraints
        // Background
        background.fillSuperView()
        
        // Menu
        menu.view.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 15, left: 15, bottom: 0, right: -15), size: .init(width: 0, height: 45))
        
        // Container
        container.anchor(top: menu.view.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        
        // CollectionView
        collectionView.view.fillSuperView()
        
        /// CollectionView Gradient
        gradientView.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
        gradientView.locations = [0, 0.06, 0.9, 1]  /// Idk why this doesn't work with the same locations as the others
        container.layer.mask = gradientView
        //container.layer.addSublayer(gradientView)
    }
    
    
// MARK: - Create Data Source
    
    /// Create UICollectionViewDiffableDataSource
    func makeDataSource() -> DataSource {
        /// Cell Provider
        let dataSource = DataSource(collectionView: collectionView.collectionView, cellProvider: { (collectionView, indexPath, combo) -> UICollectionViewCell? in
            guard let cell: ComboCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? ComboCell else {
                fatalError("Unable to dequeue ComboCell.")
            }
            // Set Coordinator
            cell.coordinator = self.coordinator
            if cell.coordinator == nil {
                print("SearchCell doesn't have coordinator")
            }
            
            // Add moves to new array since combo.moves is an NSOrderedSet
            var moves = [LearnedMove]()
            for move in combo.moves {
                moves.append(move as! LearnedMove)
            }
            // Add the stackView to the cell
            //cell.stackView = self.createStack(moves)
            // Set Combo
            cell.setCombo(combo)
            return cell
        })
        /// Supplementary View Provider
        dataSource.supplementaryViewProvider = { collectionView, kind, indexPath in
            // Ensure the supplementary view provider asks for a header
            if kind == UICollectionView.elementKindSectionHeader {
                // Dequeue a new header view
                let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header", for: indexPath) as? ComboHeaderView
                // Retrieve the section from the dataSource and set the label's text to the section's title
                let section = self.dataSource.snapshot().sectionIdentifiers[indexPath.section]
                view?.label.text = section.title
                return view
            }
             if kind == "badge-element-kind" {
                let view = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Badge", for: indexPath) as? StarView
                let combo = self.dataSource.snapshot().itemIdentifiers[indexPath.item]
                view?.isHidden = !combo.favorite
                return view
            }
            return nil
        }
        
        return dataSource
    }
    
    
    /// Applies snapshot to data source & takes bool that determines if changes should animate
    func applySnapshot(hoopCount: Int, animatingDifferences: Bool = true) {
        // Create Snapshot
        var snapshot = Snapshot()
        // Check if favorites
        if hoopCount == 0 {
            // Favorites tab
            // Find all favorites & append them to designated section
            if let combos = findCombos(1, true) {
                let sortedCombos = combos.sorted(by: { $0.created < $1.created })
                snapshot.appendSections([ComboSection.oneHoop])
                snapshot.appendItems(sortedCombos, toSection: ComboSection.oneHoop)
            }
            if let combos = findCombos(2, true) {
                snapshot.appendSections([ComboSection.twoHoop])
                snapshot.appendItems(combos, toSection: ComboSection.twoHoop)
            }
            if let combos = findCombos(3, true) {
                snapshot.appendSections([ComboSection.threeHoop])
                snapshot.appendItems(combos, toSection: ComboSection.threeHoop)
            }
            if let combos = findCombos(4, true) {
                snapshot.appendSections([ComboSection.fourHoop])
                snapshot.appendItems(combos, toSection: ComboSection.fourHoop)
            }
            if let combos = findCombos(5, true) {
                snapshot.appendSections([ComboSection.fiveHoop])
                snapshot.appendItems(combos, toSection: ComboSection.fiveHoop)
            }
        } else {
            // Add section to snapshot
            let section = ComboSection.singleSection
            snapshot.appendSections([section])
            // Get Combos
            if let combos = findCombos(hoopCount) {
                // Add items to section
                print("Snapshot Combos:")
                for combo in combos {
                    print(combo.docID)
                }
                //let sortedCombos = combos.sorted(by: { $0.created < $1.created })
                snapshot.appendItems(combos, toSection: section)
            }
        }
        // Apply Snapshot
        dataSource.apply(snapshot, animatingDifferences: animatingDifferences)
    }
    
    
// MARK: - Methods
    
    /// Get all combos that have the hoopCount
    func findCombos(_ hoopCount: Int, _ showingFavorites: Bool = false) -> [Combo]? {
        var combos = [Combo]()
        if !showingFavorites {
            if data.combos.contains(where: {$0.hoopCount == hoopCount}) {
                for combo in data.combos {
                    if combo.hoopCount == hoopCount {
                        combos.append(combo)
                    }
                }
                return combos
            } else {
                return nil
            }
        } else {
            if data.combos.contains(where: {$0.hoopCount == hoopCount && $0.favorite == true}) {
                for combo in data.combos {
                    if combo.hoopCount == hoopCount && combo.favorite == true {
                        combos.append(combo)
                    }
                }
                return combos
            } else {
                 return nil
            }
        }
        
    }
    
    
    /// Create stack view for cells
    func createStack(_ moves: [LearnedMove]) -> UIStackView {
        // Create new stackView
        let s = UIStackView()
        s.axis = .vertical
        s.distribution = .equalSpacing
        s.alignment = .center
        s.spacing = 5
        
        // Add buttons
        for move in moves {
            let button = UIButton()
            let title = move.name
            let attributes: [NSAttributedString.Key: Any] = [
                .foregroundColor: UIColor.Library.mid]
            let attributedTitle = NSAttributedString(string: title, attributes: attributes)
            button.setAttributedTitle(attributedTitle, for: .normal)
            //button.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
            s.addArrangedSubview(button)
            print("Added button: \(move.name)")
        }
        return s
    }


@objc func longPress(gesture: UILongPressGestureRecognizer) {

    if gesture.state == .began {
        
        let item = gesture.location(in: collectionView.collectionView)
        
        // Get the cell at indexPath (the one that was selected)
        if let indexPath = collectionView.collectionView.indexPathForItem(at: item) {
            let cell = collectionView.collectionView.cellForItem(at: indexPath) as! ComboCell
                
            // If the combo isn't already favorited
            if cell.combo.favorite == false {
                    
                // Haptic feedback
                let generator = UINotificationFeedbackGenerator()
                generator.notificationOccurred(.warning)
                print("Long Pressed")
                
                // Alert
                showAlert(forCombo: cell.combo, alreadyFavorite: false)
                } else {
                    showAlert(forCombo: cell.combo, alreadyFavorite: true)
                }
            } else {
                print("Couldn't find indexPath")
            }
        }
    
    }
    
    
    /// Give options when user long presses a combo cell
    func showAlert(forCombo combo: Combo, alreadyFavorite: Bool) {
        // Ask if they want to delete or remove from favorites and respond accordingly
        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        
        let fav = UIAlertAction(title: "Favorite", style: .default) { (action: UIAlertAction) in
            // Add to Favorites
            self.coordinator?.dataManager?.toggleFavorite(combo, true)
            // Add Star Badge
            var currentSnapshot = self.dataSource.snapshot()
            currentSnapshot.reloadItems([combo])
            self.dataSource.apply(currentSnapshot)
        }
        let unfav = UIAlertAction(title: "Unfavorite", style: .default) { (action: UIAlertAction) in
            // Remove from Favorites
            self.coordinator?.dataManager?.toggleFavorite(combo, false)
            // Remove Star Badge
            var currentSnapshot = self.dataSource.snapshot()
            currentSnapshot.reloadItems([combo])
            self.dataSource.apply(currentSnapshot)
        }
        let del = UIAlertAction(title: "Delete Combo", style: .cancel) { (action: UIAlertAction) in
            self.showDeleteAlert(combo)
        }
            
        // Add actions based on if the combo is favorited or not
        if alreadyFavorite {
            alert.addAction(unfav)
        } else {
            alert.addAction(fav)
        }
        alert.addAction(del)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func showDeleteAlert(_ combo: Combo) {
        let alert = UIAlertController(title: "Are you sure?", message: "Once you delete this combo it cannot be undone", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Delete", style: .default) { (action: UIAlertAction) in
            // Remove Combo from Library
            self.coordinator?.dataManager?.removeCombo(combo)
            // Remove from CollectionView/Data Source
            var currentSnapshot = self.dataSource.snapshot()
            currentSnapshot.deleteItems([combo])
            self.dataSource.apply(currentSnapshot)
        }
        
        let no = UIAlertAction(title: "Cancel", style: .default)
        
        alert.addAction(yes)
        alert.addAction(no)
        self.present(alert, animated: true)
    }


}


// MARK: - UICollectionViewDelegate

extension CombosViewController: UICollectionViewDelegate {
    
    /// Clear labels when no longer needed
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? ComboCell else { return }
        for view in cell.stackView.subviews {
            cell.stackView.removeArrangedSubview(view)
        }
    }
    
}
    
// MARK: - Protocol Conformance
    
extension CombosViewController: MenuDelegate {
    
    func didTapMenuItem(indexPath: IndexPath) {
        
        // Filter the combos
        switch indexPath.item {
        case 0:
            applySnapshot(hoopCount: 1)
        case 1:
            applySnapshot(hoopCount: 2)
        case 2:
            applySnapshot(hoopCount: 3)
        case 3:
            applySnapshot(hoopCount: 4)
        case 4:
            applySnapshot(hoopCount: 5)
        default:
            applySnapshot(hoopCount: 0)
        }

    }
    
    
    
//    func didTapMenuItem(indexPath: IndexPath) {
//
//        // Clear any previously filtered combos
//        data.filteredCombos = []
//
//        // Filter the combos
//        switch indexPath.item {
//        case 0:
//            if let combos = findCombos(1) {
//                data.filteredCombos = combos
//            }
//            data.isShowingFavorites = false
//            print("Current Combos:")
//            for combo in data.filteredCombos {
//                print(combo.docID)
//            }
//            collectionView.collectionView.reloadData()
//        case 1:
//            if let combos = findCombos(2) {
//                data.filteredCombos = combos
//            }
//            data.isShowingFavorites = false
//            collectionView.collectionView.reloadData()
//            print("Current Combos:")
//            for combo in data.filteredCombos {
//                print(combo.docID)
//            }
//        case 2:
//            if let combos = findCombos(3) {
//                data.filteredCombos = combos
//            }
//            data.isShowingFavorites = false
//            collectionView.collectionView.reloadData()
//        case 3:
//            if let combos = findCombos(4) {
//                data.filteredCombos = combos
//            }
//            data.isShowingFavorites = false
//            collectionView.collectionView.reloadData()
//        case 4:
//            if let combos = findCombos(5) {
//                data.filteredCombos = combos
//            }
//            data.isShowingFavorites = false
//            collectionView.collectionView.reloadData()
//        default:
//            if let combos = findFavorites() {
//                data.filteredCombos = combos
//            }
//            filterFavorites()
//            data.isShowingFavorites = true
//            collectionView.collectionView.reloadData()
//
//        }
//
//    }
    
}

