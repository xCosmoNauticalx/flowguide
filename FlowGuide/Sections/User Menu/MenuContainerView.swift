//
//  MenuContainerView.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/13/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class MenuContainerView: UIView {
    
// MARK: - Properties
    
    /// Coordinators
    var delegate: UICollectionViewDelegate?
    
    /// Data
    let data = UserMenuData()

    /// Views
    let statsView = UserStatsView()
    var collectionView: UICollectionView!
    
    let hooper: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "lyra")
        return i
    }()
    
    var name: UILabel = {
        let l = UILabel()
        l.text = "User Name"
        l.textAlignment = .center
        l.textColor = .white
        l.font = UIFont(name: "Helvetica", size: 28)
        return l
    }()
    
   
// MARK: - Init
    
    init(delegate: UICollectionViewDelegate) {
        self.delegate = delegate
        super.init(frame: CGRect())
        configureCollectionView()
        setUp()
    }
    

    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }

    
// MARK:- Methods
    
    func setUp() {
        
        self.backgroundColor = .none
        
        addSubview(hooper)
        addSubview(name)
        addSubview(statsView)
        addSubview(collectionView)
        
        
        /// Hooper Image
        hooper.anchor(top: self.topAnchor, leading: nil, bottom: nil, trailing: nil, padding: .init(top: 30, left: 0, bottom: 0, right: 0), size: .init(width: 120, height: 120))
        hooper.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        
        /// User Name
        name.anchor(top: hooper.bottomAnchor, leading: self.leadingAnchor, bottom: nil, trailing: self.trailingAnchor, padding: .init(top: 45, left: 0, bottom: 0, right: 0))
        
        /// Stats View
        statsView.anchor(top: name.bottomAnchor, leading: self.leadingAnchor, bottom: nil, trailing: self.trailingAnchor, padding: .init(top: 25, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 80))
        statsView.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        

        collectionView.anchor(top: statsView.bottomAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor, padding: .init(top: 45, left: 0, bottom: 0, right: 0))
        
    }
    
    func configureCollectionView() {
        collectionView = UICollectionView(frame: CGRect(), collectionViewLayout: createLayout())
        collectionView.delegate = delegate
        collectionView.dataSource = data
        collectionView.collectionViewLayout = createLayout()
            
        collectionView.register(OptionsCell.self, forCellWithReuseIdentifier: "Cell")
        collectionView.backgroundColor = .clear

            

    }
    
    private func createLayout() -> UICollectionViewLayout {
        
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(0.25))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(0.5))
        let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitems: [item])
        group.interItemSpacing = .fixed(30)
        
        let section = NSCollectionLayoutSection(group: group)
        
        let layout = UICollectionViewCompositionalLayout(section: section)
        
        return layout
    }
    
}

