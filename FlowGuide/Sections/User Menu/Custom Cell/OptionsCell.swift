//
//  OptionsCell.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/13/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class OptionsCell: UICollectionViewCell {
    
    var isLogOutButton: Bool = true
    
    var icon: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "medal")
        i.contentMode = .scaleAspectFill
        return i
    }()
    
    var label: UILabel = {
        let l = UILabel()
        l.text = "Some Words"
        l.textAlignment = .center
        l.textColor = .white
        l.font = UIFont(name: "Helvetica", size: 20)
        return l
    }()
    
    var sublabel: UILabel = {
        let l = UILabel()
        l.text = "Active or Inactive?"
        l.textAlignment = .center
        l.textColor = .white
        l.font = UIFont(name: "HelveticaNeue-Thin", size: 14)
        return l
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    
    private func setUp() {
        
        addSubview(icon)
        addSubview(label)
        addSubview(sublabel)
        
    }
    
    
    
}

