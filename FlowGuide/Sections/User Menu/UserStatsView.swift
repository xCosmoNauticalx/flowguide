//
//  UserStatsView.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/13/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class UserStatsView: UIView {
    
    let horizontalBar = WhiteBar()
    let verticalBar = WhiteBar()

    var moveCount: UILabel = {
        let l = UILabel()
        l.textAlignment = .center
        l.textColor = .white
        l.font = UIFont(name: "HelveticaNeue-Bold", size: 25)
        return l
    }()
    
    var comboCount: UILabel = {
        let l = UILabel()
        l.textAlignment = .center
        l.textColor = .white
        l.font = UIFont(name: "HelveticaNeue-Bold", size: 25)
        return l
    }()
    
    let movesLabel: UILabel = {
        let l = UILabel()
        l.text = "Moves Learned"
        l.textAlignment = .center
        l.textColor = .white
        l.alpha = 0.58
        l.font = UIFont(name: "HelveticaNeue-Thin", size: 16)
        return l
    }()
    
    let combosLabel: UILabel = {
        let l = UILabel()
        l.text = "Combos Created"
        l.textAlignment = .center
        l.textColor = .white
        l.alpha = 0.58
        l.font = UIFont(name: "HelveticaNeue-Thin", size: 16)
        return l
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    func setUp() {

        
        addSubview(horizontalBar)
        addSubview(verticalBar)
        addSubview(moveCount)
        addSubview(movesLabel)
        addSubview(comboCount)
        addSubview(combosLabel)
        
        
        horizontalBar.anchor(top: nil, leading: nil, bottom: self.bottomAnchor, trailing: nil, size: .init(width: 0, height: 1))
        horizontalBar.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 28).isActive = true
        horizontalBar.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -24).isActive = true
        horizontalBar.layer.cornerRadius = 10
    
        verticalBar.anchor(top: self.topAnchor, leading: nil, bottom: horizontalBar.topAnchor, trailing: nil, padding: .init(top: 0, left: 0, bottom: -18, right: 0), size: .init(width: 1, height: 0))
        verticalBar.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        verticalBar.layer.cornerRadius = 3

        moveCount.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: nil, trailing: verticalBar.leadingAnchor)
        movesLabel.anchor(top: moveCount.bottomAnchor, leading: self.leadingAnchor, bottom: nil, trailing: verticalBar.leadingAnchor, padding: .init(top: 8, left: 0, bottom: 0, right: 0))
        
        comboCount.anchor(top: self.topAnchor, leading: verticalBar.trailingAnchor, bottom: nil, trailing: self.trailingAnchor)
        combosLabel.anchor(top: comboCount.bottomAnchor, leading: verticalBar.trailingAnchor, bottom: nil, trailing: self.trailingAnchor, padding: .init(top: 8, left: 0, bottom: 0, right: 0))

    }
}

