//
//  MenuData.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 3/31/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class UserMenuData: NSObject, UICollectionViewDataSource, UICollectionViewDelegate {
    
    var user: User!
    
    var userIsEntitled = PurchasesHelper.isUserEntitled()
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell: OptionsCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? OptionsCell else {
            fatalError("Unable to dequeue OptionsCell.")
        }
        
        if indexPath.item == 0 {
            cell.label.text = "Flow Guide Premium"
            
            if userIsEntitled {
                cell.sublabel.text = "Active Member"
            } else {
                cell.sublabel.text = "Upgrade to Premium"
            }
            
            cell.icon.anchor(top: cell.topAnchor, leading: cell.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 0, left: 40, bottom: 0, right: 0), size: .init(width: 35, height: 45))
            cell.label.anchor(top: nil, leading: cell.icon.trailingAnchor, bottom: nil, trailing: nil, padding: .init(top: 0, left: 20, bottom: 0, right: 0))
            cell.label.centerYAnchor.constraint(equalTo: cell.icon.centerYAnchor).isActive = true
            cell.sublabel.anchor(top: cell.label.bottomAnchor, leading: cell.label.leadingAnchor, bottom: nil, trailing: nil)
        } else {
            cell.label.text = "Log Out"
            cell.icon.image = UIImage(named: "logOut")
            cell.icon.anchor(top: cell.topAnchor, leading: cell.leadingAnchor, bottom: nil, trailing: nil, padding: .init(top: 0, left: 40, bottom: 0, right: 0), size: .init(width: 30, height: 35))
            cell.label.anchor(top: nil, leading: cell.icon.trailingAnchor, bottom: nil, trailing: nil, padding: .init(top: 0, left: 25, bottom: 0, right: 0))
            cell.label.centerYAnchor.constraint(equalTo: cell.icon.centerYAnchor).isActive = true
        }
        
        return cell

    }
    
}
