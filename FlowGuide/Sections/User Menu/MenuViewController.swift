//
//  MenuViewController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/13/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit
import FirebaseAuth

private let reuseIdentifier = "Cell"

class MenuViewController: UIViewController {

// MARK: - Properties
    
    /// Coordinators
    weak var slideDelegate: Slideable?
    weak var premium: Premium?
    
    /// Data
    var user: User!
    var offset: CGFloat = 60
        
    /// Views
    var containerView: MenuContainerView!
    
    let background: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "darkBackground")
        i.contentMode = .scaleAspectFill
        return i
    }()

    let closeButton: UIButton = {
        let b = UIButton()
        b.setBackgroundImage(UIImage(named: "x"), for: .normal)
        b.addTarget(self, action: #selector(done), for: .touchUpInside)
        return b
    }()
    
        
// MARK: - Init
    
    override func loadView() {
        super.loadView()
        setUp()
    }
        
    override func viewDidLoad() {
        super.viewDidLoad()

    }
        
        
// MARK: - Handlers
    
    private func setUp() {
        
        
        containerView = MenuContainerView(delegate: self)
        
        /// Configure & Add Subviews
        view.addSubview(background)
        view.addSubview(containerView)
        view.addSubview(closeButton)

        containerView.name.text = user.name
        containerView.statsView.moveCount.text = String(user.movesLearned)
        containerView.statsView.comboCount.text = String(user.combosCreated)
        containerView.data.user = user


        
        /// Background
        background.fillSuperView()
        
        /// Container
        containerView.anchor(top: closeButton.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: nil)
        containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: -offset).isActive = true

        /// Close Button
        closeButton.anchor(top: self.view.safeAreaLayoutGuide.topAnchor, leading: self.view.leadingAnchor, bottom: nil, trailing: nil, padding: UIEdgeInsets(top: 10, left: 20, bottom: 0, right: 0), size: CGSize(width: 15, height: 15))
        
        
    }
    
    func refreshStats(move: Int, combo: Int) {
        user.movesLearned += move
        user.combosCreated += combo
        containerView.statsView.moveCount.text = String(user.movesLearned)
        containerView.statsView.comboCount.text = String(user.combosCreated)
    }
    
        
        
}

extension MenuViewController {
    
    @objc func done() {
        slideDelegate?.handleMenuToggle()
    }
    
}


extension MenuViewController: UICollectionViewDelegate {
    
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.item == 0 {
            premium?.presentPremiumPage()
        } else {
            slideDelegate?.logOut()
        }
    }
        
        
        
}

