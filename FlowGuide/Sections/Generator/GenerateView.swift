//
//  GenerateView.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/14/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class GenerateView: UIView {

// MARK: - Properties
    
    /// Data
    let delegate = GeneratorTextFieldDelegate()
    
    /// Views
    let hoopLabel: UILabel = {
        let l = UILabel()
        l.text = "# of hoops"
        l.font = UIFont(name: "HelveticaNeue", size: 30)
        l.textColor = UIColor.Combo.mid
        l.textAlignment = .center
        return l
    }()
    
    let moveLabel: UILabel = {
        let l = UILabel()
        l.text = "# of moves"
        l.font = UIFont(name: "HelveticaNeue", size: 30)
        l.textColor = UIColor.Combo.mid
        l.textAlignment = .center
        return l
    }()
    
    let hoopCountTextField: UITextField = {
        let t = UITextField()
        t.keyboardAppearance = .dark
        t.returnKeyType = .done
        t.textColor = UIColor.Combo.mid
        t.borderStyle = .roundedRect
        t.backgroundColor = .clear
        t.layer.borderColor = UIColor.Combo.mid.cgColor
        t.layer.borderWidth = 1
        t.layer.cornerRadius = 4
        t.keyboardType = .numberPad
        t.textAlignment = .center
        t.font = UIFont(name: "HelveticaNeue", size: 20)
        return t
    }()
    
    let moveCountTextField: UITextField = {
        let t = UITextField()
        t.keyboardAppearance = .dark
        t.returnKeyType = .done
        t.textColor = UIColor.Combo.mid
        t.borderStyle = .roundedRect
        t.backgroundColor = .clear
        t.layer.borderColor = UIColor.Combo.mid.cgColor
        t.layer.borderWidth = 1
        t.layer.cornerRadius = 4
        t.keyboardType = .numberPad
        t.textAlignment = .center
        t.font = UIFont(name: "HelveticaNeue", size: 20)
        return t
    }()
    
    let hoopStack: UIStackView = {
        let s = UIStackView()
        s.axis = .horizontal
        s.alignment = .center
        s.distribution = .equalSpacing
        s.spacing = 30
        s.translatesAutoresizingMaskIntoConstraints = false
        return s
    }()
    
    
    let moveStack: UIStackView = {
        let s = UIStackView()
        s.axis = .horizontal
        s.alignment = .center
        s.distribution = .equalSpacing
        s.spacing = 30
        s.translatesAutoresizingMaskIntoConstraints = false
        return s
    }()
    
    let fullStack: UIStackView = {
        let s = UIStackView()
        s.axis = .vertical
        s.alignment = .trailing
        s.distribution = .equalSpacing
        s.spacing = 23
        s.translatesAutoresizingMaskIntoConstraints = false
        return s
    }()
    
    
// MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    
// MARK: - Set Up
    
    private func setUp() {
        
        /// Notification Center
        NotificationCenter.default.addObserver(self, selector: #selector(clearField), name: .clearField, object: nil)
        
        /// Set Delegates
        hoopCountTextField.delegate = delegate
        moveCountTextField.delegate = delegate
        
        /// Set tags for text fields
        hoopCountTextField.tag = 1
        moveCountTextField.tag = 2
        
        /// iPad variations
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            hoopLabel.font = UIFont(name: "HelveticaNeue", size: 40)
            moveLabel.font = UIFont(name: "HelveticaNeue", size: 40)
        }
        
        /// Stack Views
        hoopStack.addArrangedSubview(hoopLabel)
        hoopStack.addArrangedSubview(hoopCountTextField)
        
        moveStack.addArrangedSubview(moveLabel)
        moveStack.addArrangedSubview(moveCountTextField)
        
        fullStack.addArrangedSubview(hoopStack)
        fullStack.addArrangedSubview(moveStack)
        
        
        addSubview(fullStack)
        fullStack.fillSuperView()
        
        self.translatesAutoresizingMaskIntoConstraints = false
        
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            
            hoopCountTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
            hoopCountTextField.widthAnchor.constraint(equalToConstant: 47).isActive = true
            hoopCountTextField.font = UIFont(name: "HelveticaNeue", size: 20)
            
            moveCountTextField.heightAnchor.constraint(equalToConstant: 40).isActive = true
            moveCountTextField.widthAnchor.constraint(equalToConstant: 47).isActive = true
            moveCountTextField.font = UIFont(name: "HelveticaNeue", size: 20)
            
        } else {
            
            hoopCountTextField.heightAnchor.constraint(equalToConstant: 35).isActive = true
            hoopCountTextField.widthAnchor.constraint(equalToConstant: 42).isActive = true
            
            moveCountTextField.heightAnchor.constraint(equalToConstant: 35).isActive = true
            moveCountTextField.widthAnchor.constraint(equalToConstant: 42).isActive = true
            
        }
        
    }
    
    /// Clear textField
    @objc func clearField() {
        moveCountTextField.text = nil
    }

}

