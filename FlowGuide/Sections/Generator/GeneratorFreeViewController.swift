//
//  GeneratorFreeViewController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/8/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class GeneratorFreeViewController: UIViewController {
    
// MARK: - Properties
    
    /// Coordinators
    weak var coordinator: GeneratorCoordinator?
    weak var titleDelegate: Titleable?
    
    /// Views
    var premiumView: PremiumView!
    
    let background: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "generatorBackground")
        i.contentMode = .scaleToFill
        return i
    }()
    

// MARK: - Life Cycle
    override func loadView() {
        super.loadView()
        print("Generator Free loadView()")
        setUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        /// Tab Bar Color
        Utilities.styleTabBar(self, color: UIColor.Combo.light)
        
    }
    
    
// MARK: - Set Up
    
    private func setUp() {
        
        guard let premium = coordinator?.premium else { return }
        
        premiumView = PremiumView(color: UIColor.Combo.mid, premium: premium)
        
        view.addSubview(background)
        view.addSubview(premiumView)
        
        background.fillSuperView()
        
        switch UIScreen.main.traitCollection.userInterfaceIdiom {
        case .pad:
            premiumView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 0, left: 150, bottom: 0, right: -150))
        default:
            premiumView.fillSuperView()
        }
        

    }
    
    


}
