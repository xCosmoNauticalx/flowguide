//
//  GeneratorContainerController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/2/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit
import Purchases

class GeneratorContainerController: UIViewController {

    // MARK: - Properties
    
    /// Coordinators
    weak var coordinator: GeneratorCoordinator?
    
    /// Data
    var userIsEntitled = PurchasesHelper.isUserEntitled()
    
    /// Views
    var titleLabel = TitleLabel()
    var titleLabel2 = TitleLabel()
    var backArrow: UIBarButtonItem!

    
    
    // MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        print("Generator Container loadView()")
        setUp()
        //configureCorrectViewController()
        configureHomeViewController()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let frame = CGRect(x: (self.navigationController?.navigationBar.frame.width)!/4, y: -35, width: (self.navigationController?.navigationBar.frame.width)!/2, height: 100)
        changeBackTitleText(to: "Combo", frame: frame)
    }

    
    
    // MARK: - Configure
    
    private func setUp() {
        
        /// Notification Center
        NotificationCenter.default.addObserver(self, selector: #selector(refreshAfterSubscribe), name: .purchaseNotification, object: nil)
        
        /// Make Navigation Bar Transparent
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        
        /// Format Title
        let firstFrame = CGRect(x: (self.navigationController?.navigationBar.frame.width)!/4, y: -35, width: (self.navigationController?.navigationBar.frame.width)!/2, height: 100)
        titleLabel.frame = firstFrame
        titleLabel.text = "Combo"
        titleLabel.textColor = UIColor.background
        titleLabel.font = UIFont(name: "Helvetica", size: 38)
        titleLabel.layer.shadowColor = UIColor.background.cgColor
        titleLabel.layer.shadowRadius = 1.5
        titleLabel.layer.shadowOpacity = 0.25
        titleLabel.layer.shadowOffset = CGSize(width: 3, height: 3)
        
        let secondFrame = CGRect(x: (self.navigationController?.navigationBar.frame.width)!/4, y: 5, width: (self.navigationController?.navigationBar.frame.width)!/2, height: 100)
        titleLabel2.frame = secondFrame
        titleLabel2.text = "Generator"
        titleLabel2.textColor = UIColor.background
        titleLabel2.font = UIFont(name: "Helvetica", size: 38)
        titleLabel2.layer.shadowColor = UIColor.background.cgColor
        titleLabel2.layer.shadowRadius = 1.5
        titleLabel2.layer.shadowOpacity = 0.25
        titleLabel2.layer.shadowOffset = CGSize(width: 3, height: 3)
        
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            titleLabel.font = UIFont(name: "HelveticaNeue", size: 49)
            titleLabel2.font = UIFont(name: "HelveticaNeue", size: 49)
        }

        self.navigationController?.navigationBar.addSubview(titleLabel)
        self.navigationController?.navigationBar.addSubview(titleLabel2)
        
        self.navigationController?.navigationBar.tintColor = UIColor.background
    

    }
    
    
    func configureCorrectViewController() {
        // Route the view depending if we have a premium user or not
        if userIsEntitled {
            configureHomeViewController()
        } else {
            configureFreeViewController()
        }
    }
    
    
    
    func configureHomeViewController() {
        let homeController = GeneratorHomeViewController()
        homeController.titleDelegate = self
        homeController.coordinator = coordinator

        view.addSubview(homeController.view)
        // Add the navigationController containing the HomeController as a child to the ContainerController
        addChild(homeController)
        // Home controller didMove to the parent container of self, which is the ContainerController
        homeController.didMove(toParent: self)
    }
        

    
    
    func configureFreeViewController() {
        let homeController = GeneratorFreeViewController()
        homeController.titleDelegate = self
        homeController.coordinator = coordinator
        
        view.addSubview(homeController.view)
        // Add the navigationController containing the HomeController as a child to the ContainerController
        addChild(homeController)
        // Home controller didMove to the parent container of self, which is the ContainerController
        homeController.didMove(toParent: self)
    }
    
    
    
    @objc func refreshAfterSubscribe(_ notification: Notification) {
        configureHomeViewController()
    }
    
    
}


// MARK: - Protocol Conformance

extension GeneratorContainerController: Titleable {
    

    func changeTitleText(forTitle title: String) {
        
        // Change attributes for first label
        titleLabel.text = title
        titleLabel.frame = CGRect(x: (self.navigationController?.navigationBar.frame.width)!/6, y: -30, width: (self.navigationController?.navigationBar.frame.width)!/6 * 4, height: 100)
        titleLabel.font = UIFont(name: "Helvetica", size: 39)
        titleLabel.layer.shadowColor = UIColor.black.cgColor
        titleLabel.layer.shadowRadius = 3.0
        titleLabel.layer.shadowOpacity = 0.5
        titleLabel.layer.shadowOffset = CGSize(width: 4, height: 4)
        titleLabel.layer.masksToBounds = false
        
        

    }
    
    
    
}
