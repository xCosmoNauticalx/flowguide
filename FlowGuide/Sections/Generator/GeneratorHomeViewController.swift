//
//  GgeneratorHomeViewController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/13/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import UIKit

class GeneratorHomeViewController: UIViewController, Layout {
    
// MARK: - Properties
    
    /// Coordinators
    weak var coordinator: GeneratorCoordinator?
    var titleDelegate: Titleable?
    
    /// Constraints
    var compactConstraints: [NSLayoutConstraint] = []
    var regularConstraints: [NSLayoutConstraint] = []
    var sharedConstraints: [NSLayoutConstraint] = []

    /// Views
    let background: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "generatorBackground")
        i.contentMode = .scaleToFill
        return i
    }()
    
    let generateButton: UIButton = {
        let b = UIButton()
        b.setBackgroundImage(UIImage(named: "Generate!"), for: .normal)
        b.addTarget(self, action: #selector(generate), for: .touchUpInside)
        return b
    }()
    
    var generateView = GenerateView()
    
    

// MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        setUp()
        setUpConstraints()
        
        NSLayoutConstraint.activate(sharedConstraints)
        layoutTrait(traitCollection: UIScreen.main.traitCollection)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        /// Tab Bar Color
        Utilities.styleTabBar(self, color: UIColor.Combo.light)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        generateButton.subviews.first?.contentMode = .scaleAspectFit
    }
    
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        layoutTrait(traitCollection: traitCollection)
    }

    
// MARK: - Set Up
    
    private func setUp() {
        
        /// Add Views
        view.addSubview(background)
        view.addSubview(generateView)
        view.addSubview(generateButton)
        
    }
    
    
// MARK: - Layout Constraints
    
    private func setUpConstraints() {
        
        /// Shared
        // Background
        sharedConstraints.append(contentsOf: Utilities.fillSuperView(background))
        // Generate View
        sharedConstraints.append(contentsOf: [generateView.centerXAnchor.constraint(equalTo: view.centerXAnchor), generateView.centerYAnchor.constraint(equalTo: view.centerYAnchor)])
        // Generate Button
        sharedConstraints.append(generateButton.centerXAnchor.constraint(equalTo: view.centerXAnchor))
        
        /// Regular
        // Generate Button
        regularConstraints.append(contentsOf: Utilities.anchor(generateButton, top: nil, leading: nil, bottom: view.bottomAnchor, trailing: nil, padding: .init(top: 0, left: 0, bottom: -175, right: 0), size: .init(width: 390, height: 90)))
        
        /// Compact
        // Generate Button
        compactConstraints.append(contentsOf: Utilities.anchor(generateButton, top: nil, leading: nil, bottom: view.bottomAnchor, trailing: nil, padding: .init(top: 0, left: 0, bottom: -140, right: 0), size: .init(width: 290, height: 80)))
        
    }
    
    
}



// MARK: - Button Methods

extension GeneratorHomeViewController {
    
    /// Allows user to tap background to dismiss keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    /// Allows user to hit Done button to dismiss keyboard
    @objc func done() {
        self.view.endEditing(true)
    }
    
    /// Generates Combos
    @objc func generate(sender: UIButton!) {
        
        print("Generate!")
        
        self.view.endEditing(true)
        
        // Remove user's input
        let hoopCountInput = generateView.hoopCountTextField.text ?? ""
        let moveCountInput = generateView.moveCountTextField.text ?? ""
        
        // If either field is empty, do nothing
        if hoopCountInput.isEmpty {
            return
        }
        if moveCountInput.isEmpty {
            return
        }
        
        // Store user's input to pass to next VC
        let hoopInt = (hoopCountInput as NSString).integerValue
        let moveInt = (moveCountInput as NSString).integerValue
        
        // Generate combo with stored values and push next VC
        coordinator?.generateCombo(hoopCount: hoopInt, moveCount: moveInt, titleDelegate: titleDelegate!)

    }
}


