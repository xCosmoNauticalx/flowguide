//
//  GeneratorResultController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/27/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class GeneratorResultController: UICollectionViewController {
    
// MARK: - Properties
    
    let layout = CenterVerticallyLayout()
    
// MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        setUp()
    }

    
    
// MARK: - Set Up
    
    private func setUp() {
        
        switch UIScreen.main.traitCollection.userInterfaceIdiom {
        case .pad:
            collectionView.collectionViewLayout = createPadLayout()
        default:
            collectionView.collectionViewLayout = createPhoneLayout()

        }

        /// Layout
        //collectionView.collectionViewLayout = layout
        //layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
        layout.headerReferenceSize = CGSize(width: 0, height: 20)
        layout.footerReferenceSize = CGSize(width: 0, height: 10)
        
        /// CollectionView Set Up
        collectionView.backgroundColor = .clear
        collectionView.register(MoveCell.self, forCellWithReuseIdentifier: "Cell")
        collectionView.register(GeneratorErrorCell.self, forCellWithReuseIdentifier: "ErrorCell")
        collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: "Header")
        collectionView.register(UICollectionReusableView.self, forSupplementaryViewOfKind: UICollectionView.elementKindSectionFooter, withReuseIdentifier: "Footer")

    }
    
    
//    func centerItemsInCollectionView(cellHeight: Double, numberOfItems: Double, lineSpaceBetweenCell: Double) -> UIEdgeInsets {
//        let totalHeight = cellHeight * numberOfItems
//        let totalSpacingHeight = lineSpaceBetweenCell * (numberOfItems - 1)
//        let topInset = (collectionView.frame.height - CGFloat(totalHeight + totalSpacingHeight)) / 2
//        let bottomInset = topInset
//        return UIEdgeInsets(top: topInset, left: 0, bottom: bottomInset, right: 0)
//    }
    
    
    private func createPhoneLayout() -> UICollectionViewLayout {
        
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(45))
        let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitems: [item])
        
        let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(20.0))
        let header = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)
        
        let footerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(10.0))
        let footer = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: footerSize, elementKind: UICollectionView.elementKindSectionFooter, alignment: .bottom)
        
        let section = NSCollectionLayoutSection(group: group)
        section.boundarySupplementaryItems = [header, footer]
        section.interGroupSpacing = 10
        
        let layout = UICollectionViewCompositionalLayout(section: section)
        
        return layout
    }
    
    
    private func createPadLayout() -> UICollectionViewLayout {
        
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(60))
        let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitems: [item])
        
        let headerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(20.0))
        let header = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: headerSize, elementKind: UICollectionView.elementKindSectionHeader, alignment: .top)
        
        let footerSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(30.0))
        let footer = NSCollectionLayoutBoundarySupplementaryItem(layoutSize: footerSize, elementKind: UICollectionView.elementKindSectionFooter, alignment: .bottom)
        
        let section = NSCollectionLayoutSection(group: group)
        section.boundarySupplementaryItems = [header, footer]
        section.interGroupSpacing = 20
        
        let layout = UICollectionViewCompositionalLayout(section: section)
        
        return layout
    }
    

}
