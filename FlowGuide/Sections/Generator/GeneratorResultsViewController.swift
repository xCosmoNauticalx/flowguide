//
//  GgeneratorResultsViewController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/14/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit


class GeneratorResultsViewController: UIViewController, UICollectionViewDelegate, Layout {
    
// MARK: - Properties
    
    /// Coordinators
    weak var coordinator: GeneratorCoordinator?
    var titleDelegate: Titleable?
    
    /// Constraints
    var compactConstraints: [NSLayoutConstraint] = []
    var regularConstraints: [NSLayoutConstraint] = []
    var sharedConstraints: [NSLayoutConstraint] = []
    
    /// Data
    let data = GeneratorData()
    var docID: String!
    
    /// Views
    let background: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "generatorBackground")
        i.contentMode = .scaleToFill
        return i
    }()
    
    let generateButton: UIButton = {
        let b = UIButton()
        b.setBackgroundImage(UIImage(named: "Generate!"), for: .normal)
        b.addTarget(self, action: #selector(generate), for: .touchUpInside)
        return b
    }()
    
    var saveButton: UIButton = {
        let b = UIButton()
        b.setBackgroundImage(UIImage(named: "saveCombo"), for: .normal)
        b.setBackgroundImage(UIImage(named: "saveComboSelected"), for: .selected)
        b.addTarget(self, action: #selector(saveCombo), for: .touchUpInside)
        return b
    }()
    
    let collectionView = GeneratorResultController(collectionViewLayout: UICollectionViewFlowLayout())
    let container = UIView()
    let gradientView = CAGradientLayer()

    
// MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        setUp()
        setUpConstraints()
        
        NSLayoutConstraint.activate(sharedConstraints)
        layoutTrait(traitCollection: UIScreen.main.traitCollection)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {

        let frame = CGRect(x: (self.navigationController?.navigationBar.frame.width)!/4, y: -35, width: (self.navigationController?.navigationBar.frame.width)!/2, height: 100)
        titleDelegate?.changeBackTitleText(to: "Combo", frame: frame)
        
        /// Tab Bar
        Utilities.styleTabBar(self, color: UIColor.Combo.light)
        
        /// Nav Tint Color
        self.navigationController?.navigationBar.tintColor = .background

    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        gradientView.frame = container.bounds
    }
    
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        layoutTrait(traitCollection: traitCollection)
    }
    
    
    
// MARK: - Set Up
    
    private func setUp() {
        
        /// Generate Combo
        generateCombo()
        
        /// CollectionView
        collectionView.collectionView.dataSource = data
        collectionView.collectionView.delegate = self
        
        /// Add Views
        view.addSubview(background)
        view.addSubview(container)
        container.addSubview(collectionView.view)
        view.addSubview(generateButton)
        view.addSubview(saveButton)
        
        /// Back Button
        Utilities.styleNavBar(self, color: .background)
        
        /// Layout Butons & Set Content Mode
        generateButton.layoutIfNeeded()
        saveButton.layoutIfNeeded()
        
        generateButton.subviews.first?.contentMode = .scaleAspectFit
        saveButton.subviews.first?.contentMode = .scaleAspectFit
        
        /// CollectionView Gradient
        gradientView.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
        gradientView.locations = [0, 0.1, 0.8, 1]
        container.layer.mask = gradientView
        
    }
    
 // MARK: - Layout Constraints
    
    private func setUpConstraints() {
        
        /// Shared
        // Background
        sharedConstraints.append(contentsOf: Utilities.fillSuperView(background))
        // Collection View
        sharedConstraints.append(contentsOf: Utilities.fillSuperView(collectionView.view))
        // Generate Button
        sharedConstraints.append(generateButton.centerXAnchor.constraint(equalTo: view.centerXAnchor))
        // Save Button
        sharedConstraints.append(saveButton.centerXAnchor.constraint(equalTo: view.centerXAnchor))
        
        /// Regular
        // Generate Button
        regularConstraints.append(contentsOf: Utilities.anchor(generateButton, top: nil, leading: nil, bottom: view.bottomAnchor, trailing: nil, padding: .init(top: 0, left: 0, bottom: -175, right: 0), size: .init(width: 390, height: 90)))
        // Save Button
        regularConstraints.append(contentsOf: Utilities.anchor(saveButton, top: generateButton.bottomAnchor, leading: nil, bottom: nil, trailing: nil, padding: .init(top: 50, left: 0, bottom: 0, right: 0), size: .init(width: 390, height: 90)))
        // Result View
        regularConstraints.append(contentsOf: Utilities.anchor(container, top: view.topAnchor, leading: view.leadingAnchor, bottom: generateButton.topAnchor, trailing: view.trailingAnchor, padding: .init(top: 280, left: 150, bottom: -50, right: -150)))
        
        /// Compact
        // Generate Button
        compactConstraints.append(contentsOf: Utilities.anchor(generateButton, top: nil, leading: nil, bottom: view.bottomAnchor, trailing: nil, padding: .init(top: 0, left: 0, bottom: -140, right: 0), size: .init(width: 290, height: 80)))
        // Save Button
        compactConstraints.append(contentsOf: Utilities.anchor(saveButton, top: generateButton.bottomAnchor, leading: nil, bottom: nil, trailing: nil, padding: .init(top: 20, left: 0, bottom: 0, right: 0), size: .init(width: 290, height: 80)))
        // Result View
        compactConstraints.append(contentsOf: Utilities.anchor(container, top: view.topAnchor, leading: view.leadingAnchor, bottom: generateButton.topAnchor, trailing: view.trailingAnchor, padding: .init(top: 200, left: 50, bottom: -20, right: -50)))
        
    }
    
    
    

// MARK: - Collection View Delegate
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let docID = data.tempCombo.moves[indexPath.item].docID
        coordinator?.moveDetail(docID: docID, titleDelegate: titleDelegate!)
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell: MoveCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? MoveCell else {
            fatalError("Unable to dequeue MoveCell.")
        }
        
        cell.label.text = nil
    }
    
}

// MARK: - Methods

extension GeneratorResultsViewController {
    
    /// Button Functions
    @objc func generate(sender: UIButton!) {
        generateCombo()
        collectionView.collectionView.reloadData()
    }
    
    @objc func saveCombo(sender: UIButton!) {
        if sender.isSelected == false {
            coordinator?.dataManager?.addCombo(data.tempCombo)
            FirestoreService.addCombo(data.tempCombo)
            coordinator?.updateComboIncrement(by: 1)
            sender.isSelected = true
        } else {
            coordinator?.dataManager?.removeCombo(data.tempCombo)
            FirestoreService.removeCombo(data.tempCombo)
            coordinator?.updateComboIncrement(by: -1)
            sender.isSelected = false
        }

    }
    
    /// Randomly pick user's desired # of moves
    func generateCombo() {
        
        if data.moveCount <= data.moves.count {
            let chosenMoves = data.moves[randomPick: data.moveCount]
            let docID = UUID().uuidString
            let tempCombo = TempCombo(hoopCount: data.hoopCount, docID: docID, moves: chosenMoves)
            data.tempCombo = tempCombo
            print("UUID: \(docID)")
        } else {
            print("Could not randomly pick moves")
        }
        
        saveButton.isSelected = false
        
    }
    
    
}




