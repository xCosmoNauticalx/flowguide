//
//  GeneratorTextFieldDelegate.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/2/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class GeneratorTextFieldDelegate: NSObject, UITextFieldDelegate {
    
    /// Make sure user can't input invalid entries
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            
        print("shouldChangeCharactersInRange")
        // Get text
        guard let text = textField.text else {
            return true
        }
        // Put text into new string
        let newStr = (text as NSString).replacingCharacters(in: range, with: string)
        // Turn new string into int value
        guard let intValue = Int(newStr) else {
            return true
        }
            
        var min = Int()
        var max = Int()
        print("My Tag \(textField.tag)")
            
        switch textField.tag {
        case 1:
            print("Case 1: Hoop Count \(range)")
            min = 1
            max = 3
        case 2:
            print("Case 2: Move Count \(range)")
            min = 2
            max = 9
        default :
            print("Didn't detect any field")
        }
        
        return intValue >= min && intValue <= max
    
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }

}
