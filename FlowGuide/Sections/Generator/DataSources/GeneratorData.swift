//
//  GeneratorData.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/19/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class GeneratorData: NSObject, UICollectionViewDataSource {

    var moveCount = Int()
    var hoopCount = Int()
    var moves = [LearnedMove]()
    var tempCombo = TempCombo()
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // Section 1 is for moves
        // Section 2 is for error label
        return 2
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if section == 0 { return tempCombo.moves.count}
            
        else if section == 1 {
            if tempCombo.moves.count < 1 { return 1 }
        } else {
            return 0
        }

        return 0
    }
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        guard let cell: MoveCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? MoveCell else {
            fatalError("Unable to dequeue MoveCell.")
        }
        
        guard let errorCell: GeneratorErrorCell = collectionView.dequeueReusableCell(withReuseIdentifier: "ErrorCell", for: indexPath) as? GeneratorErrorCell else {
            fatalError("Unable to dequeue GeneratorErrorCell.")
        }

        if indexPath.section == 0 {
            let move = tempCombo.moves[indexPath.item]
            cell.label.text = move.name
            cell.label.textColor = UIColor.Combo.mid
            
            if UIScreen.main.traitCollection.userInterfaceIdiom == .phone {
               cell.label.font = UIFont(name: "Helvetica", size: 22)
            }
        
            return cell
            
        } else {
            return errorCell
        }
        
    }
    
    
    @objc func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        
        switch kind {

        case UICollectionView.elementKindSectionHeader:

            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Header", for: indexPath)

            headerView.backgroundColor = UIColor.clear
            return headerView

        case UICollectionView.elementKindSectionFooter:
            let footerView = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: "Footer", for: indexPath)

            footerView.backgroundColor = UIColor.clear
            return footerView

        default:

            assert(false, "Unexpected element kind")
        }
    }
    
}
