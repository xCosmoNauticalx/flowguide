//
//  GeneratorErrorCell.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/7/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class GeneratorErrorCell: UICollectionViewCell {
    
    let label: UILabel = {
        let l = UILabel()
        l.font = UIFont(name: "HelveticaNeue-Bold", size: 38)
        l.text = "Oops!"
        l.textColor = .red
        l.textAlignment = .center
        return l
    }()
    
    let subLabel: UILabel = {
        let l = UILabel()
        l.font = UIFont(name: "HelveticaNeue-Thin", size: 19)
        l.text = "You haven't learned enough moves. \nTry learning some new moves or choose a smaller move count."
        l.textColor = .white
        l.textAlignment = .center
        l.numberOfLines = 0
        l.lineBreakMode = .byWordWrapping
        return l
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        addSubview(label)
        addSubview(subLabel)
        
        label.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: subLabel.topAnchor, trailing: self.trailingAnchor)
        subLabel.anchor(top: label.bottomAnchor, leading: self.leadingAnchor, bottom: nil, trailing: self.trailingAnchor)
        
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
    }
    
    
}
