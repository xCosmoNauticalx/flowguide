//
//  HomeViewController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/2/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit


class HomeViewController: UIViewController, Layout {
    
// MARK: - Properties
    
    /// Coordinators
    weak var coordinator: LoginCoordinator?
    
    /// Constraints
    var compactConstraints: [NSLayoutConstraint] = []
    var regularConstraints: [NSLayoutConstraint] = []
    var sharedConstraints: [NSLayoutConstraint] = []
    
    /// Views
    let background: UIImageView = {
       let i = UIImageView()
        i.image = UIImage(named: "darkBackground")
        i.contentMode = .scaleAspectFill
        return i
    }()
    
    let hooper: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "gradientHooperLogo")
        i.contentMode = .scaleAspectFit
        i.layer.shadowColor = UIColor.black.cgColor
        i.layer.shadowRadius = 3.0
        i.layer.shadowOpacity = 0.5
        i.layer.shadowOffset = CGSize(width: 4, height: 4)
        i.layer.masksToBounds = false
        return i
    }()
    
    var signUpButton: UIButton = {
        let b = UIButton()
        let title = "Sign Up"
        let attributes: [NSAttributedString.Key: Any] = [
            .font: UIFont(name: "HelveticaNeue", size: 20)!,
            .foregroundColor: UIColor.white]
        let attributedTitle = NSAttributedString(string: title, attributes: attributes)
        b.setAttributedTitle(attributedTitle, for: .normal)
        b.addTarget(self, action: #selector(signUpTapped), for: .touchUpInside)
        return b
    }()
    
    var loginButton: UIButton = {
        let b = UIButton()
        let title = "Log In"
        let attributes: [NSAttributedString.Key: Any] = [
            .font: UIFont(name: "HelveticaNeue", size: 20)!,
            .foregroundColor: UIColor.white]
        let attributedTitle = NSAttributedString(string: title, attributes: attributes)
        b.setAttributedTitle(attributedTitle, for: .normal)
        b.addTarget(self, action: #selector(loginTapped), for: .touchUpInside)
        return b
    }()
    
// MARK: - Life Cycle
    override func loadView() {
        super.loadView()
        setUp()
        setUpConstraints()
        
        NSLayoutConstraint.activate(sharedConstraints)
        layoutTrait(traitCollection: UIScreen.main.traitCollection)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        signUpButton.gradientBorder([UIColor.Archive.light.cgColor, UIColor.Progress.dark.cgColor, UIColor.Progress.light.cgColor])
        loginButton.gradientBorder([UIColor.Archive.light.cgColor, UIColor.Progress.dark.cgColor, UIColor.Progress.light.cgColor])
    }
    

    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        layoutTrait(traitCollection: traitCollection)
    }
    
    
// MARK: - Methods
    private func setUp() {
        
        /// Add Views
        view.addSubview(background)
        view.addSubview(signUpButton)
        view.addSubview(loginButton)
        view.addSubview(hooper)
        
    }
    
    
// MARK: - Layout Constraints
    
    func setUpConstraints() {
        
        hooper.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: signUpButton.topAnchor, trailing: view.trailingAnchor, padding: .init(top: 0, left: 0, bottom: -50, right: 0))
        
        /// Shared
        sharedConstraints.append(contentsOf: Utilities.fillSuperView(background))
        sharedConstraints.append(contentsOf: Utilities.anchor(loginButton, top: nil, leading: nil, bottom: view.bottomAnchor, trailing: nil, padding: .init(top: 0, left: 0, bottom: -40, right: 0), size: .init(width: 0, height: 50)))
        sharedConstraints.append(contentsOf: Utilities.anchor(signUpButton, top: nil, leading: loginButton.leadingAnchor, bottom: loginButton.topAnchor, trailing: loginButton.trailingAnchor, padding: .init(top: 0, left: 0, bottom: -20, right: 0), size: .init(width: 0, height: 50)))
        
        /// Regular
        regularConstraints.append(contentsOf: Utilities.anchor(loginButton, top: nil, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 0, left: 100, bottom: 0, right: -100)))
        
        /// Compact
        compactConstraints.append(contentsOf: Utilities.anchor(loginButton, top: nil, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 0, left: 40, bottom: 0, right: -40)))
    }
    
    
    
}


// MARK: - Extensions

extension HomeViewController {
    
    @objc func signUpTapped(sender: UIButton!) {
        coordinator?.signUp()
    }
    
    @objc func loginTapped(sender: UIButton!) {
        coordinator?.login()
    }
    
}
