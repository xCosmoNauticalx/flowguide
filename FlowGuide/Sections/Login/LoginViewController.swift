//
//  LoginnViewController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/10/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class LoginViewController: UIViewController, Layout, UITextFieldDelegate {
    
// MARK: - Properties
    
    /// Coordinators
    weak var coordinator: LoginCoordinator?
    
    /// Constraints
    var compactConstraints: [NSLayoutConstraint] = []
    var regularConstraints: [NSLayoutConstraint] = []
    var sharedConstraints: [NSLayoutConstraint] = []

        /// Views
    let background: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "darkBackground")
        i.contentMode = .scaleAspectFill
        return i
    }()
    
    let header: UILabel = {
        let l = UILabel()
        l.text = "Welcome back!"
        l.textColor = .white
        l.font = UIFont(name: "HelveticaNeue", size: 50)
        l.textAlignment = .center
        l.adjustsFontSizeToFitWidth = true
        l.minimumScaleFactor = 0.7
        l.layer.shadowColor = UIColor.black.cgColor
        l.layer.shadowRadius = 3.0
        l.layer.shadowOpacity = 0.5
        l.layer.shadowOffset = CGSize(width: 4, height: 4)
        l.layer.masksToBounds = false
        return l
    }()
    
    var emailTextField: UITextField = {
        let t = UITextField()
        t.spellCheckingType = .no
        t.autocapitalizationType = .none
        t.clearButtonMode = .whileEditing
        t.keyboardAppearance = .dark
        t.returnKeyType = .done
        t.textColor = .white
        t.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        return t
    }()
    
    var passwordTextField: UITextField = {
        let t = UITextField()
        t.spellCheckingType = .no
        t.autocapitalizationType = .none
        t.clearButtonMode = .whileEditing
        t.keyboardAppearance = .dark
        t.returnKeyType = .done
        t.textColor = .white
        t.clearsOnBeginEditing = true
        t.isSecureTextEntry = true
        t.autocorrectionType = .no
        t.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        return t
    }()
    
    var loginButton: UIButton = {
        let b = UIButton()
        b.setTitle("Log In", for: .normal)
        b.setTitleColor(.white, for: .normal)
        b.addTarget(self, action: #selector(loginTapped), for: .touchUpInside)
        return b
    }()
    
    var errorLabel: UILabel = {
        let l = UILabel()
        l.font = UIFont(name: "HelveticaNueue", size: 15)
        l.textColor = .red
        l.textAlignment = .center
        l.numberOfLines = 0
        l.lineBreakMode = .byWordWrapping
        l.alpha = 0
        return l
    }()
    
    let forgotButton: UIButton = {
        let b = UIButton()
        b.setTitle("Forgot Password?", for: .normal)
        b.setTitleColor(.white, for: .normal)
        b.backgroundColor = .clear
        b.addTarget(self, action: #selector(forgotTapped), for: .touchUpInside)
        let title = "Forgot Password?"
        let attributes: [NSAttributedString.Key: Any] = [
            .font: UIFont(name: "HelveticaNeue", size: 15)!,
            .foregroundColor: UIColor.white]
        let attributedTitle = NSAttributedString(string: title, attributes: attributes)
        b.setAttributedTitle(attributedTitle, for: .normal)
        return b
    }()
    
    

// MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        setUp()
        setUpConstraints()
        
        NSLayoutConstraint.activate(sharedConstraints)
        layoutTrait(traitCollection: UIScreen.main.traitCollection)
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(true)
        clearFields()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        emailTextField.addBottomBorder([UIColor.Library.mid.cgColor, UIColor.Combo.mid.cgColor])
        passwordTextField.addBottomBorder([UIColor.Combo.mid.cgColor, UIColor.Progress.mid.cgColor])
        loginButton.styleHollowButton(color: UIColor.white.cgColor)
    }
    

// MARK: - Overrides
    
    /// Change layout constraints when device rotates
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        layoutTrait(traitCollection: traitCollection)
    }
    
    /// Allows user to tap background to dismiss keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}

// MARK: - Set Up

extension LoginViewController {
    
    private func setUp() {
        
        /// Back Button
        Utilities.styleNavBar(self, color: .white)
        
        /// Add views
        view.addSubview(background)
        view.addSubview(header)
        view.addSubview(emailTextField)
        view.addSubview(passwordTextField)
        view.addSubview(loginButton)
        view.addSubview(forgotButton)
        view.addSubview(errorLabel)
        
        /// Set Delegates
        emailTextField.delegate = self
        passwordTextField.delegate = self

    }
    
    
// MARK: - Layout Constraints
    
    func setUpConstraints() {
        
        /// Shared
        // Background
        sharedConstraints.append(contentsOf: Utilities.fillSuperView(background))
        // Header
        sharedConstraints.append(contentsOf: Utilities.anchor(header, top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor))
        // Email Text Field
        sharedConstraints.append(contentsOf: Utilities.anchorHeight(emailTextField, top: header.bottomAnchor, bottom: nil, topPadding: 30, size: .init(width: 0, height: 40)))
        // Password Text Field
        sharedConstraints.append(contentsOf: Utilities.anchor(passwordTextField, top: emailTextField.bottomAnchor, leading: emailTextField.leadingAnchor, bottom: nil, trailing: emailTextField.trailingAnchor, padding: .init(top: 20, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 40)))
        // Login Button
        sharedConstraints.append(contentsOf: Utilities.anchor(loginButton, top: passwordTextField.bottomAnchor, leading: emailTextField.leadingAnchor, bottom: nil, trailing: emailTextField.trailingAnchor, padding: .init(top: 35, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 40)))
        // Forgot Button
        sharedConstraints.append(contentsOf: Utilities.anchor(forgotButton, top: loginButton.bottomAnchor, leading: emailTextField.leadingAnchor, bottom: nil, trailing: emailTextField.trailingAnchor, padding: .init(top: 15, left: 0, bottom: 0, right: 0)))
        // Error Label
        sharedConstraints.append(contentsOf: Utilities.anchor(errorLabel, top: forgotButton.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 15, left: 0, bottom: 0, right: 0)))
        
        /// Regular
        // Email Text Field
        regularConstraints.append(contentsOf: Utilities.anchorWidth(emailTextField, leading: view.leadingAnchor, trailing: view.trailingAnchor, leftPadding: 150, rightPadding: -150))
        
        /// Compact
        // Email Text Field
        compactConstraints.append(contentsOf: Utilities.anchorWidth(emailTextField, leading: view.leadingAnchor, trailing: view.trailingAnchor, leftPadding: 20, rightPadding: -20))
    }
    
    
// MARK: - Methods
    
    func validateFields() -> String? {
    
        // Check all fields are filled in
        if emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == ""// Takes away all white spaces and new lines & check if it's empty
            {
            return "Please fill in all fields"
        }
        return nil
    }
    
    @objc func loginTapped(sender: UIButton!) {
        
        print("Login Tapped")
        
        // Validate fields
        let error = validateFields()
        
        if error != nil {
            
            // There's something wrong with the fields
            errorLabel.text = error
            Utilities.animateErrorLabel(errorLabel)
            
        } else {
            
            // Clean user data
            let email = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            
            // Signing in the user
            Auth.auth().signIn(withEmail: email, password: password) { (result, error) in
                // If error occurs:
                if error != nil {
                    if let errorCode = AuthErrorCode(rawValue: error!._code) {
                        self.errorLabel.text = errorCode.errorMessage  // Used an AuthErrorCode extension to set strings
                        Utilities.animateErrorLabel(self.errorLabel)
                    }

                }
            }
        }
    }
    
    
    @objc func forgotTapped(sender: UIButton!) {
        coordinator?.forgotPassword()
    }
    
    
    func clearFields() {
        emailTextField.text = nil
        passwordTextField.text = nil
    }


// MARK: - TexField Delegate

    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}


