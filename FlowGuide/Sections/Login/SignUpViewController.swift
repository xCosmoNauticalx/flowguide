//
//  SignUpViewController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/2/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class SignUpViewController: UIViewController, Layout {
    
// MARK: - Properties
    
    /// Coordinators
    weak var coordinator: LoginCoordinator?
    weak var dataManager: DataManaging?
    
    /// Constraints
    var compactConstraints: [NSLayoutConstraint] = []
    var regularConstraints: [NSLayoutConstraint] = []
    var sharedConstraints: [NSLayoutConstraint] = []
    
    
    /// Views
    let background: UIImageView = {
       let i = UIImageView()
        i.image = UIImage(named: "darkBackground")
        i.contentMode = .scaleAspectFill
        return i
    }()
    
    let header: UILabel = {
        let l = UILabel()
        l.text = "Welcome to"
        l.textColor = .white
        l.font = UIFont(name: "HelveticaNeue", size: 50)
        l.textAlignment = .center
        l.adjustsFontSizeToFitWidth = true
        l.minimumScaleFactor = 0.7
        l.layer.shadowColor = UIColor.black.cgColor
        l.layer.shadowRadius = 3.0
        l.layer.shadowOpacity = 0.5
        l.layer.shadowOffset = CGSize(width: 4, height: 4)
        l.layer.masksToBounds = false
        return l
    }()
    
    let gradientHeader: GradientLabel = {
        let g = GradientLabel()
        g.text = "Flow Guide"
        g.font = UIFont(name: "HelveticaNeue", size: 50)
        g.gradientColors = [UIColor.Archive.mid.cgColor, UIColor.Progress.dark.cgColor, UIColor.Combo.mid.cgColor, UIColor.Library.mid.cgColor]
        g.textAlignment = .center
        g.adjustsFontSizeToFitWidth = true
        g.minimumScaleFactor = 0.7
        g.layer.shadowColor = UIColor.black.cgColor
        g.layer.shadowRadius = 3.0
        g.layer.shadowOpacity = 0.5
        g.layer.shadowOffset = CGSize(width: 4, height: 4)
        g.layer.masksToBounds = false
        return g
    }()
    
    var firstNameTextField: UITextField = {
        let t = UITextField()
        t.spellCheckingType = .yes
        t.autocapitalizationType = .words
        t.clearButtonMode = .whileEditing
        t.keyboardAppearance = .dark
        t.returnKeyType = .done
        t.textColor = .white
        t.attributedPlaceholder = NSAttributedString(string: "First Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        return t
    }()
    
    var lastNameTextField: UITextField = {
        let t = UITextField()
        t.spellCheckingType = .yes
        t.autocapitalizationType = .words
        t.clearButtonMode = .whileEditing
        t.keyboardAppearance = .dark
        t.returnKeyType = .done
        t.textColor = .white
        t.attributedPlaceholder = NSAttributedString(string: "Last Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        return t
    }()
    
    var emailTextField: UITextField = {
        let t = UITextField()
        t.spellCheckingType = .yes
        t.autocapitalizationType = .none
        t.clearButtonMode = .whileEditing
        t.keyboardAppearance = .dark
        t.returnKeyType = .done
        t.textColor = .white
        t.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        return t
    }()
    
    var passwordTextField: UITextField = {
        let t = UITextField()
        t.spellCheckingType = .no
        t.autocapitalizationType = .none
        t.clearButtonMode = .whileEditing
        t.keyboardAppearance = .dark
        t.returnKeyType = .done
        t.textColor = .white
        t.clearsOnBeginEditing = true
        t.isSecureTextEntry = true
        t.autocorrectionType = .no
        t.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        return t
    }()
    
    var errorLabel: UILabel = {
        let l = UILabel()
        l.text = "Error"
        l.font = UIFont(name: "HelveticaNeue", size: 17)
        l.textColor = .red
        l.textAlignment = .center
        l.numberOfLines = 0
        l.lineBreakMode = .byWordWrapping
        l.alpha = 0
        return l
    }()
    
    var signUpButton: UIButton = {
        let b = UIButton()
        b.setTitle("Sign Up", for: .normal)
        b.addTarget(self, action: #selector(signUpTapped), for: .touchUpInside)
        return b
    }()
    
    
// MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        setUp()
        setUpConstraints()
        
        NSLayoutConstraint.activate(sharedConstraints)
        layoutTrait(traitCollection: UIScreen.main.traitCollection)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        firstNameTextField.addBottomBorder([UIColor.Archive.mid.cgColor, UIColor.Library.mid.cgColor])
        lastNameTextField.addBottomBorder([UIColor.Library.mid.cgColor, UIColor.Combo.mid.cgColor])
        emailTextField.addBottomBorder([UIColor.Combo.mid.cgColor, UIColor.Progress.mid.cgColor])
        passwordTextField.addBottomBorder([UIColor.Progress.mid.cgColor, UIColor.Archive.mid.cgColor])
        signUpButton.styleHollowButton(color: UIColor.white.cgColor)
        
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        clearFields()
    }
    
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        layoutTrait(traitCollection: traitCollection)
    }
    
}

// MARK: - Overrides

extension SignUpViewController {
    
    /// Allows user to tap background to dismiss keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
}


// MARK: - Set Up

extension SignUpViewController {
    
    
    func setUp() {
        
        /// Navigation Bar
        Utilities.styleNavBar(self, color: .white)
        
        /// Add views
        view.addSubview(background)
        view.addSubview(header)
        view.addSubview(gradientHeader)
        view.addSubview(firstNameTextField)
        view.addSubview(lastNameTextField)
        view.addSubview(emailTextField)
        view.addSubview(passwordTextField)
        view.addSubview(signUpButton)
        view.addSubview(errorLabel)
        
        /// Assign delegates
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
        emailTextField.delegate = self
        passwordTextField.delegate = self
        
    }
    

// MARK: - Layout Constraints
    
    func setUpConstraints() {
        
        /// Shared
        // Background
        sharedConstraints.append(contentsOf: Utilities.fillSuperView(background))
        // Header
        sharedConstraints.append(contentsOf: Utilities.anchor(header, top: view.topAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 0, left: 20, bottom: 0, right: -20)))
        // Gradient Header
        sharedConstraints.append(contentsOf: Utilities.anchor(gradientHeader, top: header.bottomAnchor, leading: header.leadingAnchor, bottom: nil, trailing: header.trailingAnchor, padding: .init(top: 5, left: 0, bottom: 0, right: 0)))
        // First Name Text Field
        sharedConstraints.append(contentsOf: Utilities.anchorHeight(firstNameTextField, top: gradientHeader.bottomAnchor, bottom: nil, topPadding: 30, size: .init(width: 0, height: 40)))
        // Last Name Text Field
        sharedConstraints.append(contentsOf: Utilities.anchor(lastNameTextField, top: firstNameTextField.bottomAnchor, leading: firstNameTextField.leadingAnchor, bottom: nil, trailing: firstNameTextField.trailingAnchor, padding: .init(top: 20, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 40)))
        // Email Text Field
        sharedConstraints.append(contentsOf: Utilities.anchor(emailTextField, top: lastNameTextField.bottomAnchor, leading: firstNameTextField.leadingAnchor, bottom: nil, trailing: firstNameTextField.trailingAnchor, padding: .init(top: 20, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 40)))
        // Password Text Field
        sharedConstraints.append(contentsOf: Utilities.anchor(passwordTextField, top: emailTextField.bottomAnchor, leading: firstNameTextField.leadingAnchor, bottom: nil, trailing: firstNameTextField.trailingAnchor, padding: .init(top: 20, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 40)))
        // Sign Up Button
        sharedConstraints.append(contentsOf: Utilities.anchor(signUpButton, top: passwordTextField.bottomAnchor, leading: firstNameTextField.leadingAnchor, bottom: nil, trailing: firstNameTextField.trailingAnchor, padding: .init(top: 35, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 40)))
        // Error Label
        sharedConstraints.append(contentsOf: Utilities.anchor(errorLabel, top: signUpButton.bottomAnchor, leading: firstNameTextField.leadingAnchor, bottom: nil, trailing: firstNameTextField.trailingAnchor, padding: .init(top: 15, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 40)))

        
        /// Regular
        // First Name Text Field
        regularConstraints.append(contentsOf: Utilities.anchorWidth(firstNameTextField, leading: view.leadingAnchor, trailing: view.trailingAnchor, leftPadding: 150, rightPadding: -150))
        
        
        /// Compact
        // First Name Text Field
        compactConstraints.append(contentsOf: Utilities.anchorWidth(firstNameTextField, leading: view.leadingAnchor, trailing: view.trailingAnchor, leftPadding: 20, rightPadding: -20))
        
        
        
    }
    
    
  // MARK: - Methods
    
    /// Check the fields and validate that the data is correct. If everything is correct, return nil. Otherwise, it returns the error message.
    func validateFields() -> String? {
        
        // Check all fields are filled in
        if firstNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||// Takes away all white spaces and new lines & check if it's empty
            lastNameTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" ||
            passwordTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) == "" {
            return "Please fill in all fields"
        }
        
        // Check if email is valid (using regular expressions, look it up)
        
        // Check if password is secure
        let cleanedPassword = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        if Utilities.isPasswordValid(cleanedPassword) == false {
           // Password isn't secure enough
            return "Password must contain at least 8 charachters, one number and one special charachter"
        }
        
        return nil
    }
    
    

    @objc func signUpTapped(sender: UIButton!) {
        
        // Validate fields
        let error = validateFields()
        
        if error != nil {
            
            // There's something wrong with the fields
            errorLabel.text = error
            Utilities.animateErrorLabel(errorLabel)
            
        } else {
            
            // Clean user's data
            let firstName = firstNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let lastName = lastNameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let email  = emailTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            let password = passwordTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
            
            let deviceID = UIDevice.current.identifierForVendor?.uuidString
            print("DeviceID: \(deviceID)")
            
            // Create User in Core Data
            let fullName = firstName + " " + lastName
            
            // Check if another user already owns this device
            if dataManager?.checkIfOwned() == true {
                // Someone does, so make sure they want to proceed
                let title = "This device already belongs to someone"
                let message = "With the free version of Flow Guide, you are only allowed to sign in on one device. Creating an account on this device would mean you are limited to this device only. Would you still like to proceed?"
                let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
                
                // For iPad
                if let popoverPresentationController = alert.popoverPresentationController {
                    popoverPresentationController.sourceView = self.view
                    popoverPresentationController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
                    popoverPresentationController.permittedArrowDirections = []
                }
                
                let no = UIAlertAction(title: "No", style: .default) { (action: UIAlertAction) in
                    // Stop Firestore from creating user
                    self.navigationController?.popViewController(animated: true)
                    return
                }
                let yes = UIAlertAction(title: "Yes", style: .default) { (action: UIAlertAction) in
                    self.createUser(name: fullName, email: email, password: password, deviceID: (deviceID)!)
                }
                alert.addAction(no)
                alert.addAction(yes)
                
                self.present(alert, animated: true, completion: nil)
                
            } else {
                // No one has made an account with this device before, so create one.
                createUser(name: fullName, email: email, password: password, deviceID: deviceID!)
                
            }
        }
        
    }
    
    
    /// Create user in Firebase and Core Data
    func createUser(name: String, email: String, password: String, deviceID: String) {
        // Create account
        Auth.auth().createUser(withEmail: email, password: password) { (result, err) in
            // Check for errors
            if err != nil {
                if let errorCode = AuthErrorCode(rawValue: err!._code) {
                    self.errorLabel.text = errorCode.errorMessage
                    Utilities.animateErrorLabel(self.errorLabel)
                }
            } else {
                // Create User on disk
                self.dataManager?.createUser(name: name, uid: result!.user.uid, primaryDevice: deviceID)
                // Get Firestore Messaging Token
                let dispatch = DispatchGroup()
                FirestoreService.getToken(dispatch: dispatch) {(token) in
                    dispatch.notify(queue: .main, execute: {
                        // Create User document
                        let db = Firestore.firestore()
                        db.collection("Users").document(result!.user.uid).setData(["name":name, "uid":result!.user.uid, "movesLearned":0, "combosCreated":0, "primaryDevice":deviceID, "tokens": [token]]) { (error) in
                            // Check for errors
                            if error != nil {
                                if let errorCode = AuthErrorCode(rawValue: err!._code) {
                                    self.errorLabel.text = errorCode.errorMessage
                                    Utilities.animateErrorLabel(self.errorLabel)
                                }
                            }
                        }
                    })
                }
            }
        }
    }
    
    
    
    func clearFields() {

        firstNameTextField.text = nil
        lastNameTextField.text = nil
        emailTextField.text = nil
        passwordTextField.text = nil
    }

}


// MARK: - TexField Delegate

extension SignUpViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}


