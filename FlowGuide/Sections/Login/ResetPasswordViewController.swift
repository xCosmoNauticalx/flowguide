//
//  ResetPasswordViewController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/7/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase

class ResetPasswordViewController: UIViewController, Layout {
    
// MARK: - Properties
    
    /// Coordinators
    weak var coordinator: LoginCoordinator?
    
    /// Constraints
    var compactConstraints: [NSLayoutConstraint] = []
    var regularConstraints: [NSLayoutConstraint] = []
    var sharedConstraints: [NSLayoutConstraint] = []
    
    /// Views
    let background: UIImageView = {
       let i = UIImageView()
        i.image = UIImage(named: "darkBackground")
        i.contentMode = .scaleAspectFill
        return i
    }()
    
    let submitButton: HollowFilledButton = {
        let b = HollowFilledButton()
        b.setTitle("Submit", for: .normal)
        b.setTitleColor(.white, for: .normal)
        b.backgroundColor = .clear
        b.setTitle("Email Sent!", for: .selected)
        b.setTitleColor(.background, for: .selected)
        b.addTarget(self, action: #selector(resetPassword), for: .touchUpInside)
        b.isUserInteractionEnabled = true
        return b
    }()
    
    let label: UILabel = {
        let l = UILabel()
        l.text = "Forgot your password?"
        l.font = UIFont(name: "HelveticaNeue-Bold", size: 30)
        l.textColor = .white
        l.textAlignment = .left
        l.adjustsFontSizeToFitWidth = true
        l.minimumScaleFactor = 0.7
        return l
    }()
    
    let subLabel: UILabel = {
        let l = UILabel()
        l.text = "We've got you. \nEnter the email address you used to register, and we'll send you a secure link to reset your password"
        l.numberOfLines = 0
        l.textAlignment = .left
        l.font = UIFont(name: "HelveticaNeue", size: 18)
        l.textColor = .white
        l.lineBreakMode = .byWordWrapping
        return l
    }()
    
    var emailTextField: UITextField = {
        let t = UITextField()
        t.spellCheckingType = .no
        t.autocapitalizationType = .none
        t.clearButtonMode = .unlessEditing
        t.keyboardAppearance = .dark
        t.returnKeyType = .done
        t.textColor = .white
        t.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        return t
    }()
    
    let errorLabel: UILabel = {
        let l = UILabel()
        l.alpha = 0
        l.numberOfLines = 0
        l.textAlignment = .center
        l.font = UIFont(name: "HelveticaNeue", size: 18)
        l.textColor = .red
        return l
    }()
    
    

// MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        setUp()
        setUpConstraints()
        
        NSLayoutConstraint.activate(sharedConstraints)
        layoutTrait(traitCollection: UIScreen.main.traitCollection)
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        emailTextField.addBottomBorder([UIColor.Progress.mid.cgColor, UIColor.Archive.mid.cgColor])
    }
    
    
// MARK: - Overrides
    
    /// Change layout constraints when device rotates
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        layoutTrait(traitCollection: traitCollection)
    }
    
    
// MARK: - Set Up
    
    private func setUp() {
        
        /// Back Button
        Utilities.styleNavBar(self, color: .white)
        
        /// Add views & constraints
        view.addSubview(background)
        view.addSubview(label)
        view.addSubview(subLabel)
        view.addSubview(emailTextField)
        view.addSubview(submitButton)
        view.addSubview(errorLabel)
        
        /// Set Delegates
        emailTextField.delegate = self
        
        /// Style views
        submitButton.styleHollowButton(color: UIColor.white.cgColor)
        
        emailTextField.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.white])
        
    }
    
    // MARK: - Layout Constraints
    
    func setUpConstraints() {
        
        /// Shared
        // Background
        sharedConstraints.append(contentsOf: Utilities.fillSuperView(background))
        // Label
        sharedConstraints.append(contentsOf: Utilities.anchorHeight(label, top: view.topAnchor, bottom: nil))
        // Sublabel
        sharedConstraints.append(contentsOf: Utilities.anchor(subLabel, top: label.bottomAnchor, leading: label.leadingAnchor, bottom: nil, trailing: label.trailingAnchor, padding: .init(top: 5, left: 0, bottom: 0, right: 0)))
        // Email Text Field
        sharedConstraints.append(contentsOf: Utilities.anchorHeight(emailTextField, top: subLabel.bottomAnchor, bottom: nil, topPadding: 40, size: .init(width: 0, height: 40)))
        // Submit Button
        sharedConstraints.append(contentsOf: Utilities.anchor(submitButton, top: emailTextField.bottomAnchor, leading: emailTextField.leadingAnchor, bottom: nil, trailing: emailTextField.trailingAnchor, padding: .init(top: 40, left: 0, bottom: 0, right: 0), size: .init(width: 0, height: 40)))
        // Error Label
        sharedConstraints.append(contentsOf: Utilities.anchor(errorLabel, top: submitButton.bottomAnchor, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 30, left: 0, bottom: 0, right: 0)))
        
        /// Regular
        // Label
        regularConstraints.append(contentsOf: Utilities.anchorWidth(label, leading: view.leadingAnchor, trailing: view.trailingAnchor, leftPadding: 150, rightPadding: -170))
        // Email Text Field
        regularConstraints.append(contentsOf: Utilities.anchorWidth(emailTextField, leading: view.leadingAnchor, trailing: view.trailingAnchor, leftPadding: 150, rightPadding: -150))
        
        /// Compact
        // Label
        compactConstraints.append(contentsOf: Utilities.anchorWidth(label, leading: view.leadingAnchor, trailing: view.trailingAnchor, leftPadding: 20, rightPadding: -40))
        // Email Text Field
        compactConstraints.append(contentsOf: Utilities.anchorWidth(emailTextField, leading: view.leadingAnchor, trailing: view.trailingAnchor, leftPadding: 20, rightPadding: -20))
        
    }
    
    
// MARK: - Methods
    
    @objc func test(sender: UIButton!) {
        
        if sender.isSelected == false {
            sender.isSelected = true

        } else {
            sender.isSelected = false

        }
        
    }
    
    @objc func resetPassword(sender: UIButton!) {
        
        if sender.isSelected == false {
            guard let email = emailTextField.text?.trimmingCharacters(in: .whitespacesAndNewlines) else {
                return
                }
            Auth.auth().sendPasswordReset(withEmail: email) { err in
                
                if err != nil {
                    if let errorCode = AuthErrorCode(rawValue: err!._code) {
                        self.errorLabel.text = errorCode.errorMessage  // Used a AuthErrorCode extension to set strings
                        self.errorLabel.alpha = 1
                    }
                } else {
                    sender.isSelected = true
                    sender.isUserInteractionEnabled = false
                    self.view.endEditing(true)
                }
            }
        }

        
    }
    


}

extension ResetPasswordViewController {
    
    /// Allows user to tap background to dismiss keyboard
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        self.view.endEditing(true)
    }
    
    /// Make status bar light
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
}

// MARK: - TexField Delegate

extension ResetPasswordViewController: UITextFieldDelegate {
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
}
