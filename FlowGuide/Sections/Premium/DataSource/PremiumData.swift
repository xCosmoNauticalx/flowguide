//
//  PremiumData.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/9/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class PremiumData: NSObject, UICollectionViewDataSource {
    
    let colors = [UIColor.Archive.dark, UIColor.Progress.dark, UIColor.Progress.light, UIColor.Combo.light, UIColor.Combo.mid]
    
    let strings = ["Unlock your flow with the Combo Generator! Moves you've learned are chosen at random, challenging you to find new pathways between moves.", "Learning a new move, but need to take a break? Save individual videos to your In Progress tab so you can easily pick up right where you left off.", "Back up your Library to the cloud and access it from any device, protecting your data if your phone is lost or stolen.", "Directly support the developer to ensure Flow Guide can continue to grow and improve with consistent updates.", "Ad free."]
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return strings.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: PremiumCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? PremiumCell else {
        fatalError("Unable to dequeue PremiumCell.")
        }
        
        let color = colors[indexPath.item]
        let string = strings[indexPath.item]
        
        cell.label.text = string
        cell.bullet = CircleView(color: color)
        cell.setUp()
        
        return cell
    }
    

}
