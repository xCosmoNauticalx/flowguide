//
//  PremiumViewController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/9/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit
import StoreKit
import Purchases
import FirebaseAuth

class PremiumViewController: UIViewController, Layout {

// MARK: - Properties
    
    /// Coordinators
    weak var premium: Premium?
    
    /// Constraints
    var compactConstraints: [NSLayoutConstraint] = []
    var regularConstraints: [NSLayoutConstraint] = []
    var sharedConstraints: [NSLayoutConstraint] = []
    
    /// Data
    let data = PremiumData()
    var tempUser: TempUser!
    var price: String = ""
    var canMakePayments = Purchases.canMakePayments()
    var offering: Purchases.Offering!
    var package: Purchases.Package? {
        didSet {
            guard let package = package else { return }
                priceFormatter.locale = package.product.priceLocale
                price = package.localizedPriceString
        }
    }
    
    let priceFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
      
        formatter.formatterBehavior = .behavior10_4
        formatter.numberStyle = .currency
      
        return formatter
    }()
    
    /// Views
    let background: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "darkBackground")
        i.contentMode = .scaleAspectFill
        return i
    }()
    
    let header: UILabel = {
        let l = UILabel()
        l.text = "Flow Guide"
        l.font = UIFont(name: "HelveticaNeue-Bold", size: 30)
        l.textColor = .white
        l.textAlignment = .left
        l.translatesAutoresizingMaskIntoConstraints = false
        return l
    }()
    
    let gradientHeader: GradientLabel = {
        let l = GradientLabel()
        l.text = "Premium"
        l.font = UIFont(name: "HelveticaNeue-Bold", size: 30)
        l.gradientColors = [UIColor.Archive.dark.cgColor, UIColor.Progress.dark.cgColor, UIColor.Progress.light.cgColor, UIColor.Combo.dark.cgColor]
        l.textAlignment = .left
        l.translatesAutoresizingMaskIntoConstraints = false
        return l
    }()
    
    let disclaimer: UILabel = {
        let l = UILabel()
        l.text = "Subscription auto-renews monthly"
        l.font = UIFont(name: "HelveticaNeue-Light", size: 12)
        l.textColor = .white
        l.textAlignment = .left
        l.translatesAutoresizingMaskIntoConstraints = false
        return l
    }()
    
    let button: HighlightedButton = {
        let b = HighlightedButton()
        b.backgroundColor = .clear
        let title = "GET PREMIUM FOR $4.99 / MONTH"
        let attributes: [NSAttributedString.Key: Any] = [
            .font: UIFont(name: "HelveticaNeue", size: 15)!,
            .foregroundColor: UIColor.white]
        let attributedTitle = NSAttributedString(string: title, attributes: attributes)
        b.setAttributedTitle(attributedTitle, for: .normal)
        b.addTarget(self, action: #selector(buttonTapped), for: .touchUpInside)
        b.translatesAutoresizingMaskIntoConstraints = false
        return b
    }()
    
    let restoreButton: HighlightedButton = {
        let b = HighlightedButton()
        b.backgroundColor = .clear
        let title = "Restore Purchase"
        let attributes: [NSAttributedString.Key: Any] = [
            .font: UIFont(name: "HelveticaNeue", size: 14)!,
            .foregroundColor: UIColor.lightGray]
        let attributedTitle = NSAttributedString(string: title, attributes: attributes)
        b.setAttributedTitle(attributedTitle, for: .normal)
        b.tintColor = .white
        b.addTarget(self, action: #selector(restoreTapped), for: .touchUpInside)
        b.translatesAutoresizingMaskIntoConstraints = false
        return b
    }()
    
    var collectionView: UICollectionView!
    
    
    
    
// MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        setUp()
        setUpConstraints()
        
        NSLayoutConstraint.activate(sharedConstraints)
        layoutTrait(traitCollection: UIScreen.main.traitCollection)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        button.gradientBorder([UIColor.Combo.dark.cgColor, UIColor.Progress.light.cgColor, UIColor.Progress.dark.cgColor])
        //restoreButton.styleHollowButton(color: UIColor.white.cgColor)
    }
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        layoutTrait(traitCollection: traitCollection)
    }
    


// MARK: - Set Up
    
    func setUp() {
        
        /// Get Product
        //product = products[0]
        package = offering.availablePackages[0]
        
        /// CollectionView
        configureCollectionView()

        /// Add SubViews
        view.addSubview(background)
        view.addSubview(header)
        view.addSubview(gradientHeader)
        view.addSubview(button)
        view.addSubview(disclaimer)
        view.addSubview(collectionView)
        view.addSubview(restoreButton)
        
        var title = ""
        var fontSize = CGFloat(15)
        
        if canMakePayments == true {
            title = "GET PREMIUM FOR \(price) / MONTH"
            button.isEnabled = true
        } else {
            title = "NOT AVAILABLE"
            button.isEnabled = false
        }
        
        /// Vary for iPad
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            header.font = UIFont(name: "HelveticaNeue-Bold", size: 40)
            gradientHeader.font = UIFont(name: "HelveticaNeue-Bold", size: 40)
            disclaimer.font = UIFont(name: "HelveticaNeue-Light", size: 16)
            fontSize = 20
        }
        
        let attributes: [NSAttributedString.Key: Any] = [
            .font: UIFont(name: "HelveticaNeue", size: fontSize)!,
            .foregroundColor: UIColor.white]
        let attributedTitle = NSAttributedString(string: title, attributes: attributes)
        button.setAttributedTitle(attributedTitle, for: .normal)
        
        
        /// Add observer
        NotificationCenter.default.addObserver(self, selector: #selector(subscriptionPurchased), name: .purchaseNotification, object: nil)
        
    }
    
    private func configureCollectionView() {
        
        collectionView = UICollectionView(frame: CGRect(), collectionViewLayout: createLayout())
        collectionView.delegate = self
        collectionView.dataSource = data
        collectionView.collectionViewLayout = createLayout()
            
        collectionView.register(PremiumCell.self, forCellWithReuseIdentifier: "Cell")
        collectionView.backgroundColor = .clear

    }
    
    /// Dynamic Cell Height
    private func createLayout() -> UICollectionViewLayout {
        
        let size = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .estimated(10))
        let item = NSCollectionLayoutItem(layoutSize: size)
        item.edgeSpacing = NSCollectionLayoutEdgeSpacing(leading: nil, top: NSCollectionLayoutSpacing.fixed(20), trailing: nil, bottom: NSCollectionLayoutSpacing.fixed(0))
        
        let group = NSCollectionLayoutGroup.vertical(layoutSize: size, subitems: [item])
        
        let section = NSCollectionLayoutSection(group: group)
        
        let layout = UICollectionViewCompositionalLayout(section: section)
        
        return layout
    }
    
    
// MARK: - Layout Constraints
    
    private func setUpConstraints() {
        
        /// Shared
        // Background
        sharedConstraints.append(contentsOf: Utilities.fillSuperView(background))
        // Header
        sharedConstraints.append(header.topAnchor.constraint(equalTo: view.topAnchor, constant: 50))
        // Gradient Header
        sharedConstraints.append(contentsOf: [gradientHeader.topAnchor.constraint(equalTo: header.topAnchor), gradientHeader.leadingAnchor.constraint(equalTo: header.trailingAnchor, constant: 10)])
        // Button
        sharedConstraints.append(button.topAnchor.constraint(equalTo: header.bottomAnchor, constant: 30))
        // Disclaimer
        sharedConstraints.append(contentsOf: [disclaimer.topAnchor.constraint(equalTo: button.bottomAnchor, constant: 5), disclaimer.leadingAnchor.constraint(equalTo: button.leadingAnchor, constant: 6)])
        // Collection View
        sharedConstraints.append(contentsOf: Utilities.anchor(collectionView, top: disclaimer.bottomAnchor, leading: nil, bottom: restoreButton.topAnchor, trailing: nil, padding: .init(top: 20, left: 0, bottom: 0, right: 0)))
        sharedConstraints.append(contentsOf: Utilities.anchor(restoreButton, top: collectionView.bottomAnchor, leading: button.leadingAnchor, bottom: view.bottomAnchor, trailing: button.trailingAnchor, padding: .init(top: 0, left: 50, bottom: -40, right: -50)))
        
        /// Regular
        // Header
        regularConstraints.append(header.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 50))
        // Button
        regularConstraints.append(contentsOf: Utilities.anchor(button, top: nil, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 0, left: 100, bottom: 0, right: -100), size: .init(width: 0, height: 50)))
        // Collection View
        regularConstraints.append(contentsOf: Utilities.anchorWidth(collectionView, leading: view.leadingAnchor, trailing: view.trailingAnchor, leftPadding: 75, rightPadding: -75))
        
        /// Compact
        // Header
        compactConstraints.append(header.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 20))
        // Button
        compactConstraints.append(contentsOf: Utilities.anchor(button, top: nil, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 0, left: 40, bottom: 0, right: -40), size: .init(width: 0, height: 40)))
        // Collection View
        compactConstraints.append(contentsOf: Utilities.anchorWidth(collectionView, leading: view.leadingAnchor, trailing: view.trailingAnchor, leftPadding: 25, rightPadding: -25))
        
    }
    
    
// MARK: - Methods
    
    @objc func buttonTapped(_ sender: UIButton!) {
        purchaseSubscription()
    }

    /// Dismiss after subscription is purchased
    @objc func subscriptionPurchased() {
        self.dismiss(animated: true, completion: nil)
    }
    
    /// Restore purchase
    @objc func restoreTapped(_ sender: AnyObject) {
        PurchasesHelper.restorePurchase(self)
        // Copy data from Firestore to disk
    }
    
    /// Purchase subscription
    @objc func purchaseSubscription() {
        if tempUser.primaryDevice == UIDevice.current.identifierForVendor?.uuidString {
            PurchasesHelper.purchase(package!)
        } else {
            let alert = UIAlertController(title: "Please use your original device to upgrade", message: "When you upgrade, data from your device is uploaded to the cloud. Please upgrade on the device that contains your data so we can upload it.", preferredStyle: .alert)
            let ok = UIAlertAction(title: "Ok", style: .default) { (action: UIAlertAction) in
                do {
                    try Auth.auth().signOut()
                } catch {
                    print("Unable to sign out user: \(error)")
                }
            }
            let lost = UIAlertAction(title: "I lost my device", style: .cancel) { (action: UIAlertAction) in
                self.presentLostAlert()
            }
            alert.addAction(ok)
            alert.addAction(lost)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    
    func presentLostAlert() {
        let alert = UIAlertController(title: nil, message: "Unfortunately, if you lost your original device we can't get to your data. Would you like to start fresh and upgrade on this device?", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Upgrade now", style: .default) { (action: UIAlertAction) in
            // TODO:
            // Purchase
            // Replace primary device in Firestore
            // Post user migrated notification
            // Dismiss VC
        }
        let no = UIAlertAction(title: "Nevermind", style: .cancel) { (action: UIAlertAction) in
            do {
                try Auth.auth().signOut()
            } catch {
                print("Unable to sign out user: \(error)")
            }
        }
        let migrate = UIAlertAction(title: "Migrate Basic Account", style: .default) { (action: UIAlertAction) in
            // Replace Primary Device in Firestore
            // Post user migrated notification
            // Dismiss VC
        }
        alert.addAction(yes)
        alert.addAction(no)
        alert.addAction(migrate)
        self.present(alert, animated: true, completion: nil)
    }
    
    
}



// MARK: - UICollectionViewDelegate

extension PremiumViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell: PremiumCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? PremiumCell else {
             fatalError("Unable to dequeue PremiumCell.")
         }
        
        cell.label.text = nil
    }
    
}

