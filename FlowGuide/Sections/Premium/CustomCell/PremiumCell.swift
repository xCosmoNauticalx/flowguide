//
//  PremiumCell.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/9/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class PremiumCell: UICollectionViewCell {
    
// MARK: - Properties
        
    /// Views
    var bullet: CircleView!
    let label: UILabel = {
        let l = UILabel()
        l.font = UIFont(name: "HelveticaNeue-Light", size: 17)
        l.textColor = .white
        l.textAlignment = .left
        l.numberOfLines = 0
        l.lineBreakMode = .byWordWrapping
        return l
    }()

        
        
        
// MARK: - Set Up
        
    func setUp() {
            
        /// Add subviews
        addSubview(bullet)
        addSubview(label)
            
        /// Constraints
        bullet.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: nil, trailing: nil, size: .init(width: 30, height: 30))
        label.anchor(top: bullet.topAnchor, leading: bullet.trailingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor, padding: .init(top: 5, left: 20, bottom: 0, right: 0))
        
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            label.font = UIFont(name: "HelveticaNeue-Light", size: 22)
        }
        
    }
}
