//
//  MoveViewController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/4/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit
import Firebase
import GoogleMobileAds
import SafariServices


// MARK: - Parent Enum
enum parent {
    case archive
    case library
    case generator
    case progress
}


// MARK: - Class
class MoveDetailViewController: UIViewController, Storyboarded, Layout {
    
    
// MARK: - Properties
    
    /// Coordinators
    weak var coordinator: MoveCoordinator?
    weak var titleDelegate: Titleable?

    
    /// Constraints
    var compactConstraints: [NSLayoutConstraint] = []
    var regularConstraints: [NSLayoutConstraint] = []
    var sharedConstraints: [NSLayoutConstraint] = []

    /// IBOutlets
    @IBOutlet var learnedButton: UIButton!
    @IBOutlet var saveButton: UIButton!
    
    /// Data
    let data = MoveData()
    var text: String = "Tutorials"
    var tab: parent = .archive
    var user: User!
    var userIsEntitled = PurchasesHelper.isUserEntitled()
    var tempUserIsEntitled = true
    var shouldShowAd = true
    
    
    /// Views
    var radial = UIImageView()
    var titleLabel = TitleLabel()
    var collectionView: UICollectionView!
    
    /// Ads
    var interstitial: GADInterstitial!
    var bannerView: GADBannerView!
    
    
    
// MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        setUp()
        setUpConstraints()
        
        NSLayoutConstraint.activate(sharedConstraints)
        layoutTrait(traitCollection: UIScreen.main.traitCollection)
    }
    
    
    override func viewDidLoad() {
        /// Gesture Recognizer
            let longPressGR = UILongPressGestureRecognizer(target: self, action: #selector(longPress))
            longPressGR.minimumPressDuration = 0.5
            longPressGR.delaysTouchesBegan = true
            collectionView.addGestureRecognizer(longPressGR)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        /// Change Title
        text = data.page.name
        titleDelegate?.changeTitleText(to: text)
        titleDelegate?.changeTitleColor(to: UIColor.Archive.dark)

        
        /// Reload CollectionView
        //data.counter = 1
        //collectionView.reloadData()
        
        
        /// Set Tab Bar Tint
        switch tab {
        case .archive:
            self.tabBarController?.tabBar.tintColor = UIColor.Archive.light
            self.tabBarController?.tabBar.barTintColor = UIColor.Archive.light
            self.tabBarController?.tabBar.unselectedItemTintColor = UIColor.Archive.light
            self.tabBarController?.tabBar.tintColorDidChange()
            Utilities.styleNavBar(self, color: UIColor.Archive.dark)
        case .library:
            self.tabBarController?.tabBar.tintColor = UIColor.Library.light
            self.tabBarController?.tabBar.barTintColor = UIColor.Library.light
            self.tabBarController?.tabBar.unselectedItemTintColor = UIColor.Library.light
            self.tabBarController?.tabBar.tintColorDidChange()
            Utilities.styleNavBar(self, color: UIColor.Library.dark)
        default:
            self.tabBarController?.tabBar.tintColor = UIColor.Combo.light
            self.tabBarController?.tabBar.barTintColor = UIColor.Combo.light
            self.tabBarController?.tabBar.unselectedItemTintColor = UIColor.Combo.light
            self.tabBarController?.tabBar.tintColorDidChange()
            Utilities.styleNavBar(self, color: UIColor.Combo.dark)
        }
        
        
        /// Add radial if in Library or Generator tab
        if tab != .archive {
            let radialFrame = CGRect(x: -45, y: -45, width: (self.view.frame.width) + 100, height: 150)
            radial = UIImageView(frame: radialFrame)
            radial.image = UIImage(named: "blueRadial")
            radial.alpha = 0.33
            self.navigationController?.navigationBar.addSubview(radial)
        }
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
        /// Remove radial if in Library or Generator tab
        if tab != .archive {
            radial.removeFromSuperview()
        }
        
        /// Remove TitleLabel if in Generator tab
        if tab == .generator {
            titleLabel.removeFromSuperview()
        }
        
        
    }
    
    
    override func traitCollectionDidChange(_ previousTraitCollection: UITraitCollection?) {
        super.traitCollectionDidChange(previousTraitCollection)
        layoutTrait(traitCollection: traitCollection)
    }
    
    
// MARK: - Set Up
    
    private func setUp() {
        
        /// Notification Center
        NotificationCenter.default.addObserver(self, selector: #selector(enableLongPress), name: .purchaseNotification, object: nil)
        if !userIsEntitled {
           NotificationCenter.default.addObserver(self, selector: #selector(showInterstitial), name: UIApplication.didBecomeActiveNotification, object: nil)
        }
        

        /// Back Button
        Utilities.styleNavBar(self, color: UIColor.Archive.dark)
        
        /// Hide Save fo later button
        saveButton.alpha = 0
        
        /// Get user data
        user = coordinator?.dataManager?.getUser()
        
        /// UIButton Targets
        learnedButton.addTarget(self, action: #selector(markLearned), for: .touchUpInside)
        saveButton.addTarget(self, action: #selector(saveForLater), for: .touchUpInside)
        saveButton.setTitleColor(UIColor.Archive.light, for: .normal)
        saveButton.setTitle("Saved!", for: .selected)
        saveButton.tintColor = UIColor.Archive.light
        
        /// Set Learned Button
        checkIfLearned(data.page)
        
        /// Adjust storyboard elements based on device
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            
            // Constraints
            learnedButton.anchor(top: nil, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor, padding: .init(top: 0, left: 200, bottom: 0, right: -200))
            saveButton.anchor(top: nil, leading: learnedButton.leadingAnchor, bottom: nil, trailing: learnedButton.trailingAnchor, padding: .init(top: 0, left: 50, bottom: 0, right: -50), size: .init(width: 0, height: 50))
            
            // Save button attributes
            var title = "Save for later?"
            let attributes: [NSAttributedString.Key: Any] = [
                .font: UIFont(name: "HelveticaNeue", size: 25)!]
            var attributedTitle = NSAttributedString(string: title, attributes: attributes)
            saveButton.setAttributedTitle(attributedTitle, for: .normal)
            title = "Saved!"
            attributedTitle = NSAttributedString(string: title, attributes: attributes)
            saveButton.setAttributedTitle(attributedTitle, for: .selected)
        }
        
        /// Collection View
        switch UIScreen.main.traitCollection.userInterfaceIdiom {
        case .pad:
            collectionView = UICollectionView(frame: CGRect(), collectionViewLayout: createPadLayout())
            
        default:
            collectionView = UICollectionView(frame: CGRect(), collectionViewLayout: createPhoneLayout())
        }
        collectionView.dataSource = data
        collectionView.delegate = self
        collectionView.register(URLCell.self, forCellWithReuseIdentifier: "Cell")
        collectionView.backgroundColor = .clear
        
        /// Add Views
        view.addSubview(collectionView)

        
        /// Create Ads
        if !userIsEntitled {
            interstitial = createAndLoadInterstitial()
            createAndLoadBanner()
        }
        
    }
    
    private func createPhoneLayout() -> UICollectionViewLayout {
        
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.113), heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(42)) // Was 55
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        group.interItemSpacing = .flexible(20)
        
        let section = NSCollectionLayoutSection(group: group)
        section.interGroupSpacing = 20
        
        let layout = UICollectionViewCompositionalLayout(section: section)
        
        return layout
        
    }
    
    private func createPadLayout() -> UICollectionViewLayout {
        
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(0.113), heightDimension: .fractionalHeight(1.0))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .absolute(55))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitems: [item])
        group.interItemSpacing = .flexible(15)
        
        let section = NSCollectionLayoutSection(group: group)
        section.interGroupSpacing = 20
        
        let layout = UICollectionViewCompositionalLayout(section: section)
        
        return layout
        
    }
    
    

// MARK: - Layout Constraints
    
    private func setUpConstraints() {
        
        /// Shared
        // Collection View
        sharedConstraints.append(contentsOf: Utilities.anchorHeight(collectionView, top: view.topAnchor, bottom: learnedButton.topAnchor, topPadding: 170, bottomPadding: -20))
        
        /// Regular
        // Collection View
        regularConstraints.append(contentsOf: Utilities.anchorWidth(collectionView, leading: view.leadingAnchor, trailing: view.trailingAnchor, leftPadding: 175, rightPadding: -175))
        
        /// Compact
        // Collection View
        compactConstraints.append(contentsOf: Utilities.anchorWidth(collectionView, leading: view.leadingAnchor, trailing: view.trailingAnchor, leftPadding: 20, rightPadding: -20))
        
    }

}


// MARK: - Collection View Delegate

extension MoveDetailViewController: UICollectionViewDelegate {
        
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let youtubeID = data.page.urls[indexPath.item]
        coordinator?.play(youtubeID, self)
        if !userIsEntitled {
            shouldShowAd = true
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell: URLCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? URLCell else {
            fatalError("Unable to dequeue GeneratorCell.")
        }
        cell.number.text = nil
    }
        

}


// MARK: - @objc

extension MoveDetailViewController {
    
    /// Save or delete move from Library based on button toggle
    @objc func markLearned(sender: UIButton!) {
        if sender.isSelected == true {
            // Remove move from Library
            presentAlert(sender)
            
        } else {
            // Add move to Library
            if userIsEntitled {
                if let devices = coordinator?.dataManager?.getOtherDevices() {
                    if !devices.isEmpty {
                        // Other devices exist, make sure we add them to the document
                        FirestoreService.addLearnedMove(data.page, devices: devices)
                    } else {
                        FirestoreService.addLearnedMove(data.page)
                    }
                }
            }
            coordinator?.dataManager?.addLearnedMove(data.page)
            
            // Increment Move counter
            coordinator?.userUpdater?.updateMoveIncrement(by: 1)
            FirestoreService.updateMoveCount(by: 1)

            // Toggle button state
            sender.isSelected = true
        }
    }
    
    
    /// Save URL to InProgress
    @objc func saveForLater(sender: UIButton!) {
        if sender.isSelected == false {
            sender.isSelected = true
            // Add functionality
        } else {
            sender.isSelected = false
            // Add functionality
        }
    }
    
    
    @objc func enableLongPress() {
        userIsEntitled = true
    }
    
    
    @objc func longPress(gesture: UILongPressGestureRecognizer) {
        if tempUserIsEntitled {
            // Start after minimum press duration has passed so the function only fires once
            if gesture.state == .began {
                
                let item = gesture.location(in: self.collectionView)
                
                // Get the cell at indexPath (the one that was selected)
                if let indexPath = self.collectionView.indexPathForItem(at: item) {
                    let cell = self.collectionView.cellForItem(at: indexPath) as! URLCell
                    
                    // If the video isn't already saved
                    if cell.greenFill.alpha != 1 {
                        
                        // Haptic feedback
                        let generator = UINotificationFeedbackGenerator()
                        generator.notificationOccurred(.warning)
                        print("Long Pressed")
                        
                        // Ask if they want to save and respond accordingly
                        let alert = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                            let yes = UIAlertAction(title: "Save to In Progress", style: .default) { (action: UIAlertAction) in
                                self.coordinator?.dataManager?.saveVideo(forMoveNamed: self.data.page.name, withURL: self.data.page.urls[indexPath.item], hoopCount: self.data.page.hoopCount)
                                FirestoreService.addVideo(forMoveNamed: self.data.page.name, withURL: self.data.page.urls[indexPath.item], hoopCount: self.data.page.hoopCount)
                                cell.greenFill.alpha = 1
                                cell.number.textColor = .background
                            }
                            let no = UIAlertAction(title: "Nevermind", style: .cancel, handler: nil)
                            
                            alert.addAction(yes)
                            alert.addAction(no)
                            
                            self.present(alert, animated: true, completion: nil)
                            
                            
                        } else {
                            print("Couldn't find indexPath")
                        }
                        
                } else {
                    return
                }
            }
        
        }
    
    }

    
}


// MARK: - Methods

extension MoveDetailViewController {
    
    /// Check if move is in Library so button displays correct state
    func checkIfLearned(_ move: TempMove) {
        
        if coordinator?.dataManager?.checkIfLearned(move) == true {
            learnedButton.isSelected = true
        } else {
            learnedButton.isSelected = false
        }
        
    }
    
    
    /// Present alert when user wants to unlearn a move
    func presentAlert(_ sender: UIButton) {
        let title = "Are you sure?"
        let message = "Unlearning this move will delete any combos you've saved that include this move"
        let alert = UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
        
        // For iPad
        if let popoverPresentationController = alert.popoverPresentationController {
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverPresentationController.permittedArrowDirections = []
        }
        
        let yes = UIAlertAction(title: "Yes", style: .default) { (action: UIAlertAction) in
            // Remove from Library
            if self.userIsEntitled {
                if let devices = self.coordinator?.dataManager?.getOtherDevices() {
                    if !devices.isEmpty {
                        // Other devices exist, make sure we add them to the document
                        FirestoreService.removeLearnedMove(self.data.page, devices: devices)
                    } else {
                        // No other devices exist, proceed with delete
                        FirestoreService.removeLearnedMove(self.data.page)
                    }
                }
            }
            self.coordinator?.dataManager?.removeLearnedMove(self.data.page)
            
            // Decrement User Profile counter
            self.coordinator?.userUpdater?.updateMoveIncrement(by: -1)
            FirestoreService.updateMoveCount(by: -1)

            // Toggle button state
            sender.isSelected = false
        }
        
        let no = UIAlertAction(title: "No", style: .cancel, handler: nil)
        
        alert.addAction(no)
        alert.addAction(yes)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
}

// MARK: - Ads Delegates

extension MoveDetailViewController: GADInterstitialDelegate {
    
    func createAndLoadInterstitial() -> GADInterstitial {
        print("Creating new Interstitial")
        let interstitial = GADInterstitial(adUnitID: AdIdentifier.interstitialVideo)
        interstitial.delegate = self
        interstitial.load(GADRequest())
        return interstitial
    }
    
    @objc func showInterstitial() {
        /// Show Ad
        if shouldShowAd {
            if interstitial.isReady {
                interstitial.present(fromRootViewController: self)
            } else {
                print("Ad wasn't ready")
            }
        }
        
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial) {
        interstitial = createAndLoadInterstitial()
        shouldShowAd = false
    }
    
    /// Tells the delegate an ad request succeeded.
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
      print("interstitialDidReceiveAd")
    }
    
    /// Tells the delegate an ad request failed.
    func interstitial(_ ad: GADInterstitial, didFailToReceiveAdWithError error: GADRequestError) {
      print("interstitial:didFailToReceiveAdWithError: \(error.localizedDescription)")
    }
    
    /// Tells the delegate that an interstitial will be presented.
    func interstitialWillPresentScreen(_ ad: GADInterstitial) {
      print("interstitialWillPresentScreen")
    }

    /// Tells the delegate the interstitial is to be animated off the screen.
    func interstitialWillDismissScreen(_ ad: GADInterstitial) {
        
      print("interstitialWillDismissScreen")
    }
    
}


extension MoveDetailViewController: GADBannerViewDelegate {
    
    func createAndLoadBanner() {
        bannerView = GADBannerView()
        bannerView.adUnitID = AdIdentifier.banner
        bannerView.rootViewController = self
        view.addSubview(bannerView)
        bannerView.anchor(top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, size: .init(width: 0, height: 60))
        bannerView.load(GADRequest())
    }
    
    
}


// MARK: - SFSafariViewControllerDelegate

extension MoveDetailViewController: SFSafariViewControllerDelegate {
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        if !userIsEntitled {
            // Since the same VC is presenting the safariVC and the ad, wait for the safariVC to be finished dismissing
            let seconds = 0.6
            DispatchQueue.main.asyncAfter(deadline: .now() + seconds, execute: {
                self.showInterstitial()
            })
            
        }
    }
    
}


// MARK: - UICollectionViewDelegateFlowLayout

extension MoveDetailViewController: UICollectionViewDelegateFlowLayout {
    
    func centerItemsInCollectionView(cellWidth: Double, numberOfItems: Double, spaceBetweenCell: Double, collectionView: UICollectionView) -> UIEdgeInsets {
        let totalWidth = cellWidth * numberOfItems
        let totalSpacingWidth = spaceBetweenCell * (numberOfItems - 1)
        let leftInset = (collectionView.frame.width - CGFloat(totalWidth + totalSpacingWidth)) / 2
        let rightInset = leftInset
        return UIEdgeInsets(top: 0, left: leftInset, bottom: 0, right: rightInset)
    }
    
}

