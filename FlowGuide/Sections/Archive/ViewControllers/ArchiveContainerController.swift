//
//  ArchiveContainerController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 3/12/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class ArchiveContainerController: UIViewController, Titleable {

    // MARK: - Properties
    
    weak var coordinator: ArchiveCoordinator?
    
    var homeController: ArchiveHomeViewController!
    
    var titleLabel = TitleLabel()
    
    let testContainer = UIView()
    let gradient = CAGradientLayer()
    
    
    // MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        setUp()
        configureHomeViewController()
    }
    
    
    override func viewDidLayoutSubviews() {
        testContainer.frame = view.bounds
    }
    
    
    
    // MARK: - Handlers
    
    private func setUp() {
        
//        view.addSubview(testContainer)
//        testContainer.fillSuperView()
//        testContainer.frame = view.bounds
//
//        gradient.colors = [UIColor.Archive.dark.cgColor, UIColor.Archive.mid.cgColor, UIColor.Archive.light.cgColor, UIColor.Progress.dark.cgColor, UIColor.Progress.mid.cgColor, UIColor.Progress.light.cgColor, UIColor.Combo.light.cgColor, UIColor.Combo.mid.cgColor, UIColor.Combo.dark.cgColor, UIColor.Library.light.cgColor, UIColor.Library.mid.cgColor, UIColor.Library.dark.cgColor]
//        gradient.startPoint = CGPoint(x: 0, y: 0)
//        gradient.endPoint = CGPoint(x: 0.7, y: 1)
//        gradient.frame = view.bounds
//        testContainer.layer.addSublayer(gradient)
        
        /// Format Title
        let frame = CGRect(x: (self.navigationController?.navigationBar.frame.width)!/6, y: -30, width: (self.navigationController?.navigationBar.frame.width)!/6 * 4, height: 100)
        titleLabel.frame = frame
        titleLabel.text = "Tutorials"
        titleLabel.textColor = UIColor.Archive.dark
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            titleLabel.font = UIFont(name: "HelveticaNeue", size: 49)
        }
        self.navigationController?.navigationBar.addSubview(titleLabel)



        /// Add radial
        let radialFrame = CGRect(x: -45, y: -45, width: (self.view.frame.width) + 100, height: 150)
        let radial = UIImageView(frame: radialFrame)
        radial.image = UIImage(named: "blueRadial")
        radial.alpha = 0.33
        self.navigationController?.navigationBar.addSubview(radial)


        self.navigationController?.navigationBar.layer.masksToBounds = false
    

    }
    
    
    func configureHomeViewController() {
        
        homeController = ArchiveHomeViewController()
        homeController.titleDelegate = self
        homeController.coordinator = coordinator

        view.addSubview(homeController.view)
        // Add the HomeController as a child to the ContainerController
        addChild(homeController)
        // Home controller didMove to the parent container of self, which is the ContainerController
        homeController.didMove(toParent: self)
    }


}

