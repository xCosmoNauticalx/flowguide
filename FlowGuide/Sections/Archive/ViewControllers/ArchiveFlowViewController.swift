//
//  ArchiveFlowViewController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/4/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit
import GoogleMobileAds

class ArchiveFlowViewController: UIViewController {

// MARK: - Properties
    
    /// Coordinators
    weak var coordinator: ArchiveCoordinator?
    var titleDelegate: Titleable?

    /// Data
    let data = ArchiveFlowData()
    let userIsEntitled = PurchasesHelper.isUserEntitled()
    var parents = [Node]()
    
    /// Views
    let background: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "darkBackground")
        i.contentMode = .scaleAspectFill
        return i
    }()
    
    let container = UIView()
    var gradientView = CAGradientLayer()
    
    let collectionView = ArchiveFlowController(collectionViewLayout: UICollectionViewFlowLayout())
    let directory = DirectoryController(collectionViewLayout: UICollectionViewFlowLayout())
    
    /// Ads
    var bannerView: GADBannerView!
    
    
// MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        setUp()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        /// Replace Title
        let text = data.page.name
        titleDelegate?.changeTitleText(to: text)
        
        /// Style Tab Bar
        Utilities.styleTabBar(self, color: UIColor.Archive.light)
        
    }
    
    
    override func viewDidLayoutSubviews() {
        if !userIsEntitled {
            gradientView.frame = container.bounds
        } else {
            gradientView.frame = view.bounds
        }
    }

    
// MARK: - Set Up
    
    private func setUp() {
        
        /// Back Button
        Utilities.styleNavBar(self, color: UIColor.Archive.dark)
        
        /// CollectionView
        collectionView.collectionView.delegate = self
        collectionView.collectionView.dataSource = data
        
        /// Directory
        directory.data.parents = parents
        guard let coordinatorr = coordinator else { return }
        directory.setDelegate(with: coordinatorr)

        
        /// Add Subviews
        view.addSubview(background)
        view.addSubview(container)
        container.addSubview(collectionView.view)
        view.addSubview(directory.view)
        
        /// Constraints
        background.fillSuperView()
        
        collectionView.view.fillSuperView()
        
        if !userIsEntitled {
            createAndLoadBanner()
            container.anchor(top: directory.view.bottomAnchor, leading: view.leadingAnchor, bottom: bannerView.topAnchor, trailing: view.trailingAnchor)
        } else {
            container.anchor(top: directory.view.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor)
        }
        
        directory.view.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: container.topAnchor, trailing: view.trailingAnchor, padding: .init(top: 8, left: 15, bottom: 0, right: -15), size: .init(width: 0, height: 20))
        
        /// CollectionView Gradient
        gradientView.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
        gradientView.locations = [0, 0.07, 0.9, 1]
        container.layer.mask = gradientView
        
    }
    
}


// MARK: - Collection View Delegate

extension ArchiveFlowViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        collectionView.deselectItem(at: indexPath, animated: true)
        
        let nextPage = data.page.children[indexPath.item]
        // Add self to parents
        var newParents = parents
        newParents.append(data.page)
        // Check if next page is going to be moves or another flow page
        if nextPage.finalGroup == .archive {
            coordinator?.movesPage(page: nextPage, parents: newParents, titleDelegate: titleDelegate!)
        } else {
            coordinator?.next(page: nextPage, parents: newParents, titleDelegate: titleDelegate!)
        }
    }

    
    /// Clear labels when no longer needed
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let cell = collectionView.cellForItem(at: indexPath) as? GroupCell else { return }
        cell.label.text = nil
    }
    
    
}


extension ArchiveFlowViewController: GADBannerViewDelegate {
    
    func createAndLoadBanner() {
        bannerView = GADBannerView()
        bannerView.adUnitID = AdIdentifier.banner
        bannerView.rootViewController = self
        view.addSubview(bannerView)
        bannerView.anchor(top: container.bottomAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, size: .init(width: 0, height: 60))
        bannerView.load(GADRequest())
    }
    
}


