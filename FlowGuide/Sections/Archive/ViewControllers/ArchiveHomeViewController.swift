//
//  ArchieHomeViewController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/14/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class ArchiveHomeViewController: UIViewController {

// MARK: - Properties
    
    /// Coordinators
    weak var coordinator: ArchiveCoordinator?
    var titleDelegate: Titleable?

    /// Data
    let data = ArchiveHomeData()
    
    /// Views
    let container = UIView()
    var gradientView = CAGradientLayer()
    
    let collectionView = ArchiveHomeController.instantiate()

    
// MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        setUp()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        
        /// Replace Title
        let text = "Tutorials"
        titleDelegate?.changeTitleText(to: text)
        
        Utilities.styleTabBar(self, color: UIColor.Archive.light)
    }
    
    override func viewDidLayoutSubviews() {
        gradientView.frame = view.bounds
    }

    private func setUp() {
        

        /// CollectionView
        collectionView.collectionView.dataSource = data
        collectionView.collectionView.delegate = self
        
        /// Add Subviews
        view.addSubview(container)
        container.addSubview(collectionView.view)
        
        /// Constraints
        container.fillSuperView()
        collectionView.view.fillSuperView()
        
        /// CollectionView Gradient
        gradientView.colors = [UIColor.clear.cgColor, UIColor.black.cgColor, UIColor.black.cgColor, UIColor.clear.cgColor]
        gradientView.locations = [0, 0.07, 0.9, 1]
        container.layer.mask = gradientView
        
        
        
    }
    

}


// MARK: - UICollectionViewDelegate

extension ArchiveHomeViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let nextPage = data.page.children[indexPath.item]
        coordinator?.next(page: nextPage, titleDelegate: titleDelegate!)
    }

 

}
