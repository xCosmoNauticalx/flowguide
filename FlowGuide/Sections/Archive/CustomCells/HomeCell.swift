//
//  HomeCell.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 11/26/19.
//  Copyright © 2019 CosmoNautical. All rights reserved.
//

import UIKit

class HomeCell: UICollectionViewCell {
    
    weak var coordinator: ArchiveCoordinator?
    
    @IBOutlet var imageView: UIImageView!
    @IBOutlet var gradientView: UIView!
    
    var category: Node?
    
    override var isHighlighted: Bool {
        didSet {
            toggleIsHighlighted()
        }
    }
    
    init(frame: CGRect, gradientView: UIView, category: Node, imageView: UIImageView){
        super.init(frame: frame)
        
    }


    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    
    func toggleIsHighlighted() {
        UIView.animate(withDuration: 0.01, delay: 0, options: [.curveEaseOut], animations: {
            self.alpha = self.isHighlighted ? 0.5 : 1.0
        })
    }
    
}
