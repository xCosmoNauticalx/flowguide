//
//  ArchiveFlowData.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/19/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class ArchiveFlowData: NSObject, UICollectionViewDataSource {

    var page: Node = tutorials

    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return page.children.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: GroupCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? GroupCell else {
            fatalError("Unable to dequeue GroupCell.")
        }
        
        let buttonName = page.children[indexPath.item]
        cell.label.text = buttonName.name
        
        // Have cell layout views so frame can be known
        cell.layoutSubviews()
        
        func gradient(frame: CGRect) -> CAGradientLayer {
            let layer = CAGradientLayer()
            layer.frame = frame
            layer.colors = [UIColor.Archive.dark.cgColor, UIColor.Archive.mid.cgColor, UIColor.Archive.light.cgColor]

            return layer
        }
              
        cell.gradientView.layer.insertSublayer(gradient(frame: cell.gradientView.bounds), at: 0)
        cell.gradientView.mask = cell.border
        
        cell.textGradient.layer.insertSublayer(gradient(frame: cell.textGradient.bounds), at: 0)
        cell.textGradient.mask = cell.label

        
        
        return cell
        
    }
    
}
