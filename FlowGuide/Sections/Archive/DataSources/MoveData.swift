//
//  MoveData.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/19/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class MoveData: NSObject, UICollectionViewDataSource {

    weak var dataManager: DataManaging?
    var page = TempMove()
    var counter = 1
    
    var videos = [Video]()
    
        
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return page.urls.count
    }
        
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: URLCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? URLCell else {
            fatalError("Unable to dequeue URLCell.")
        }
        
        // Check if any of the urls are saved & mark them
        if let videos = dataManager?.getVideo(withURL: page.urls[indexPath.item]) {
            if videos.count != 0 {
                cell.number.textColor = .background
                cell.greenFill.alpha = 1
            }
        }
        
        cell.layoutSubviews()
        
        func gradient(frame: CGRect) -> CAGradientLayer {
            let layer = CAGradientLayer()
            layer.frame = frame
            layer.colors = [UIColor.Progress.mid.cgColor, UIColor.Progress.dark.cgColor]
            //layer.startPoint = CGPoint(x: 0.0, y: 0.5)
            //layer.endPoint = CGPoint(x: 1.0, y: 0.5)

            return layer
        }
              
        cell.greenFill.layer.insertSublayer(gradient(frame: cell.greenFill.bounds), at: 0)

        cell.number.text = String(describing: counter)
        counter += 1
      
        return cell
            
    }
    
}
