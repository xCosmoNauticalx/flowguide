//
//  ArchiveHomeData.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/19/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class ArchiveHomeData: NSObject, UICollectionViewDataSource {

    var page: Node = tutorials
    let categories = ["single_hoop_blue", "doubles", "multi_hoops", "misc", "beginner_tips", "recently_added"]
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
          
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return page.children.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
      
        guard let cell: HomeCell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeButton", for: indexPath) as? HomeCell else {
        fatalError("Unable to dequeue HomeCell.")
        }
          
          
        let image = categories[indexPath.item]
        cell.imageView.image = UIImage(named: image)
          
        func gradient(frame: CGRect) -> CAGradientLayer {
            let layer = CAGradientLayer()
            layer.frame = frame
            layer.colors = [UIColor.Archive.dark.cgColor, UIColor.Archive.mid.cgColor, UIColor.Archive.light.cgColor]
              
    
            return layer
        }
          
        cell.gradientView.layer.insertSublayer(gradient(frame: cell.bounds), at: 0)
        cell.gradientView.mask = cell.imageView
        
        /*cell.layer.shadowColor = UIColor.Archive.dark.cgColor
        cell.layer.shadowOffset = CGSize(width: 0, height: 2)
        cell.layer.shadowRadius = 2
        cell.layer.shadowOpacity = 1
        cell.layer.masksToBounds = false*/
          
      
          return cell
      }
    
}
