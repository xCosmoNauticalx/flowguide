//
//  ArchiveFinalData.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/19/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class ArchiveFinalData: NSObject, UICollectionViewDataSource {

    var page: Node = tutorials
    var moves = [TempMove]()
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }


    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        
        return moves.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: MoveCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? MoveCell else {
            fatalError("Unable to dequeue TutorialCell.")
        }
        
        let move = moves[indexPath.item]
        cell.label.text = move.name
        
        // Have cell layout views so frame can be known
        cell.layoutSubviews()
        
        func gradient(frame: CGRect) -> CAGradientLayer {
            let layer = CAGradientLayer()
            layer.frame = frame
            layer.colors = [UIColor.Archive.dark.cgColor, UIColor.Archive.mid.cgColor, UIColor.Archive.light.cgColor]
            layer.startPoint = CGPoint(x: 0.0, y: 0.5)
            layer.endPoint = CGPoint(x: 1.0, y: 0.5)

            return layer
        }
              
        cell.gradientView.layer.insertSublayer(gradient(frame: cell.gradientView.bounds), at: 0)
        cell.gradientView.mask = cell.label
        
        
    
        return cell
    }
    
}
