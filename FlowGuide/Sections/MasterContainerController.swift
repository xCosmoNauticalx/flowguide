//
//  MasterContainerController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 3/12/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit
import FirebaseAuth
import CoreData

class MasterContainerController: UIViewController, UserUpdating, Premium {
    
    
    // MARK: - Properties
    
    /// View Controllers
    var menuController: MenuViewController!
    var centerController: MainTabBarController!

    /// Data
    var isExpanded = false
    // Have this stored so that every time the menu button is pressed, we don't create a new menuController each time
    
    // Core Data Manager
    var dataManager: DataManaging?
    
    var user: User?
    var tempUser: TempUser!
    
    var moveIncrement = 0
    var comboIncrement = 0
    
    var userIsEntitled = PurchasesHelper.isUserEntitled()
    
    
    /// Views
    let background: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "darkBackground")
        i.contentMode = .scaleAspectFill
        return i
    }()

    
    init(dataManager: DataManaging, tempUser: TempUser) {
        self.dataManager = dataManager
        self.tempUser = tempUser
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
// MARK: - Override Variables
    
    /// Animate status bar when menu slides out
    override var preferredStatusBarUpdateAnimation: UIStatusBarAnimation {
        return .slide
    }
    
    
    override var prefersStatusBarHidden: Bool {
        return isExpanded
    }
    
    
    // MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        
        setUp()
        
        // If user is not subscribed, make sure this is the device they're supposed to use
        if !userIsEntitled {
            validateDeviceID()
        } else {
            // User is subscribed, check if this is a new device
            checkUserInDisk() // Make sure there's a user in disk
            // Update devices
            guard let thisDevice = UIDevice.current.identifierForVendor?.uuidString else { return }
            guard let devices = dataManager?.getAllDevices() else { return }
            var isNewDevice = true
            for device in devices {
                if device == thisDevice {
                    isNewDevice = false
                    break
                }
            }
            switch isNewDevice {
            case true:
                // New device
                dataManager?.updateDevices(withDevice: thisDevice)  // Add to disk
                guard let uid = user?.uid else { return }
                FirestoreService.updateDevices(forUser: uid, withDevice: thisDevice)    // Add to Firestore
                // Download all moves
                let dispatch = DispatchGroup()
                FirestoreService.getLearnedMoves(forUser: uid, dispatch: dispatch) { (moves) in
                    dispatch.notify(queue: .main, execute: {
                        self.dataManager?.batchAddLearnedMove(moves)
                    })
                }
            case false:
                // Not a new device
                checkForUpdates()   // Check for Updates
                // Sync any other devices that were added
                user?.otherDevices = tempUser.otherDevices
            }
            
        }
        
        Notifyer.askToRegisterForNotifications(self)
        Notifyer.checkIfNotificationsAreEnabled(self)

    }


    // MARK: - Set Up
    
    private func setUp() {
        
        /// Notification Center
        NotificationCenter.default.addObserver(self, selector: #selector(userDidSubscribe), name: .purchaseNotification, object: nil)
        
        /// Hide Nav Bar
        self.navigationController?.isNavigationBarHidden = true
        
        /// Add background
        view.insertSubview(background, at: 0)
        background.fillSuperView()
    }
    
    
    func configureHomeController() {
        
        centerController = MainTabBarController(dataManager: dataManager!, slideDelegate: self, userUpdater: self, premium: self)

        view.addSubview(centerController.view)
        // Add the navigationController containing the HomeController as a child to the ContainerController
        addChild(centerController)
        // Home controller didMove to the parent container of self, which is the ContainerController
        centerController.didMove(toParent: self)
        
    }
    
    
    func configureMenuController() {
        
        // Check to see if menuController has already been created. If not, create it.
        if menuController == nil {
            // add our menuController
            menuController = MenuViewController()
            menuController.user = user
            print(String(describing:menuController.user.name))
            menuController.slideDelegate = self
            menuController.premium = self
            
            /// Count Stats from Persistent Storage
            user?.movesLearned = dataManager!.countLearnedMoves()
            user?.combosCreated = dataManager!.countCombos()
            print(user?.movesLearned as Any)
            
            /// Vary for iPad
            if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
                menuController.offset = self.centerController.view.frame.width / 2
            }
            
            view.insertSubview(menuController.view, at: 1)
            addChild(menuController)
            menuController.didMove(toParent: self)
        }
        
    }
    
    
// MARK: - Methods
    
    /// Compare this deviceID to logged-in user's deviceID
    func validateDeviceID() {
        // Check if deviceID we got from Firestore earlier matches current deviceID
        if tempUser.primaryDevice == UIDevice.current.identifierForVendor?.uuidString {
            // Success, check if user is stored in disk
            print("Device ID Match")
            checkUserInDisk()
            // Now that we know if we have a user, proceed with app
            configureHomeController()
            // TODO: Ask to allow notifications
        } else {
            print("DeviceID not valid")
            userNotAllowedOnDevice()
        }
    }
    
    
    /// Check if there's a user stored in disk
    func checkUserInDisk() {
        if let currentUser = self.dataManager?.getUser() {
            user = currentUser
        } else {
            // They deleted and reinstalled the app on the same device, or they're a Premium user on a new device. Create new user in disk
            print("User not found, creating now")
            dataManager?.restoreUser(tempUser)
            if let currentUser = self.dataManager?.getUser() {
                user = currentUser
            }
        }
    }
    
    
    /// Handle user logging in to wrong device
    func userNotAllowedOnDevice() {
        // Show alert saying this device isn't registered to this account, give option to upgrade, sign out, or migrate.
        let title = "Your account is already registered to a different device"
        let message = "Flow Guide Premium enables you to access your account from multiple devices, plus other awesome features. Would you like to upgrade now?"
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        // For iPad
        if let popoverPresentationController = alert.popoverPresentationController {
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverPresentationController.permittedArrowDirections = []
        }
        
        let yes = UIAlertAction(title: "Learn More", style: .default) { (action: UIAlertAction) in
            self.presentPremiumPage()
        }
            
        let no = UIAlertAction(title: "Sign Out", style: .cancel) { (action: UIAlertAction) in
            do {
                try Auth.auth().signOut()
            } catch {
                print("Unable to sign out user: \(error)")
            }
        }
        
        let migrate = UIAlertAction(title: "Migrate to This Device", style: .default) { (action: UIAlertAction) in
            self.showMigrateAlert()
        }
        
        // TODO: Add action for migrating to new device
        
        alert.addAction(yes)
        alert.addAction(no)
        alert.addAction(migrate)
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func showMigrateAlert() {
        let alert = UIAlertController(title: "Are you sure?", message: "Once you migrate, you'll only be able to use this device from now on and any previous user's data will be deleted. If you're not subscribed to Flow Guide Premium, all of your data will be lost.", preferredStyle: .alert)
        let yes = UIAlertAction(title: "Migrate now", style: .default) { (action: UIAlertAction) in
            // TODO:
            if let thisDevice = UIDevice.current.identifierForVendor?.uuidString {
                FirestoreService.replacePrimaryDevice(forUser: self.tempUser.uid, with: thisDevice)
                self.dataManager?.deleteAndRebuild()
                self.tempUser.primaryDevice = thisDevice
                self.dataManager?.createUser(name: self.tempUser.name, uid: self.tempUser.uid, primaryDevice: self.tempUser.primaryDevice)
                self.user = self.dataManager?.getUser()
                self.configureHomeController()
            }
        }
        let no = UIAlertAction(title: "Nevermind", style: .cancel) { (action: UIAlertAction) in
            do {
                try Auth.auth().signOut()
            } catch {
                print("Error signing out user: \(error)")
            }
        }
        alert.addAction(yes)
        alert.addAction(no)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func checkForUpdates() {
        /// Install Moves
        let installMoveDispatch = DispatchGroup()
        FirestoreService.syncMoveUpdates(forType: .install, dispatch: installMoveDispatch) { (moves) in
            installMoveDispatch.notify(queue: .main, execute: {
                for move in moves {
                    self.dataManager?.addLearnedMove(move)
                }
            })
        }
        
        /// Install Combos
        let installComboDispatch = DispatchGroup()
        FirestoreService.syncComboUpdates(forType: .install, dispatch: installComboDispatch) { (combos) in
            installComboDispatch.notify(queue: .main, execute: {
                for combo in combos {
                    self.dataManager?.addCombo(combo)
                }
            })
        }
        
        /// Delete Moves
        let deleteMoveDispatch = DispatchGroup()
        FirestoreService.syncMoveUpdates(forType: .delete, dispatch: deleteMoveDispatch) { (moves) in
            deleteMoveDispatch.notify(queue: .main, execute: {
                for move in moves {
                    self.dataManager?.removeLearnedMove(move)
                }
            })
        }
        
        /// Delete Combos
        let deleteComboDispatch = DispatchGroup()
        FirestoreService.syncComboUpdates(forType: .delete, dispatch: deleteComboDispatch) { (combos) in
            deleteComboDispatch.notify(queue: .main, execute: {
                for combo in combos {
                    self.dataManager?.removeCombo(combo)
                }
            })
        }
    }
    

    func animatePanel(shouldExpand: Bool) {
        
        if shouldExpand {
            // Show menu
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                
                switch UIScreen.main.traitCollection.userInterfaceIdiom {
                case .phone:
                    self.centerController.view.frame.origin.x = self.centerController.view.frame.width - 60
                default:
                    self.centerController.view.frame.origin.x = self.centerController.view.frame.width - (self.centerController.view.frame.width / 2)
                    
                }
            }, completion: nil)
            centerController.view.layer.shadowOpacity = 0.8
            centerController.view.layer.opacity = 0.5
        } else {
            // Hide menu
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
                self.centerController.view.frame.origin.x = 0
            }, completion: nil)

            centerController.view.layer.shadowOpacity = 0.0
            centerController.view.layer.opacity = 1.0
        }
        animateStatusBar()
        
    }
    
    
    func animateStatusBar() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: .curveEaseInOut, animations: {
            self.setNeedsStatusBarAppearanceUpdate()
        }, completion: nil)
        
    }
    
    func updateIncrements() {
        menuController.refreshStats(move: moveIncrement, combo: comboIncrement)
        moveIncrement = 0
        comboIncrement = 0
    }
    
    
    
    @objc func userDidSubscribe() {
        guard let moves = dataManager?.fetchLearnedMoves() else { return }
        FirestoreService.batchWriteMoves(moves)
    }

}


// MARK: - Protocol Conformance

extension MasterContainerController: Slideable {
    
    func handleMenuToggle() {
        if !isExpanded { // Another way of writing if isExpanded == false
            configureMenuController()
            updateIncrements()
        }
        // Toggle isExpanded and set it equal to the opposite of it's value when it was called
        isExpanded.toggle()
        animatePanel(shouldExpand: isExpanded)
    }
    
    func logOut () {
        let alert = UIAlertController(title: "Are you sure you want to log out?", message: nil, preferredStyle: .actionSheet)
        
        // For iPad
        if let popoverPresentationController = alert.popoverPresentationController {
            popoverPresentationController.sourceView = self.view
            popoverPresentationController.sourceRect = CGRect(x: self.view.bounds.midX, y: self.view.bounds.midY, width: 0, height: 0)
            popoverPresentationController.permittedArrowDirections = []
        }
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default) { (action: UIAlertAction) in
                do {
                    try Auth.auth().signOut()
                } catch (let err) {
                    print("Auth sign out failed: \(err)")
                }
            })
        alert.addAction(UIAlertAction(title: "No", style: .cancel, handler: nil))
        
        navigationController?.present(alert, animated: true)
    }
    
}


