//
//  ProgressData.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/22/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class ProgressData: NSObject, UITableViewDataSource {
    
    weak var dataManager: DataManaging?
    
    var videos = [Video]()
    
    var single = [Video]()
    var doubles = [Video]()
    var multi = [Video]()
    var quads = [Video]()
    
    var sections = ["Single", "Doubles", "Multi", "Quads"]


    func numberOfSections(in tableView: UITableView) -> Int {
        var counter = 0
        
        
        
        if single.count != 0 {
            counter += 1
        }
        if doubles.count != 0 {
            counter += 1
        }
        if multi.count != 0 {
            counter += 1
        }
        if quads.count != 0 {
            counter += 1
        }
        
        /*if (videos.contains(where: { $0.hoopCount == 1 })) {
            counter += 1
        }
        if (videos.contains(where: { $0.hoopCount == 2 })) {
            counter += 1
        }
        if (videos.contains(where: { $0.hoopCount == 3 })) {
            counter += 1
        }
        if (videos.contains(where: { $0.hoopCount == 4 })) {
            counter += 1
        }*/
        
        
        return counter
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if section == 0 {
            print(single.count)
            return single.count
        } else if section == 1 {
            return doubles.count
        } else if section == 2 {
            return multi.count
        } else if section == 3 {
            return quads.count
        }
        
        return 0
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell: ProgressCell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as? ProgressCell else {
            fatalError("Unable to dequeue GroupCell.")
        }
        
        var video: Video?
        
        if indexPath.section == 0 {
            video = single[indexPath.row]
        } else if indexPath.section == 1 {
            video = doubles[indexPath.row]
        } else if indexPath.section == 2 {
            video = multi[indexPath.row]
        } else if indexPath.section == 3 {
            video = quads[indexPath.row]
        }

        cell.label.text = video?.moveName
        if let url = video?.url {
            cell.url = url
        }
        
        // Have cell layout views so frame can be known
        cell.layoutSubviews()
        
        func gradient(frame: CGRect) -> CAGradientLayer {
            let layer = CAGradientLayer()
            layer.frame = frame
            layer.colors = [UIColor.Progress.dark.cgColor, UIColor.Progress.mid.cgColor, UIColor.Progress.light.cgColor]
            layer.startPoint = CGPoint(x: 0.0, y: 0.5)
            layer.endPoint = CGPoint(x: 1.0, y: 0.5)

            return layer
        }
              
        cell.gradientView.layer.insertSublayer(gradient(frame: cell.gradientView.bounds), at: 0)
        cell.gradientView.mask = cell.label

        return cell
    }

    
    // Override to support editing the table view.
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            var video: Video?

            
            switch indexPath.section {
            case 0:
                video = single[indexPath.row]
                single.remove(at: indexPath.row)

            case 1:
                video = doubles[indexPath.row]
                doubles.remove(at: indexPath.row)

            case 2:
                video = multi[indexPath.row]
                multi.remove(at: indexPath.row)

            default:
                video = quads[indexPath.row]
                quads.remove(at: indexPath.row)

            }
            
            FirestoreService.removeVideo(video!)
            dataManager?.removeVideo(video!)
            
            
            
            
            if tableView.numberOfRows(inSection: indexPath.section) == 1 {
                tableView.deleteSections(NSIndexSet(index: indexPath.section) as IndexSet, with: .fade)
            } else {
                tableView.deleteRows(at: [indexPath], with: .fade)
            }
            
            if single.isEmpty && doubles.isEmpty && multi.isEmpty && quads.isEmpty {
                NotificationCenter.default.post(name: .noMoreVideos, object: nil)
            }
            
            
        }
        
    }
    
    
    

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

}
