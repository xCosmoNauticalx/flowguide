//
//  InProgressContainerController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/8/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit
import Purchases

class InProgressContainerController: UIViewController {

// MARK: - Properties
        
    /// Coordinators
    weak var coordinator: InProgressCoordinator?
        
    /// Data
    var userIsEntitled = PurchasesHelper.isUserEntitled()
        
    /// Views
    var titleLabel = TitleLabel()
    var backArrow: UIBarButtonItem!

        
        
// MARK: - Life Cycle
        
    override func loadView() {
        super.loadView()
        setUp()
        //configureCorrectViewController()
        configureHomeViewController()
    }
        
        
        
// MARK: - Configure
        
    private func setUp() {
        
        /// Notification Center
        NotificationCenter.default.addObserver(self, selector: #selector(refreshAfterSubscribe), name: .purchaseNotification, object: nil)
            
        /// Make Navigation Bar Transparent
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
            
        /// Format Title
        let frame = CGRect(x: (self.navigationController?.navigationBar.frame.width)!/4, y: -30, width: (self.navigationController?.navigationBar.frame.width)!/2, height: 100)
        titleLabel.frame = frame
        titleLabel.text = "In Progress"
        titleLabel.textColor = UIColor.background
        titleLabel.font = UIFont(name: "Helvetica", size: 38)
        titleLabel.layer.shadowColor = UIColor.background.cgColor
        titleLabel.layer.shadowRadius = 1.5
        titleLabel.layer.shadowOpacity = 0.25
        titleLabel.layer.shadowOffset = CGSize(width: 3, height: 3)
        
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            titleLabel.font = UIFont(name: "HelveticaNeue", size: 49)
        }

        self.navigationController?.navigationBar.addSubview(titleLabel)
            
        self.navigationController?.navigationBar.tintColor = UIColor.background
        

    }
    
    func configureCorrectViewController() {
        // Route the view depending if we have a premium user or not
        if userIsEntitled {
            configureHomeViewController()
        } else {
            configureFreeViewController()
        }
    }
        
        
    func configureHomeViewController() {
        let homeController = InProgressViewController()
        homeController.titleDelegate = self
        homeController.coordinator = coordinator

        view.addSubview(homeController.view)
        // Add the navigationController containing the HomeController as a child to the ContainerController
        addChild(homeController)
        // Home controller didMove to the parent container of self, which is the ContainerController
        homeController.didMove(toParent: self)
    }
    
    
    func configureFreeViewController() {
        let homeController = InProgressFreeViewController()
        homeController.titleDelegate = self
        homeController.coordinator = coordinator
        
        view.addSubview(homeController.view)
        // Add the navigationController containing the HomeController as a child to the ContainerController
        addChild(homeController)
        // Home controller didMove to the parent container of self, which is the ContainerController
        homeController.didMove(toParent: self)
    }
    
    
    /// Refresh after user upgrades
    @objc func refreshAfterSubscribe(_ notification: Notification) {
        configureHomeViewController()
    }
    
    
    func parseVideos(_ vc: InProgressViewController) -> Bool {
        
        guard let videos = coordinator?.dataManager?.getVideos() else { return false }
            print(videos.count)
            for video in videos {
                if video.hoopCount == 1 {
                    vc.data.single.append(video)
                }
            }
            
            for video in videos {
                if video.hoopCount == 2 {
                    vc.data.doubles.append(video)
                }
            }
            
            for video in videos {
                if video.hoopCount == 3 {
                    vc.data.multi.append(video)
                }
            }
            
            for video in videos {
                if video.hoopCount == 4 {
                    vc.data.quads.append(video)
                }
            
        }
        
        if videos.count != 0 {
            return true
        }
        
        return false
    }
        
        
}


// MARK: - Protocol Conformance

extension InProgressContainerController: Titleable {
        

    func changeTitleText(forTitle title: String) {
            
        titleLabel.text = title
        titleLabel.frame = CGRect(x: (self.navigationController?.navigationBar.frame.width)!/6, y: -30, width: (self.navigationController?.navigationBar.frame.width)!/6 * 4, height: 100)
        titleLabel.font = UIFont(name: "Helvetica", size: 39)
        titleLabel.layer.shadowColor = UIColor.black.cgColor
        titleLabel.layer.shadowRadius = 3.0
        titleLabel.layer.shadowOpacity = 0.5
        titleLabel.layer.shadowOffset = CGSize(width: 4, height: 4)
        titleLabel.layer.masksToBounds = false

    }
        
        
        
}


