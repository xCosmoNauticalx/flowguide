//
//  InProgressTableController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/22/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit
import CoreData

private let reuseIdentifier = "Cell"

class InProgressTableController: UITableViewController {

    
    override func loadView() {
        super.loadView()
        setUp()
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    private func setUp() {
        
        /// Table View
        self.tableView.backgroundColor = .clear
        self.tableView.register(ProgressCell.self, forCellReuseIdentifier: reuseIdentifier)
        self.tableView.register(ProgressHeader.self, forHeaderFooterViewReuseIdentifier: "Header")
        self.tableView.tableFooterView = UIView()
    }

    


}
