//
//  InProgressFreeViewController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/8/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class InProgressFreeViewController: UIViewController {
    
// MARK: - Properties
    
    /// Coordinators
    weak var coordinator: InProgressCoordinator?
    weak var titleDelegate: Titleable?
    
    /// Views
    var premiumView: PremiumView!
    
    let background: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "progressBackground")
        i.contentMode = .scaleToFill
        return i
    }()
    
    
// MARK: - Life Cycle

    override func loadView() {
        super.loadView()
        setUp()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        Utilities.styleTabBar(self, color: UIColor.Progress.light)
    }
    

// MARK: - Methods
    
    private func setUp() {
        
        /// Config PremiumView
        guard let premium = coordinator?.premium else { return }
        
        premiumView = PremiumView(color: UIColor.Progress.mid, premium: premium)
        
        /// Add Views
        view.addSubview(background)
        view.addSubview(premiumView)
        
        background.fillSuperView()
        
        switch UIScreen.main.traitCollection.userInterfaceIdiom {
        case .pad:
            premiumView.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 0, left: 150, bottom: 0, right: -150))
        default:
            premiumView.fillSuperView()
        }
        
    }

}
