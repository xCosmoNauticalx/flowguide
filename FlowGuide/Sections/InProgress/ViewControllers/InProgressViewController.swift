//
//  InProgressViewController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 1/14/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class InProgressViewController: UIViewController, Storyboarded {

// MARK: - Properties
    
    /// Coordinators
    weak var coordinator: InProgressCoordinator?
    weak var titleDelegate: Titleable?
    
    /// Data
    let data = ProgressData()
    var shouldShowTableView: Bool = false
    
    /// Views
    let background: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "progressBackground")
        i.contentMode = .scaleToFill
        return i
    }()
    
    let nothingView = NothingHereView(text: "When you hold down on a move's URL link, the video will be sent here, giving you easy access to pick up right where you left off", colors: [UIColor.Progress.mid.cgColor, UIColor.Progress.dark.cgColor])
    let tableView = InProgressTableController()
    let container = UIView()
    var gradientView = CAGradientLayer()
    

// MARK: - Life Cycle
    override func loadView() {
        super.loadView()
        setUp()
    }

    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        Utilities.styleTabBar(self, color: UIColor.Progress.light)
        setNeedsStatusBarAppearanceUpdate()
        
        /// Decide which view to show
        if !shouldShowTableView {
            shouldShowTableView = parseVideos()
        }
        
        showView()
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        .darkContent
    }
    
    

// MARK: - Methods

    private func setUp() {
        
        /// Notification Center
        NotificationCenter.default.addObserver(self, selector: #selector(refreshTable), name: .userDidSaveVideo, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(hideTable), name: .noMoreVideos, object: nil)
        
        /// Decide which view to show
        shouldShowTableView = parseVideos()
        
        /// Table View
        tableView.tableView.dataSource = data
        tableView.tableView.delegate = self
        data.dataManager = coordinator?.dataManager
        
        
        /// Add Views
        view.addSubview(background)
        background.fillSuperView()
        
        view.addSubview(nothingView)
        nothingView.anchor(top: nil, leading: view.leadingAnchor, bottom: nil, trailing: view.trailingAnchor)
        nothingView.centerYAnchor.constraint(equalTo: view.centerYAnchor, constant: -50).isActive = true
        nothingView.alpha = 0
        
        view.addSubview(container)
        container.addSubview(tableView.view)
        container.anchor(top: view.topAnchor, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor, padding: .init(top: 220, left: 20, bottom: 0, right: -20))
        tableView.view.fillSuperView()
        tableView.view.alpha = 0
        

        
    }
    
    
    @objc func refreshTable(_ notification: NSNotification) {
        if let video = notification.userInfo?["video"] as? Video {
            switch video.hoopCount {
            case 1:
                data.single.append(video)
            case 2:
                data.doubles.append(video)
            case 3:
                data.multi.append(video)
            default:
                data.quads.append(video)
            }
            tableView.tableView.reloadData()
        }
        
    }
    
    
    func showView() {
        
        if shouldShowTableView == false {
            nothingView.alpha = 1
            tableView.view.alpha = 0
        } else {
            tableView.view.alpha = 1
            nothingView.alpha = 0
        }
    }
    
    
    func parseVideos() -> Bool {
        
        guard let videos = coordinator?.dataManager?.getVideos() else { return false }
            print("Number of Videos found: \(videos.count)")
            for video in videos {
                if video.hoopCount == 1 {
                    data.single.append(video)
                }
            }
            
            for video in videos {
                if video.hoopCount == 2 {
                    data.doubles.append(video)
                }
            }
            
            for video in videos {
                if video.hoopCount == 3 {
                    data.multi.append(video)
                }
            }
            
            for video in videos {
                if video.hoopCount == 4 {
                    data.quads.append(video)
                }
            
        }
        
        if videos.count != 0 {
            return true
        }
        
        return false
    }
    
    
    @objc func hideTable() {
        
        UIView.animate(withDuration: 0.5, animations: {
            self.tableView.view.alpha = 0
            self.nothingView.alpha = 1
        })
        
    }

}

extension InProgressViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        print("View For Header")
        let header = tableView.dequeueReusableHeaderFooterView(withIdentifier: "Header") as! ProgressHeader
        
        switch section {
        case 0:
            header.label.text = "Single Hoop"
        case 1:
            header.label.text = "Doubles"
        case 2:
            header.label.text = "Multi"
        default:
            header.label.text = "Quads"
        }
        
    
        return header
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 50
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        75
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        guard let cell = tableView.cellForRow(at: indexPath) as? ProgressCell else { return }
        
        print("Cell URL: \(cell.url)")
        
        coordinator?.play(cell.url)
    }
    
    
}
