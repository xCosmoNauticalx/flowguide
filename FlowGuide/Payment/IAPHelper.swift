//
//  IAPHelper.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/28/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import StoreKit

public typealias ProductIdentifier = String
public typealias ProductsRequestCompletionHandler = (_ success: Bool, _ products: [SKProduct]?) -> Void

open class IAPHelper: NSObject  {
  
    private let productIdentifiers: Set<ProductIdentifier>
    // Tracks which itms have been purchased
    private var purchasedProductIdentifiers: Set<ProductIdentifier> = []
    // These two are used by the SKProductRequest delegate to perform requests to Apple servers.
    private var productsRequest: SKProductsRequest?
    private var productsRequestCompletionHandler: ProductsRequestCompletionHandler?
  
    // An IAPHelper instance is created by passing in a set of product identifiers
    public init(productIds: Set<ProductIdentifier>) {
        productIdentifiers = productIds
        super.init()
        // Add self as observer for payment transactions
        SKPaymentQueue.default().add(self)
    }
  
}

// MARK: - StoreKit API

extension IAPHelper {
  
    /// Request products
    public func requestProducts(dispatch: DispatchGroup, _ completionHandler: @escaping ProductsRequestCompletionHandler) {
        dispatch.enter()
        productsRequest?.cancel()
        // Saves the user's completion handler for future execution
        productsRequestCompletionHandler = completionHandler
    
        // Creates & initiates a request to Apple via an SKProductRequest object.
        productsRequest = SKProductsRequest(productIdentifiers: productIdentifiers)
        productsRequest!.delegate = self
        productsRequest!.start()
        dispatch.leave()
    }

    public func buyProduct(_ product: SKProduct) {
    
        print("Buying \(product.productIdentifier)...")
        // Creates a payment object
        let payment = SKPayment(product: product)
        // Adds it to the payment queue. Utilizes a singleton called .default()
        SKPaymentQueue.default().add(payment)
    
    }

    /// Check if a product has been purchased already
    public func isProductPurchased(_ productIdentifier: ProductIdentifier) -> Bool {
        return purchasedProductIdentifiers.contains(productIdentifier)
    }
  
    public class func canMakePayments() -> Bool {
        // Product cells should behave differently depending on the value returned by canMakePayments()
        // Ex. If it returns false, then the buy button should not be shown and the price should be replaced with "Not Available"
        return SKPaymentQueue.canMakePayments()
    }
  
    public func restorePurchases() {
    
        SKPaymentQueue.default().restoreCompletedTransactions()
    
    }
}


// MARK: - SKProductsRequestDelegate

/// Used to get a list of products, their titles, descriptions & prices from Apple's servers by implementing the two methods required
extension IAPHelper: SKProductsRequestDelegate {
  
    /// Called when the list is successfully retrieved.
    public func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse) {
    
        print("Loaded list of products...")
        // Recieves an array of SKProduct objects
        let products = response.products
        // Passes them to the previously saved completion handler. The handler reloads the table with new data
        productsRequestCompletionHandler?(true, products)
        // When finished, clear the request and completion handler
        clearRequestAndHandler()
    
        for p in products {
            print("Found product: \(p.productIdentifier) \(p.localizedTitle) \(p.price.floatValue)")
        }
    
  }
  
  
    /// If a problem occurs, this method is called
    public func request(_ request: SKRequest, didFailWithError error: Error) {
    
        print("Failed to load list of products.")
        print("Error: \(error.localizedDescription)")
        productsRequestCompletionHandler?(false, nil)
        // When finished, clear the request and completion handler
        clearRequestAndHandler()
    
    }
  
  
    private func clearRequestAndHandler() {
        productsRequest = nil
        productsRequestCompletionHandler = nil
    }
  
}


// MARK: - SKPaymentTransactionObserver

extension IAPHelper: SKPaymentTransactionObserver {
  
    /// Required by protocol, gets called when one or more transaction states change.
    public func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
    
        // Evaluates the state of each transaction in an array of updated transactions and calls the relevant helper method
        for transaction in transactions {
            switch transaction.transactionState {
            case .purchased:
                complete(transaction: transaction)
                break
            case .failed:
                fail(transaction: transaction)
                break
            case .restored:
                restore(transaction: transaction)
                break
            case .deferred:
                break
            case .purchasing:
                break
            default:
                break
            }
        }
    
    }
  
  
      private func complete(transaction: SKPaymentTransaction) {
        print("Complete...")
        deliverPurchaseNotificationFor(identifier: transaction.payment.productIdentifier)
        // Mark transaction as finished
        SKPaymentQueue.default().finishTransaction(transaction)
      }
  
  
      private func restore(transaction: SKPaymentTransaction) {
        guard let productIdentifier = transaction.original?.payment.productIdentifier else { return }
        print("Restore... \(productIdentifier)")
        deliverPurchaseNotificationFor(identifier: productIdentifier)
        // Mark transaction as finished
        SKPaymentQueue.default().finishTransaction(transaction)
      }
  
  
      private func fail(transaction: SKPaymentTransaction) {
        print ("Fail...")
        if let transactionError = transaction.error as NSError?, let localizedDescription = transaction.error?.localizedDescription, transactionError.code != SKError.paymentCancelled.rawValue {
          print("Transaction Error: \(localizedDescription)")
        }
        
        SKPaymentQueue.default().finishTransaction(transaction)
      }
  
      /// Adds the purchase to the set of purchases and saves the identifier in UserDefaults (Again, don't save this in UserDefaults)
      private func deliverPurchaseNotificationFor(identifier: String?) {
        guard let identifier = identifier else { return }
        
        NotificationCenter.default.post(name: .purchaseNotification, object: identifier)
      }
  
}

