//
//  FlowGuideProducts.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/28/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation

public struct FlowGuideProducts {
  
  public static let Premium = "com.CosmoNautical.FlowGuide.Premium"
  
  private static let productIdentifiers: Set<ProductIdentifier> = [FlowGuideProducts.Premium]

  public static let store = IAPHelper(productIds: FlowGuideProducts.productIdentifiers)
}

func resourceNameForProductIdentifier(_ productIdentifier: String) -> String? {
  return productIdentifier.components(separatedBy: ".").last
}
