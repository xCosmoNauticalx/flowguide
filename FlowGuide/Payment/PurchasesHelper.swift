//
//  Purchases.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/1/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import Purchases


class PurchasesHelper {
    
    /// Get Offerings
    static func getOfferings(dispatch: DispatchGroup, completed: @escaping (Purchases.Offering) -> Void) {
        
        var offering = Purchases.Offering()
        
        dispatch.enter()
        Purchases.shared.offerings { (offerings, error) in
            if let error = error {
                print("Error getting offerings: \(error)")
            } else {
                guard let o = offerings?.current else { return }
                offering = o
            }
            dispatch.leave()
        }
        
        dispatch.notify(queue: .main, execute: {
            completed(offering)
            print("Got Offering")
        })
        
    }
    
    
    static func purchase(_ package: Purchases.Package) {
        
        Purchases.shared.purchasePackage(package) { (transaction, purchaserInfo, error, cancelled) in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
            
            if purchaserInfo?.entitlements["Premium"]?.isActive == true {
                NotificationCenter.default.post(name: .purchaseNotification, object: nil)
            }
        }
        
    }
    
    
    static func restorePurchase(_ vc: UIViewController) {
        Purchases.shared.restoreTransactions { (purchaserInfo, error) in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
            
            if purchaserInfo?.entitlements["Premium"]?.isActive == true {
                NotificationCenter.default.post(name: .purchaseNotification, object: nil)
            } else {
                // User was never subscribed, let them know
                let alert = UIAlertController(title: "No Previous Purchases to Restore", message: nil, preferredStyle: .alert)
                let ok = UIAlertAction(title: "OK", style: .default, handler: nil)
                alert.addAction(ok)
                vc.present(alert, animated: true, completion: nil)
            }
        }
    }
    
    
    static func isUserEntitled() -> Bool {
        
        var entitled = false
        
        Purchases.shared.purchaserInfo { (purchaserInfo, error) in
            if let error = error {
                print("Error: \(error.localizedDescription)")
            }
            
            if purchaserInfo?.entitlements["Premium"]?.isActive == true {
                entitled = true
            }
        }
        
        return entitled
        
    }
    
    
    
    
    
}
