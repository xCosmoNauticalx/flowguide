//
//  Tricks.swift
//  Flow Guide
//
//  Created by Kelsey Garcia on 9/29/19.
//  Copyright © 2019 Kelsey Garcia. All rights reserved.
//

import Foundation

class Node: NSObject {
    
    let name: String
    var children: [Node] = []
    //weak var parent: Node?
    
    // Add property that creates the button
    
    init(name: String, learned: Bool = false) {
        
        self.name = name
        
    }
    
    func add(child: Node) {
        
        children.append(child)
        //child.parent = self           Blocking this because child view controllers should not know about their parents
        
    }
    
    
}

//extension Node: CustomStringConvertible {
// Using this extension to print out the tree
    
    //var description: String {
        
        //var text = "\(name)"
        
        //if !children.isEmpty {
            //text += " {" + children.map { $0.description }.joined(separator: ", ") + "} "
        //}
        
        //return text
        
    //}
    
//}







// MARK: Creating Nodes

// Root
let tutorials = Node(name: "Tutorials")


// MARK: Level 1                                          ////

let single = Node(name: "Single Hoop")
let doubles = Node(name: "Doubles")
let multiHoops = Node(name: "Multi-Hoops")
let foundation = Node(name: "Hooping Foundations")
let misc = Node(name: "Miscellaneous")
let beginners = Node(name: "Beginner Tips")

let tutorialsArray = [single, doubles, multiHoops, foundation, misc, beginners]


// MARK: Level 2                                          ////    ////

    // Single
    let onBody = Node(name: "On Body")
    let offBody = Node(name: "Off Body")
    let isolations = Node(name: "Isolations")
    let wrapHingeFold = Node(name: "Wrap, Hinge & Folds")

    let singleArray = [onBody, offBody, isolations, wrapHingeFold]

    // Doubles
    let doublesBasics = Node(name: "Doubles Basics")
    let beatWeaves = Node(name: "Beat Weaves")
    let weaves = Node(name: "Weaves")
    let hipReels = Node(name: "Hip Reels")
    let shoulderReels = Node(name: "Shoulder Reels")
    let extendedReels = Node(name: "Extended Reels")
    let threadTheNeedle = Node(name: "Thread the Needle")
    let turns = Node(name: "Turns")
    let rolls = Node(name: "Rolls")
    let elbow = Node(name: "Elbow")
    let butterflies = Node(name: "Butterflies")
    let sphereGlobe = Node(name: "Sphere/Globe")
    let horizontalPlane = Node(name: "Horizontal Plane")
    let threeD = Node(name: "3D (Both Horizontal & Vertical Planes")
    let dbhingeFold = Node(name: "Hinge & Fold")
    let escalatorWedgie = Node(name: "Escalators & Wedgies")
    let antiSpinInSpin = Node(name: "Anti-Spin & In-Spin")
    let caps = Node(name: "CAPS (Continuous Assembly Pattern)")
    let wheelPlane = Node(name: "Wheel Plane")
    let wallPlane = Node(name: "Wall Plane")
    let twoHoopsOneHand = Node(name: "2 Hoops, 1 Hand")
    let ochos = Node(name: "Ochos")
    let linkedHoops = Node(name: "Linked Hoops")
    let breaks = Node(name: "Breaks")
    let dhMisc = Node(name: "Miscellaneous")
    let twinIsolations = Node(name: "Twin Isolations")
    let hybridVS = Node(name: "Hybrid/VS")

    let doublesArray = [doublesBasics, beatWeaves, weaves, hipReels, shoulderReels, extendedReels, threadTheNeedle, turns, rolls, elbow, butterflies, sphereGlobe, horizontalPlane, threeD, dbhingeFold, escalatorWedgie, antiSpinInSpin, caps, wheelPlane, wallPlane, twoHoopsOneHand, ochos, linkedHoops, breaks, dhMisc, twinIsolations, hybridVS]

    // Multi Hoops
    let multiHoopsGeneral = Node(name: "Multi Hoop Basics")
    let twoHoops = Node(name: "2 Hoops")
    let threeHoops = Node(name: "3 Hoops")
    let fourHoops = Node(name: "4 Hoops")
    let fiveHoops = Node(name: "5 Hoops")

    let multiHoopsArray = [multiHoopsGeneral, twoHoops, threeHoops, fourHoops, fiveHoops]

    // Hooping Foundation
    let tosses = Node(name: "Tosses")
    let spins = Node(name: "Spins")
    let passing = Node(name: "Passing")
    let concept = Node(name: "Hooping Concepts")

    let foundationArray = [tosses, spins, passing, concept]


// MARK: Level 3                                          ////    ////    ////

    // Single

        // On Body
        let legKneeFoot = Node(name: "Leg/Knee/Foot")
        let waist = Node(name: "Waist")
        let chestShoulder = Node(name: "Chest/Shoulder")
        let chestShoulderVertical = Node(name: "Chest/Shoulder Vertical")
        let neckHeadNose = Node(name: "Neck/Head/Nose")

        let onBodyArray = [legKneeFoot, waist, chestShoulder, chestShoulderVertical, neckHeadNose]

        // Off Body
        let horizontal = Node(name: "Horizontal")
        let vertical = Node(name: "Vertical")

        let offBodyArray = [horizontal, vertical]

        // Isolations
        let isoIsolations = Node(name: "Isolations")
        let oneHandIsolations = Node(name: "One Hand Isolations")
        let twoHandIsolations = Node(name: "Two Hand Isolations")
        let verticalPlaneIsolations = Node(name: "Vertical Plane Isolations")
        let horizontalPlaneIsolations = Node(name: "Horizontal Plane Isolations")
        let threeDIsolations = Node(name: "3D Isolations")
        let barrelRolls = Node(name: "Barrel Rolls")
        let ghostingTracing = Node(name: "Ghosting/Isolations")
        let outsideGrip = Node(name: "Outside Grip")
        let behindTheBack = Node(name: "Behind the Back")
        let miscIsolations = Node(name: "Miscellaneous")
        let otherIsolations = Node(name: "Combos/Transitions")


        let isolationsArray = [isoIsolations, oneHandIsolations, twoHandIsolations, verticalPlaneIsolations, horizontalPlaneIsolations, threeDIsolations, barrelRolls, ghostingTracing, outsideGrip, behindTheBack, miscIsolations, otherIsolations]

        // Wrap, Hinge & Fold
        let escalator = Node(name: "Escalator")
        let wedgies = Node(name: "Wedgies & Circus Starts")
        let wraps = Node(name: "Wraps")
        let hingeFold = Node(name: "Hinge & Fold")

        let wrapHingeFoldArray = [escalator, wedgies, wraps, hingeFold]

    // Doubles

        // Beat Weaves
        let noBeatWeave = Node(name: "No Beat Weave")
        let oneAndHalfBeatWeave = Node(name: "1.5 Beat Weave")
        let twoBeatWeave = Node(name: "2 Beat Weave")
        let twoBeatChaseWeave = Node(name: "2 Beat/Chase Weave")
        let btbTwoThreeBeatWeave = Node(name: "2 & 3 BTB Weaves") // There's two of these, add all URLs to this one
        let threeBeatWeave = Node(name: "3 Beat Weave")
        let reverseThreeBeatWeave = Node(name: "Reverse 3 Beat Weave")
        let threeBeatWeaveExtentension = Node(name: "3 Beat Weave w/ Extensions")
        let threeBeatWeaveLockout = Node(name: "3 Beat Weave w/ Lockouts")
        let threeBeatWeaveHandRoll = Node(name: "3 Beat Weave w/ Hand Roll")
        let threeBeatWeaveBTBReverse = Node(name: "3 Beat Weave BTB Reverse")
        let threeBeatWeaveBackRoll = Node(name: "3 Beat Weave with Back Roll")
        let threeBeatWeaveBreak180Turn = Node(name: "3 Beat Weave w/ Break & 180 Turn")
        let threeBeatWeaveSplit = Node(name: "3 Beat Split Weave")
        let threeBeatWeaveSweeping = Node(name: "3 Beat Weave Sweeping/Extended/Archer")
        let threeBeatWeavePalmSpin = Node(name:"3 Beat Weave w/ Palm Spin")
        let threeBeatWeaveEggbeater = Node(name: "3 Beat Eggbeater Weave")
        let threeBeatWeaveToss = Node(name: "3 Beat Weave Toss (5 Beat Weave Variation)")
        let threeBeatWeaveHug = Node(name: "3 Beat Weave to Hug/Crossover Transition")
        let threeBeatWeavePendulum = Node(name: "3 Beat Weave w/ Pendulums")
        let threeBeatTwoHoopsOneHand = Node(name: "3 Beat Weave to 2 Hoops 1 Hand")
        let threeBeatWeaveTracers = Node(name: "3 Beat Weave Tracers")
        let forwardToReverseThreeBeatWeaveTracers = Node(name: "Forward to Reverse 3 Beat Weave Tracers")
        let fiveBeatWeave = Node(name: "5 Beat Weave")
        let fiveBeatWeaveThreadedJumpThru = Node(name: "5 Beat Weave Threaded Jump Thru")
        let beatTracerWeave = Node(name: "Tracer Weave")
        let infinityBeatWeave = Node(name: "Infinity Beat Weave")

        let beatWeavesArray = [noBeatWeave, oneAndHalfBeatWeave, twoBeatWeave, twoBeatChaseWeave, btbTwoThreeBeatWeave, threeBeatWeave, reverseThreeBeatWeave, threeBeatWeaveExtentension, threeBeatWeaveLockout, threeBeatWeaveHandRoll, threeBeatWeaveBTBReverse, threeBeatWeaveBackRoll, threeBeatWeaveBreak180Turn, threeBeatWeaveSplit, threeBeatWeaveSweeping, threeBeatWeavePalmSpin, threeBeatWeaveEggbeater, threeBeatWeaveToss, threeBeatWeaveHug, threeBeatWeavePendulum, threeBeatTwoHoopsOneHand, threeBeatWeaveTracers, forwardToReverseThreeBeatWeaveTracers, fiveBeatWeave, fiveBeatWeaveThreadedJumpThru, beatTracerWeave, infinityBeatWeave]

        // Weaves
        let butterflySplitWeave = Node(name: "Butterfly/Split Weave")
        let fountain = Node(name: "Fountain")
        let sweepingExtendedArcherWeave = Node(name: "Sweeping/Extended Archer Weave")
        let juggleWeave = Node(name: "Juggle Weave")
        let dumboWeave = Node(name: "Dumbo Weave")
        let horizontalOverVerticalFlip = Node(name: "Horizontal Over Vertical Flip")
        let weaveJumpThru = Node(name: "Weave Jump Through")
        let jumpThruAndToss = Node(name: "Jump Through & Toss")
        let neckWompIntoThreeBeat = Node(name: "Neck Womp Into 3 Beat Weave")
        let btbSpins = Node(name: "BTB Spins")
        let weaveBTBCrazy8 = Node(name: "Weave BTB to Crazy 8")
        let underLegCrazy8 = Node(name: "Under the Leg Crazy 8 Weave")
        let underLegWeave = Node(name: "Under Leg Weave")
        let crissCrossToss = Node(name: "Criss-Cross & Toss")
        let hugulator3000 = Node(name: "Hugulator 3000")
        let hugCrosserWeave = Node(name: "Hug/Crosser Weave")
        let hugCrosserWeaveTransitions = Node(name:"Hug/Crosser Weave Transitions")
        let chestRollHugTransition = Node(name: "Chest Roll to Hug Weave Transition")
        let crossedArmWeave = Node(name: "Crossed Arm Weave")
        let isolationFoldWeave = Node(name: "Isolation Fold Weave")
        let tracerWeave = Node(name: "Tracer Weave")
        let tracerFoldWeave = Node(name: "Tracer Fold Weaves")
        let doubleSmearWeave = Node(name: "Double Smear Weave")
        let waistWrapWeave = Node(name: "Waist Wrap")
        let waistWrapThreeBeatTransition = Node(name: "Waist Wrap to 3 Beat Transition")
        let waistWrapBTBWeave = Node(name: "Waist Wrap BTB")
        let waistWrapBTBWeaveTurn = Node(name: "Waist Wrap BTB 360 Turn")
        let meltdown = Node(name: "Meltdown")
        let hingeWeave = Node(name: "Hinge Weave")

        let weavesArray = [butterflySplitWeave, fountain, sweepingExtendedArcherWeave, juggleWeave, dumboWeave, horizontalOverVerticalFlip, weaveJumpThru, jumpThruAndToss, neckWompIntoThreeBeat, btbSpins, weaveBTBCrazy8, underLegCrazy8, underLegWeave, crissCrossToss, hugulator3000, hugCrosserWeave, hugCrosserWeaveTransitions, chestRollHugTransition, crossedArmWeave, isolationFoldWeave, tracerWeave, tracerFoldWeave, doubleSmearWeave, waistWrapWeave, waistWrapThreeBeatTransition, waistWrapBTBWeave, waistWrapBTBWeaveTurn, meltdown, hingeWeave]


        // Hip Reels
        let reels = Node(name: "Reels")
        let reelSameWay = Node(name: "Same Way")
        let reelSplit = Node(name: "Split Reels (The Wave)")
        let reelBackwards = Node(name: "Backward Reels")
        let reelBTB = Node(name: "BTB Reels")
        let reelTurn = Node(name: "Reels & Turns")

        let hipReelsArray = [reels, reelSameWay, reelSplit, reelBackwards, reelBTB, reelTurn]

        // Shoulder Reels
        let windmill = Node(name: "Windmill")
        let windmillTracers = Node(name: "Windmill Tracers")
        let windmillContinuousExtensions = Node(name: "Windmill w/ Continuous Extensions")
        let shoulderReelSplit = Node(name: "Split Reels (The Sweep)")

        let shoulderReelsArray = [windmill, windmillTracers, windmillContinuousExtensions, shoulderReelSplit]

        // Extended Reels
        let extendedReel = Node(name: "Extended Reels")
        let extendedBodyReelForward = Node(name: "Extended Body Reel (Whip Isolation) Forward")
        let extendedBodyReelReverse = Node(name: "Whip Isolation Reverse")
        let extendedBodyReelAddedBeats = Node(name: "Whip Isolation w/ Added Beats")

        let extendedReelsArray = [extendedReel, extendedBodyReelForward, extendedBodyReelReverse, extendedBodyReelAddedBeats]

        // Thread The Needle
        let ttn = Node(name: "Thread the Needle")
        let ttnVariations = Node(name: "Thread the Needle Variations")
        let ttnVerticalCatEyeVSExtension = Node(name: "Vertical Cat Eye VS Extension")
        let ttnBTBBreaks = Node(name: "TTN BTB Breaks")
        let ttnQuadrupleBreaks = Node(name: "Quadruple Breaks")
        let ttnUnderhandTickTockBreaks = Node(name: "Underhand Tick Tock Breaks")
        let ttnInsideOut = Node(name: "Inside Out")
        let ttnShoulderTakeOuts = Node(name: "Shoulder Take Outs")
        let ttnPalmSpinTakeOut = Node(name: "Palm Spin Take Out")
        let ttnUnderhandBackward = Node(name: "Underhand/Backward")
        let ttnUnderhandBackwardVariations = Node(name: "Underhand Backward Variations")
        let ttnUnderhandBackwardRolls = Node(name: "Underhand/Backward w/ Rolls")
        let ttnLinearIsolation = Node(name: "TTN w/ Linear Isolations")
        let ttnCombos = Node(name: "TTN Combos")
        let ttnPendulum = Node(name: "TTN Pendulum")
        let ttnShoulderRoll = Node(name: "TTN w/ Shoulder Roll to Opposite Hand")

        let threadTheNeedleArray = [ttn, ttnVariations, ttnVerticalCatEyeVSExtension, ttnBTBBreaks, ttnQuadrupleBreaks, ttnUnderhandTickTockBreaks, ttnInsideOut, ttnShoulderTakeOuts, ttnPalmSpinTakeOut, ttnUnderhandBackward, ttnUnderhandBackwardVariations, ttnUnderhandBackwardRolls, ttnLinearIsolation, ttnCombos, ttnPendulum, ttnShoulderRoll]

        // Turns
        let basicForwardReverse = Node(name: "Basic Forward & Reverse")
        let oppositeDirections = Node(name: "Opposite Directions")
        let crossArms = Node(name: "Cross Arms")
        let underTheLeg = Node(name: "Under the Leg")
        let turn180 = Node(name: "180 Turn")
        let threeBeatWeaveTurnReverse = Node(name: "3 Beat Weave 180 Turn to Reverse 3 Beat")
        let threeBeatWeaveBreakTurn = Node(name: "3 Beat Weave w/ Break & Turn")
        let weave360Turn = Node(name: "Weave/360 Turn")
        let elbowCrank = Node(name: "Elbow Crank")
        let turnTwistFold180 = Node(name: "Twist & Fold 180 Turn")
        let capsTurn = Node(name: "CAPS")
            // threeDQTAtomicTurns (Doubles: 3D)

        let turnsArray = [basicForwardReverse, oppositeDirections, crossArms, underTheLeg, turn180, threeBeatWeaveTurnReverse, threeBeatWeaveBreakTurn, weave360Turn, elbowCrank, turnTwistFold180, capsTurn, threeDQTAtomicTurns]

        // Rolls
        let chestRolls = Node(name: "Chest Rolls")
        let chestRollGlobeToss = Node(name: "Chest Roll Globe Toss")
        let shoulderRollWeave = Node(name: "Shoulder Roll to Weave")
        let backRolls = Node(name: "Back Rolls")
        let ochoSwitchChestRoll = Node(name: "Ocho Switch to Chest Roll")
        let boomerangRoll = Node(name: "Boomerang Roll")

        let rollsArray = [chestRolls, chestRollGlobeToss, shoulderRollWeave, backRolls, ochoSwitchChestRoll, boomerangRoll]

        // Elbow
        let elbowSliders = Node(name: "Elbow Sliders")
        let elbowFrogKickBreaks = Node(name: "ELbow Frog Kick Breaks")
        let alternatingArms = Node(name: "Alternating Arms")

        let elbowArray = [elbowSliders, elbowFrogKickBreaks, alternatingArms]

        // Butterflies
        let butterfly = Node(name: "Butterfly (2 Hoops, 1 Hand")
        let butterflyToss = Node(name: "Butterfly Toss")
        let isolationButterfly = Node(name: "Isolation Butterfly")
        let jumpThruButterfly = Node(name: "Jump Thru to Butterfly")
        let butterflyWhomp = Node(name: "Butterfly Whomp")
        let butterflop = Node(name: "Butterflop")
        let splitTimeButterfly = Node(name: "Split Time Butterfly")
            // butterflySplitWeave (Doubles: Weaves)
        let flutterby = Node(name: "Fllutterby")
        let flipFlerpFlutterby = Node(name: "Flip Flerp Flutterby")
        let butterflyTossUp = Node(name: "Butterfly w/ Toss up")


        let butterfliesArray = [butterfly, butterflyToss, isolationButterfly, jumpThruButterfly, butterflyWhomp, butterflop, splitTimeButterfly, butterflySplitWeave, flutterby, flipFlerpFlutterby, butterflyTossUp]

        // Sphere/Globe
        let sphereTwirling = Node(name: "The Sphere-Get In & Twirling")
        let kickUpIntoSphere = Node(name: "Kick Up Into Sphere")
        let foldingSphere = Node(name: "Folding Sphere")
            // chestRollGlobeToss (Doubles: Rolls)
        let pickUpTheWorld = Node(name: "Pick Up the World")
        let lassoToSphere = Node(name: "Lasso to Sphere")
        let sphereFlipping = Node(name: "Sphere Flipping")
        let sphereCombo = Node(name: "Sphere Combo")

        let sphereGlobeArray = [sphereTwirling, kickUpIntoSphere, foldingSphere, chestRollGlobeToss, pickUpTheWorld, lassoToSphere, sphereCombo]

        // Horizontal Plane
        let corkscrew = Node(name: "Corkscrew & Variations")
            // lassoToSphere (Doubles: Sphere/Globe)
        let pizzaToss = Node(name: "Pizza Toss")
        let shoulderDuckOut = Node(name: "Shoulder Duck Out")
        let revolvingDoor = Node(name: "Revolving Door")
        let foldicopter = Node(name: "Foldicopter")
        let ochocopter = Node(name: "Ochocopter")
        let legocopter = Node(name: "Legocopter")
        let twinin8tors = Node(name: "Twinin8tors")
        let twinLassos = Node(name: "Twin Lassos")
        let doublesOnBody = Node(name: "On Body")
        let wristTwistTurn = Node(name: "(Wrist) Twist & Turn")
        let buzzsawCAPHorizontalPlaneBreak = Node(name: "Buzzsaw CAP to Horizontal Plane Break")

        let horizontalPlaneArray = [corkscrew, lassoToSphere, pizzaToss, shoulderDuckOut, revolvingDoor, foldicopter, ochocopter, legocopter, twinin8tors, twinLassos, doublesOnBody, wristTwistTurn, buzzsawCAPHorizontalPlaneBreak]

        // 3D
        let atomicthreeBeatWeave = Node(name: "Atomic 3 Beat Weave")
        let atomicSmear = Node(name: "Atomic Smear")
        let atomicFold = Node(name: "Atomic Fold")
        let threeDPlaneSplitTimeAntiSpinFlower = Node(name: "3D Plane Split Time Anti-spin Flower")
            //buzzsawCAPHorizontalPlaneBreak (Doubles: Horizontal Plane)
        let isolationFoldPush = Node(name: "Isolation Fold & Push")
        let threeDTracers = Node(name: "3D Tracers")
        let threeDStallChaser = Node(name: "3D Stall Chaser")
        let haloVSSmear = Node(name: "Halo VS Smear")
        let threeDTwist = Node(name: "3D Twist")
        let toroidTriangle = Node(name: "Toroid Triangle")
        let threeDQTAtomicTurns = Node(name: "3D QT Atomic Turns")
        let threeDPlaneSplitTimeAniSpinFlower = Node(name: "3D Plane Split Time Anti-Spin Flower")

        let threeDArray = [atomicthreeBeatWeave, atomicSmear, atomicFold, threeDPlaneSplitTimeAntiSpinFlower, buzzsawCAPHorizontalPlaneBreak, isolationFoldPush, threeDTracers, threeDStallChaser, haloVSSmear, threeDTwist, toroidTriangle, threeDQTAtomicTurns]

        // Hinge & Fold
        let waterfall = Node(name: "Waterfall")
        let mothFolds = Node(name: "Moth Folds")
        let indigoFolds = Node(name: "Indigo Folds")
            // foldicopter (Doubles: Horizontal Plane)
        let foldyFoldySteppyUnwravel = Node(name: "Foldy Foldy Steppy Unwravel")
        let planetaryFold = Node(name: "Planetary Fold")
        let transformer = Node(name: "The Transformer")
            // isolationFoldWeave (Doubles: Weaves)
        let hingeSmearTechMess = Node(name: "Hinge Smear Tech Mess")
        let barrelRollWithFolds = Node(name: "Barrel Roll With Folds")
        let foldsOneHandSmear = Node(name: "Folds & One Hand Smear/Coin Spin")
        let foldToChestCross = Node(name: "Fold to Chest Cross")

        let hingeFoldArray = [waterfall, mothFolds, indigoFolds, foldicopter, foldyFoldySteppyUnwravel, planetaryFold, transformer, isolationFoldWeave, hingeSmearTechMess, barrelRollWithFolds, foldsOneHandSmear, foldToChestCross]

        // Escalators & Wedgies
        let twinEscalatorTricks = Node(name: "Twin Escalator Tricks")
        let flourishIntoTwinEscalator = Node(name: "Flourish Into Twin Escalator")
        let continuousEscalator = Node(name: "Continuous Escalator")
        let continuousFoldingDownEscalator = Node(name: "Continuous Folding Down Escalator")
        let downEscalatorBTBPass = Node(name: "Down Escalator w/ BTB Pass")
        let bodyWrapChinCatchEscalator = Node(name: "Body Wrap Chin Catch to Escalator")
        let escalatorToWedgie = Node(name: "Escalator to Wedgie")
        let twinWedgie = Node(name: "Twin Wedgie")
        let twinContinuousWedgieKickUp = Node(name: "Twin Continuous Wedgie Kick Up")
        let wedgieWalk = Node(name: "Wedgie Walk")
        let twinWedgieToeFlick = Node(name: "Twin Wedgie Flourish Toe Kick")
        let wedgieVSIsolation = Node(name: "Wedgie VS Isolation")
        let twinWedgieTransitions = Node(name: "Twin Wedgie Transitions")
        let twinWedgieCalfBreaks = Node(name: "Twin Wedgie Calf Breaks")
        let twinFoldingEscalatorWedgie = Node(name: "Twin Folding Escalator to Wedgie")
        let neckSplitEscalator = Node(name: "Neck Split Escalator")

        let escalatorWedgieArray = [twinEscalatorTricks, flourishIntoTwinEscalator, continuousEscalator, continuousFoldingDownEscalator, downEscalatorBTBPass, bodyWrapChinCatchEscalator, escalatorToWedgie, twinWedgie, twinContinuousWedgieKickUp, wedgieWalk, twinWedgieToeFlick, wedgieVSIsolation, twinWedgieTransitions, twinWedgieCalfBreaks, twinFoldingEscalatorWedgie, neckSplitEscalator]

        // Anti-Spin & In-Spin
        let antiSpinFlowers = Node(name: "Anti-Spin Flowers")
        let antiSpinFlowerOneHand = Node(name: "Anti-Spin Flower (One Hand)")
        let antiSpinIsoFlower = Node(name: "Anti-Spin Iso Flower")
        let wallPlane3PetalFlower = Node(name: "Wall Plane: 3 Petal Anti-Spin Flowers")
        let wallPlane4PetalFlower = Node(name: "Wall Plane: 4 Petal Anti-Spin Flowers")
        let splitOppFlower4Petal = Node(name: "4 Petal Split Opp Flower w/ BTH Tracer")
            // threeDPlaneSplitTimeAniSpinFlower (Doubles: 3D)
        let triquetrasWallPlaneSTSD = Node(name: "Triquetras: Wall Plane Split Time Same Direction")
        let barrelRollMinis = Node(name: "Barrel Roll w/ Minis")
        let quarterTimeAntiSpin = Node(name: "Quarter Time Anti-Spin")
        let inSpin = Node(name: "In-Spin")
        let splitOppInSpin2Petal = Node(name: "2 Petal Split Opp In-Spin")
        let antiSpin4PetalVSInSpin2Petal = Node(name: "4 Petal Anti-Spin VS 2 Petal In-Spin")
        let antiSpinVSExtension = Node(name: "Anti-Spin VS Extension")
        let transitionWheelFlowerWallCAPS = Node(name: "Transition: Wheel Flowers to Wall CAPs")
        let fourPetalFlowerTriangle = Node(name: "4 Petal Flower Triangle Pattern")

        let antiSpinInSpinArray = [antiSpinFlowers, antiSpinFlowerOneHand, antiSpinIsoFlower, wallPlane3PetalFlower, wallPlane4PetalFlower, splitOppFlower4Petal, threeDPlaneSplitTimeAniSpinFlower, triquetrasWallPlaneSTSD, barrelRollMinis, quarterTimeAntiSpin, inSpin, splitOppInSpin2Petal, antiSpin4PetalVSInSpin2Petal,antiSpinVSExtension, transitionWheelFlowerWallCAPS, fourPetalFlowerTriangle]

        // CAPS
        let fullDiscussionCAPS = Node(name: "Full Discussion")
        let wheelPlaneCAPS = Node(name: "Wheel Plane")
        let wallPlaneCAPS = Node(name: "Wall Plane")
        let transitionWheelFlowersWallCAPS = Node(name: "Transition: Wheel Flowers to Wall CAPs")

        let capsArray = [fullDiscussionCAPS, wheelPlaneCAPS, wallPlaneCAPS, transitionWheelFlowerWallCAPS]

        // Two Hoops, One Hand












/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////





func addChildren () {
    
// Level 1
    for item in tutorialsArray {
        
        tutorials.add(child: item)
        
    }
    
//Level 2
    
    // Single
    for item in singleArray {
        
        single.add(child: item)
        
    }
    
    // Doubles
    for item in doublesArray {
        
        doubles.add(child: item)
        
    }
    
    // Multi Hoops
    for item in multiHoopsArray {
        
        multiHoops.add(child: item)
        
    }
    
    // Hooping Foundation
    for item in foundationArray {
        
        foundation.add(child: item)
        
    }
    
    
    
    
// Level 3
    
    // Single
    
        // On Body
        for item in onBodyArray {
        
            onBody.add(child: item)
        
        }
    
        // Off Body
        for item in offBodyArray {
        
            offBody.add(child: item)
        
        }
    
        // Isolations
        for item in isolationsArray {
        
            isolations.add(child: item)
        
        }
    
        // Wrap, Hinge & Fold
        for item in wrapHingeFoldArray {
        
            wrapHingeFold.add(child: item)
        
        }
    
    // Doubles
    
        // Beat Weaves
        for item in beatWeavesArray {
            
            beatWeaves.add(child: item)
        
        }
    
        // Weaves
        for item in weavesArray {
        
            weaves.add(child: item)
        
        }
    
        // Hip Reels
        for item in hipReelsArray {
        
            hipReels.add(child: item)
        
        }
    
        // Shoulder Reels
        for item in shoulderReelsArray {
        
            shoulderReels.add(child: item)
        
        }
    
        // Extended Reels
        for item in extendedReelsArray {
        
            extendedReels.add(child: item)
        
        }
    
        // Thread The Needle
        for item in threadTheNeedleArray {
        
            threadTheNeedle.add(child: item)
        
        }
    
        // Turns
        for item in turnsArray {
        
            turns.add(child: item)
        
        }
    
        // Rolls
        for item in rollsArray {
        
            rolls.add(child: item)
        
        }
    
        // Elbow
        for item in elbowArray {
        
            elbow.add(child: item)
        
        }
    
        // Butterfly
        for item in butterfliesArray {
        
            butterflies.add(child: item)
        
        }

        // Sphere/Globe
        for item in sphereGlobeArray {
        
            sphereGlobe.add(child: item)
        
        }
    
        // Horzontal Plane
        for item in horizontalPlaneArray {
        
            horizontalPlane.add(child: item)
            
        }
    
        // 3D (Both Vertical & Horizontal Planes)
        for item in threeDArray {
        
            threeD.add(child: item)
        
        }
    
        // Hinge & Fold
        for item in hingeFoldArray {
        
            hingeFold.add(child: item)
        
        }
    
        // Escalators & Wedgies
        for item in escalatorWedgieArray {
    
            escalatorWedgie.add(child: item)
        
        }
    
        // Anti-Spin & In-Spin
        for item in antiSpinInSpinArray {
        
            antiSpinInSpin.add(child: item)
        
        }
    
        // CAPS
    for item in capsArray {
        
        caps.add(child: item)
        
    }
            



