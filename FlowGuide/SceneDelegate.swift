//
//  SceneDelegate.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 11/22/19.
//  Copyright © 2019 CosmoNautical. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import Purchases

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?
    var coordinator: LoginCoordinator?



    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        // Use this method to optionally configure and attach the UIWindow `window` to the provided UIWindowScene `scene`.
        // If using a storyboard, the `window` property will automatically be initialized and attached to the scene.
        // This delegate does not imply the connecting scene or session are new (see `application:configurationForConnectingSceneSession` instead).
        
        guard let windowScene = (scene as? UIWindowScene) else { return }

        /// Create the main Navigation Controller to be used for the app
        let navController = UINavigationController()
        coordinator = LoginCoordinator(navigationController: navController)
        let dataManager = DataManager()
        coordinator?.dataManager = dataManager
        // send that navigation controller into our coordinator so that it can display the view controllers
        coordinator?.start(slideDelegate: nil)
        // Tells the coordinator to take over control
               
               
        /// Create a basic UIWindow and activate it
        window = UIWindow(frame: windowScene.coordinateSpace.bounds)
        window?.windowScene = windowScene

        
        /// Check if user is already logged in
        Auth.auth().addStateDidChangeListener { (auth, user) in
            if user == nil {
                // We are logged out of Firestore
                // Reset Purchases App User ID
                Purchases.shared.reset()
                // Move to LoginViewController
                self.window?.rootViewController = navController
                navController.popToRootViewController(animated: true)
            } else {
                // We are logged in to Firestore
                // Get User data & pass to main app
                let dispatch = DispatchGroup()
                FirestoreService.getCurrentUser(dispatch: dispatch) { (tempUser) in
                    dispatch.notify(queue: .main, execute: {
                        // Set Purchases App User ID
                        Purchases.shared.identify(tempUser.uid) { (purchaserInfo, error) in
                            if let error = error {
                                print("Purchases: Error identifying user: \(error)")
                            } else {
                                print("Purchases: Success identifying user")
                            }
                        }
                        // Move to main app
                        let rootView = MasterContainerController(dataManager: dataManager, tempUser: tempUser)
                        self.window?.rootViewController = UINavigationController(rootViewController: rootView)
                    })
                }
            }
        }
        
        
        // Sets the navigation controller to the root view
        window?.makeKeyAndVisible()

        navController.navigationBar.topItem?.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: self, action: nil)

        
        
    }

    func sceneDidDisconnect(_ scene: UIScene) {
        // Called as the scene is being released by the system.
        // This occurs shortly after the scene enters the background, or when its session is discarded.
        // Release any resources associated with this scene that can be re-created the next time the scene connects.
        // The scene may re-connect later, as its session was not neccessarily discarded (see `application:didDiscardSceneSessions` instead).
    }

    func sceneDidBecomeActive(_ scene: UIScene) {
        // Called when the scene has moved from an inactive state to an active state.
        // Use this method to restart any tasks that were paused (or not yet started) when the scene was inactive.
    }

    func sceneWillResignActive(_ scene: UIScene) {
        // Called when the scene will move from an active state to an inactive state.
        // This may occur due to temporary interruptions (ex. an incoming phone call).
    }

    func sceneWillEnterForeground(_ scene: UIScene) {
        // Called as the scene transitions from the background to the foreground.
        // Use this method to undo the changes made on entering the background.
    }

    func sceneDidEnterBackground(_ scene: UIScene) {
        // Called as the scene transitions from the foreground to the background.
        // Use this method to save data, release shared resources, and store enough scene-specific state information
        // to restore the scene back to its current state.
        

        // Save changes in the application's managed object context when the application transitions to the background.
        (UIApplication.shared.delegate as? AppDelegate)?.saveContext()
    }


}


