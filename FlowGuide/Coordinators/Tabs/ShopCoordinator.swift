//
//  ShopCoordinator.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 1/28/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import UIKit

class ShopCoordinator: NSObject, Coordinator {
    
// MARK: - Properties
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    let userIsEntitled = PurchasesHelper.isUserEntitled()
    
    
// MARK: - Init
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    

    }
       
    
// MARK: - Methods
    
    func start(slideDelegate: Slideable?) {
        let vc = ShopViewController(collectionViewLayout: createLayout())
        vc.tabBarItem.image = UIImage(named: "shop_blue")
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            vc.tabBarItem.imageInsets = UIEdgeInsets(top: 2, left: 0, bottom: -2, right: 0)
        } else {
            vc.tabBarItem.imageInsets = UIEdgeInsets(top: 11, left: 0, bottom: -10, right: 0)
        }
        vc.tabBarController?.tabBar.tintColorDidChange()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    
    }
    
    
    func createSearchCoordinator() -> SearchCoordinator {
        let child = SearchCoordinator(navigationController: UINavigationController())
        child.parentCoordinator = self
        childCoordinators.append(child)
        return child
    }
    
    
    private func createLayout() -> UICollectionViewLayout{
        var height = CGFloat(1.45)
        
        if !userIsEntitled {
            height = 1.31
        }
        
        let itemSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(1.0), heightDimension: .fractionalHeight(CGFloat(height)))
        let item = NSCollectionLayoutItem(layoutSize: itemSize)
        
        let groupSize = NSCollectionLayoutSize(widthDimension: .fractionalWidth(2.0), heightDimension: .fractionalWidth(1.0))
        let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 2)
        
        let section = NSCollectionLayoutSection(group: group)

        let layout = UICollectionViewCompositionalLayout(section: section)
         
        return layout
    }
    
}



