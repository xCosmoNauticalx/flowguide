//
//  LoginCoordinator.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/2/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class LoginCoordinator: NSObject, Coordinator {
    
// MARK: - Properties
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    var dataManager: DataManaging?
    

// MARK: - Init
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    
// MARK: - Methods
    
    func start(slideDelegate: Slideable?) {
        let vc = HomeViewController()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
 
    }
    
    func login() {
        let vc = LoginViewController()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    func signUp() {
        let vc = SignUpViewController()
        vc.coordinator = self
        vc.dataManager = dataManager
        navigationController.pushViewController(vc, animated: true)
    }
    
    func forgotPassword() {
        let vc = ResetPasswordViewController()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: true)
    }
    
    
    
}


// MARK: - UINavigationControllerDelegate

extension LoginCoordinator: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        switch operation {
        case .push:
            // Check if we're moving to the ResetPasswordVC
            if let _ = toVC as? ResetPasswordViewController {
                // We are, so dont' do the custom transition
                return nil
            } else {
                // We're not, so let's do the custom transition
                return FadePushAnimation()
            }
        case .pop:
            // Check if we're moving from the ResetPasswordVC
            if let _ = fromVC as? ResetPasswordViewController {
                // We are, so dont' do the custom transition
                return nil
            } else {
                // We're not, so let's do the custom transition
                return FadePopAnimation()
            }
        default:
            return nil
        }
    }
    
}
