//
//  LibraryCoordinator.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 11/26/19.
//  Copyright © 2019 CosmoNautical. All rights reserved.
//

import Foundation
import UIKit

class LibraryCoordinator: NSObject, Coordinator, DirectoryDelegate {

    /// Controls the Library Tab

// MARK: - Properties
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    weak var userUpdater: UserUpdating?
    weak var dataManager: DataManaging?
    weak var titleDelegate: Titleable?

    
// MARK: - Init
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    
// MARK: - Methods
    
    ///Init Root
    func start(slideDelegate: Slideable?) {
        
        navigationController.delegate = self
        
        var n: CGFloat = 10
        
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            n = 2
        }
        
        let vc = LibraryContainerController()
        vc.tabBarItem.image = UIImage(named: "user_blue")
        vc.tabBarItem.imageInsets = UIEdgeInsets(top: n, left: 0, bottom: -n, right: 0)
        vc.tabBarItem.title = nil
        vc.coordinator = self
        vc.slideDelegate = slideDelegate
        navigationController.pushViewController(vc, animated: false)
    }

    /// Moves to LibraryFlowViewController after getting user's learned moves
    func next(withHoopCount hoopCount: Int, page: Node) {
        let vc = LibraryFlowViewController()
        
        guard let moves = dataManager?.fetchLearnedMoves(withHoopCount: hoopCount) else { return }
        if moves.count == 0 {
            vc.shouldShowCollectionView = false
        }
        vc.data.moves = moves
        vc.coordinator = self
        vc.titleDelegate = titleDelegate
        vc.data.page = page
        self.navigationController.pushViewController(vc, animated: true)
        
    }
    
    func next(withMoves moves: [LearnedMove], page: Node, parents: [Node]) {
        let vc = LibraryFlowViewController()
        vc.coordinator = self
        vc.data.page = page
        vc.data.moves = moves
        vc.parents = parents
        vc.titleDelegate = titleDelegate
        navigationController.pushViewController(vc, animated: true)
    }
    
    func movesPage(page: Node, moves: [LearnedMove], parents: [Node]) {
        let vc = LibraryMovesViewController()
        vc.coordinator = self
        vc.titleDelegate = titleDelegate
        vc.data.page = page
        vc.parents = parents
        vc.data.moves = moves
        print(moves)
        navigationController.pushViewController(vc, animated: true)
    }
    
    /// Goes to MoveViewController
    func moveDetail(docID: String) {
        let dispatch = DispatchGroup()
        // Find the move in Firestore & pass it as a TempMove
        FirestoreService.getMove(withID: docID, dispatch: dispatch) { (tempMove) in
            dispatch.notify(queue: .main, execute: {
                let child = MoveCoordinator(navigationController: self.navigationController)
                self.childCoordinators.append(child)
                guard let titleDelegate = self.titleDelegate else { return }
                child.go(fromParent: .library, withMove: tempMove, titleDelegate: titleDelegate, dataManager: self.dataManager!, userUpdater: self.userUpdater!)
            })
        }
        
    }
    
    /// Remove Child Coordinator when it's done
    func childDidFinish(_ child: Coordinator?) {
        for (index, coordinator) in childCoordinators.enumerated() {
            if coordinator === child {
                childCoordinators.remove(at: index)
                break
            }
        }
    }
    
    
    /// Goes to Combos Page
    func combos() {
        guard let combos = dataManager?.fetchCombos() else { return }
        let vc = CombosViewController()
        vc.coordinator = self
        vc.titleDelegate = titleDelegate
        vc.data.combos = combos
        print("Combos:")
        for combo in combos {
            print(combo.docID)
        }
        navigationController.pushViewController(vc, animated: true)
    }
    
    
    /// Updates the Incrementer on MasterContainerController
    func updateMoveIncrement(by number: Int) {
        userUpdater?.moveIncrement += number
    }
    
    func updateComboIncrement(by number: Int) {
        userUpdater?.comboIncrement += number
    }
}


// MARK: - NavigationController Delegate
    
extension LibraryCoordinator: UINavigationControllerDelegate {
    
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        // Read the view controller we’re moving from.
        guard let fromViewController = navigationController.transitionCoordinator?.viewController(forKey: .from) else {
            return
        }

        // Check whether our view controller array already contains that view controller. If it does it means we’re pushing a different view controller on top rather than popping it, so exit.
        if navigationController.viewControllers.contains(fromViewController) {
            return
        }

        // We’re still here – it means we’re popping the view controller, so we can check whether it’s a move view controller
        if let moveViewController = fromViewController as? MoveDetailViewController {
            // We're popping a move view controller; end its coordinator
            childDidFinish(moveViewController.coordinator)
        }
    }
    
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        switch operation {
        case .push:
            // Check if we're moving to the MoveDetailVC
            if let _ = toVC as? MoveDetailViewController {
                // We are, so let's do the custom transition
                return FadePushAnimation()
            } else {
                // We're not, so don't do the custom transition
                return nil
            }
        case .pop:
            // Check if we're moving from the MoveDetailVC
            if let _ = fromVC as? MoveDetailViewController {
                // We are, so let's do the custom transition
                return FadePopAnimation()
            } else {
                // We're not, so don't do the custom transition
                return nil
            }
        default:
            return nil
        }
    }
    
}
