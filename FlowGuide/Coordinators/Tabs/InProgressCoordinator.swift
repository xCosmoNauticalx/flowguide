//
//  InProgressCoordinator.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 1/14/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import UIKit
import SafariServices

class InProgressCoordinator: Coordinator, Playable {
    
    /// Controls the In Progress tab
    
// MARK: - Properties
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    var safariVC: SFSafariViewController!
    
    weak var userUpdater: UserUpdating?
    weak var premium: Premium?
    weak var dataManager: DataManaging?
    
    
// MARK: - Init
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    

// MARK: - Methods
    
    /// Init Root
    func start(slideDelegate: Slideable?) {
        
        var n: CGFloat = 10
        
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            n = 2
        }
        
        let vc = InProgressContainerController()
        vc.tabBarItem.image = UIImage(named: "clock")
        vc.tabBarItem.imageInsets = UIEdgeInsets(top: n, left: 0, bottom: -n, right: 0)
        vc.tabBarController?.tabBar.tintColorDidChange()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
 
    }
    
    
}
