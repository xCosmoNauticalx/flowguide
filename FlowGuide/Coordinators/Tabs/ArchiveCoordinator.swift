//
//  MainCoordinator.swift
//  Flow Guide
//
//  Created by Kelsey Garcia on 10/7/19.
//  Copyright © 2019 Kelsey Garcia. All rights reserved.
//

import Foundation
import UIKit

class ArchiveCoordinator: NSObject, Coordinator, DirectoryDelegate {
    
    
// MARK: - Properties
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    weak var userUpdater: UserUpdating?
    weak var dataManager: DataManaging?
    
    var alreadyFetched = [String]()
    
    
// MARK: - Init
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    
// MARK: - Methods

    func start(slideDelegate: Slideable?) {
        
        navigationController.delegate = self
        
        var n: CGFloat = 10
        
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            n = 2
        }
        
        let vc = ArchiveContainerController()
        vc.tabBarItem.image = UIImage(named: "deselected_tutorials")
        vc.tabBarItem.imageInsets = UIEdgeInsets(top: n, left: 0, bottom: -n, right: 0)
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
 
    }
  
    
    /// Refreshes ViewController, passing in Item from the TutorialCell that is pressed.
    func next(page: Node, parents: [Node]? = nil, titleDelegate: Titleable) {
        let vc = ArchiveFlowViewController()
        vc.coordinator = self
        vc.titleDelegate = titleDelegate
        vc.data.page = page
        if parents != nil {
            if let parents = parents {
                vc.parents = parents
            }
        }
        navigationController.pushViewController(vc, animated: true)
        
    }

    
    /// Goes to FinalGroupViewController
    func movesPage(page: Node, parents: [Node], titleDelegate: Titleable) {
        
        let parentNode = page.instanceName
        let dispatch = DispatchGroup()
        
        if alreadyFetched.contains(parentNode) {
            FirestoreService.getMoves(belongingTo: parentNode, fromCache: true, dispatch: dispatch) {(moves) in
                dispatch.notify(queue: .main, execute: {
                    print("Number of moves final: \(moves.count)")
                    pushVC(moves)
                })
            }
        } else {
            /// Get moves from Firestore using a dispatch group since it's asynchronous
            FirestoreService.getMoves(belongingTo: parentNode, fromCache: false, dispatch: dispatch) {(moves) in
                dispatch.notify(queue: .main, execute: {
                    print("Number of moves final: \(moves.count)")
                    // Add node to alreadyFetched so we can get from cache next time
                    self.alreadyFetched.append(parentNode)
                    pushVC(moves)
                })
            }
        }
        
        func pushVC(_ moves: [TempMove]) {
            let vc = ArchiveMovesViewController()
            vc.data.moves = moves
            vc.coordinator = self
            vc.titleDelegate = titleDelegate
            vc.data.page = page
            vc.parents = parents
            self.navigationController.pushViewController(vc, animated: true)
        }
    }
    
    
    /// Goes to MoveViewController
    func moveDetail(page: TempMove, titleDelegate: Titleable) {
        let child = MoveCoordinator(navigationController: navigationController)
        childCoordinators.append(child)
        child.go(fromParent: .archive, withMove: page, titleDelegate: titleDelegate, dataManager: dataManager!, userUpdater: userUpdater!)
    }
    
    
    /// Remove Child Coordinator when it's done
    func childDidFinish(_ child: Coordinator?) {
        for (index, coordinator) in childCoordinators.enumerated() {
            if coordinator === child {
                childCoordinators.remove(at: index)
                break
            }
        }
    }
    
    
    /// Updates the Incrementer on MasterContainerController
    func updateMoveIncrement(by number: Int) {
        userUpdater?.moveIncrement += number
    }
    
    
}


// MARK: - NavigationController Delegate

extension ArchiveCoordinator: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        // Read the view controller we’re moving from.
        guard let fromViewController = navigationController.transitionCoordinator?.viewController(forKey: .from) else {
            return
        }

        // Check whether our view controller array already contains that view controller. If it does it means we’re pushing a different view controller on top rather than popping it, so exit.
        if navigationController.viewControllers.contains(fromViewController) {
            return
        }

        // We’re still here – it means we’re popping the view controller, so we can check whether it’s a buy view controller
        if let moveViewController = fromViewController as? MoveDetailViewController {
            // We're popping a buy view controller; end its coordinator
            childDidFinish(moveViewController.coordinator)
        }
    }
    
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        switch operation {
        case .push:
            // Check if we're moving to the MoveDetailVC
            if let _ = toVC as? MoveDetailViewController {
                // We are, so let's do the custom transition
                return FadePushAnimation()
            } else {
                // We're not, so don't do the custom transition
                return nil
            }
        case .pop:
            // Check if we're moving from the MoveDetailVC
            if let _ = fromVC as? MoveDetailViewController {
                // We are, so let's do the custom transition
                return FadePopAnimation()
            } else {
                // We're not, so don't do the custom transition
                return nil
            }
        default:
            return nil
        }
    }
    
    
}

