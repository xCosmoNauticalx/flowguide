//
//  GeneratorCoordinator.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 11/30/19.
//  Copyright © 2019 CosmoNautical. All rights reserved.
//

import Foundation
import UIKit

class GeneratorCoordinator: NSObject, Coordinator {

    /// Controls the Combo Generator Tab

// MARK: - Properties
    
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    weak var userUpdater: UserUpdating?
    weak var premium: Premium?
    weak var dataManager: DataManaging?

    
// MARK: - Init
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    
// MARK: - Methods
    
    /// Init Root
    func start(slideDelegate: Slideable?) {
        
        navigationController.delegate = self
        
        var n: CGFloat = 10
        
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            n = 2
        }
        
        let vc = GeneratorContainerController()
        vc.tabBarItem.image = UIImage(named: "shuffle_blue")
        vc.tabBarItem.imageInsets = UIEdgeInsets(top: n, left: 0, bottom: -n, right: 0)
        vc.tabBarItem.title = nil
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    

    /// Generate combo & push next VC
    func generateCombo(hoopCount: Int, moveCount: Int, titleDelegate: Titleable) {
        let vc = GeneratorResultsViewController()
        guard let moves = dataManager?.fetchLearnedMoves(withHoopCount: hoopCount) else { return }
        vc.data.moves = moves
        vc.data.moveCount = moveCount
        vc.data.hoopCount = hoopCount
        print(vc.data.moves)
        vc.coordinator = self
        vc.titleDelegate = titleDelegate
        self.navigationController.pushViewController(vc, animated: true)
        
        NotificationCenter.default.post(name: .clearField, object: nil)
        
    }

    
     /// Goes to MoveViewController
       func moveDetail(docID: String, titleDelegate: Titleable) {
           let dispatch = DispatchGroup()
           // Find the move in Firestore & pass it as a TempMove
           FirestoreService.getMove(withID: docID, dispatch: dispatch) { (tempMove) in
               dispatch.notify(queue: .main, execute: {
                   let child = MoveCoordinator(navigationController: self.navigationController)
                   self.childCoordinators.append(child)
                   child.go(fromParent: .generator, withMove: tempMove, titleDelegate: titleDelegate, dataManager: self.dataManager!, userUpdater: self.userUpdater!)
               })
           }
           
       }
    
    /// Remove Child Coordinator when it's done
    func childDidFinish(_ child: Coordinator?) {
        for (index, coordinator) in childCoordinators.enumerated() {
            if coordinator === child {
                childCoordinators.remove(at: index)
                break
            }
        }
    }
    
    /// Update Combo Increment
    func updateComboIncrement(by number: Int) {
        userUpdater?.comboIncrement += number
    }
    
}


// MARK: - NavigationController Delegate

extension GeneratorCoordinator: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        // Read the view controller we’re moving from.
        guard let fromViewController = navigationController.transitionCoordinator?.viewController(forKey: .from) else {
            return
        }

        // Check whether our view controller array already contains that view controller. If it does it means we’re pushing a different view controller on top rather than popping it, so exit.
        if navigationController.viewControllers.contains(fromViewController) {
            return
        }

        // We’re still here – it means we’re popping the view controller, so we can check whether it’s a buy view controller
        if let moveViewController = fromViewController as? MoveDetailViewController {
            // We're popping a buy view controller; end its coordinator
            childDidFinish(moveViewController.coordinator)
        }
    }
    
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        switch operation {
        case .push:
            // Check if we're moving to the MoveDetailVC
            if let _ = toVC as? MoveDetailViewController {
                // We are, so dont' do the custom transition
                return nil
            } else {
                // We're not, so let's do the custom transition
                return FadePushAnimation()
            }
        case .pop:
            // Check if we're moving from the MoveDetailVC
            if let _ = fromVC as? MoveDetailViewController {
                // We are, so dont' do the custom transition
                return nil
            } else {
                // We're not, so let's do the custom transition
                return FadePopAnimation()
            }
        default:
            return nil
        }
    }

}
