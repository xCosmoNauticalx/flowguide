//
//  MoveCoordinator.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/19/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import UIKit
import SafariServices


class MoveCoordinator: Coordinator, Playable {
    
// MARK: - Properties
    
    weak var parentCoordinator: Coordinator?
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    var safariVC: SFSafariViewController!
    
    weak var userUpdater: UserUpdating?
    weak var titleDelegate: Titleable?
    weak var dataManager: DataManaging?
    
    
// MARK: - Init
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    
// MARK: - Methods
    
    func start(slideDelegate: Slideable?) {
        return
    }
    
    /// Goes to MoveViewController
    func go(fromParent parent: parent, withMove move: TempMove, titleDelegate: Titleable, dataManager: DataManaging, userUpdater: UserUpdating) {
        self.userUpdater = userUpdater
        self.dataManager = dataManager
        
        let vc = MoveDetailViewController.instantiate()
        vc.coordinator = self
        vc.titleDelegate = titleDelegate
        vc.data.page = move
        vc.data.dataManager = dataManager
        vc.tab = parent
        navigationController.pushViewController(vc, animated: true)
    }
    
}
