//
//  MainTabBarController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 11/26/19.
//  Copyright © 2019 CosmoNautical. All rights reserved.
//

import UIKit

class MainTabBarController: UITabBarController {
    
    /// Set Delegates
    weak var dataManager: DataManaging?
    weak var slideDelegate: Slideable?
    weak var userUpdater: UserUpdating?
    weak var premium: Premium?

    /// Create instances of each tab's coordinator
    let archiveCoordinator = ArchiveCoordinator(navigationController: UINavigationController())
    let libraryCoordinator = LibraryCoordinator(navigationController: UINavigationController())
    let generatorCoordinator = GeneratorCoordinator(navigationController: UINavigationController())
    let inProgressCoordinator = InProgressCoordinator(navigationController: UINavigationController())
    let shopCoordinator = ShopCoordinator(navigationController: UINavigationController())
    
    /// Create indicator bar
    var indicator: UIView?

    init(dataManager: DataManaging, slideDelegate: Slideable, userUpdater: UserUpdating, premium: Premium) {
        self.dataManager = dataManager
        self.slideDelegate = slideDelegate
        self.userUpdater = userUpdater
        self.premium = premium
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
            setUp()
            setUpViewControllers()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    
    private func setUpViewControllers() {
        archiveCoordinator.start(slideDelegate: nil)
        libraryCoordinator.start(slideDelegate: slideDelegate)
        generatorCoordinator.start(slideDelegate: nil)
        inProgressCoordinator.start(slideDelegate: nil)
        shopCoordinator.start(slideDelegate: nil)
        
        archiveCoordinator.userUpdater = userUpdater
        libraryCoordinator.userUpdater = userUpdater
        generatorCoordinator.userUpdater = userUpdater
        
        archiveCoordinator.dataManager = dataManager
        libraryCoordinator.dataManager = dataManager
        generatorCoordinator.dataManager = dataManager
        inProgressCoordinator.dataManager = dataManager
        
        generatorCoordinator.premium = premium
        inProgressCoordinator.premium = premium

        addChildren()
        viewControllers = [archiveCoordinator.navigationController, libraryCoordinator.navigationController, generatorCoordinator.navigationController, inProgressCoordinator.navigationController, shopCoordinator.navigationController]
    }
    
    private func setUp() {
        
        self.tabBar.backgroundImage = UIImage(named: "tab_bar_background2")
        
        let numberOfItems = CGFloat(5)
        let tabBarItemSize = CGSize(width: (tabBar.frame.width / numberOfItems), height: tabBar.frame.height)
        
        switch UIScreen.main.traitCollection.userInterfaceIdiom {
        case .pad:
            indicator = UIView(frame: CGRect(x: 0, y: 0, width: tabBarItemSize.width - 20, height: 1))
        default:
            indicator = UIView(frame: CGRect(x: 0, y: 0, width: tabBarItemSize.width, height: 1))
        }
        indicator?.backgroundColor = UIColor.Archive.light
        indicator?.layer.cornerRadius = 10
        
        switch UIScreen.main.traitCollection.userInterfaceIdiom {
        case .pad:
            indicator?.center.x = tabBar.frame.width / 5 / 2 + 40
        default:
            indicator?.center.x = tabBar.frame.width / 5 / 2
        }
        
        tabBar.addSubview(indicator!)
        
        
    }

    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let number = -(tabBar.items?.firstIndex(of: item)?.distance(to: 0))! + 1
        
        // Move bar
        switch UIScreen.main.traitCollection.userInterfaceIdiom {
        case .pad:
            UIView.animate(withDuration: 0.3) {
                if number == 1 {
                    self.indicator?.center.x =  tabBar.frame.width / 5 / 2 + 40
                } else if number == 2 {
                    self.indicator?.center.x =  tabBar.frame.width / 5 / 2 + tabBar.frame.width / 5 + 15
                } else if number == 3 {
                    self.indicator?.center.x =  tabBar.frame.width / 2 - 7
                } else if number == 4 {
                    self.indicator?.center.x = tabBar.frame.width / 5 / 2 + (tabBar.frame.width / 5) * 3 - 25
                } else {
                    self.indicator?.center.x = tabBar.frame.width - tabBar.frame.width / 5 / 2 - 45
                }
            }
        default:
            UIView.animate(withDuration: 0.3) {
                if number == 1 {
                    self.indicator?.center.x =  tabBar.frame.width / 5 / 2
                } else if number == 2 {
                    self.indicator?.center.x =  tabBar.frame.width / 5 / 2 + tabBar.frame.width / 5
                } else if number == 3 {
                    self.indicator?.center.x =  tabBar.frame.width / 2
                } else if number == 4 {
                    self.indicator?.center.x = tabBar.frame.width / 5 / 2 + (tabBar.frame.width / 5) * 3
                } else {
                    self.indicator?.center.x = tabBar.frame.width - tabBar.frame.width / 5 / 2
                }
            }
        }
        
        
        // Change bar color
        UIView.animate(withDuration: 0.0) {
            if number == 1 {
                self.indicator?.backgroundColor = UIColor.Archive.light
            } else if number == 2 {
                self.indicator?.backgroundColor = UIColor.Library.light
            } else if number == 3 {
                self.indicator?.backgroundColor = UIColor.Combo.light
            } else if number == 4 {
                self.indicator?.backgroundColor = UIColor.Progress.light
            } else {
                self.indicator?.backgroundColor = UIColor.white
            }
        }
    }
    
    


}
