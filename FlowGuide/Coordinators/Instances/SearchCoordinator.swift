//
//  SearchCoordinator.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/2/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import UIKit
import SafariServices
import GoogleMobileAds

class SearchCoordinator: NSObject, Coordinator {
    
// MARK: - Properties
    
    weak var parentCoordinator: Coordinator?
    var childCoordinators = [Coordinator]()
    var navigationController: UINavigationController
    
    var smiths = [HoopSmith]()
    let userIsEntitled = PurchasesHelper.isUserEntitled()
    
    var safariVC: SFSafariViewController!
    
    /// Ads
    var interstitial: GADInterstitial!
    var shouldShowAd = true
    
    
// MARK: - Init
    
    init(navigationController: UINavigationController) {
        self.navigationController = navigationController
    }
    
    
// MARK: - Methods
    
    func start(slideDelegate: Slideable?) {
        navigationController.delegate = self
        
        smiths = HoopSmith.smiths()
        let vc = SearchViewController()
        vc.coordinator = self
        navigationController.pushViewController(vc, animated: false)
    }
    
    
    func next(_ country: HoopSmith.Country, titleDelegate: Titleable) {
        
        var localSmiths = [HoopSmith]()
        var internationalSmiths = [HoopSmith]()
        
        /// Filter Smiths
        for smith in smiths {
            // Find the smiths located in the selected region
            if smith.location == country {
                localSmiths.append(smith)
            }
            // Find the smiths that ship to the selected region
            if smith.shipsTo == country || smith.shipsTo == HoopSmith.Country.global {
                // If the smith is local, don't add it to the international smiths
                if !localSmiths.contains(smith) {
                    internationalSmiths.append(smith)
                }
            }
            // Check if the region is in Europe, and if so, include smiths that ship to the EU
            for e in europe {
                if country == e {
                    if smith.shipsTo == .europe {
                        if !localSmiths.contains(smith) {
                            internationalSmiths.append(smith)
                        }
                    }
                }
            }
            // Reverse the order of internationalSmiths if country is in Europe so that US doesn't show first
            for e in europe {
                if country == e {
                    internationalSmiths.reverse()
                }
            }
        }
        
        /// Push VC
        let vc = SearchDetailViewController()
        vc.titleDelegate = titleDelegate
        vc.coordinator = self
        vc.country = country
        vc.data.localSmiths = localSmiths
        vc.data.internationalSmiths = internationalSmiths
        navigationController.pushViewController(vc, animated: true)
    }
    
    
    func showSafari(_ urlString: String) {
        
        if let url = URL(string: urlString) {
            print("Converted URL")
            let config = SFSafariViewController.Configuration()
            safariVC = SFSafariViewController(url: url, configuration: config)
            safariVC.delegate = self
            parentCoordinator?.navigationController.present(safariVC, animated: true)
            print("Completed")
        }
        
    }
    
}


// MARK: - Safari View Controller

extension SearchCoordinator: SFSafariViewControllerDelegate {
    
    func safariViewControllerDidFinish(_ controller: SFSafariViewController) {
        if !userIsEntitled {
            // Since the same VC is presenting the safariVC and the ad, wait for the safariVC to be finished dismissing
            let seconds = 0.6
            DispatchQueue.main.asyncAfter(deadline: .now() + seconds, execute: {
                NotificationCenter.default.post(name: .showInterstitial, object: nil, userInfo: ["shouldShowAd": true])
            })
            
        }
    }
    
}


// MARK: - UINavigationControllerDelegate

extension SearchCoordinator: UINavigationControllerDelegate {
    
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationController.Operation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        switch operation {
        case .push:
            return FadePushAnimation()
        case .pop:
            return FadePopAnimation()
        default:
            return nil
        }
    }
    
}

