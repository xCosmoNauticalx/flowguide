//
//  DataManager.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/18/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import CoreData


class DataManager: DataManaging {
    
// MARK: - Properties
    
    let persistentContainer = NSPersistentContainer(name: "FlowGuide")
    
    
// MARK: - Init
    init() {
        
        
        persistentContainer.loadPersistentStores(completionHandler: { storeDescription, err in
            
            // Instructs Core Data to allow updates to objects. If an object exists with message A, and an object with the same unique constraint("docID") exists in memory with message B, the in-memory version overwrites tha data store version.
            //self.persistentContainer.viewContext.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
            
            
            if let err = err {
                print("Load Persistent Stores: unresolved error \(err)")
            }
        })
        
    }
    

    
        
    
}
