//
//  DataManager.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/18/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import CoreData
import UIKit

// MARK: - Protocol Definition

protocol DataManaging: AnyObject {
    
    var persistentContainer: NSPersistentContainer { get }
    
    func saveContext()
    
}

// MARK: - Extension

extension DataManaging {
    
    
    
// MARK: - Users
    
    /// Create User in disk
    func createUser(name: String, uid: String, primaryDevice: String) {
        let user = User(context: self.persistentContainer.viewContext)
        user.name = name
        user.uid = uid
        user.movesLearned = 0
        user.combosCreated = 0
        user.primaryDevice = primaryDevice
        
        saveContext()
        print("Created User: \(String(describing: user.name))")
    }
    
    
    /// Create User if first run but account exists
    func restoreUser(_ authUser: TempUser) {
        let user = User(context: self.persistentContainer.viewContext)
        user.name = authUser.name
        user.uid = authUser.uid
        user.movesLearned = authUser.movesLearned
        user.combosCreated = authUser.combosCreated
        user.primaryDevice = authUser.primaryDevice
        if let otherdevices = authUser.otherDevices {
            user.otherDevices = otherdevices
        }
        
        saveContext()
        print("Restored user named: \(String(describing: user.name))")
    }
    
    
    /// Check if device already belongs to an account
    func checkIfOwned() -> Bool {
        var result = true
        let request = User.createFetchRequest()
        
        do {
            let users = try persistentContainer.viewContext.fetch(request)
            if users.count == 0 {
                result = false
            } else {
                result = true
            }
        } catch {
            print("Couldn't check if disk already contains user: \(error)")
        }
        return result
    }
    
    
    /// Pull User from disk
    func getUser() -> User? {
        var user: User?
        
        let request = User.createFetchRequest()
        do {
            let users = try persistentContainer.viewContext.fetch(request)
            if users.count != 0 {
                user = users[0]
            } else {
                return nil
            }
            
        } catch {
            print("Error getting User from disk: \(error)")
        }
        return user
    }
    
    /// Get list of devices other than this one
    func getOtherDevices() -> [String] {
        let user = getUser()
        var devices = [String]()
        let thisDevice = UIDevice.current.identifierForVendor?.uuidString
        if let primary = user?.primaryDevice {
            if primary != thisDevice {
                devices.append(primary)
            }
        }
        // Check if there are other devices
        if let otherDevices = user?.otherDevices {
            for device in otherDevices {
                if device != thisDevice {
                    devices.append(device)
                }
            }
        }
        print("Got \(devices.count) other devices: \(devices)")
        return devices
    }
    
    
    /// Get all devices that belongs to this user
    func getAllDevices() -> [String] {
        let user = getUser()
        var devices = [String]()
        if let primary = user?.primaryDevice {
            devices.append(primary)
        }
        if let otherDevices = user?.otherDevices {
            for device in otherDevices {
                devices.append(device)
            }
        }
        print("Got \(devices.count) devices: \(devices)")
        return devices
    }
    
    
    /// Update device list
    func updateDevices(withDevice deviceID: String) {
        let request = User.createFetchRequest()
        do {
            let users = try persistentContainer.viewContext.fetch(request)
            if users[0].otherDevices != nil {
                // Already a list of other devices, add this one
                users[0].otherDevices?.append(deviceID)
            } else {
                // Create new list of other devices
                users[0].otherDevices = [deviceID]
            }
            saveContext()
        } catch {
            print("Couldn't update device list: \(error)")
        }
    }
    
    
    /// Update User Stats
    func updateStats(for category: MoveOrCombo, by num: Int) {
        let request = User.createFetchRequest()
        
        do {
            let users = try persistentContainer.viewContext.fetch(request)
            for user in users {
                user.setValue(NSNumber(value: num), forKey: category.rawValue)
            }
            saveContext()
        } catch {
            print("Couldn't update User Stats: \(error)")
        }
    }
    
    
// MARK: - Library
    
    /// Add Learned Move to Library
    func addLearnedMove(_ move: TempMove) {
        let learnedMove = LearnedMove(context: self.persistentContainer.viewContext)
        learnedMove.name = move.name
        learnedMove.docID = move.docID
        learnedMove.hoopCount = move.hoopCount
        learnedMove.parentNode = move.parentNode
        
        saveContext()
        print("Saved Learned Move \(learnedMove)")
    }
    
    
    /// Remove Learned Move from Library
    func removeLearnedMove(_ move: TempMove) {
        let request = LearnedMove.createFetchRequest()
        request.predicate = NSPredicate(format: "docID == %@", move.docID)
        
        do {
            let learnedMoves = try persistentContainer.viewContext.fetch(request)
            for move in learnedMoves {
                persistentContainer.viewContext.delete(move)
            }
            saveContext()
        } catch {
            print("Couldn't get LearnedMove: \(error)")
        }
    }
    
    
    /// Batch write Learned Move
    func batchAddLearnedMove(_ moves: [TempMove]) {
        for move in moves {
            let learnedMove = LearnedMove(context: self.persistentContainer.viewContext)
            learnedMove.name = move.name
            learnedMove.docID = move.docID
            learnedMove.hoopCount = move.hoopCount
            learnedMove.parentNode = move.parentNode
        }
        saveContext()
        print("Saved \(moves.count) moves")
    }
    
    
    /// Count number of Learned Moves stored in disk
    func countLearnedMoves() -> Int {
        var moves = [LearnedMove]()
        let request = LearnedMove.createFetchRequest()
        
        do {
            moves = try persistentContainer.viewContext.fetch(request)
        } catch {
            print("Couldn't fetch LearnedMoves: \(error)")
        }
        
        return moves.count
    }
    
    
    func fetchLearnedMoves() -> [LearnedMove] {
        var moves = [LearnedMove]()
        let request = LearnedMove.createFetchRequest()
        
        do {
            moves = try persistentContainer.viewContext.fetch(request)
            print("Got \(moves.count) moves")
        } catch {
            print("Couldn't fetch Learned Moves: \(error)")
        }
        return moves
    }
    
    
    /// Get moves that belong to a hoop count
    func fetchLearnedMoves(withHoopCount count: Int) -> [LearnedMove] {
        var moves = [LearnedMove]()
        let request = LearnedMove.createFetchRequest()
        request.predicate = NSPredicate(format: "hoopCount == %@", NSNumber(value: count))
        
        do {
            moves = try persistentContainer.viewContext.fetch(request)
            print(moves.count)
        } catch {
            print("Couldn't fetch LearnedMove: \(error)")
        }
        return moves
    }
    
    
    /// Get moves that belong to a hoop count
    func fetchLearnedMove(withDocID docID: String) -> LearnedMove {
        var moves = [LearnedMove]()
        var move: LearnedMove!
        let request = LearnedMove.createFetchRequest()
        request.predicate = NSPredicate(format: "hoopCount == %@", docID)
        
        do {
            moves = try persistentContainer.viewContext.fetch(request)
            print(moves.count)
            move = moves.first
        } catch {
            print("Couldn't fetch LearnedMove: \(error)")
        }
        return move
    }
    
    /// Check if the Temp Move created by Archive matches any Learned Moves stored in disk
    func checkIfLearned(_ move: TempMove) -> Bool {
        
        var moves = [LearnedMove]()
        let request = LearnedMove.createFetchRequest()
        request.predicate = NSPredicate(format: "docID == %@", move.docID)
        
        do {
            moves = try persistentContainer.viewContext.fetch(request)
        } catch {
            print("Couldn't check if move is learned \(error)")
        }
        
        if moves.isEmpty {
            return false
        } else {
            return true
        }
        
    }
    
    
// MARK: - Combos
    
    /// Add Combo
    func addCombo(_ tempCombo: TempCombo) {
        let now = Date().currentTimeMillis()
        let combo = Combo(context: self.persistentContainer.viewContext)
        combo.docID = tempCombo.docID
        combo.hoopCount = tempCombo.hoopCount
        combo.favorite = false
        combo.created = now
        if !tempCombo.moves.isEmpty {
            combo.moves = NSOrderedSet(array: tempCombo.moves)
        } else {
            var moves = [LearnedMove]()
            guard let ids = tempCombo.moveDocIDs else { return }
            for id in ids {
                moves.append(fetchLearnedMove(withDocID: id))
            }
            combo.moves = NSOrderedSet(array: moves)
        }
        
        saveContext()
        print("Saved Combo for \(tempCombo.hoopCount) with moves: \(tempCombo.moves)")
    }
    
    
    /// Remove Combo
    func removeCombo(_ tempCombo: TempCombo) {
        let request = Combo.createFetchRequest()
        request.predicate = NSPredicate(format: "docID == %@", tempCombo.docID)
        
        do {
            let combos = try persistentContainer.viewContext.fetch(request)
            for combo in combos {
                persistentContainer.viewContext.delete(combo)
            }
            saveContext()
        } catch {
            print("Couldn't get Combo: \(error)")
        }
    }
    
    
    func removeCombo(_ combo: Combo) {
        let request = Combo.createFetchRequest()
        request.predicate = NSPredicate(format: "docID == %@", combo.docID)
        
        do {
            let combos = try persistentContainer.viewContext.fetch(request)
            for combo in combos {
                persistentContainer.viewContext.delete(combo)
            }
            saveContext()
        } catch {
            print("Couldn't get Combo: \(error)")
        }
    }
    
    
    /// Batch write Combo
    func batchAddCombo(_ combos: [TempCombo]) {
        for tempCombo in combos {
            let combo = Combo(context: self.persistentContainer.viewContext)
            combo.docID = tempCombo.docID
            combo.hoopCount = tempCombo.hoopCount
            combo.favorite = tempCombo.favorite
            if let name = tempCombo.name {
                combo.name = name
            }
            // Use the docIDs to get the LearnedMoves from disk and add to Combo
            var moves = [LearnedMove]()
            guard let ids = tempCombo.moveDocIDs else { return }
            for id in ids {
                moves.append(fetchLearnedMove(withDocID: id))
            }
            combo.moves = NSOrderedSet(array: moves)
        }
        saveContext()
        print("Saved \(combos.count) moves")
    }
    
    
    /// Fetch all Combos
    func fetchCombos() -> [Combo] {
        var combos = [Combo]()
        let request = Combo.createFetchRequest()
        
        do {
            combos = try persistentContainer.viewContext.fetch(request)
            print("Got \(combos.count) combos")
        } catch {
            print("Couldn't fetch Combos: \(error)")
        }
        return combos
    }
    
    
    /// Count number of Learned Moves stored in disk
    func countCombos() -> Int {
        var combos = [Combo]()
        let request = Combo.createFetchRequest()
        
        do {
            combos = try persistentContainer.viewContext.fetch(request)
        } catch {
            print("Couldn't fetch Combos: \(error)")
        }
        
        return combos.count
    }
    
    
    /// Toggle favorite
    func toggleFavorite(_ combo: Combo, _ favorite: Bool) {
        let request = Combo.createFetchRequest()
        request.predicate = NSPredicate(format: "docID == %@", combo.docID)
        
        do {
            let combos = try persistentContainer.viewContext.fetch(request)
            for combo in combos {
                combo.favorite = favorite
            }
            saveContext()
        } catch {
            print("Couldn't get Combo: \(error)")
        }
    }
    
    
// MARK: - Videos
    
    /// Add video to In Progress
    func saveVideo(forMoveNamed name: String, withURL url: String, hoopCount: Int) {
        let video = Video(context: self.persistentContainer.viewContext)
        video.moveName = name
        video.url = url
        video.hoopCount = hoopCount
        
        saveContext()
        print("Saved for later: \(video.moveName) with url \(video.url)")
        NotificationCenter.default.post(name: .userDidSaveVideo, object: nil, userInfo: ["video" : video])
    }
    
    
    /// Get saved Videos
    func getVideos() -> [Video] {
        var videos = [Video]()
        let request = Video.createFetchRequest()
        print("Starting Video request")
        
        do {
            videos = try persistentContainer.viewContext.fetch(request)
            for video in videos {
                print(video)
            }
        } catch {
            print("Couldn't retrieve saved Videos from disk: \(error)")
        }
        return videos
    }
    
    
    /// Get specific Video
    func getVideo(withURL url: String) -> [Video] {
        var videos = [Video]()
        let request = Video.createFetchRequest()
        request.predicate = NSPredicate(format: "url == %@", url)
        print("Starting Video request")
        
        do {
            videos = try persistentContainer.viewContext.fetch(request)
            for video in videos {
                print(video.url)
            }
        } catch {
            print("Couldn't retrieve saved Videos from disk: \(error)")
        }
        return videos
    }
    
    
    /// Delete Video
    func removeVideo(_ video: Video) {
        
        print("CoreData URL: \(video.url)")
        
        let request = Video.createFetchRequest()
        request.predicate = NSPredicate(format: "url == %@", video.url)
        
        do {
            let videos = try persistentContainer.viewContext.fetch(request)
            for v in videos {
                persistentContainer.viewContext.delete(v)
            }
            saveContext()
        } catch {
            print("Couldn't get Video: \(error)")
        }
        
    }
    
    
// MARK: - Delete All
    
    func deleteAndRebuild() {
        persistentContainer.viewContext.deleteAllData()
    }
    
    
// MARK: - Save
    
    /// Save changes
    func saveContext() {
        if persistentContainer.viewContext.hasChanges {
            do {
                try persistentContainer.viewContext.save()
            } catch {
                print("An error occurred while saving: \(error)")
            }
        }
    }
    
    
}


// MARK: - Enum (MoveOrCombo)

enum MoveOrCombo: String {
    
    case move = "movesLearned"
    case combo = "combosCreated"
    
}
