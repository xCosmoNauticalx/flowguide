//
//  Premium.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/8/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import UIKit
import StoreKit

protocol Premium: AnyObject {
    
    var navigationController: UINavigationController? { get }
    
    var tempUser: TempUser! { get set }
    
    func presentPremiumPage()
    
}

extension Premium {
    
    func presentPremiumPage() {
        
        let dispatch = DispatchGroup()
        PurchasesHelper.getOfferings(dispatch: dispatch) { (offering) in
            dispatch.notify(queue: .main, execute:  {
                let vc = PremiumViewController()
                vc.premium = self
                vc.offering = offering
                vc.tempUser = self.tempUser
                self.navigationController?.present(vc, animated: true, completion: nil)
            })
        }
 
        
    }
    
    
}





