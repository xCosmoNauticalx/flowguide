//
//  Coordinator.swift
//  Flow Guide
//
//  Created by Kelsey Garcia on 10/7/19.
//  Copyright © 2019 Kelsey Garcia. All rights reserved.
//

import Foundation
import UIKit

protocol Coordinator: AnyObject {
    
    /// Controls each tab
    
    var childCoordinators: [Coordinator] { get set }
    // Child coordinator array
    var navigationController: UINavigationController { get set }
    // Sets up a Navigation Controller
    
    func start(slideDelegate: Slideable?)
    
    
}
