//
//  Generating.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/2/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import Firebase

/*protocol Generating {
    
    var data: GeneratorData { get set }
    var collectionView: UICollectionView! { get set }
    
    func fetchMoves(hoopCount: Int, moveCount: Int)
    func generateCombo()
    
}

extension Generating {
    
    func fetchMoves(hoopCount: Int, moveCount: Int) {
        
        let dispatch = DispatchGroup()
        // Get moves from Firestore using a dispatch group since it's asynchronous
        FirestoreService.getLearnedMoves(hoopCount: hoopCount, dispatch: dispatch) {(move) in
            dispatch.notify(queue: .main, execute: {
                print("Number of moves final: \(move.count)")
                self.data.moves = move
                self.data.moveCount = moveCount
                self.data.hoopCount = hoopCount
                print(self.data.moves)
                self.generateCombo()
                print("Generated")
                self.collectionView.reloadData()
            })
        }
        
        print("Outside Dispatch")
    }
    
    /// Randomly pick user's desired # of moves
    func generateCombo() {
        
        if data.moveCount <= data.moves.count {
            data.newMoves = data.moves[randomPick: data.moveCount]
        } else {
            print("Could not randomly pick moves")
        }
        
        // Toggle saveButton.isSelected on VC
        
    }

    
}*/
