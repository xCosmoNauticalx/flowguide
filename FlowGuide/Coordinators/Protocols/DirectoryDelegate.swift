//
//  DirectoryDelegate.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/27/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import UIKit

protocol DirectoryDelegate: AnyObject {
    
    func directorySelected(at location: Location, count: Int)
    var navigationController: UINavigationController { get set }
    
}

extension DirectoryDelegate {
    
    func directorySelected(at location: Location, count: Int = 1) {
        var counter = 0
        switch location {
        case .first:
            counter = count
        case .middle:
            counter = 2
        case .last:
            counter = 1
        case .single:
            counter = 1
        }
        
        while counter > 0 {
            navigationController.popViewController(animated: false)
            counter -= 1
        }

    }
    
    func backTwo() {
        let viewControllers: [UIViewController] = navigationController.viewControllers as [UIViewController]
        navigationController.popToViewController(viewControllers[viewControllers.count - 3], animated: true)
    }
    
}

enum Location {
    case first
    case middle
    case last
    case single
}
