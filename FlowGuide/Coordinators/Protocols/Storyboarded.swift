//
//  Storyboarded.swift
//  Flow Guide
//
//  Created by Kelsey Garcia on 10/7/19.
//  Copyright © 2019 Kelsey Garcia. All rights reserved.
//

import Foundation
import UIKit

protocol Storyboarded { // Lets us create view controllers from storyboard
    
    static func instantiate() -> Self // Whatever conforms to storyboarded will an instantiate method on the type itself, that, when called, will return the type.
    
}

extension Storyboarded where Self: UIViewController {  // Allows us to create view controllers in the storyboard have the same storyboard ID as their class name.
    
    static func instantiate() -> Self {
        
        let fullName = NSStringFromClass(self)  // Pulls out "FlowGuide.ViewController", which is the name of the view controller
        let className = fullName.components(separatedBy: ".")[1]    // Splits by the dot and uses everything after, giving "ViewController"
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)    //Load our storyboard
        
        return storyboard.instantiateViewController(withIdentifier: className) as! Self     //Instantiate a view controller with that identifier, and force cast as the type that was requested
        
    }
    
}
