//
//  Layout.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/12/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import UIKit

protocol Layout {
    
    var compactConstraints: [NSLayoutConstraint] { get set }
    var regularConstraints: [NSLayoutConstraint] { get set }
    var sharedConstraints: [NSLayoutConstraint] { get set }
    
    func layoutTrait(traitCollection:UITraitCollection)
    
}

extension Layout {
    
    func layoutTrait(traitCollection:UITraitCollection) {
        if (!sharedConstraints[0].isActive) {
           // activating shared constraints
           NSLayoutConstraint.activate(sharedConstraints)
        }
        if traitCollection.horizontalSizeClass == .compact && traitCollection.verticalSizeClass == .regular {
            if regularConstraints.count > 0 && regularConstraints[0].isActive {
                NSLayoutConstraint.deactivate(regularConstraints)
            }
            // activating compact constraints
            NSLayoutConstraint.activate(compactConstraints)
        } else {
            if compactConstraints.count > 0 && compactConstraints[0].isActive {
                NSLayoutConstraint.deactivate(compactConstraints)
            }
            // activating regular constraints
            NSLayoutConstraint.activate(regularConstraints)
        }
    }
    
}
