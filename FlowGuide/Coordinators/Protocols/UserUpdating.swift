//
//  UserUpdating.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/1/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation

protocol UserUpdating: AnyObject {
    
    var user: User? { get set }
    
    var moveIncrement: Int { get set }
    
    var comboIncrement: Int { get set }
    
}

extension UserUpdating {
    

    
    /// Updates the Incrementer on MasterContainerController
    func updateMoveIncrement(by number: Int) {
        moveIncrement += number
    }
    
    func updateComboIncrement(by number: Int) {
        comboIncrement += number
    }
    
}
