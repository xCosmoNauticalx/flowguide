//
//  TitleProtocol.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 3/10/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import UIKit

protocol Titleable: AnyObject {
    
    var titleLabel: TitleLabel { get set }
    
    func changeTitleText(to title: String)
    
    func changeBackTitleText(to title: String, frame: CGRect)
    
    func changeTitleColor(to color: UIColor)
    
}

extension Titleable {
    
    func changeTitleText(to title: String) {
        titleLabel.text = title
    }
    
    func changeTitleColor(to color: UIColor) {
        UIView.animate(withDuration: 0.3, animations: {
            self.titleLabel.textColor = color
        })
    }
    
    func changeBackTitleText(to title: String, frame: CGRect) {
        titleLabel.frame = frame
        titleLabel.text = title
        titleLabel.textColor = UIColor.background
        titleLabel.layer.shadowColor = UIColor.background.cgColor
        titleLabel.layer.shadowRadius = 1.5
        titleLabel.layer.shadowOpacity = 0.25
        titleLabel.layer.shadowOffset = CGSize(width: 3, height: 3)
        
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            titleLabel.font = UIFont(name: "HelveticaNeue", size: 49)
        } else {
            titleLabel.font = UIFont(name: "Helvetica", size: 39)
        }
    }
    
}
