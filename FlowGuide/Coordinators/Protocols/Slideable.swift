//
//  LibraryMenuDelegate.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 3/12/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import UIKit

protocol Slideable: UITabBarControllerDelegate {
    
    func handleMenuToggle()
    
    func logOut()

}



