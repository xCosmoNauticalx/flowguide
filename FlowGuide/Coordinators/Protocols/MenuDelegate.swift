//
//  MenuDelegate.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/29/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation

protocol MenuDelegate: AnyObject {
    func didTapMenuItem(indexPath: IndexPath)
}
