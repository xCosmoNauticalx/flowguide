//
//  Playable.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/19/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import UIKit
import SafariServices
import GoogleMobileAds

protocol Playable: AnyObject {
    
    var navigationController: UINavigationController { get set }
    
    var safariVC: SFSafariViewController! { get set }
    
    
}

extension Playable {
    
    func play(_ youtubeID: String, _ delegate: SFSafariViewControllerDelegate? = nil) {
        // check if user has Youtube installed
        if let youtubeURL = URL(string: "youtube://\(youtubeID)"),
            UIApplication.shared.canOpenURL(youtubeURL) {
                // redirect to youtube
            UIApplication.shared.open(youtubeURL, options: [:], completionHandler: nil)
        } else {
            let youtubeURL = "https://www.youtube.com/watch?v=\(youtubeID)"
            playInSafari(youtubeURL, delegate)
        }
        

    }
    
    
    func playInSafari(_ urlString: String, _ delegate: SFSafariViewControllerDelegate? = nil) {
        if let url = URL(string: urlString) {
            print("Converted URL")
            let config = SFSafariViewController.Configuration()
            safariVC = SFSafariViewController(url: url, configuration: config)
            
            if delegate != nil {
                safariVC.delegate = delegate
            }
            
            navigationController.present(safariVC, animated: true)
            print("Completed")
        }
        
    }

    
}
