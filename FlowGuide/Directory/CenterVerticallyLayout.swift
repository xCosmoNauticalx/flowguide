//
//  CenterVerticallyLayout.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/28/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import UIKit

// NOTE: Doesn't work for horizontal layout!
final class CenterVerticallyLayout: UICollectionViewFlowLayout {

    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        guard let superAttributes = super.layoutAttributesForElements(in: rect) else { return nil }
        // Copy each item to prevent "UICollectionViewFlowLayout has cached frame mismatch" warning
        guard let attributes = NSArray(array: superAttributes, copyItems: true) as? [UICollectionViewLayoutAttributes] else { return nil }

        // Constants
        let topPadding: CGFloat = 8
        let lineSpacing = minimumLineSpacing

        // Tracking values
        var topMargin: CGFloat = topPadding // Modified to determine origin.x for each item
        var maxX: CGFloat = -1.0 // Modified to determine origin.y for each item
        var rowSizes: [[CGFloat]] = [] // Tracks the starting and ending y-values for the row's height
        var currentRow: Int = 0 // Tracks the current row
        attributes.forEach { layoutAttribute in

            // Each layoutAttribute represents its own item
            if layoutAttribute.frame.origin.x >= maxX {

                // This layoutAttribute represents the top-most item in the row
                topMargin = topPadding

                // Register its origin.y in rowSizes for use later
                if rowSizes.count == 0 {
                    // Add to first row
                    rowSizes = [[topMargin, 0]]
                } else {
                    // Append a new row
                    rowSizes.append([topMargin, 0])
                    currentRow += 1
                }
            }

            layoutAttribute.frame.origin.y = topMargin

            topMargin += layoutAttribute.frame.height + lineSpacing
            maxX = max(layoutAttribute.frame.maxX, maxX)

            // Add bottom-most y value for the row
            rowSizes[currentRow][1] = topMargin - lineSpacing
        }

        // At this point, all cells are top aligned
        // Reset tracking values and add extra top padding to center align the row
        topMargin = topPadding
        maxX = -1.0
        currentRow = 0
        attributes.forEach { layoutAttribute in

            // Each layoutAttribute is its own item
            if layoutAttribute.frame.origin.x >= maxX {

                // This layoutAttribute represents the top-most item in the row
                topMargin = topPadding

                // Need to bump it up by an appended margin
                let rowHeight = rowSizes[currentRow][1] - rowSizes[currentRow][0] // last.y - first.y
                let appendedMargin = (collectionView!.frame.height - topPadding  - rowHeight - topPadding) / 2
                topMargin += appendedMargin

                currentRow += 1
            }

            layoutAttribute.frame.origin.y = topMargin

            topMargin += layoutAttribute.frame.height + lineSpacing
            maxX = max(layoutAttribute.frame.maxX, maxX)
        }

        return attributes
    }
    
    
//    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
//        guard let collectionView = collectionView else {
//            fatalError()
//        }
//        guard let layoutAttributes = super.layoutAttributesForItem(at: indexPath)?.copy() as? UICollectionViewLayoutAttributes else {
//            return nil
//        }
//
//        layoutAttributes.frame.origin.x = sectionInset.left
//        layoutAttributes.frame.size.width = collectionView.safeAreaLayoutGuide.layoutFrame.width - sectionInset.left - sectionInset.right
//        return layoutAttributes
//    }
}

