//
//  DirectoryController.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/21/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

private let reuseIdentifier = "Cell"

class DirectoryController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
// MARK: - Properties
    
    /// Delegate
    weak var delegate: DirectoryDelegate?
    
    /// Data
    let data = DirectoryData()
    var layout = CenterLayout()
    
    
// MARK: - Life Cycle
    
    override func loadView() {
        super.loadView()
        setUp()
    }
    
    
// MARK: - Set Up
    
    func setUp() {
        collectionView.dataSource = data
        collectionView.collectionViewLayout = layout
        collectionView.backgroundColor = .clear
        self.collectionView.register(DirectoryCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        layout.estimatedItemSize = UICollectionViewFlowLayout.automaticSize
    }
    
    func setDelegate(with delegate: DirectoryDelegate) {
        self.delegate = delegate
    }
    
    
// MARK: - UICollectionViewDelegate
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let item = data.parents[indexPath.item]
        // If there's more than one option,
        if data.parents.count > 1 {
            // Check if it's the last in the list
            if item == data.parents.last {
                delegate?.directorySelected(at: .last)
            // Check if it's the first in the list
            } else if item == data.parents.first {
                // Figure out how many steps it will take to get back to the beginning
                if data.parents.count == 2 {
                    delegate?.directorySelected(at: .first, count: 2)
                } else if data.parents.count == 3 {
                    delegate?.directorySelected(at: .first, count: 3)
                }
            // It's neither, so it must be in the middle
            } else {
                delegate?.directorySelected(at: .middle)
            }
        } else {
            // There is only one option
            delegate?.directorySelected(at: .first, count: 1)
        }
    }

}
