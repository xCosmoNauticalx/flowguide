//
//  DirectoryData.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/21/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class DirectoryData: NSObject, UICollectionViewDataSource {
    
    var parents = [Node]()
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return parents.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell: DirectoryCell = collectionView.dequeueReusableCell(withReuseIdentifier: "Cell", for: indexPath) as? DirectoryCell else {
        fatalError("Unable to dequeue DirectoryCell.")
        }
        
        let parent = parents[indexPath.item]
        
        let last = parents.count - 1
        
        if indexPath.item == last {
            cell.label.text = parent.name
        } else {
            cell.label.text = parent.name + "   >"
        }
        
        
        
        return cell
    }
    

}
