//
//  UIButton.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/9/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {

    func gradientBorder(_ colors: [CGColor]) {
    
        let gradientBorder = CAGradientLayer()
        gradientBorder.frame = bounds
        //Set gradient to be horizontal
        gradientBorder.startPoint = CGPoint(x: 0, y: 0.5)
        gradientBorder.endPoint = CGPoint(x: 1, y: 0.5)
        gradientBorder.colors = colors

        self.layer.cornerRadius = 10
        self.clipsToBounds = true
        
        let shape = CAShapeLayer()
        shape.lineWidth = 2
        shape.path = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.layer.cornerRadius).cgPath
        shape.strokeColor = UIColor.black.cgColor
        shape.fillColor = UIColor.clear.cgColor
        gradientBorder.mask = shape

        layer.addSublayer(gradientBorder)
        
    }
    
    func styleFilledButton(_ button:UIButton) {
        
        // Filled rounded corner style
        button.backgroundColor = UIColor.white
        button.setTitleColor(UIColor.background, for: .normal)
        button.layer.cornerRadius = 10
        button.clipsToBounds = true
        button.tintColor = UIColor.background

    }
    
    func styleHollowButton(color: CGColor) {
        
        // Hollow rounded corner style
        self.layer.borderWidth = 1
        self.layer.borderColor = color
        self.layer.cornerRadius = 10.0
        self.tintColor = UIColor.white
    }

}



