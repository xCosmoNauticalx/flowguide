//
//  Date+Extension.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/22/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation


extension Date {
    
    /// Get Unix Timestamp
    func currentTimeMillis() -> Int64 {
        return Int64(self.timeIntervalSince1970 * 1000)
    }
    
}
