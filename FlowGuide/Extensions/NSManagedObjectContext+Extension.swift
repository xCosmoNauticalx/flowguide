//
//  NSManagedObjectContext+Extension.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/11/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import CoreData

extension NSManagedObjectContext
{
    func deleteAllData()
    {
        guard let persistentStore = persistentStoreCoordinator?.persistentStores.last else {
            return
        }

        guard let url = persistentStoreCoordinator?.url(for: persistentStore) else {
            return
        }

        performAndWait { () -> Void in
            self.reset()
            do {
                try self.persistentStoreCoordinator?.remove(persistentStore)
                try FileManager.default.removeItem(at: url)
                try self.persistentStoreCoordinator?.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: url, options: nil)
            } catch {
                print("Error deleting all data: \(error)")
            }
        }
    }
}
