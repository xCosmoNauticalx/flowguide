//
//  UITextField.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/10/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    
    func addBottomBorder(_ colors: [CGColor]){
        
        let bottomLine = CAGradientLayer()
        bottomLine.frame = CGRect(x: 0, y: self.frame.height - 2, width: self.frame.width, height: 2)
        bottomLine.colors = colors
        bottomLine.startPoint = CGPoint(x: 0.0, y: 0.5)
        bottomLine.endPoint = CGPoint(x: 1.0, y: 0.5)
        
        // Remove border on text field
        self.borderStyle = .none
        
        // Add the line to the text field
        self.layer.addSublayer(bottomLine)
    }
    
}
