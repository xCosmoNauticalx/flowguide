//
//  Colors.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/8/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import UIKit

extension UIColor {

        // static attaches the variable to the class rather than an object
    struct Archive {
        static var dark: UIColor { return UIColor(red:0.18, green:0.53, blue:0.58, alpha:1.0)}
        static var mid: UIColor { return UIColor(red:0.32, green:0.69, blue:0.72, alpha:1.0)}
        static var light: UIColor { return UIColor(red:0.36, green:0.70, blue:0.65, alpha:1.0)}
    }
        
    struct Library {
        static var dark: UIColor { return UIColor(red: 0.87, green: 0.19, blue: 0.61, alpha: 1.00)}
        static var mid: UIColor { return UIColor(red: 0.91, green: 0.20, blue: 0.56, alpha: 1.00)}
        static var light: UIColor { return UIColor(red: 0.95, green: 0.23, blue: 0.52, alpha: 1.00)}
    }
        
    struct Combo {
        static var dark: UIColor { return UIColor(red:0.94, green:0.62, blue:0.40, alpha:1.0)}
        static var mid: UIColor { return UIColor(red:0.96, green:0.75, blue:0.39, alpha:1.0)}
        static var light: UIColor { return UIColor(red:0.98, green:0.88, blue:0.38, alpha:1.0)}
    }
    
    struct Progress {
        static var dark: UIColor { return UIColor(red:0.25, green:0.86, blue:0.45, alpha:1.0)}
        static var mid: UIColor { return UIColor(red:0.41, green:0.83, blue:0.35, alpha:1.0)}
        static var light: UIColor { return UIColor(red:0.61, green:0.85, blue:0.25, alpha:1.0)}
    }
    
    static var background: UIColor { return UIColor(red:0.11, green:0.14, blue:0.21, alpha:1.0)}

    
    func blendedColorWithFraction(fraction: CGFloat, ofColor color: UIColor) -> UIColor {
        var r1: CGFloat = 1.0, g1: CGFloat = 1.0, b1: CGFloat = 1.0, a1: CGFloat = 1.0
        var r2: CGFloat = 1.0, g2: CGFloat = 1.0, b2: CGFloat = 1.0, a2: CGFloat = 1.0

        self.getRed(&r1, green: &g1, blue: &b1, alpha: &a1)
        color.getRed(&r2, green: &g2, blue: &b2, alpha: &a2)

        return UIColor(red: r1 * (1 - fraction) + r2 * fraction,
            green: g1 * (1 - fraction) + g2 * fraction,
            blue: b1 * (1 - fraction) + b2 * fraction,
            alpha: a1 * (1 - fraction) + a2 * fraction);
    }
}
