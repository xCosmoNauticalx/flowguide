//
//  Notification.Name+Extension.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/10/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation

extension Notification.Name {
    
    static let clearField = Notification.Name("clearField")
    static let userDidSaveVideo = Notification.Name("userDidSaveVideo")
    static let noMoreVideos = Notification.Name("noMoreVideos")
    static let purchaseNotification = Notification.Name("purchaseNotification")
    static let checkDevice = Notification.Name("checkDevice")
    static let deviceChecked = Notification.Name("deviceChecked")
    static let showInterstitial = Notification.Name("showInterstitial")
    static let shouldShowAd = Notification.Name("shouldShowAd")
}
