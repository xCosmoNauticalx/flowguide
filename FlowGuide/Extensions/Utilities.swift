//
//  Utilities.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/8/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAuth

class Utilities {
    
// MARK: - Style Bars
    
    static func styleTabBar(_ viewController: UIViewController, color: UIColor) {
        viewController.tabBarController?.tabBar.tintColor = color
        viewController.tabBarController?.tabBar.barTintColor = color
        viewController.tabBarController?.tabBar.unselectedItemTintColor = color
        viewController.tabBarController?.tabBar.tintColorDidChange()
        viewController.tabBarController?.tabBarItem.title = nil
    }
    
    static func styleNavBar(_ viewController: UIViewController, color: UIColor) {
        let backImage = UIBarButtonItem(image: UIImage(named: "arrow_blue"), style: .plain, target: viewController.navigationController, action: #selector(UINavigationController.popViewController(animated:)))
        viewController.navigationItem.leftBarButtonItem = backImage
        viewController.navigationController?.navigationBar.tintColor = color
        viewController.navigationItem.backBarButtonItem?.title = ""
    }
    
    
    
// MARK: - Validate Password
    
    static func isPasswordValid(_ password : String) -> Bool {
        
        let passwordTest = NSPredicate(format: "SELF MATCHES %@", "^(?=.*[a-z])(?=.*[$@$#!%*?&])[A-Za-z\\d$@$#!%*?&]{8,}")
        return passwordTest.evaluate(with: password)
    }
    
    
    
    
    
// MARK: - Draw
    
    static func createBubble() -> UIBezierPath {
        
        let path = UIBezierPath()
        path.move(to: CGPoint(x: 139.5, y: 41.5))
        path.addCurve(to: CGPoint(x: 139.5, y: 67.75), controlPoint1: CGPoint(x: 139.5, y: 41.5), controlPoint2: CGPoint(x: 139.5, y: 67.75))
        path.addLine(to: CGPoint(x: 123.38, y: 67.75))
        path.addCurve(to: CGPoint(x: 114, y: 76.5), controlPoint1: CGPoint(x: 118.44, y: 71.69), controlPoint2: CGPoint(x: 114, y: 76.5))
        path.addCurve(to: CGPoint(x: 104.79, y: 67.75), controlPoint1: CGPoint(x: 114, y: 76.5), controlPoint2: CGPoint(x: 109.73, y: 71.74))
        path.addLine(to: CGPoint(x: 88.5, y: 67.75))
        path.addLine(to: CGPoint(x: 88.5, y: 41.5))
        path.addLine(to: CGPoint(x: 139.5, y: 41.5))
        path.addLine(to: CGPoint(x: 139.5, y: 41.5))
        path.close()
        UIColor.white.setFill()
        path.fill()
        UIColor.white.setStroke()
        path.lineWidth = 5
        path.lineJoinStyle = .round
        path.stroke()
        
        return path
        
    }
    
    
// MARK: - Animations
    
    static func animateErrorLabel(_ label: UILabel) {
        
        UIView.animate(withDuration: 0.25, animations: {
            label.alpha = 1.0
        }) { (completed) in
            UIView.animate(withDuration: 0.25, delay: 4, options: [], animations: {
                label.alpha = 0.0
            }, completion: nil)
        }
        
    }
    
    
    
    
// MARK: - Adaptive Constraints
    static func anchor(_ view: UIView, top: NSLayoutYAxisAnchor?, leading: NSLayoutXAxisAnchor?, bottom: NSLayoutYAxisAnchor?, trailing: NSLayoutXAxisAnchor?, padding: UIEdgeInsets = .zero, size: CGSize = .zero) -> [NSLayoutConstraint] {
        view.translatesAutoresizingMaskIntoConstraints = false
        
        var constraints = [NSLayoutConstraint]()
        
        if let top = top {
            constraints.append(view.topAnchor.constraint(equalTo: top, constant: padding.top))
        }
        
        if let leading = leading {
            constraints.append(view.leadingAnchor.constraint(equalTo: leading, constant: padding.left))
        }
        
        if let bottom = bottom {
            constraints.append(view.bottomAnchor.constraint(equalTo: bottom, constant: padding.bottom))
        }
        
        if let trailing = trailing {
            constraints.append(view.trailingAnchor.constraint(equalTo: trailing, constant: padding.right))
        }
        
        if size.width != 0 {
            constraints.append(view.widthAnchor.constraint(equalToConstant: size.width))
        }
        
        if size.height != 0 {
            constraints.append(view.heightAnchor.constraint(equalToConstant: size.height))
        }
        
        return constraints
        
    }
    
    static func anchorHeight(_ view: UIView, top: NSLayoutYAxisAnchor?, bottom: NSLayoutYAxisAnchor?, topPadding: CGFloat = 0, bottomPadding: CGFloat = 0, size: CGSize = .zero) -> [NSLayoutConstraint] {
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        var constraints = [NSLayoutConstraint]()
        
        if let top = top {
            constraints.append(view.topAnchor.constraint(equalTo: top, constant: topPadding))
        }
        
        if let bottom = bottom {
            constraints.append(view.bottomAnchor.constraint(equalTo: bottom, constant: bottomPadding))
        }
        
        if size.height != 0 {
            constraints.append(view.heightAnchor.constraint(equalToConstant: size.height))
        }
        
        return constraints
        
    }
    
    
    static func anchorWidth(_ view: UIView, leading: NSLayoutXAxisAnchor?, trailing: NSLayoutXAxisAnchor?, leftPadding: CGFloat = 0, rightPadding: CGFloat = 0, size: CGSize = .zero) -> [NSLayoutConstraint] {
        
        view.translatesAutoresizingMaskIntoConstraints = false
        
        var constraints = [NSLayoutConstraint]()
        
        if let leading = leading {
            constraints.append(view.leadingAnchor.constraint(equalTo: leading, constant: leftPadding))
        }
        
        if let trailing = trailing {
            constraints.append(view.trailingAnchor.constraint(equalTo: trailing, constant: rightPadding))
        }
        
        if size.width != 0 {
            constraints.append(view.widthAnchor.constraint(equalToConstant: size.width))
        }
        
        return constraints
        
    }
    
    static func anchorSize(_ current: UIView, to view: UIView) -> [NSLayoutConstraint] {
        
        var constraints = [NSLayoutConstraint]()
        
        constraints.append(current.widthAnchor.constraint(equalTo: view.widthAnchor))
        constraints.append(current.heightAnchor.constraint(equalTo: view.heightAnchor))
        
        return constraints
    }
    
    
    static func fillSuperView(_ view: UIView) -> [NSLayoutConstraint] {
        
        let constraints = Utilities.anchor(view, top: view.superview?.topAnchor, leading: view.superview?.leadingAnchor, bottom: view.superview?.bottomAnchor, trailing: view.superview?.trailingAnchor)
        
        return constraints
    }
    
    
}


