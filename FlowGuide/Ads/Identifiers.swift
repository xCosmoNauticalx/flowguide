//
//  Identifiers.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/11/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation

struct AdIdentifier {
    
    static let banner = "ca-app-pub-3940256099942544/2934735716"
    static let interstitial = "ca-app-pub-3940256099942544/4411468910"
    static let interstitialVideo = "ca-app-pub-3940256099942544/5135589807"
    static let native = "ca-app-pub-3940256099942544/3986624511"
    static let nativeVideo = "ca-app-pub-3940256099942544/2521693316"
    
}
