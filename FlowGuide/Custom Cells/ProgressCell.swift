//
//  ProgressCell.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/22/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class ProgressCell: UITableViewCell {

// MARK: - Properties
    
    /// Data
    
    var url: String = ""
    
    /// Views
    let label: UILabel = {
        let l = UILabel()
        l.font = UIFont(name: "HelveticaNeue", size: 25)
        l.textAlignment = .left
        l.adjustsFontSizeToFitWidth = true
        l.minimumScaleFactor = 0.7
        l.numberOfLines = 0
        l.lineBreakMode = .byTruncatingTail
        return l
    }()
    
    let gradientView: UIView = {
        let v = UIView()
        return v
    }()
    
    let selectedBackground: UIView = {
        let v = UIView()
        v.backgroundColor = .clear
        return v
    }()
    
    let container = UIView()
    
    
// MARK: - Overrides
    
    override var isSelected: Bool {
        didSet {
            toggleIsSelected()
        }
    }
    
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        gradientView.frame = bounds
        selectedBackground.frame = bounds
        
    }
    
    
// MARK: - Init
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        setUp()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setUp() {
        
        self.backgroundColor = .clear
        self.selectedBackgroundView = selectedBackground
        
        /// Change font for iPad
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            label.font = UIFont(name: "HelveticaNeue", size: 37)
        }
        
        /// Add Views
        addSubview(container)
        addSubview(label)
        addSubview(gradientView)
        
        /// Constraints
        container.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor, padding: .init(top: 10, left: 30, bottom: -10, right: 0))
        label.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor)
        gradientView.anchor(top: container.topAnchor, leading: container.leadingAnchor, bottom: container.bottomAnchor, trailing: container.trailingAnchor)
        
    }
    
// MARK: - Methods
    
    func toggleIsSelected() {
        
        UIView.animate(withDuration: 0.01, delay: 0, options: [.curveEaseOut], animations: {
            self.alpha = self.isSelected ? 0.5 : 1.0
        })
    }

}
