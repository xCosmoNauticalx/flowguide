//
//  DirectoryCell.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/21/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class DirectoryCell: UICollectionViewCell {
    
// MARK: - Properties
    
    let label: UILabel = {
        let l = UILabel()
        l.font = UIFont(name: "HelveticaNeue", size: 15)
        l.textColor = .white
        l.textAlignment = .center
        return l
    }()
    
    
// MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
// MARK: - Set Up
    
    func setUp() {
        self.addSubview(label)
        label.fillSuperView()
    }
    
}
