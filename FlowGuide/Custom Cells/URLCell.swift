//
//  URLCell.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/13/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class URLCell: UICollectionViewCell {
    
// MARK: - Properties
    
    let border: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "square")
        i.contentMode = .scaleAspectFit
        return i
    }()
    
    let number: UILabel = {
        let l = UILabel()
        l.font = UIFont(name: "HelveticaNeue", size: 22)
        l.text = "27"
        l.textColor = UIColor.Archive.mid
        l.textAlignment = .center
        return l
    }()
    
    let greenFill: UIView = {
        let v = UIView()
        v.alpha = 0
        return v
    }()
    
    
// MARK: - Overrides
    
    override var isSelected: Bool {
        didSet {
            toggleIsSelected()
        }
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        greenFill.frame = bounds
    }
    
    
// MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setUp() {
        
        /// Change font for iPad
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            number.font = UIFont(name: "HelveticaNeue", size: 25)
        }
        
        addSubview(border)
        addSubview(greenFill)
        addSubview(number)
        
        border.fillSuperView()
        greenFill.anchor(top: border.topAnchor, leading: border.leadingAnchor, bottom: border.bottomAnchor, trailing: border.trailingAnchor)
        number.anchor(top: border.topAnchor, leading: border.leadingAnchor, bottom: border.bottomAnchor, trailing: border.trailingAnchor)
        
    }
   
    
    
// MARK: - Methods
    
    func toggleIsSelected() {
        
        UIView.animate(withDuration: 0.01, delay: 0, options: [.curveEaseOut], animations: {
            self.alpha = self.isSelected ? 0.5 : 1.0
            //self.transform = self.isSelected ? CGAffineTransform.identity.scaledBy(x: 0.97, y: 0.97) : CGAffineTransform.identity
        })
    }
}
