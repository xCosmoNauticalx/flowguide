//
//  MoveCell.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/13/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class MoveCell: UICollectionViewCell {
    
// MARK: - Properties
    
    let label: UILabel = {
        let l = UILabel()
        l.font = UIFont(name: "HelveticaNeue", size: 27)
        l.textAlignment = .center
        l.adjustsFontSizeToFitWidth = true
        l.minimumScaleFactor = 0.7
        l.numberOfLines = 0
        l.lineBreakMode = .byTruncatingTail
        return l
    }()
    
    let gradientView = UIView()
    
    
// MARK: - Overrides
    
    override var isSelected: Bool {
        didSet {
            toggleIsSelected()
        }
    }
    
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        gradientView.frame = bounds
    }
    
    
// MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
// MARK: - Set Up
    
    private func setUp() {
        
        /// Change font for iPad
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            label.font = UIFont(name: "HelveticaNeue", size: 37)
        }
        
        /// Add Views
        addSubview(label)
        addSubview(gradientView)
        
        /// Constraints
        
        label.fillSuperView()
        gradientView.anchor(top: label.topAnchor, leading: label.leadingAnchor, bottom: label.bottomAnchor, trailing: label.trailingAnchor)
        
    }
    
// MARK: - Methods
    
    func toggleIsSelected() {
        
        UIView.animate(withDuration: 0.01, delay: 0, options: [.curveEaseOut], animations: {
            self.alpha = self.isSelected ? 0.5 : 1.0
        })
    }
    
}
