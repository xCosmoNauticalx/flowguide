//
//  GroupCell.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 4/13/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import UIKit

class GroupCell: UICollectionViewCell {
    
// MARK: - Properties
    
    var border: UIImageView = {
        let i = UIImageView()
        i.image = UIImage(named: "groupBorder")
        i.contentMode = .scaleAspectFit
        return i
    }()
    
    var label: UILabel = {
        let l = UILabel()
        l.font = UIFont(name: "HelveticaNeue", size: 25)
        l.textColor = .white
        l.textAlignment = .center
        l.adjustsFontSizeToFitWidth = true
        l.minimumScaleFactor = 0.2
        l.numberOfLines = 0
        l.lineBreakMode = .byTruncatingTail
        return l
    }()
    
    var gradientView = UIView()
    var textGradient = UIView()
    

// MARK: - Overrides
    
    override var isSelected: Bool {
        didSet {
            toggleIsSelected()
        }
    }
    

// MARK: - Init
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setUp()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    public override func layoutSubviews() {
        super.layoutSubviews()
        gradientView.frame = bounds
        textGradient.frame = bounds
    }
    
    func setUp() {
        
        /// Change font for iPad
        if UIScreen.main.traitCollection.userInterfaceIdiom == .pad {
            label.font = UIFont(name: "HelveticaNeue", size: 30)
        }
        
        /// Add Views
        addSubview(gradientView)
        addSubview(border)
        addSubview(textGradient)
        addSubview(label)
        
        /// Constraints
        gradientView.fillSuperView()
        border.anchor(top: self.topAnchor, leading: self.leadingAnchor, bottom: self.bottomAnchor, trailing: self.trailingAnchor, padding: .init(top: 0, left: 60, bottom: 0, right: -60))
        label.anchor(top: border.topAnchor, leading: border.leadingAnchor, bottom: border.bottomAnchor, trailing: border.trailingAnchor, padding: .init(top: 5, left: 30, bottom: -5, right: -30))
        label.centerYAnchor.constraint(equalTo: border.centerYAnchor).isActive = true
        textGradient.anchor(top: label.topAnchor, leading: label.leadingAnchor, bottom: label.bottomAnchor, trailing: label.trailingAnchor)
        
        
    }
    
    
// MARK: - Methods
    
    func toggleIsSelected() {
        UIView.animate(withDuration: 0.01, delay: 0, options: [.curveEaseOut], animations: {
            self.alpha = self.isSelected ? 0.5 : 1.0
        })
    }
}

