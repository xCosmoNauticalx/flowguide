//
//  Shop.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/1/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation

struct HoopSmith: Decodable, Equatable {
    
    let name: String
    let url: String
    let location: Country
    let shipsTo: Country
    let hoopType: [HoopType]
    
    enum Country: Decodable, Equatable {
        case us
        case canada
        case australia
        case uk
        case germany
        case switzerland
        case netherlands
        case ireland
        case austria
        case italy
        case slovenia
        case europe
        case global
    }
    
    enum HoopType: Decodable, Equatable {
        case day
        case led
        case fire
    }
    
}

extension HoopSmith.Country: CaseIterable { }

extension HoopSmith.Country: RawRepresentable {
    typealias RawValue = String
    
    init?(rawValue: RawValue) {
        switch rawValue {
        case "US":
            self = .us
        case "Canada":
            self = .canada
        case "Australia":
            self = .australia
        case "UK":
            self = .uk
        case "Germany":
            self = .germany
        case "Switzerland":
            self = .switzerland
        case "Netherlands":
            self = .netherlands
        case "Ireland" :
            self = .ireland
        case "Austria":
            self = .austria
        case "Italy":
            self = .italy
        case "Slovenia":
            self = .slovenia
        case "Europe":
            self = .europe
        case "World":
            self = .global
        default:
            return nil
        }
    }
    
    var rawValue: RawValue {
        switch self {
        case .us:
            return "United States"
        case .canada:
            return "Canada"
        case .australia:
            return "Australia"
        case .uk:
            return "United Kingdom"
        case .germany:
            return "Germany"
        case .switzerland:
            return "Switzerland"
        case .netherlands:
            return "Netherlands"
        case .ireland:
            return "Ireland"
        case .austria:
            return "Austria"
        case .italy:
            return "Itlay"
        case .slovenia:
            return "Slovenia"
        case .europe:
            return "Europe"
        case .global:
            return "Worldwide"
      }
    }
}

extension HoopSmith.HoopType: CaseIterable { }

extension HoopSmith.HoopType: RawRepresentable {
    typealias RawValue = String
    
    init?(rawValue: RawValue) {
        switch rawValue {
        case "Day":
            self = .day
        case "LED":
            self = .led
        case "Fire":
            self = .fire
        default:
            return nil
        }
    }
    
    var rawValue: RawValue {
        switch self {
        case .day:
            return "Day"
        case .led:
            return "LED"
        case .fire:
            return "Fire"
        }
    }
}


extension HoopSmith {
  static func smiths() -> [HoopSmith] {
    guard
      let url = Bundle.main.url(forResource: "HoopSmiths", withExtension: "json"),
      let data = try? Data(contentsOf: url)
      else {
        return []
    }
    
    do {
      let decoder = JSONDecoder()
      return try decoder.decode([HoopSmith].self, from: data)
    } catch {
      return []
    }
  }
}


/// Groups
let allRegions = [HoopSmith.Country.us, HoopSmith.Country.australia, HoopSmith.Country.austria, HoopSmith.Country.canada, HoopSmith.Country.europe, HoopSmith.Country.germany, HoopSmith.Country.ireland, HoopSmith.Country.italy, HoopSmith.Country.netherlands, HoopSmith.Country.slovenia, HoopSmith.Country.switzerland, HoopSmith.Country.uk]
let europe = [HoopSmith.Country.austria, HoopSmith.Country.germany, HoopSmith.Country.ireland, HoopSmith.Country.italy, HoopSmith.Country.netherlands, HoopSmith.Country.slovenia, HoopSmith.Country.switzerland, HoopSmith.Country.uk ]


