//
//  Moves.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 2/4/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation

class TempMove {
    
    var name: String = "PlaceHolder"
    var urls: [String] = ["PlaceHolder"]
    var docID: String = "PlaceHolder"
    var hoopCount: Int = 0
    var parentNode: [String] = ["PlaceHolder"]
    var deleteCounter: Bool = false
    
    convenience init(name: String, urls: [String], docID: String, hoopCount: Int, parentNode: [String]) {
        self.init()
        self.name = name
        self.urls = urls
        self.docID = docID
        self.hoopCount = hoopCount
        self.parentNode = parentNode
    }
    
    deinit {
        print("Deallocating TempMove: \(name)")
    }
    
}


