//
//  Archive.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 11/23/19.
//  Copyright © 2019 CosmoNautical. All rights reserved.
//

// MARK: Class Definition

import Foundation

enum group {
    case archive
    case combo
    case none
}

public class Node: Hashable {
    
    let name: String
    var children: [Node] = []
    let finalGroup: group
    let instanceName: String
    
    
    init(name: String, finalGroup: group, instanceName: String) {
        self.name = name
        self.finalGroup = finalGroup
        self.instanceName = instanceName
    }
    
    func add(child: Node) {
        children.append(child)
    }

    public static func == (lhs: Node, rhs: Node) -> Bool {
        return lhs.instanceName == rhs.instanceName && lhs.children == rhs.children && lhs.finalGroup == rhs.finalGroup && lhs.instanceName == rhs.instanceName
    }
    
    public func hash(into hasher: inout Hasher) {
        hasher.combine(instanceName)
    }
    
}








// MARK: Creating Nodes

// Root
let tutorials = Node(name: "Tutorials" , finalGroup: .none, instanceName: "")
let library = Node(name: "My Library", finalGroup: .none, instanceName: "")


// MARK: Level 1

let single = Node(name: "Single Hoop", finalGroup: .none, instanceName: "single")
let doubles = Node(name: "Doubles", finalGroup: .none, instanceName: "doubles")
let multiHoops = Node(name: "Multi-Hoops", finalGroup: .none, instanceName: "multiHoops")
let misc = Node(name: "Miscellaneous", finalGroup: .none, instanceName: "misc")
let beginners = Node(name: "Beginner Tips", finalGroup: .none, instanceName: "beginners")
let recentlyAdded = Node(name: "Recently Added", finalGroup: .none, instanceName: "recentlyAdded")

let tutorialsArray = [single, doubles, multiHoops, misc, beginners, recentlyAdded]

let combos = Node(name: "Saved Combos", finalGroup: .combo, instanceName: "combos")
let libraryArray = [single, doubles, multiHoops, combos, misc]


// MARK: Level 2

    // Single
let onBody = Node(name: "On Body" , finalGroup: .none, instanceName: "onBody")
let offBody = Node(name: "Off Body", finalGroup: .none, instanceName: "offBody")
let isolations = Node(name: "Isolations", finalGroup: .none, instanceName: "isolations")
let wrapHingeFold = Node(name: "Wrap, Hinge & Folds", finalGroup: .none, instanceName: "wrapHingeFold")

    let singleArray = [onBody, offBody, isolations, wrapHingeFold]

    // Doubles
let doublesBasics = Node(name: "Doubles Basics", finalGroup: .archive, instanceName: "doublesBasics")
let beatWeaves = Node(name: "Beat Weaves", finalGroup: .archive, instanceName: "beatWeaves")
let weaves = Node(name: "Weaves", finalGroup: .archive, instanceName: "weaves")
let hipReels = Node(name: "Hip Reels", finalGroup: .archive, instanceName: "hipReels")
let shoulderReels = Node(name: "Shoulder Reels", finalGroup: .archive, instanceName: "shoulderReels")
let extendedReels = Node(name: "Extended Reels", finalGroup: .archive, instanceName: "extendedReels")
let threadTheNeedle = Node(name: "Thread the Needle", finalGroup: .archive, instanceName: "threadTheNeedle")
let turns = Node(name: "Turns", finalGroup: .archive, instanceName: "turns")
let rolls = Node(name: "Rolls", finalGroup: .archive, instanceName: "rolls")
let elbow = Node(name: "Elbow", finalGroup: .archive, instanceName: "elbow")
let butterflies = Node(name: "Butterflies", finalGroup: .archive, instanceName: "butterflies")
let sphereGlobe = Node(name: "Sphere/Globe", finalGroup: .archive, instanceName: "sphereGlobe")
let horizontalPlane = Node(name: "Horizontal Plane", finalGroup: .archive, instanceName: "horizontalPlane")
let threeD = Node(name: "3D", finalGroup: .archive, instanceName: "threeD")
let dbhingeFold = Node(name: "Hinge & Fold", finalGroup: .archive, instanceName: "dbhingeFold")
let escalatorWedgie = Node(name: "Escalators & Wedgies", finalGroup: .archive, instanceName: "escalatorWedgie")
let antiSpinInSpin = Node(name: "Anti-Spin & In-Spin", finalGroup: .archive, instanceName: "antiSpinInSpin")
let caps = Node(name: "CAPS", finalGroup: .archive, instanceName: "caps")
let wheelPlane = Node(name: "Wheel Plane", finalGroup: .archive, instanceName: "wheelPlane")
let wallPlane = Node(name: "Wall Plane", finalGroup: .archive, instanceName: "wallPlane")
let twoHoopsOneHand = Node(name: "2 Hoops, 1 Hand", finalGroup: .archive, instanceName: "twoHoopsOneHand")
let ochos = Node(name: "Ochos", finalGroup: .archive, instanceName: "ochos")
let linkedHoops = Node(name: "Linked Hoops", finalGroup: .archive, instanceName: "linkedHoops")
let breaks = Node(name: "Breaks", finalGroup: .archive, instanceName: "breaks")
let dbMisc = Node(name: "Miscellaneous", finalGroup: .archive, instanceName: "dbMisc")
let twinIsolations = Node(name: "Twin Isolations", finalGroup: .archive, instanceName: "twinIsolations")
let hybridVS = Node(name: "Hybrid/VS", finalGroup: .archive, instanceName: "hybridVS")

    let doublesArray = [doublesBasics, beatWeaves, weaves, hipReels, shoulderReels, extendedReels, threadTheNeedle, turns, rolls, elbow, butterflies, sphereGlobe, horizontalPlane, threeD, dbhingeFold, escalatorWedgie, antiSpinInSpin, caps, wheelPlane, wallPlane, twoHoopsOneHand, ochos, linkedHoops, breaks, dbMisc, twinIsolations, hybridVS]

    // Multi Hoops
let multiHoopsBasics = Node(name: "Multi Hoop Basics", finalGroup: .archive, instanceName: "multiHoopsGeneral")
let twoHoops = Node(name: "2 Hoops", finalGroup: .archive, instanceName: "twoHoops")
let threeHoops = Node(name: "3 Hoops", finalGroup: .archive, instanceName: "threeHoops")
let fourHoops = Node(name: "4 Hoops", finalGroup: .archive, instanceName: "fourHoops")
let fiveHoops = Node(name: "5 Hoops", finalGroup: .archive, instanceName: "fiveHoops")


    let multiHoopsArray = [multiHoopsBasics, twoHoops, threeHoops, fourHoops, fiveHoops]

    // Hooping Foundations
//let tosses = Node(name: "Tosses", finalGroup: .none, instanceName: "tosses") // Misc Tricks - Tosses
//let spins = Node(name: "Spins", finalGroup: .none, instanceName: "spins")    // Misc Tricks - Spins
//let passing = Node(name: "Passing", finalGroup: .none, instanceName: "passing")    // Misc Tricks - Passing
//let concept = Node(name: "Hooping Concepts", finalGroup: .none, instanceName: "concept") // Other/Beyond & Beginners - Other


    // Beginner Tips
let tips = Node(name: "Tips", finalGroup: .archive, instanceName: "tips")
let flow = Node(name: "Finding Flow", finalGroup: .archive, instanceName: "flow")
let concepts = Node(name: "Hooping Basics", finalGroup: .none, instanceName: "")
let dance = Node(name: "Dance", finalGroup: .archive, instanceName: "dance")
let fireHooping = Node(name: "Fire Hoopings", finalGroup: .archive, instanceName: "fireHooping")
let beyondTricks = Node(name: "Beyond Tricks", finalGroup: .archive, instanceName: "beyondTricks")

let beginnersArray = [tips, flow, concepts, dance, fireHooping, beyondTricks]

    // Miscellaneous
let miscMisc = Node(name: "Misc Tricks", finalGroup: .archive, instanceName: "miscMisc")
let miscOther = Node(name: "Other", finalGroup: .archive, instanceName: "miscOther")

let miscArray = [miscMisc, miscOther]


// MARK: Level 3

    // Single

        // On Body
let legKneeFoot = Node(name: "Leg/Knee/Foot", finalGroup: .none, instanceName: "legKneeFoot")
let waist = Node(name: "Waist", finalGroup: .none, instanceName: "waist")
let chestShoulder = Node(name: "Chest/Shoulder", finalGroup: .none, instanceName: "chestShoulder")
let neckHeadNose = Node(name: "Neck/Head/Nose", finalGroup: .archive, instanceName: "neckHeadNose")
let verticalAngle = Node(name: "Chest/Shoulder Vertical", finalGroup: .none, instanceName: "verticalAngle")


        let onBodyArray = [legKneeFoot, waist, chestShoulder, neckHeadNose, verticalAngle]


        // Off Body
let horizontal = Node(name: "Horizontal", finalGroup: .none, instanceName: "horizontal")
let vertical = Node(name: "Vertical", finalGroup: .none, instanceName: "vertical")

        let offBodyArray = [horizontal, vertical]

        // Isolations
let isoIsolations = Node(name: "Isolations", finalGroup: .none, instanceName: "isoIsolations")
let oneHandIsolations = Node(name: "One Hand Isolations", finalGroup: .none, instanceName: "oneHandIsolations")
let twoHandIsolations = Node(name: "Two Hand Isolations", finalGroup: .none, instanceName: "twoHandIsolations")
let verticalPlaneIsolations = Node(name: "Vertical Plane Isolations", finalGroup: .none, instanceName: "verticalPlaneIsolations")
let horizontalPlaneIsolations = Node(name: "Horizontal Plane Isolations", finalGroup: .none, instanceName: "horizontalPlaneIsolations")
let threeDIsolations = Node(name: "3D Isolations", finalGroup: .none, instanceName: "threeDIsolations")
let barrelRolls = Node(name: "Barrel Rolls", finalGroup: .none, instanceName: "barrelRolls")
let ghostingTracing = Node(name: "Ghosting/Isolations", finalGroup: .none, instanceName: "ghostingTracing")
let outsideGrip = Node(name: "Outside Grip", finalGroup: .none, instanceName: "outsideGrip")
let behindTheBack = Node(name: "Behind the Back", finalGroup: .none, instanceName: "behindTheBack")
let miscIsolations = Node(name: "Miscellaneous", finalGroup: .none, instanceName: "miscIsolations")
let otherIsolations = Node(name: "Combos/Transitions", finalGroup: .none, instanceName: "otherIsolations")


        let isolationsArray = [isoIsolations, oneHandIsolations, twoHandIsolations, verticalPlaneIsolations, horizontalPlaneIsolations, threeDIsolations, barrelRolls, ghostingTracing, outsideGrip, behindTheBack, miscIsolations, otherIsolations]

        // Wrap, Hinge & Fold
let escalator = Node(name: "Escalator", finalGroup: .none, instanceName: "escalator")
let wedgies = Node(name: "Wedgies & Circus Starts", finalGroup: .none, instanceName: "wedgies")
let wraps = Node(name: "Wraps", finalGroup: .none, instanceName: "wraps")
let hingeFold = Node(name: "Hinge & Fold", finalGroup: .none, instanceName: "hingeFold")

        let wrapHingeFoldArray = [escalator, wedgies, wraps, hingeFold]


        



// MARK: Level 4

     // Single

        // On Body

            // Leg/Knee/Foot
let legHooping = Node(name: "Leg Hooping", finalGroup: .archive, instanceName: "legHooping")
let gettingOut = Node(name: "Getting Out", finalGroup: .archive, instanceName: "gettingOut")
let oneLeg = Node(name: "One Leg", finalGroup: .archive, instanceName: "oneLeg")
let pickUps = Node(name: "Pick Ups", finalGroup: .archive, instanceName: "pickUps")
let lkfPassing = Node(name: "Passing", finalGroup: .archive, instanceName: "lkfPassing")
let verticalKnee = Node(name: "Vertical Knee", finalGroup: .archive, instanceName: "verticalKnee")
let lkfBreaks = Node(name: "Breaks", finalGroup: .archive, instanceName: "lkfBreaks")
let footHooping = Node(name: "Foot Hooping", finalGroup: .archive, instanceName: "footHooping")
let lkfMisc = Node(name: "Miscellaneous", finalGroup: .archive, instanceName: "lkfMisc")

let legKneeFootArray = [legHooping, gettingOut, oneLeg, pickUps, lkfPassing, verticalKnee, lkfBreaks, footHooping, lkfMisc]

            // Waist
let waistCore = Node(name: "Core", finalGroup: .archive, instanceName: "waistCore")
let verticalWaist = Node(name: "Vertical Waist", finalGroup: .archive, instanceName: "verticalWaist")
let waistMovingTheHoop = Node(name: "Moving the Hoop", finalGroup: .archive, instanceName: "waistMovingTheHoop")
let liftsAndDrops = Node(name: "Lifts & Drops", finalGroup: .archive, instanceName: "liftsAndDrops")
let waistBreaks = Node(name: "Breaks", finalGroup: .archive, instanceName: "waistBreaks")
let waistMisc = Node(name: "Misc Tricks", finalGroup: .archive, instanceName: "waistMisc")

let waistArray = [waistCore, verticalWaist, waistMovingTheHoop, liftsAndDrops, waistBreaks, waistMisc]

            // Chest / Shoulder
let shoulderCore = Node(name: "Core", finalGroup: .archive, instanceName: "shoulderCore")
let shoulderMovingTheHoop = Node(name: "Moving the Hoop", finalGroup: .archive, instanceName: "shoulderMovingTheHoop")
let shoulderDuckInOut = Node(name: "Duck In/Out", finalGroup: .archive, instanceName: "shoulderDuckInOut")
let breaksAndPaddles = Node(name: "Breaks & Paddles", finalGroup: .archive, instanceName: "breaksAndPaddles")
let shoulderMisc = Node(name: "Misc Tricks", finalGroup: .archive, instanceName: "shoulderMisc")

let chestShoulderArray = [shoulderCore, shoulderMovingTheHoop, shoulderDuckInOut, breaksAndPaddles, shoulderMisc]

            // Neck / Head / Nose
// No sub categories

            // Vertical / Angle
let verticalChest = Node(name: "Vertical Chest", finalGroup: .archive, instanceName: "verticalChest")
let verticalShoulder = Node(name: "Vertical Shoulder", finalGroup: .archive, instanceName: "verticalShoulder")
let verticalDuckInOut = Node(name: "Duck In/Out", finalGroup: .archive, instanceName: "verticalDuckInOut")
// verticalWaist
// verticalKnee
let buttHooping = Node(name: "Butt Hooping", finalGroup: .archive, instanceName: "buttHooping")
let verticalBreaks = Node(name: "Breaks", finalGroup: .archive, instanceName: "verticalBreaks")

let verticalAngleArray = [verticalChest, verticalShoulder, verticalDuckInOut, verticalWaist, verticalKnee, buttHooping, verticalBreaks]

        // Off Body
    
            // Horizontal
let hand = Node(name: "Hand", finalGroup: .archive, instanceName: "hand")
let offBodyPassing = Node(name: "Passing", finalGroup: .archive, instanceName: "offBodyPassing")
let offBodyLiftsAndDrops = Node(name: "Lifts & Drops", finalGroup: .archive, instanceName: "offBodyLiftsAndDrops")
let offBodyBehindTheBack = Node(name: "Behind the Back", finalGroup: .archive, instanceName: "offBodyBehindTheBack")
let offBodySpins = Node(name: "Spins", finalGroup: .archive, instanceName: "offBodySpins")
let stepAndJump = Node(name: "Step & Jump", finalGroup: .archive, instanceName: "stepAndJump")
let offBodyWraps = Node(name: "Wraps", finalGroup: .archive, instanceName: "offBodyWraps")
let revolvingDoor = Node(name: "Revolving Door", finalGroup: .archive, instanceName: "revolvingDoor")
let offBodyBreaks = Node(name: "Breaks", finalGroup: .archive, instanceName: "offBodyBreaks")
let offBodyMisc = Node(name: "Misc Tricks", finalGroup: .archive, instanceName: "offBodyMisc")

let horizontalArray = [hand, offBodyPassing, offBodyLiftsAndDrops, offBodyBehindTheBack, offBodySpins, stepAndJump, offBodyWraps, revolvingDoor, offBodyBreaks, offBodyMisc]

            // Verical
let handAndArm = Node(name: "Hand & Arm", finalGroup: .archive, instanceName: "handAndArm")
let antiSpinExtension = Node(name: "Anti-Spin & Extensions", finalGroup: .archive, instanceName: "antiSpinExtension")
let verticalOffBodySpins = Node(name: "Spins", finalGroup: .archive, instanceName: "verticalOffBodySpins")
let offBodyTosses = Node(name: "Tosses", finalGroup: .archive, instanceName: "offBodyTosses")
let offbodyWeaves = Node(name: "Weaves", finalGroup: .archive, instanceName: "offbodyWeaves")
let chestRolls = Node(name: "Chest Rolls", finalGroup: .archive, instanceName: "chestRolls")
let verticalRolls = Node(name: "Vertical & K-Rolls", finalGroup: .archive, instanceName: "verticalRolls")
let rotatingRolls = Node(name: "Rotating Rolls", finalGroup: .archive, instanceName: "rotatingRolls")
let backRolls = Node(name: "Back/Shoulder Rolls", finalGroup: .archive, instanceName: "backRolls")
let otherRolls = Node(name: "Other Rolls", finalGroup: .archive, instanceName: "otherRolls")
let verticalBTB = Node(name: "Behind the Back", finalGroup: .archive, instanceName: "verticalBTB")
let verticalStepAndJump = Node(name: "Step & Jump Throughs", finalGroup: .archive, instanceName: "verticalStepAndJump")
let underLeg = Node(name: "Under Leg", finalGroup: .archive, instanceName: "underLeg")
// revolvingDoor
let offBodyVerticalWraps = Node(name: "Wraps", finalGroup: .archive, instanceName: "offBodyVerticalWraps")
let offBodyVerticalDuckInOut = Node(name: "Duck In/Out", finalGroup: .archive, instanceName: "offBodyVerticalDuckInOut")
let offBodyVerticalElbow = Node(name: "Vertical Elbow", finalGroup: .archive, instanceName: "offBodyVerticalElbow")
let offBodyVerticalKnee = Node(name: "Vertical Knee", finalGroup: .archive, instanceName: "offBodyVerticalKnee")
let offBodyVerticalBreaks = Node(name: "Breaks", finalGroup: .archive, instanceName: "offBodyVerticalBreaks")
let offBodyVerticalMisc = Node(name: "Misc Tricks", finalGroup: .archive, instanceName: "offBodyVerticalMisc")

let verticalArray = [handAndArm, antiSpinExtension, verticalOffBodySpins, offBodyTosses, offbodyWeaves, chestRolls, verticalRolls, rotatingRolls, backRolls, otherRolls, verticalBTB, verticalStepAndJump, underLeg, revolvingDoor, offBodyVerticalWraps, offBodyVerticalDuckInOut, offBodyVerticalKnee, offBodyVerticalBreaks, offBodyVerticalMisc]





// MARK: Add Children




func addChildren () {
    
// MARK: Level 1
    for item in tutorialsArray {
        tutorials.add(child: item)
    }
    
    for item in libraryArray {
        library.add(child: item)
    }
    
// MARK: Level 2
    
    // Single
    for item in singleArray {
        single.add(child: item)
    }
    
    // Doubles
    for item in doublesArray {
        doubles.add(child: item)
    }
    
    // Multi Hoops
    for item in multiHoopsArray {
        multiHoops.add(child: item)
    }
    
    // Beginner Tips
    for item in beginnersArray {
        beginners.add(child: item)
    }
    
    // Miscellaneous
    for item in miscArray {
        misc.add(child: item)
    }
    
    
    
    
// MARK: Level 3
    
    // Single
    
        // On Body
    for item in onBodyArray {
        onBody.add(child: item)
    }
    
        // Off Body
    for item in offBodyArray {
        offBody.add(child: item)
    }
    
        // Isolations
    for item in isolationsArray {
            isolations.add(child: item)
        }
    
        // Wrap, Hinge & Fold
    for item in wrapHingeFoldArray {
        wrapHingeFold.add(child: item)
    }
    

    
// MARK: Level 4
    
    // Single
    
        // On Body
    
            // Leg Knee Foot
    for item in legKneeFootArray {
        legKneeFoot.add(child: item)
    }
    
            // Waist
    for item in waistArray {
        waist.add(child: item)
    }
    
            // Chest / Shoulder
    for item in chestShoulderArray {
        chestShoulder.add(child: item)
    }
    
            // Vertical / Angle
    for item in verticalAngleArray {
        verticalAngle.add(child: item)
    }
        
        // Off Body
    
            // Horizontal
    for item in horizontalArray {
        horizontal.add(child: item)
    }
    
            // Vertical
    for item in verticalArray {
        vertical.add(child: item)
    }
    
}
