//
//  User.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 3/31/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation

class TempUser {
    
    var name: String = "Placeholder"
    var movesLearned: Int = 0
    var combosCreated: Int = 0
    var uid: String = "Placeholder"
    var primaryDevice: String = "Placeholder"
    var otherDevices: [String]? = nil
    
    convenience init(name: String, movesLearned: Int, combosCreated: Int) {
        self.init()
        self.name = name
        self.movesLearned = movesLearned
        self.combosCreated = combosCreated
        print("TempUser \(name) was initialized")
    }
    
    deinit {
        print("Deallocating TempUser: \(name)")
    }
    
}

