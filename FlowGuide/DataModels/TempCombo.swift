//
//  TempCombo.swift
//  FlowGuide
//
//  Created by Kelsey Garcia on 5/12/20.
//  Copyright © 2020 CosmoNautical. All rights reserved.
//

import Foundation

class TempCombo {
    
    var hoopCount: Int = 0
    var moves: [LearnedMove] = [LearnedMove]()
    var docID: String = ""
    var favorite: Bool = false
    var name: String? = nil
    var moveDocIDs: [String]? = nil
    
    convenience init(hoopCount: Int, docID: String, moves: [LearnedMove] = [], favorite: Bool = false, moveDocIDs: [String]? = nil, name: String? = nil) {
        self.init()
        self.hoopCount = hoopCount
        self.moves = moves
        self.docID = docID
        self.favorite = favorite
        self.moveDocIDs = moveDocIDs
        self.name = name
        print("TempCombo \(docID) was initialized")
    }
    
    deinit {
        print("Deallocating TempCombo: \(docID)")
    }
}
